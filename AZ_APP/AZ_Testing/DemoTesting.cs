﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace AZ_Testing
{
    [TestClass]
    public class DemoTesting
    {
        [TestMethod]
        public void TestMethod1()
        {
            var calculator = Substitute.For<ICalculator>();
            calculator.Add(1, 2).Returns(3);
            Assert.That(calculator.Add(1, 2), Is.EqualTo(3));
        }
    }
}
