﻿using System;

namespace AZ_Testing
{
    public interface ICalculator
    {
        int Add(int a, int b);

        string Mode { get; set; }

        event EventHandler PoweringUp;
    }
}