﻿using System.Web.Mvc;
using BanNhaAZ.Models;
using CommonHelper.Helper;
using Core.Models;

namespace BanNhaAZ.Areas.Admin.Controllers
{
    public class HomeController : Controller
    {
        private readonly CacheProvider _cacheProvider = new CacheProvider();
        // GET: Admin/Home
        public ActionResult Index()
        {
            var getCache = _cacheProvider.Get(Constants.CacheConstant.Admin) as UserLoginInformation;
            if (getCache != null)
            {
                if (getCache.Roles != null)
                {
                    if (getCache.Roles.Contains(Constants.Roles.BanNhaAZ) || getCache.Roles.Contains(Constants.Roles.Admin))
                    {
                        return View(); 
                    }
                }
            }
            return RedirectToAction("Index", "Login", new { area = "" });
        }
    }
}