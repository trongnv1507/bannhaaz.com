﻿using BanNhaAZ.Models;
using CommonHelper.Helper;
using Core.Business;
using Core.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace BanNhaAZ.Areas.Rec.Controllers
{
    public class HomeController : Controller
    {
        private readonly CacheProvider _cacheProvider = new CacheProvider();
        private readonly RecEmployeesBusiness _business = new RecEmployeesBusiness();
        // GET: Rec/Home
        public ActionResult Index()
        {
            //var itendity = (ClaimsIdentity)User.Identity;
            var getCache = _cacheProvider.Get(Constants.CacheConstant.Admin) as UserLoginInformation;
            if (getCache != null)
            {
                if (getCache.Roles != null)
                {
                    if (getCache.Roles.Contains(Constants.Roles.Rec) || getCache.Roles.Contains(Constants.Roles.Admin))
                    {
                        return View();
                    }
                }
            }
            return RedirectToAction("Login", "Home", new { area = "Rec" });
        }
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public async Task<ActionResult> Login(UserLogin model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }
            //[{"UserName":"vuitt2","Password":"123456aA@"
            string apiUrl = "http://" + Request.Url.Host +
                            (Request.Url.IsDefaultPort ? "" : ":" + Request.Url.Port) + "/token";
            using (var client = new HttpClient())
            {
                var values = new Dictionary<string, string>
                {
                    { "username", model.UserName },
                    { "password", model.Password },
                    { "grant_type", "password" },
                };

                var content = new FormUrlEncodedContent(values);
                var response = await client.PostAsync(apiUrl, content);
                if (response.IsSuccessStatusCode)
                {
                    var respStr = await response.Content.ReadAsStringAsync();
                    var fb = JsonConvert.DeserializeObject<UserLoginInformation>(respStr);

                    _cacheProvider.Set("Rec", fb, 2); //seconds
                    return RedirectToAction("Index", "Home", new
                    {
                        area = "Rec"
                    });
                }
                return View();
            }

        }
        public async Task<FileResult> Download(string candidateId)
        {
            try
            {
                var model = await _business.GetFileByCandidate(candidateId);
                if (model != null)
                {
                    var filePath = Server.MapPath("~/CVs/" + model.Link);
                    byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);
                    string fileName = model.Link.Trim();
                    return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}