﻿using BanNhaAZ.Models;
using CommonHelper.Helper;
using Core.Business;
using Core.Models;
using Core.Models.EF;
using Core.Models.Messages;
using Newtonsoft.Json;
using shortid;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace BanNhaAZ.Areas.Rec.Controllers
{
    [RoutePrefix("api/rec")]
    public class RecController : ApiController
    {
        #region + Constructor
        private readonly RecEmployeesBusiness _business = new RecEmployeesBusiness();
        #endregion

        #region + Function
        /// <summary>
        /// Function search 
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        //[Authorize(Roles = Constants.Roles.Rec + "," + Constants.Roles.Admin)]
        [Route("Search")]
        [HttpPost]
        public async Task<IHttpActionResult> Search([FromBody]ListFilter filter)
        {
            try
            {

                if (filter == null)
                {
                    filter = new ListFilter();
                }
                if (filter.Filter == null || filter.Filter.Count == 0)
                {
                    filter.Filter = new List<FilterTypeModel>();
                }
                if (filter.Sort == null || filter.Sort.Count == 0)
                {
                    filter.Sort = new List<SortTypeModel>();
                }
                var lstFilterTech = filter.Filter.Where(x => x.Field.ToLower().Equals("Technology".ToLower())).Select(x => x.ValueString).ToList();

                var lstFilterOther = filter.Filter.Where(x => !x.Field.ToLower().Equals("Technology".ToLower())).ToList();
                var filterlst = new List<FilterTypeModel>();
                filterlst.AddRange(lstFilterOther);

                if (lstFilterTech.Any())
                {
                    string valueString = string.Join("||", lstFilterTech.ToArray());
                    filterlst.Add(new FilterTypeModel() { Field = "Technology", ValueString = valueString, IsActive = true });
                }
                filter.Filter = filterlst;
                var filterDataTable = Utilities.ListToDataTable(filter.Filter);
                var sortDataTable = Utilities.ListToDataTable(filter.Sort);
                var skip = filter.Skip ?? 0;
                var take = filter.Take ?? Constants.Config.ItemsPerPage;
                var itendity = (ClaimsIdentity)User.Identity;

                //var roles = itendity.Claims.Where(c => c.Type == ClaimTypes.Role).Select(c => c.Value);

                //string createdBy = roles.Contains(Constants.Roles.Admin) ? string.Empty : itendity.Name;
                string createdBy = string.Empty;
                var results = await _business.GetData(createdBy, filter.Name != null ? filter.Name.Trim() : null, filterDataTable, sortDataTable, skip, take);

                return Ok(new ObjectResult
                {
                    StatusCode = Enums.StatusCode.Ok,
                    Result = results,
                    Message = Constants.Message.GET_DATA_SUCCESSFULLY
                });
            }
            catch (Exception ex)
            {
                var result = new ObjectResult();
                result.StatusCode = Enums.StatusCode.Error;
                result.Message = ex.Message;
                result.Result = false;
                Logger.CreateLog(Logger.Levels.ERROR, this, "Files/Search()", string.Concat("filter : ", filter), ex.ToString());
                return Content(HttpStatusCode.InternalServerError, result, Utilities.MediaTypeFormatterJson);
            }
        }

        //[Authorize(Roles = Constants.Roles.Rec + "," + Constants.Roles.Admin)]
        [HttpPost]
        [HttpOptions]
        [Route("create")]
        public async Task<IHttpActionResult> Create()
        {
            try
            {
                var model = new RecEmployeesView();
                //var itendity = (ClaimsIdentity)User.Identity;
                model.CreatedBy = "vuitt2";
                if (Request.Content.IsMimeMultipartContent())
                {
                    var httpRequest = HttpContext.Current.Request;
                    model = JsonConvert.DeserializeObject<RecEmployeesView>(httpRequest.Form["model"].ToString());
                    model.CandidateId = "VT" + ShortId.Generate(true, false, 7).ToUpper();

                    foreach (string file in httpRequest.Files)
                    {
                        var postedFile = httpRequest.Files[file];
                        string newFileName = (model.CandidateId + "_" + model.CVs.Name).Trim();
                        model.CVs.Name = model.CVs.Name.Trim();
                        model.CVs.Link = newFileName;
                        model.CVs.Extension = System.IO.Path.GetExtension(postedFile.FileName);
                        var filePath = HttpContext.Current.Server.MapPath("~/CVs/" + newFileName);
                        postedFile.SaveAs(filePath);
                    }
                }
                var result = new ObjectResult();
                var returnVal = await _business.Create(model);
                if (returnVal.Status)
                {
                    result.StatusCode = Enums.StatusCode.Ok;
                    result.Message = returnVal.Message;
                    result.Result = returnVal.Status;
                    result.IdReturn = returnVal.IdReturn;

                    return Ok(result);
                }
                result.StatusCode = Enums.StatusCode.Error;
                result.Message = returnVal.Message;
                result.Result = false;
                return Content(HttpStatusCode.InternalServerError, result, Utilities.MediaTypeFormatterJson);
            }
            catch (Exception ex)
            {
                var result = new ObjectResult();
                result.StatusCode = Enums.StatusCode.Error;
                result.Message = ex.Message;
                result.Result = false;
                Logger.CreateLog(Logger.Levels.ERROR, this, "Create()", string.Empty, ex.ToString());
                return Content(HttpStatusCode.InternalServerError, result, Utilities.MediaTypeFormatterJson);
            }
        }

        //[Authorize(Roles = Constants.Roles.Rec + "," + Constants.Roles.Admin)]
        [HttpPost]
        [HttpOptions]
        [Route("update")]
        public async Task<IHttpActionResult> Update()
        {
            try
            {
                var model = new RecEmployeesView();
                //var itendity = (ClaimsIdentity)User.Identity;
                model.ModifiedBy = "vuitt2";
                if (Request.Content.IsMimeMultipartContent())
                {
                    var httpRequest = HttpContext.Current.Request;
                    model = JsonConvert.DeserializeObject<RecEmployeesView>(httpRequest.Form["model"].ToString());

                    if (model.CVs != null)
                    {
                        if (model.CVs.Id == 0)
                        {
                            foreach (string file in httpRequest.Files)
                            {
                                var postedFile = httpRequest.Files[file];
                                string newFileName = (model.CandidateId + "_" + ShortId.Generate(7) + "_" + model.CVs.Name).Trim();
                                model.CVs.Name = model.CVs.Name.Trim();
                                model.CVs.Link = newFileName;
                                model.CVs.Extension = System.IO.Path.GetExtension(postedFile.FileName);
                                var filePath = HttpContext.Current.Server.MapPath("~/CVs/" + newFileName);
                                postedFile.SaveAs(filePath);
                            }
                        }
                    }
                }
                var result = new ObjectResult();
                var returnVal = await _business.Update(model);
                if (returnVal.Status)
                {
                    result.StatusCode = Enums.StatusCode.Ok;
                    result.Message = returnVal.Message;
                    result.Result = returnVal.Status;
                    result.IdReturn = returnVal.IdReturn;

                    return Ok(result);
                }
                result.StatusCode = Enums.StatusCode.Error;
                result.Message = returnVal.Message;
                result.Result = false;
                return Content(HttpStatusCode.InternalServerError, result, Utilities.MediaTypeFormatterJson);
            }
            catch (Exception ex)
            {
                var result = new ObjectResult();
                result.StatusCode = Enums.StatusCode.Error;
                result.Message = ex.Message;
                result.Result = false;
                Logger.CreateLog(Logger.Levels.ERROR, this, "Update()", string.Empty, ex.ToString());
                return Content(HttpStatusCode.InternalServerError, result, Utilities.MediaTypeFormatterJson);
            }
        }

        [Route("SearchCallingInfo")]
        [HttpPost]
        public async Task<IHttpActionResult> SearchCallingInfo([FromBody]ListFilter filter)
        {
            try
            {

                if (filter == null)
                {
                    filter = new ListFilter();
                }
                if (filter.Filter == null || filter.Filter.Count == 0)
                {
                    filter.Filter = new List<FilterTypeModel>();
                }
                if (filter.Sort == null || filter.Sort.Count == 0)
                {
                    filter.Sort = new List<SortTypeModel>();
                }
                var filterDataTable = Utilities.ListToDataTable(filter.Filter);
                var sortDataTable = Utilities.ListToDataTable(filter.Sort);
                var skip = filter.Skip ?? 0;
                var take = filter.Take ?? Constants.Config.ItemsPerPage;
                var results = await _business.GetCallingInfo(filter.CandidateId);

                return Ok(new ObjectResult
                {
                    StatusCode = Enums.StatusCode.Ok,
                    Result = results,
                    Message = Constants.Message.GET_DATA_SUCCESSFULLY
                });
            }
            catch (Exception ex)
            {
                var result = new ObjectResult();
                result.StatusCode = Enums.StatusCode.Error;
                result.Message = ex.Message;
                result.Result = false;
                Logger.CreateLog(Logger.Levels.ERROR, this, "Files/Search()", string.Concat("filter : ", filter), ex.ToString());
                return Content(HttpStatusCode.InternalServerError, result, Utilities.MediaTypeFormatterJson);
            }
        }

        [Route("GetFileByCandidate")]
        public async Task<IHttpActionResult> GetFileByCandidate(string candidateId)
        {
            var results = await _business.GetFileByCandidate(candidateId);

            return Ok(new ObjectResult
            {
                StatusCode = Enums.StatusCode.Ok,
                Result = results,
                Message = Constants.Message.GET_DATA_SUCCESSFULLY
            });
        }

        //[Authorize(Roles = Constants.Roles.Rec + "," + Constants.Roles.Admin)]
        [Route("deletes")]
        [HttpPost]
        public async Task<IHttpActionResult> Deletes(List<string> data)
        {
            try
            {
                if (data == null)
                {
                    data = new List<string>();
                }

                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                //var itendity = (ClaimsIdentity)User.Identity;
                var result = new ObjectResult();
                var returnVal = await _business.Deletes(data, "vuitt2");
                if (returnVal)
                {
                    result.StatusCode = Enums.StatusCode.Ok;
                    result.Message = Constants.Message.SOMETHING_ERROR;
                    result.Result = returnVal;
                    return Ok(result);
                }

                result.StatusCode = Enums.StatusCode.Error;
                result.Message = Constants.Message.SAVED_SUCCESSFULLY;
                result.Result = false;
                return Content(HttpStatusCode.InternalServerError, result, Utilities.MediaTypeFormatterJson);
            }
            catch (Exception ex)
            {
                var result = new ObjectResult();
                result.StatusCode = Enums.StatusCode.Error;
                result.Message = ex.Message;
                result.Result = false;
                Logger.CreateLog(Logger.Levels.ERROR, this, "Deletes()",
                    string.Concat("data : ", data), ex.ToString());
                return Content(HttpStatusCode.InternalServerError, result, Utilities.MediaTypeFormatterJson);
            }
        }


        [HttpPost]
        [HttpOptions]
        [Route("imports")]
        public async Task<IHttpActionResult> Imports()
        {
            try
            {
                var filePath = string.Empty;
                string extension = string.Empty;
                if (Request.Content.IsMimeMultipartContent())
                {
                    var httpRequest = HttpContext.Current.Request;
                    //model = JsonConvert.DeserializeObject<RecEmployeesView>(httpRequest.Form["model"].ToString());
                    //model.CandidateId = "VT" + ShortId.Generate(true, false, 7).ToUpper();

                    foreach (string file in httpRequest.Files)
                    {
                        var postedFile = httpRequest.Files[file];

                        string fileName = System.IO.Path.GetFileNameWithoutExtension(postedFile.FileName);
                        extension = System.IO.Path.GetExtension(postedFile.FileName);
                        string newFileName = (fileName + "_" + ShortId.Generate(true, false, 7) + extension).Trim();
                        filePath = HttpContext.Current.Server.MapPath("~/Imports/" + newFileName);
                        //postedFile.SaveAs(filePath);
                    }
                }
                var result = new ObjectResult();
                if (!string.IsNullOrEmpty(filePath))
                {
                    var returnCheck = await _business.CheckRowData(@"D:\1.Data_SSD\WWW\1.OutSource\bannhaaz.com\source\bannhaaz.com\AZ_APP\BanNhaAZ\Imports\DS ngành nghề - Vui_cRccevq.xlsx", extension);
                    if (returnCheck.Status)
                    {
                        var returnVal = await _business.Imports(@"D:\1.Data_SSD\WWW\1.OutSource\bannhaaz.com\source\bannhaaz.com\AZ_APP\BanNhaAZ\Imports\DS ngành nghề - Vui_cRccevq.xlsx", extension);
                        return Ok(new ObjectResult
                        {
                            StatusCode = Enums.StatusCode.Ok,
                            Result = returnVal,
                            Message = Constants.Message.GET_DATA_SUCCESSFULLY
                        });
                    }
                    else
                    {
                        if (returnCheck.Status)
                        {
                            result.StatusCode = Enums.StatusCode.Ok;
                            result.Message = returnCheck.Message;
                            result.Result = returnCheck.Status;

                            return Ok(result);
                        }
                    }

                }

                result.StatusCode = Enums.StatusCode.Error;
                result.Result = false;
                return Content(HttpStatusCode.InternalServerError, result, Utilities.MediaTypeFormatterJson);
            }
            catch (Exception ex)
            {
                var result = new ObjectResult();
                result.StatusCode = Enums.StatusCode.Error;
                result.Message = ex.Message;
                result.Result = false;
                Logger.CreateLog(Logger.Levels.ERROR, this, "Create()", string.Empty, ex.ToString());
                return Content(HttpStatusCode.InternalServerError, result, Utilities.MediaTypeFormatterJson);
            }
        }
        #endregion
    }
}
