﻿using System.Web.Mvc;

namespace BanNhaAZ.Areas.Rec
{
    public class RecAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Rec";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
                context.MapRoute(
                "Rec_home",
                "Rec",
                new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                new[] { "BanNhaAZ.Areas.Rec.Controllers" }
                    );
            context.MapRoute(
                "Rec_default",
                "Rec/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                new[] { "BanNhaAZ.Areas.Rec.Controllers" }
            );
        }
    }
}