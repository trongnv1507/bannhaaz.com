﻿using Core.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BanNhaAZ.Controllers
{
    public class DemoController : Controller
    {
        private readonly ProjectsBusiness _business = new ProjectsBusiness();
        // GET: Demo
        public ActionResult Index()
        {
            _business.Test();
            return View();
        }
    }
}