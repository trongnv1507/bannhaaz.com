﻿app.config(
    function ($routeProvider, $locationProvider) {

        //  $locationProvider.html5Mode(true).hashPrefix('');;
        $routeProvider.when('/Home', {
            templateUrl: '/app/view/layout/calendar.html',
            controller: 'HomeController'
        }).when('/SysGroup', {
            templateUrl: '/app/view/admin/group/group.html',
            controller: 'SysGroupController'
        }).when('/du-an', {
            templateUrl: '/FrontEnd/admin/main/projects/projects.html',
            controller: 'ProjectsController'
        }).when('/noi-dung', {
            templateUrl: '/FrontEnd/admin/main/gioithieu/gioithieu.html',
            controller: 'GioiThieuController'
        }).when('/products', {
            templateUrl: '/FrontEnd/admin/main/products/products.html',
            controller: 'ProductsController'
        }).when('/constant', {
            templateUrl: '/FrontEnd/admin/main/constant/constant.html',
            controller: 'ConstantController'
        }).when('/hinh-anh', {
            templateUrl: '/FrontEnd/admin/main/hinhanh/hinhanh.html',
            controller: 'HinhAnhController'
        }).when('/dich-vu', {
            templateUrl: '/FrontEnd/admin/main/dichvu/dichvu.html',
            controller: 'DichVuController'
        }).otherwise({
            redirectTo: '/products'
            });

    });