﻿app.config(
    function ($routeProvider, $locationProvider) {

        //  $locationProvider.html5Mode(true).hashPrefix('');;
        $routeProvider.when('/home', {
            templateUrl: '/FrontEnd/admin/main/rec/rec.html',
            controller: 'RecController'
        }).otherwise({
            redirectTo: '/home'
            });
    });