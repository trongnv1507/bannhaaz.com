angular.module('app')
    .service('ProjectsService',
        [
            '$http',
            'BASE_URL_API',
            function ($http, BASE_URL_API) {
                var router = "/projects";
                // get area list
                this.getList = function (data) {
                    var url = BASE_URL_API + router + '/search';
                    return $http.post(url, data);
                }
                // add new
                this.addNew = function (data) {
                    var url = BASE_URL_API + router + '/create';
                    return $http.post(url, data);
                }
                // update
                this.update = function (data) {
                    var url = BASE_URL_API + router + '/update';
                    return $http.post(url, data);
                }  
                this.getAll = function () {
                    var url = BASE_URL_API + router + '/getAll';
                    return $http.get(url);
                }
            }
        ])