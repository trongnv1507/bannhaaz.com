angular.module('app')
    .service('RecService',
        [
            '$http',
            'BASE_URL_API',
            function ($http, BASE_URL_API) {
                var router = "/rec";
                this.getList = function (data) {
                    var url = BASE_URL_API + router + '/search';
                    return $http.post(url, data);
                };
                this.getListCallingInfo = function (data) {
                    var url = BASE_URL_API + router + '/searchCallingInfo';
                    return $http.post(url, data);
                };
                this.getFileByCandidate = function (data) {
                    var url = BASE_URL_API + router + '/getFileByCandidate?candidateId=' + data;
                    return $http.get(url);
                };
                // create
                this.create = function (data) {
                    var url = BASE_URL_API + router + '/create';
                    // return $http.post(url, data,httpOptions);
                    return $http.post(url, data, {
                        withCredentials: true,
                        headers: { 'Content-Type': undefined },
                        transformRequest: angular.identity
                    })
                };
                // update
                this.update = function (data) {
                    var url = BASE_URL_API + router + '/update';
                    return $http.post(url, data, {
                        withCredentials: true,
                        headers: { 'Content-Type': undefined },
                        transformRequest: angular.identity
                    })
                };
                 // imports
                 this.imports = function (data) {
                    var url = BASE_URL_API + router + '/imports';
                    return $http.post(url, data, {
                        withCredentials: true,
                        headers: { 'Content-Type': undefined },
                        transformRequest: angular.identity
                    })
                };
                // deletes
                this.deletes = function (data) {
                    var url = BASE_URL_API + router + '/deletes';
                    return $http.post(url, data);
                };
                // active-deactive
                this.setStatus = function (data) {
                    var url = BASE_URL_API + router + '/setStatus';
                    return $http.post(url, data);
                };
                // active-deactive
                this.setVIP = function (data) {
                    var url = BASE_URL_API + router + '/setVIP';
                    return $http.post(url, data);
                };
            }
        ])