﻿using System;
using System.Web.Mvc;

namespace BanNhaAZ.Helpers
{
    /// <summary>
    ///  Menu current active item helper class.
    /// </summary>
    public static class MenuHelper
    {
        /// <summary>
        /// Determines whether the specified controller is selected.
        /// </summary>
        /// <param name="html">The HTML.</param>
        /// <param name="controller">The controller.</param>
        /// <param name="action">The action.</param>
        /// <returns></returns>
        public static string IsSelected(this HtmlHelper html, string controller = null, string action = null, string type = null)
        {
            const string cssClass = "active";
            var currentAction = (string)System.Web.HttpContext.Current.Request.RequestContext.RouteData.GetRequiredString("action"); ;
            var currentController = (string)System.Web.HttpContext.Current.Request.RequestContext.RouteData.GetRequiredString("controller"); ;
            string url = System.Web.HttpContext.Current.Request.Url.AbsolutePath;

            if (String.IsNullOrEmpty(controller))
                controller = currentController;

            if (String.IsNullOrEmpty(action))
                action = currentAction;

            if (action.Equals("Index") && controller.Equals("Home") && type == null && url.Equals("/"))
                return cssClass;

            if (type != null)
                return controller == currentController && action == currentAction && url.StartsWith("/" + type) ?
                    cssClass : String.Empty;
            return controller == currentController && action == currentAction && action != "Index" ?
         cssClass : String.Empty;
        }
    }
}