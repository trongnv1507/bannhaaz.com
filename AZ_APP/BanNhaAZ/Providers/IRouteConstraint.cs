﻿using System;
using System.Web;
using System.Web.Routing;

namespace BanNhaAZ.Providers
{
    public interface IRouteConstraint
    {
        bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection);
    }
    public class CustomRouteConstraint : IRouteConstraint
    {
        public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
        {
            return true;
        }
    }
    public class CustomRouteWithParameterConstraint : IRouteConstraint
    {

        private readonly string _parameterValue;
        public CustomRouteWithParameterConstraint(string parameteValue)
        {
            _parameterValue = parameteValue;
        }

        public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
        {
            object value;
            if (values.TryGetValue(parameterName, out value) && value != null)
            {
                if (value.ToString() == _parameterValue)
                {
                    return true;
                }
            }
            return false;
        }
    }
}