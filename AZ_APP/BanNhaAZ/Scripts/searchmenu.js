﻿var url = window.location.origin + '/api/'

$("#txtSearch").on('keypress', function (e) {
    if (e.which == 13) {
        search();
    }
});
$('#typeTransaction').on('change', '', function (e) {
    loadTypeProperty(this.value, 0);
    loadPrice(this.value, 0);
});
$('#city').on('change', '', function (e) {
    loadLocation('VN_Districts', this.value, 0);
    loadLocation('VN_Streets', -1, 0);
    loadLocation('VN_Wards', -1, 0);
});
$('#district').on('change', '', function (e) {
    loadLocation('VN_Streets', this.value, 0);
    loadLocation('VN_Wards', this.value, 0);
});

function search() {
    //var origin = window.location.origin + '?s=' + $("#txtSearch").val() + '&t=' + $("#t").val();
    var origin = window.location.origin + '/';
    var s = $("#txtSearch").val();
    var typeTransaction = $("#typeTransaction").find(':selected').data('url');
    var typeProperty = $("#typeProperty").find(':selected').data('url');
    var area = $("#area").find(':selected').val();
    var price = $("#price").find(':selected').val();
    var direction = $("#direction").find(':selected').val();
    var city = $("#city").find(':selected').data('url');
    var district = $("#district").find(':selected').data('url');
    var ward = $("#ward").find(':selected').data('url');
    var street = $("#street").find(':selected').data('url');
    var bedroom = $("#bedroom").find(':selected').val();
    var project = $("#projects").find(':selected').val();


    if (typeTransaction != undefined) {
        origin += typeTransaction;
    }
    if (typeProperty != undefined) {
        origin += origin.endsWith("/") ? typeProperty : ('-' + typeProperty);
    }


    var location = ''
    if (district != undefined) {
        location += district;
    } else {
        if (city != undefined) {
            location = city;
        }
    }

    if (street != undefined) {
        location += '-' + street;
    } else {
        if (ward != undefined) {
            location = city + '-' + district + '-' + ward;
        }
    }
    if (area != '0' || price != '0' || direction != '0' || bedroom != '0' || project != '0') {
        origin += (origin.endsWith("/") ? location : ('-' + location));
        origin += '/2';
        origin += (area == '0') ? '-0' : ('-' + area);
        origin += (price == '0') ? '-0' : ('-' + price);
        origin += (direction == '0') ? '-0' : ('-' + direction);
        origin += (bedroom == '0') ? '-0' : ('-' + bedroom);
        origin += (project == '0') ? '-0' : ('-' + project);
        if (s != '') {
            origin += '?s=' + s;
        };
        window.location.href = origin;
    }
    else {
        origin += (origin.endsWith("/") ? location : ('-' + location)) + '-2';
        if (s != '') {
            origin += '?s=' + s;
        };
        window.location.href = origin;
    }
}
$('#btnSearch').on('click', '', function (e) { search(); });

//getUrlParameter
var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};
var getIdFromStorage = function getIdFromStorage(key, value) {
    var list = JSON.parse(localStorage.getItem(key));
    list.filter(x => x.urlName === value);
    return list.filter(x => x.urlName === value)[0];
}

function loadTypeTransaction(valueSelected) {
    $.ajax({
        type: "GET",
        url: url + 'constant/getConstants',
        data: {
            type: 'TypeTransaction',
            id: null
        },
        success: function (res) {
            localStorage.setItem("typeTransaction", JSON.stringify(res.result));
            var sel = $('#typeTransaction');
            sel.html('');
            sel.append('<option value="0">Loại giao dịch</option>');
            $.each(res.result, function (key, item) {
                if (valueSelected == item.id) {
                    sel.append('<option data-url="' + item.urlName + '" value="' + item.id + '" selected>' + item.name + '</option>');
                } else {
                    sel.append('<option data-url="' + item.urlName + '" value="' + item.id + '">' + item.name + '</option>');
                }

            });
        },
        error: function (xhr) {

        }
    });
}
function loadDirection(valueSelected) {
    $.ajax({
        type: "GET",
        url: url + 'constant/getConstants',
        data: {
            type: 'Direction',
            id: null
        },
        success: function (res) {
            var sel = $('#direction');
            $.each(res.result, function (key, item) {
                if (valueSelected == item.id) {
                    sel.append('<option value="' + item.id + '" selected>' + item.name + '</option>');
                } else {
                    sel.append('<option value="' + item.id + '">' + item.name + '</option>');
                }
            });
        },
        error: function (xhr) {

        }
    });
}
function loadTypeProperty(idTransaction, valueSelected) {
    $.ajax({
        type: "GET",
        url: url + 'constant/getConstants',
        data: {
            type: 'TypeProperty',
            id: idTransaction
        },
        success: function (res) {
            if (idTransaction == 'lan-dau-tien') {
                localStorage.setItem("typeProperty", JSON.stringify(res.result));
            }
            var sel = $('#typeProperty');
            sel.html('');
            sel.append('<option value="0">Loại bất động sản</option>');
            $.each(res.result, function (key, item) {
                if (valueSelected == item.id) {
                    sel.append('<option data-url="' + item.urlName + '" value="' + item.id + '" selected>' + item.name + '</option>');
                } else {
                    sel.append('<option data-url="' + item.urlName + '" value="' + item.id + '">' + item.name + '</option>');
                }
            });
        },
        error: function (xhr) {

        }
    });
}
function loadPrice(idTransaction, valueSelected) {
    //Gias
    $.ajax({
        type: "GET",
        url: url + 'constant/getConstants',
        data: {
            type: 'PriceTypeTransaction',
            id: idTransaction
        },
        success: function (res) {
            var sel = $('#price');
            sel.html('');
            sel.append('<option value="0">Giá</option>');
            $.each(res.result, function (key, item) {
                if (valueSelected == item.id) {
                    sel.append('<option value="' + item.id + '" selected>' + item.name + '</option>');
                } else {
                    sel.append('<option value="' + item.id + '">' + item.name + '</option>');
                }
            });
        },
        error: function (xhr) {

        }
    });
}
function loadProject(valueSelected) {
    //Gias
    $.ajax({
        type: "GET",
        url: url + 'projects/getAll',
        data: {
        },
        success: function (res) {
            var sel = $('#projects');
            sel.html('');
            sel.append('<option value="0">Dự án</option>');
            $.each(res.result, function (key, item) {
                if (valueSelected == item.id) {
                    sel.append('<option value="' + item.id + '" selected>' + item.name + '</option>');
                } else {
                    sel.append('<option value="' + item.id + '">' + item.name + '</option>');
                }
            });
        },
        error: function (xhr) {

        }
    });
}

//Location
function loadFirstLocation(type) {
    $.ajax({
        type: "GET",
        url: url + 'constant/getLocation',
        data: {
            type: type,
            id: null
        },
        success: function (res) {
            var sel;
            if (type == 'VN_City') {
                localStorage.setItem("VN_City", JSON.stringify(res.result));
                sel = $('#city');
                sel.html('');
                $.each(res.result, function (key, item) {
                    if (1 == item.id) {
                        sel.append('<option data-url="' + item.urlName + '" value="' + item.id + '" selected>' + item.name + '</option>');
                    } else {
                        sel.append('<option data-url="' + item.urlName + '" value="' + item.id + '">' + item.name + '</option>');
                    }
                });
            }
            if (type == 'VN_Districts') {
                localStorage.setItem("VN_Districts", JSON.stringify(res.result));
            }
            if (type == 'VN_Wards') {
                localStorage.setItem("VN_Wards", JSON.stringify(res.result));
            }
            if (type == 'VN_Streets') {
                localStorage.setItem("VN_Streets", JSON.stringify(res.result));
            }
        },
        error: function (xhr) {

        }
    });
}
function loadLocation(type, id, valueSelected) {
    $.ajax({
        type: "GET",
        url: url + 'constant/getLocation',
        data: {
            type: type,
            id: id
        },
        success: function (res) {
            var sel;
            if (type == 'VN_City') {
                localStorage.setItem("VN_City", JSON.stringify(res.result));
                sel = $('#city');
                sel.html('');
            }
            if (type == 'VN_Districts') {
                if (id == 0) {
                    localStorage.setItem("VN_Districts", JSON.stringify(res.result));
                }
                sel = $('#district');
                sel.html('');
                sel.append('<option value="0">Quận/Huyện</option>');
            }
            if (type == 'VN_Wards') {
                if (id == 0) {
                    localStorage.setItem("VN_Wards", JSON.stringify(res.result));
                }
                sel = $('#ward');
                sel.html('');
                sel.append('<option value="0">Phường/Xã</option>');
            }
            if (type == 'VN_Streets') {
                if (id == 0) {
                    localStorage.setItem("VN_Streets", JSON.stringify(res.result));
                }
                sel = $('#street');
                sel.html('');
                sel.append('<option value="0">Đường/Phố</option>');
            }
            $.each(res.result, function (key, item) {
                if (valueSelected == item.id) {
                    sel.append('<option data-url="' + item.urlName + '" value="' + item.id + '" selected>' + item.name + '</option>');
                } else {
                    sel.append('<option data-url="' + item.urlName + '" value="' + item.id + '">' + item.name + '</option>');
                }
            });
        },
        error: function (xhr) {

        }
    });
}
//end Location

function init() {

    var keySearch = getUrlParameter('s');
    var typeTransaction = $("input[name='Transaction']").val();
    var typeProperty = $("input[name='Type']").val();
    //loadTypeTransaction(0);
    //loadTypeProperty('lan-dau-tien', 0);
    //var itemTran = getIdFromStorage("typeTransaction", typeTransaction);
    var area = $("input[name='Area']").val();
    var bedroom = $("input[name='Bedroom']").val();
    var price = $("input[name='Price']").val();
    var direction = $("input[name='Direction']").val();
    var city = $("input[name='City']").val();
    var district = $("input[name='District']").val();
    var ward = $("input[name='Ward']").val();
    var street = $("input[name='Street']").val();
    var order = getUrlParameter('order');
    var project = $("input[name='ProjectId']").val();

    //loadFirstLocation('VN_City');
    //loadFirstLocation('VN_Districts');
    //loadFirstLocation('VN_Wards');
    //loadFirstLocation('VN_Streets');

    if (keySearch != '') {
        $("#txtSearch").val(keySearch);
    }
    if (order != undefined) {
        $("#orders").val(order);
    }
    if (project != undefined) {
        loadProject(parseInt(project));
    } else {
        loadProject(0);
    }
    if (typeTransaction != "0") {
        loadTypeTransaction(parseInt(typeTransaction));
        loadTypeProperty(parseInt(typeTransaction), 0);
        loadPrice(parseInt(typeTransaction), 0);
    }
    else {
        loadTypeTransaction(0);
    }

    if (typeProperty != "0") {
        //var itemProp = getIdFromStorage("typeProperty", typeProperty);
        loadTypeProperty(parseInt(typeTransaction), parseInt(typeProperty));
    }
    //else {
    //    loadTypeProperty(typeTransaction, 0);
    //}

    if (area != "0") {
        $("#area").val(parseInt(area))
    }
    if (bedroom != "0") {
        $("#bedroom").val(parseInt(bedroom))
    }
    if (price != "0") {
        loadPrice(typeTransaction, parseInt(price));
    }
    //else {
    //    loadPrice(typeTransaction, 0);
    //}

    if (direction != "0") {
        loadDirection(parseInt(direction));
    } else {
        loadDirection(0);
    }
    //Location
    if (city != "0") {
        loadLocation('VN_City', null, parseInt(city));
        loadLocation('VN_Districts', parseInt(city), 0);
    } else {
        loadLocation('VN_City', null, 1);
        loadLocation('VN_Districts', 1, 0);
    }
    if (district != "0") {
        loadLocation('VN_Districts', parseInt(city), parseInt(district));
        loadLocation('VN_Wards', parseInt(district), 0);
        loadLocation('VN_Streets', parseInt(district), 0);
    }
    //} else {
    //    loadLocation('VN_Districts', parseInt(city), 0);
    //}
    if (ward != "0") {
        loadLocation('VN_Wards', parseInt(district), parseInt(ward));
    }
    if (street != "0") {
        loadLocation('VN_Streets', parseInt(district), parseInt(street));
    }
    //else {
    //    loadLocation('VN_Wards', parseInt(city), 0);
    //}
    //end Location
}

init();

