app.directive('ngFiles', ['$parse', function ($parse) {

    function fn_link(scope, element, attrs) {

        var onChange = $parse(attrs.ngFiles);

        element.on('change', function (event) {
            onChange(scope, { $files: event.target.files });
        });
        element.on('dragover', function (e) {
            e.preventDefault();
            e.stopPropagation();
        });
        element.on('dragenter', function (e) {
            e.preventDefault();
            e.stopPropagation();
        });
        element.on('drop', function (e) {
            e.preventDefault();
            e.stopPropagation();
            if (e.originalEvent.dataTransfer) {
                if (e.originalEvent.dataTransfer.files.length > 0) {
                    //upload(e.originalEvent.dataTransfer.files);
                    onChange(scope, { $files: e.originalEvent.dataTransfer.files });
                }
            }
            return false;
        });

    };

    return {
        link: fn_link
    }
}])


app.controller("RecController",
    function ($scope, URL_HOME, BASE_URL_API, RecService, ConstantService, ProjectsService, $interval, $window) {
        var vm = $scope;
        vm.search = {
            type: '',
            name: "",
            skip: 0,
            take: 10,
            filter: {},
            sort: {}
        };
        vm.searchCallingInfo = {
            type: '',
            name: "",
            skip: 0,
            take: 10,
            filter: {},
            sort: {}
        };
        vm.item = {
        };
        vm.total = 0;
        vm.totalCallingInfo = 0;
        vm.selectedRow = [];
        vm.pageSizeInit = 10;
        vm.searchData = function (keyEvent) {
            //  vm.typePropertySearchOptions.dataSource.read();

            if (keyEvent === undefined || keyEvent.which === 13) {
                vm.showButtonClear = true;
                vm.allDatasource.read();
            }
        };

        vm.allDatasource = new kendo.data.DataSource({
            transport: {
                read: function (options) {
                    vm.search.filter = [];
                    vm.search.sort = [];
                    vm.search.skip = 0;
                    vm.search.take = 10;
                    if (vm.search.candidateId) {
                        vm.search.filter.push({
                            field: "candidateId",
                            valueString: vm.search.candidateId.toString(),
                            isActive: true,
                        })
                    }
                    if (vm.search.fullname) {
                        vm.search.filter.push({
                            field: "fullname",
                            valueString: vm.search.fullname.toString(),
                            isActive: true,
                        })
                    }
                    if (vm.search.phone) {
                        vm.search.filter.push({
                            field: "phone",
                            valueString: vm.search.phone.toString(),
                            isActive: true,
                        })
                    }
                    if (vm.search.email) {
                        vm.search.filter.push({
                            field: "email",
                            valueString: vm.search.email.toString(),
                            isActive: true,
                        })
                    }
                    if (vm.search.dob) {
                        vm.search.filter.push({
                            field: "dob",
                            valueString: kendo.toString(kendo.parseDate(vm.search.dob, 'dd/MM/yyyy'), 'yyyy/MM/dd'),
                            isActive: true,
                        })
                    }
                    if (vm.search.jobRank) {
                        vm.search.filter.push({
                            field: "jobRank",
                            valueString: vm.search.jobRank.toString(),
                            isActive: true,
                        })
                    }
                    if (vm.search.site) {
                        vm.search.filter.push({
                            field: "site",
                            valueString: vm.search.site.toString(),
                            isActive: true,
                        })
                    }
                    if (vm.search.selectedTechIds) {
                        _.each(vm.search.selectedTechIds, function (m) {
                            var filter = {
                                field: "Technology",
                                valueString: m.toString(),
                                isActive: true,
                            };
                            vm.search.filter.push(filter);
                        });
                    }
                    if (options.data.filter !== null && options.data.filter !== undefined) {
                        _.each(options.data.filter.filters, function (m) {
                            var filter = {
                                field: m.field,
                                valueString: m.value,
                                isActive: true,
                            };
                            vm.search.filter.push(filter);
                        });
                    }
                    if (options.data.sort !== null && options.data.sort !== undefined) {
                        _.each(options.data.sort, function (o) {
                            var sort = {
                                field: o.field,
                                asc: o.dir === 'asc',
                                isActive: true
                            };

                            vm.search.sort.push(sort);
                        });
                    }

                    $scope.search.take = options.data.take ? options.data.take : 10000;
                    $scope.search.skip = (options.data.page - 1) * options.data.pageSize;
                    RecService.getList(vm.search).then(function (response) {
                        if (response.data.result !== null && response.data.result !== undefined) {
                            var rawData = response.data.result;
                            vm.totalFile = rawData.length > 0 ? rawData[0].total : 0;

                            _.each(rawData, function (item, i) {
                                item.statusModel = {
                                    id: item.status,
                                    name: item.statusString
                                };
                            });
                            options.success(rawData);
                        } else {
                            options.success([]);
                        }

                    }, function (error) {

                        options.error([]);

                    });
                }
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        file: {
                            type: 'string',
                            editable: false
                        },
                        no: {
                            type: 'string',
                            editable: false
                        },
                        name: {
                            type: 'string',
                            editable: true
                        },
                        phone: {
                            type: 'string',
                            editable: true
                        },
                        email: {
                            type: 'string',
                            editable: true
                        },
                        address: {
                            type: 'string',
                            editable: true
                        },
                        isActive: {
                            type: 'bool',
                            editable: true
                        },
                        createdDate: {
                            type: 'date',
                            editable: false
                        }
                    }
                },
                total: function (response) {
                    return response === null || response === undefined || response.length === 0 ? 0 : response[0].total;
                }
            },
            pageSize: 10,
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true
        });

        vm.mainGridOptions = {
            dataSource: vm.allDatasource,
            sortable: true,
            persistSelection: true,
            noRecords: true,
            messages: {
                noRecords: 'There is no data on current page'
            },
            change: function (e) {
                var lsSelected = _.map(this.select(),
                    function (row) {
                        return e.sender.dataItem(row).candidateId;
                    });
                vm.selectedRow = _.uniq(lsSelected, itm => itm);
                $scope.$evalAsync();
            },
            pageable: {
                refresh: true,
                pageSizes: [10, 20, 50],
                buttonCount: 5
            },
            columns: [
                {
                    selectable: true,
                    width: "50px"
                },{
                    field: "candidateId",
                    title: "Candidate ID",
                    width: '120px',
                    filterable: {
                        extra: false
                    }, template: function (dataItem) {
                        return `<div style='cursor: pointer;text-align:center' data-ng-click='editItem(dataItem)'>
                                    <span style="color: blue">
                                    `+ dataItem.candidateId + `
                                    </span>
                                </div>`
                    },
                }, {
                    field: "fullName",
                    title: "Full Name",
                    width: '180px',
                    filterable: {
                        extra: false
                    }, template: function (dataItem) {
                        return `<div style='cursor: pointer;' data-ng-click='editItem(dataItem)'>
                                    <span style="color: blue">
                                    `+ dataItem.fullName + `
                                    </span>
                                </div>`
                    },
                }, {
                    field: "dob",
                    title: "DOB",
                    width: '120px',
                    filterable: {
                        extra: false
                    }, template: function (data) {
                        if (!data.dob) {
                            return "N/A";
                        }
                        return kendo.toString(kendo.parseDate(data.dob), 'dd/MM/yyyy');
                    },
                }, {
                    field: "phone",
                    title: "Phone",
                    width: '120px',
                    filterable: {
                        extra: false
                    }
                }, {
                    field: "email",
                    title: "Email",
                    width: '150px',
                    filterable: {
                        extra: false
                    }
                }, {
                    field: "jobRankName",
                    title: "Job Rank",
                    filterable: {
                        extra: false
                    },
                    width: '100px'
                }, {
                    field: "siteName",
                    title: "Site",
                    filterable: {
                        extra: false
                    },
                    width: '80px'
                }, {
                    field: "technology",
                    title: "Technology",
                    filterable: {
                        extra: false
                    }, width: '200px',
                    template: function (data) {
                        var str = '';
                        if (data.technology != null) {
                            var lst = data.technology.split("||");
                            for (var i = 0; i < lst.length; i++) {
                                str += '<span data-toggle="tooltip" title="" class="badge bg-light-blue" data-original-title="' + lst[i] + '">' + lst[i] + '</span>&ensp;'
                            }
                        }
                        return str;
                    }
                }, {
                    field: "link",
                    title: "CV",
                    filterable: {
                        extra: false
                    },
                    width: "60px",
                    template: function (data) {
                        return `<div style="text-align: center;">
                                     <label style='cursor:pointer'  title="Download file"><a target="_blank" href="/Rec/Home/Download?candidateId=`+ data.candidateId + `">
                                       <span ><i class="fa fa-download fa-lg" aria-hidden="true"></i></span></a></label>
                                   </div>`;
                    }
                }
            ]
        };

        vm.detailDatasource = new kendo.data.DataSource({
            transport: {
                read: function (options) {
                    vm.searchCallingInfo.filter = [];
                    vm.searchCallingInfo.sort = [];
                    vm.searchCallingInfo.skip = 0;
                    vm.searchCallingInfo.take = 100;

                    $scope.searchCallingInfo.take = options.data.take ? options.data.take : 10000;
                    $scope.searchCallingInfo.skip = (options.data.page - 1) * options.data.pageSize;
                    RecService.getListCallingInfo(vm.searchCallingInfo).then(function (response) {
                        if (response.data.result !== null && response.data.result !== undefined) {
                            var rawData = response.data.result;
                            vm.totalCallingInfo = rawData.length > 0 ? rawData.length : 0;
                            options.success(rawData);
                        } else {
                            options.success([]);
                        }

                    }, function (error) {

                        options.error([]);

                    });
                }
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        createdDate: {
                            type: 'string',
                            editable: false
                        },
                        notes: {
                            type: 'string',
                            editable: true
                        }
                    }
                },
                total: function (response) {
                    return response === null || response === undefined || response.length === 0 ? 0 : response.length;
                }
            },
            pageSize: 100,
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true
        });
        vm.detailGridOption = {
            dataSource: vm.detailDatasource,
            persistSelection: true,
            noRecords: true,
            messages: {
                noRecords: 'There is no data on current page'
            },
            pageable: {
                refresh: true,
                pageSizes: [10, 20, 50],
                buttonCount: 5
            },
            columns: [
                {
                    field: "createdDate",
                    title: "Time",
                    template: function (data) {
                        if (!data.createdDate) {
                            return "<div style='text-align:center'>" + (data.id ? "N/A" : kendo.toString(new Date(), 'dd/MM/yyyy hh:mm:ss')) + "</div>";
                        }
                        return "<div style='text-align:center'>" + kendo.toString(kendo.parseDate(data.createdDate), 'dd/MM/yyyy hh:mm:ss') + "</div>";
                    },
                    width: '200px'
                }, {
                    field: "notes",
                    title: "Notes",
                    template: function (data) {
                        return data.notes;
                    },
                }
            ],
            editable: "inline",
        };
        vm.addRowDetail = function () {
            var grid = $("#detailGrid").data("kendoGrid");
            grid.addRow();
        }
        //================= On Action command ========================
        vm.editItem = function (dataItem) {

            vm.type = 'edit';
            $("#files").data("kendoUpload").clearAllFiles()
            vm.dialog.open();
            vm.item = dataItem;
            vm.searchCallingInfo.candidateId = vm.item.candidateId;
            vm.detailDatasource.read();
            if (vm.item.listTechnologyId) {
                vm.item.selectedTechIds = vm.item.listTechnologyId.split("||")
            }
            RecService.getFileByCandidate(vm.item.candidateId).then(
                function (response) {
                    vm.item.cvs = response.data.result;
                    vm.showCVs = !!response.data.result;
                },
                function (error) {
                    swal("Error!", {
                        icon: "error"
                    });
                })
        };
        vm.deleteItem = function (dataItem) {
            var lstData = [];
            lstData.push(dataItem.id);
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this item!",
                icon: "warning",
                buttons: true,
                dangerMode: true
            }).then((willDelete) => {
                if (willDelete) {
                    RecService.deletes(lstData).then(
                        function (response) {
                            swal("Great! Your item has been deleted!", {
                                icon: "success",
                                timer: 2000,
                                buttons: false
                            });
                            vm.searchData();
                        },
                        function (error) {
                            swal("Error!", {
                                icon: "error"
                            });
                        }
                    );
                }
            });
        };

        //================= On Action buton ========================
        vm.onAddNew = function () {
            $("#files").data("kendoUpload").clearAllFiles()
            vm.item = {
            };
            vm.type = 'add';
            vm.item.fullName = "Ngo Van Trong";
            vm.item.email = "Trong@gmail.com";
            vm.item.phone = "0973002521";
            // vm.item.dob="15/07/1995";
            vm.item.site = 67;
            vm.item.address = "So nha ABC"
            vm.item.jobRank = 71
            vm.dialog.open();

            vm.showCVs = false;
            // vm.directionOptions.dataSource.read();
            // vm.idForm.$setPristine();
        };
        vm.onDeletes = function () {
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this item!",
                icon: "warning",
                buttons: true,
                dangerMode: true
            }).then((willDelete) => {
                if (willDelete) {
                    RecService.deletes(vm.selectedRow).then(
                        function (response) {
                            vm.selectedRow = [];
                            swal("Great! Your item selected has been deleted!", {
                                icon: "success",
                                timer: 2000,
                                buttons: false
                            });
                            vm.searchData();
                        },
                        function (error) {
                            swal("Poof! Something was wrong!", {
                                icon: "error"
                            });
                        }
                    );
                }
            });
        };
        vm.clearFilter = function () {
            vm.showButtonClear = false;
            vm.search = {};
            vm.allDatasource.read();
        }
        vm.onImports=function(){
            swal("The function only for fun.", {
                icon: "warning"
            });
        }
        vm.onSaveChange = function () {
            var myLoop = new myLoading("btnSave", "");
             myLoop.start();
            var data = new FormData();

            angular.forEach(vm.item.lstCVs, function (value, key) {
                data.append(key, value);
            });
            vm.item.dob=kendo.toString(vm.item.dob, 'yyyy/MM/dd');
            console.log(vm.item);
            var lstCalling = $("#detailGrid").find("input");
            if (lstCalling.length > 0) {
                vm.item.callingInfo = lstCalling[0].value;
            }
            data.append("model", angular.toJson(vm.item));
            if (vm.type == 'add') {
                RecService.create(data).then(
                    function (response) {
                         vm.dialog.close();
                         myLoop.stop();
                        swal("Great! Your item has been created! Candidate: " + response.data.idReturn, {
                            icon: "success",
                            button: "OK!",
                        });
                        vm.searchData();
                    },
                    function (error) {
                        swal("Error!", {
                            icon: "error"
                        });
                         myLoop.stop();
                    }
                );
            } else {
                RecService.update(data).then(
                    function (response) {
                         vm.dialog.close();
                         myLoop.stop();
                        swal("Great! Your item has been updated! Candidate: " + response.data.idReturn, {
                            icon: "success",
                            button: "OK!",
                        });
                        vm.searchData();
                    },
                    function (error) {
                        swal("Error!", {
                            icon: "error"
                        });
                         myLoop.stop();
                    }
                );
            }
            // vm.item.dob=kendo.parseDate(vm.item.dob, 'dd/MM/yyyy');
        }
        //================= On Init function ========================//
        init();
        function init() {
            // vm.showCVs = true;

            $scope.dateOptions = {
                format: "dd/MM/yyyy",
                optionLabel: "Select Level...",
            };
            var files = [];
            $("#files").kendoUpload({
                async: {
                    removeUrl: "remove",
                    autoUpload: false
                },
                multiple: false,
                validation: {
                    allowedExtensions: [".doc", ".docx", ".pdf"]
                },
                upload: function (e) {
                    e.data = { id: vm.selectedItem == undefined ? 0 : vm.selectedItem.id }
                },
                remove: onRemove,
                select: onSelect,
                showFileList: true,
            });
            function onSelect(e) {
                vm.showCVs = false;
                vm.$apply();
            }
            function onRemove(e) {
                var files = e.files;
                $scope.Files = {};
            }

            vm.siteOptions = {
                optionLabel: "Select Site...",
                dataTextField: "name",
                dataValueField: "id",
                valuePrimitive: true,
                autoBind: false,
                dataSource: new kendo.data.DataSource({
                    transport: {
                        read: function (options) {
                            ConstantService.getConstants("Site", null).then(
                                function (response) {
                                    options.success(response.data.result);
                                },
                                function (error) {
                                    swal("Error!", {
                                        icon: "error"
                                    });
                                })
                        }
                    }
                })
            }
            vm.jobRankOptions = {
                optionLabel: "Select Level...",
                dataTextField: "name",
                dataValueField: "id",
                valuePrimitive: true,
                autoBind: true,
                dataSource: new kendo.data.DataSource({
                    transport: {
                        read: function (options) {
                            ConstantService.getConstants("JobRank", null).then(
                                function (response) {
                                    options.success(response.data.result);
                                },
                                function (error) {
                                    swal("Error!", {
                                        icon: "error"
                                    });
                                })
                        }
                    }
                })
            }
            vm.genderOptions = {
                optionLabel: "Select gender...",
                dataTextField: "name",
                dataValueField: "id",
                valuePrimitive: true,
                autoBind: true,
                dataSource: new kendo.data.DataSource({
                    transport: {
                        read: function (options) {
                            ConstantService.getConstants("Gender", null).then(
                                function (response) {
                                    options.success(response.data.result);
                                },
                                function (error) {
                                    swal("Error!", {
                                        icon: "error"
                                    });
                                })
                        }
                    }
                })
            }
            vm.selectTechOptions = {
                placeholder: "Select technical...",
                dataTextField: "name",
                dataValueField: "id",
                valuePrimitive: true,
                autoBind: false,
                dataSource: new kendo.data.DataSource({
                    transport: {
                        read: function (options) {
                            ConstantService.getConstants("Technology", null).then(
                                function (response) {
                                    vm.lstTechnology = response.data.result;
                                    options.success(response.data.result);
                                },
                                function (error) {
                                    swal("Error!", {
                                        icon: "error"
                                    });
                                })
                        }
                    }
                })
            }
        }
        vm.getTheFiles = function ($files) {
            vm.item.lstCVs = $files;
            vm.item.cvs = [];
            if ($files.length > 0) {
                vm.item.cvs = {
                    name: $files[0].name,
                    size: $files[0].size,
                }
            }
        };

        vm.onSubmitValidate = function () {
            if (vm.validator.validate()) {
                if (vm.item.fullName
                    && vm.item.email
                    && vm.item.phone
                )
                    vm.onSaveChange();
                else
                    swal("You must fill out all required input (*) before submit.", {
                        icon: "warning"
                    });
            } else {

            }
        }
        //================= On Change function ========================//
    })
