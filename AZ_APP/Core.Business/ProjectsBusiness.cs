﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using AutoMapper;
using CommonHelper.DataContext;
using CommonHelper.Helper;
using Core.Models;
using Core.Models.EF;
using Core.Models.Messages;
using Core.Models.Share;

namespace Core.Business
{
    public class ProjectsBusiness
    {
        #region + Constructor

        private readonly BoopAppContext _context;
        public ProjectsBusiness()
        {
            _context = new BoopAppContext();
        }
        #endregion

        #region  Method
        public List<Projects> GetAllData()
        {
            return _context.Projects.Where(x => x.IsActive).OrderBy(x => x.Name).ToList();
        }

        public async Task<IEnumerable<ProjectsView>> GetData(string name, DataTable filterDataTable, DataTable sortDataTable, int skip, int take)
        {
            string methodName = "OrdersView";
            try
            {
                Repository<ProjectsView> repository = new Repository<ProjectsView>();
                SqlParameter[] paras =
                {
                    new SqlParameter { ParameterName = "@name",  Value = (object)name??DBNull.Value, DbType = DbType.AnsiString},
                    new SqlParameter { ParameterName =  "@filter" , Value = filterDataTable , TypeName = "FilterType" , SqlDbType = SqlDbType.Structured},
                    new SqlParameter { ParameterName = "@sort" , Value = sortDataTable , TypeName = "SortType" , SqlDbType = SqlDbType.Structured},
                    new SqlParameter { ParameterName = "@skip", Value = skip , DbType = DbType.Int32},
                    new SqlParameter {ParameterName = "@take", Value = take, DbType = DbType.Int32}
                };
                var results = await repository.Get(Constants.StoreProcedure.ProjectsList, paras);
                return results;
            }
            catch (Exception ex)
            {
                Logger.CreateLog(Logger.Levels.ERROR, this, methodName, ex, ex.Message);
                return null;
            }
        }

        public async Task<ResultBase<bool>> Create(Projects model)
        {
            try
            {
                using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    var repository = new Repository<Projects>();
                    var item = new Projects();
                    item.Name = model.Name;
                    item.CreatedDate = DateTime.Now;
                    item.ModifiedDate = DateTime.Now;
                    item.IsActive = true;
                    var result = await repository.Insert(item);
                    scope.Complete();
                    if (!result)
                    {
                        return new ResultBase<bool>()
                        {
                            Status = false,
                            Message = Constants.Message.SOMETHING_ERROR
                        };
                    }
                    return new ResultBase<bool>()
                    {
                        Status = true,
                        Message = Constants.Message.SAVED_SUCCESSFULLY
                    };
                }
            }
            catch (Exception ex)
            {
                Logger.CreateLog(Logger.Levels.ERROR, this, "Create(Projects model)", ex);
                return new ResultBase<bool>()
                {
                    Status = false,
                    Message = ex.Message
                };
            }

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public async Task<ResultBase<bool>> Update(Projects data)
        {
            try
            {
                using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    #region + Update into 

                    var repository = new Repository<Projects>();
                    var item = await repository.GetOne(x => x.Id == data.Id);

                    item.Name = data.Name.Trim();
                    item.ModifiedDate = DateTime.Now;
                    item.IsActive = data.IsActive;

                    var result = await repository.Edit(item);
                    if (!result) return new ResultBase<bool>()
                    {
                        Status = false
                    };

                    #endregion + Update into 

                    scope.Complete();
                    return new ResultBase<bool>()
                    {
                        Status = true,
                        Message = Constants.Message.SAVED_SUCCESSFULLY
                    };
                }
            }
            catch (Exception ex)
            {
                Logger.CreateLog(Logger.Levels.ERROR, this, "Update(Orders data)", ex);
                return new ResultBase<bool>()
                {
                    Status = false,
                    Message = ex.Message
                };
            }
        }

        public async Task<bool> Deletes(List<int> lstId)
        {
            try
            {
                using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {

                    if (lstId.Any())
                    {
                        var itemDeletes = _context.Projects.Where(x => lstId.Any(p => p == x.Id));
                        if (itemDeletes.Any())
                        {
                            foreach (var item in itemDeletes)
                            {
                                item.IsActive = false;
                                item.IsDelete = true;
                            };
                            var result = await _context.SaveChangesAsync() > 0;
                            if (!result) return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                    scope.Complete();
                    return true;
                }
            }
            catch (Exception ex)
            {
                Logger.CreateLog(Logger.Levels.ERROR, this, "Deletes(DeleteMasterDataParameter data)", ex);
                return false;
            }
        }


        public async Task<int> Test()
        {
            //using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            //{
            var repository = new Repository<Product>();
            var lst = _context.Products.ToList();
            foreach (var item in lst)
            {
                item.ProductId = shortid.ShortId.Generate(true, false, 8);
                //await repository.Edit(item);
            }
            _context.SaveChanges();
            //}
            return 1;
        }
        #endregion
    }
}
