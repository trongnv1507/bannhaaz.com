﻿using AutoMapper;
using CommonHelper.DataContext;
using CommonHelper.Helper;
using Core.Models;
using Core.Models.EF;
using Core.Models.Messages;
using Core.Models.Share;
using shortid;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Core.Business
{
    public class RecEmployeesBusiness
    {
        #region + Constructor

        private readonly BoopAppContext _context;
        public RecEmployeesBusiness()
        {
            _context = new BoopAppContext();
        }
        #endregion

        #region  Method
        public async Task<IEnumerable<RecEmployeesView>> GetData(string userLogin, string name, DataTable filterDataTable, DataTable sortDataTable, int skip, int take)
        {
            string methodName = "GetData";
            try
            {
                Repository<RecEmployeesView> repository = new Repository<RecEmployeesView>();
                SqlParameter[] paras =
                {
                    new SqlParameter { ParameterName = "@createdBy",  Value = (object)userLogin??DBNull.Value, DbType = DbType.AnsiString},
                    new SqlParameter { ParameterName = "@name",  Value = (object)name??DBNull.Value, DbType = DbType.AnsiString},
                    new SqlParameter { ParameterName =  "@filter" , Value = filterDataTable , TypeName = "FilterType" , SqlDbType = SqlDbType.Structured},
                    new SqlParameter { ParameterName = "@sort" , Value = sortDataTable , TypeName = "SortType" , SqlDbType = SqlDbType.Structured},
                    new SqlParameter { ParameterName = "@skip", Value = skip , DbType = DbType.Int32},
                    new SqlParameter {ParameterName = "@take", Value = take, DbType = DbType.Int32}
                };
                var results = await repository.Get(Constants.StoreProcedure.RecEmployeesList, paras);
                return results;
            }
            catch (Exception ex)
            {
                Logger.CreateLog(Logger.Levels.ERROR, this, methodName, ex, ex.Message);
                return null;
            }
        }
        public async Task<IEnumerable<RecCallingInfo>> GetCallingInfo(string candidateId)
        {
            string methodName = "GetCallingInfo";
            try
            {
                Repository<RecCallingInfo> repository = new Repository<RecCallingInfo>();
                var results = await repository.Get(x => x.CandidateId.Equals(candidateId));
                return results.OrderByDescending(x => x.CreatedDate);
            }
            catch (Exception ex)
            {
                Logger.CreateLog(Logger.Levels.ERROR, this, methodName, ex, ex.Message);
                return null;
            }
        }
        public async Task<ResultBase<bool>> Create(RecEmployeesView model)
        {
            try
            {
                using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    var repository = new Repository<RecEmployees>();

                    #region +Check Exits Phone and Email
                    var ePhoneEmail = await repository.GetOne(x => x.Phone.Equals(model.Phone) && x.Email.Equals(model.Email)); ;
                    if (ePhoneEmail != null)
                    {
                        return new ResultBase<bool>()
                        {
                            Status = false,
                            Message = Constants.Message.PHONE_EMAIL_EXITS
                        };
                    }
                    var ePhone = await repository.GetOne(x => x.Phone.Equals(model.Phone));
                    if (ePhone != null)
                    {
                        return new ResultBase<bool>()
                        {
                            Status = false,
                            Message = Constants.Message.PHONE_EXITS
                        };
                    }
                    var eEmail = await repository.GetOne(x => x.Email.Equals(model.Email));
                    if (eEmail != null)
                    {
                        return new ResultBase<bool>()
                        {
                            Status = false,
                            Message = Constants.Message.EMAIL_EXITS
                        };
                    }
                    #endregion

                    var item = Mapper.Map<RecEmployeesView, RecEmployees>(model);
                    item.CreatedDate = DateTime.Now;
                    item.IsActive = true;
                    item.ModifiedDate = DateTime.Now;
                    item.Address = !string.IsNullOrEmpty(item.Address) ? item.Address.Trim() : string.Empty;
                    item.DOB = item.DOB == DateTime.MinValue ? null : item.DOB;

                    if (model.CVs != null)
                    {
                        var repositoryFile = new Repository<RecFiles>();
                        var recFileResult = await repositoryFile.InsertAndGetData(model.CVs);
                        if (recFileResult != null)
                            item.CV = recFileResult.Id;
                    }
                    if (model.SelectedTechIds != null)
                    {
                        if (model.SelectedTechIds.Count > 0)
                        {
                            var repositoryTech = new Repository<RecEmployeeTechnology>();
                            foreach (var tech in model.SelectedTechIds)
                            {
                                var emTech = new RecEmployeeTechnology();
                                emTech.CandidateId = item.CandidateId;
                                emTech.TechId = tech;
                                await repositoryTech.Insert(emTech);
                            }
                        }
                    }
                    if (!string.IsNullOrEmpty(model.CallingInfo))
                    {
                        var repositoryCalling = new Repository<RecCallingInfo>();
                        var callingModel = new RecCallingInfo();
                        callingModel.CandidateId = model.CandidateId;
                        callingModel.Notes = model.CallingInfo;
                        callingModel.CreatedDate = DateTime.Now;
                        await repositoryCalling.InsertAndGetData(callingModel);
                    }

                    var result = await repository.InsertAndGetData(item);
                    if (result == null)
                    {
                        return new ResultBase<bool>()
                        {
                            Status = false,
                            Message = Constants.Message.SOMETHING_ERROR
                        };
                    }


                    scope.Complete();
                    return new ResultBase<bool>()
                    {
                        Status = true,
                        IdReturn = model.CandidateId,
                        Message = Constants.Message.SAVED_SUCCESSFULLY
                    };
                }
            }
            catch (Exception ex)
            {
                Logger.CreateLog(Logger.Levels.ERROR, this, "Create(RecEmployeesView model)", ex);
                return new ResultBase<bool>()
                {
                    Status = false,
                    Message = ex.Message
                };
            }

        }
        public async Task<ResultBase<bool>> Update(RecEmployeesView data)
        {
            try
            {
                using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    #region + Update into 

                    var repository = new Repository<RecEmployees>();
                    #region +Check Exits Phone and Email
                    var ePhoneEmail = await repository.GetOne(x => !x.CandidateId.Equals(data.CandidateId) && x.Phone.Equals(data.Phone) && x.Email.Equals(data.Email)); ;
                    if (ePhoneEmail != null)
                    {
                        return new ResultBase<bool>()
                        {
                            Status = false,
                            Message = Constants.Message.PHONE_EMAIL_EXITS
                        };
                    }
                    var ePhone = await repository.GetOne(x => !x.CandidateId.Equals(data.CandidateId) && x.Phone.Equals(data.Phone));
                    if (ePhone != null)
                    {
                        return new ResultBase<bool>()
                        {
                            Status = false,
                            Message = Constants.Message.PHONE_EXITS
                        };
                    }
                    var eEmail = await repository.GetOne(x => !x.CandidateId.Equals(data.CandidateId) && x.Email.Equals(data.Email));
                    if (eEmail != null)
                    {
                        return new ResultBase<bool>()
                        {
                            Status = false,
                            Message = Constants.Message.EMAIL_EXITS
                        };
                    }
                    #endregion

                    var item = await repository.GetOne(x => x.CandidateId.Trim() == data.CandidateId.Trim());

                    data.Address = !string.IsNullOrEmpty(data.Address) ? data.Address.Trim() : string.Empty;
                    data.FullName = !string.IsNullOrEmpty(data.FullName) ? data.FullName.Trim() : string.Empty;
                    data.Email = !string.IsNullOrEmpty(data.Email) ? data.Email.Trim() : string.Empty;
                    data.Phone = !string.IsNullOrEmpty(data.Phone) ? data.Phone.Trim() : string.Empty;

                    data.CreatedDate = item.CreatedDate;
                    data.ModifiedDate = DateTime.Now;
                    data.IsActive = item.IsActive;
                    data.DOB = data.DOB == DateTime.MinValue ? null : data.DOB;

                    if (data.SelectedTechIds != null)
                    {
                        if (data.SelectedTechIds.Count > 0)
                        {
                            var repositoryTech = new Repository<RecEmployeeTechnology>();
                            foreach (var tech in data.SelectedTechIds)
                            {
                                var empTech = await repositoryTech.GetOne(x => x.CandidateId == item.CandidateId && x.TechId == tech);
                                if (empTech == null)
                                {
                                    empTech = new RecEmployeeTechnology();
                                    empTech.CandidateId = item.CandidateId;
                                    empTech.TechId = tech;
                                    await repositoryTech.Insert(empTech);
                                }

                            }
                        }
                    }

                    if (!string.IsNullOrEmpty(data.CallingInfo))
                    {
                        var repositoryCalling = new Repository<RecCallingInfo>();
                        var callingModel = new RecCallingInfo();
                        callingModel.CandidateId = item.CandidateId;
                        callingModel.Notes = data.CallingInfo;
                        callingModel.CreatedDate = DateTime.Now;
                        await repositoryCalling.InsertAndGetData(callingModel);
                    }
                    if (data.CVs != null)
                    {
                        if (data.CVs.Id == 0)
                        {
                            var repositoryFile = new Repository<RecFiles>();
                            var recFileResult = await repositoryFile.InsertAndGetData(data.CVs);
                            if (recFileResult != null)
                                data.CV = recFileResult.Id;
                        }
                    }
                    item = Mapper.Map(data, item);

                    var result = await repository.Edit(item);
                    if (!result) return new ResultBase<bool>()
                    {
                        Status = false
                    };

                    #endregion + Update into 

                    scope.Complete();
                    return new ResultBase<bool>()
                    {
                        Status = true,
                        IdReturn = item.CandidateId,
                        Message = Constants.Message.SAVED_SUCCESSFULLY
                    };
                }
            }
            catch (Exception ex)
            {
                Logger.CreateLog(Logger.Levels.ERROR, this, "Update(RecEmployees data)", ex);
                return new ResultBase<bool>()
                {
                    Status = false,
                    Message = ex.Message
                };
            }
        }

        public async Task<bool> Deletes(List<string> lstId, string deletedBy)
        {
            try
            {
                using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    if (lstId.Any())
                    {
                        var itemDeletes = _context.RecEmployees.Where(x => lstId.Any(p => p == x.CandidateId));
                        if (itemDeletes.Any())
                        {
                            foreach (var item in itemDeletes)
                            {
                                item.IsDelete = true;
                                item.DeletedDate = DateTime.Now;
                                item.DeletedBy = deletedBy;
                            };
                            var result = await _context.SaveChangesAsync() > 0;
                            if (!result) return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                    scope.Complete();
                    return true;
                }
            }
            catch (Exception ex)
            {
                Logger.CreateLog(Logger.Levels.ERROR, this, "Deletes(DeleteMasterDataParameter data)", ex);
                return false;
            }
        }
        public async Task<RecFiles> GetFileByCandidate(string candidateId)
        {
            string methodName = "GetFileByCandidate";
            try
            {
                Repository<RecEmployees> repository = new Repository<RecEmployees>();
                var empModel = await repository.GetOne(x => x.CandidateId.Trim().ToLower() == candidateId.Trim().ToLower());
                if (empModel != null)
                {
                    Repository<RecFiles> repositoryFiles = new Repository<RecFiles>();
                    var result = await repositoryFiles.GetOne(x => x.Id == empModel.CV);
                    return result != null ? result : null;
                }
                return null;
            }
            catch (Exception ex)
            {
                Logger.CreateLog(Logger.Levels.ERROR, this, methodName, ex, ex.Message);
                return null;
                throw;
            }
        }

        public async Task<ResultBase<bool>> CheckRowData(string filePath, string extension)
        {
            string conString = string.Empty;
            switch (extension)
            {
                case ".xls": //Excel 97-03.
                    conString = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
                    break;
                case ".xlsx": //Excel 07 and above.
                    conString = ConfigurationManager.ConnectionStrings["Excel07ConString"].ConnectionString;
                    break;
            }

            DataTable dt = new DataTable();
            conString = string.Format(conString, filePath);

            using (OleDbConnection connExcel = new OleDbConnection(conString))
            {
                using (OleDbCommand cmdExcel = new OleDbCommand())
                {
                    using (OleDbDataAdapter odaExcel = new OleDbDataAdapter())
                    {
                        cmdExcel.Connection = connExcel;

                        //Get the name of First Sheet.
                        connExcel.Open();
                        DataTable dtExcelSchema;
                        dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                        string sheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                        connExcel.Close();

                        //Read Data from First Sheet.
                        connExcel.Open();
                        cmdExcel.CommandText = "SELECT * From [" + sheetName + "]";
                        odaExcel.SelectCommand = cmdExcel;
                        odaExcel.Fill(dt);
                        connExcel.Close();
                    }
                }
            }
            int countRow = dt.Rows.Count;
            if (countRow > 500)
            {
                return new ResultBase<bool>()
                {
                    Status = false,
                    Message = Constants.Message.FILE_MAX_500
                };
            }
            return new ResultBase<bool>()
            {
                Status = true,
                IdReturn = countRow,
                Message = Constants.Message.GET_DATA_SUCCESSFULLY
            };
        }
        public async Task<IEnumerable<RecEmployeesView>> Imports(string filePath, string extension)
        {
            try
            {
                //using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                //{

                string conString = string.Empty;
                switch (extension)
                {
                    case ".xls": //Excel 97-03.
                        conString = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
                        break;
                    case ".xlsx": //Excel 07 and above.
                        conString = ConfigurationManager.ConnectionStrings["Excel07ConString"].ConnectionString;
                        break;
                }

                DataTable dt = new DataTable();
                conString = string.Format(conString, filePath);

                using (OleDbConnection connExcel = new OleDbConnection(conString))
                {
                    using (OleDbCommand cmdExcel = new OleDbCommand())
                    {
                        using (OleDbDataAdapter odaExcel = new OleDbDataAdapter())
                        {
                            cmdExcel.Connection = connExcel;

                            //Get the name of First Sheet.
                            connExcel.Open();
                            DataTable dtExcelSchema;
                            dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                            string sheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                            connExcel.Close();

                            //Read Data from First Sheet.
                            connExcel.Open();
                            cmdExcel.CommandText = "SELECT * From [" + sheetName + "]";
                            odaExcel.SelectCommand = cmdExcel;
                            odaExcel.Fill(dt);
                            connExcel.Close();
                        }
                    }
                }
                //string sqlQuery = string.Empty;
                var sqlQuery = new StringBuilder();
                sqlQuery.Append(" DECLARE  @Error TABLE(");
                sqlQuery.Append(" [Id] [int] IDENTITY(1,1) NOT NULL,");
                sqlQuery.Append(" [Row] [int]  NULL,");
                sqlQuery.Append(" [CandidateId] [nchar](12) NOT NULL,");
                sqlQuery.Append(" [FullName] [nvarchar](50) NOT NULL,");
                sqlQuery.Append(" [Phone] [varchar](30) NOT NULL,");
                sqlQuery.Append(" [Email] [nchar](100) NOT NULL,");
                sqlQuery.Append(" [DOB] [date] NULL,	");
                sqlQuery.Append(" [Technology] [nchar](200) NULL,	");
                sqlQuery.Append(" [SocialLink] [nchar](200) NULL,	");
                sqlQuery.Append(" [ErrorMessage] [Nvarchar](200) NULL");
                sqlQuery.Append(")");
                int countRow = dt.Rows.Count;
                int countCol = dt.Columns.Count;
                for (int iRow = 0; iRow < 500; iRow++)
                {
                    //for (int iCol = 0; iCol < countCol; iCol++)
                    //{
                    var candidateId = dt.Rows[iRow]["CandidateId"].ToString().Trim();
                    var fullName = dt.Rows[iRow]["FullName"].ToString().Trim();
                    var email = dt.Rows[iRow]["Email"].ToString().Trim();
                    var phone = dt.Rows[iRow]["Phone"].ToString().Trim();
                    var socialLink = dt.Rows[iRow]["SocialLink"].ToString().Trim();
                    var dob = dt.Rows[iRow]["DOB"].ToString().Trim();

                    if (string.IsNullOrEmpty(candidateId)) candidateId = "IMP_" + ShortId.Generate(true, false, 7).ToUpper();

                    sqlQuery.Append("   IF NOT EXISTS ( SELECT 1 FROM [dbo].[Rec_Employees] WHERE Email = '" + email + "' OR Phone='" + phone + "' )");
                    sqlQuery.Append("   BEGIN");
                    sqlQuery.Append("   BEGIN TRY");
                    sqlQuery.Append("   INSERT INTO [dbo].[Rec_Employees] (CandidateId,FullName,Email,Phone,DOB,SocialLink,CreatedDate,CreatedBy,IsActive,IsImport)");
                    sqlQuery.Append("   VALUES ('" + candidateId + "',N'" + fullName + "','" + email + "','" + phone + "',NULl,'" + socialLink + "',GETDATE(),'CreatedBy',1,1)");
                    sqlQuery.Append("   END TRY");
                    sqlQuery.Append("   BEGIN CATCH	");
                    sqlQuery.Append("   INSERT INTO @Error (Row,CandidateId,FullName,Email,Phone,DOB,SocialLink,ErrorMessage)");
                    sqlQuery.Append("   VALUES (" + iRow + ",'" + candidateId + "',N'" + fullName + "','" + email + "','" + phone + "',NULl,'" + socialLink + "','Error ' + CONVERT(VARCHAR, ERROR_NUMBER(), 1) + ': '+ ERROR_MESSAGE())");
                    sqlQuery.Append("   END CATCH");
                    sqlQuery.Append("   END");
                    sqlQuery.Append("   ELSE ");
                    sqlQuery.Append("   BEGIN");
                    sqlQuery.Append("   INSERT INTO @Error (Row,CandidateId,FullName,Email,Phone,DOB,SocialLink,ErrorMessage)");
                    sqlQuery.Append("   VALUES (" + iRow + ",'" + candidateId + "',N'" + fullName + "','" + email + "','" + phone + "',NULl,'" + socialLink + "','The Email or Phone already exists in the system')");
                    sqlQuery.Append("   END");
                    //}
                }
                sqlQuery.Append(" SELECT * FROM @Error");
                sqlQuery.Append(" DELETE FROM @Error");

                Repository<RecEmployeesView> repository = new Repository<RecEmployeesView>();
                var result = await repository.ExecQuery(sqlQuery.ToString());
                return result;
                //}
            }
            catch (Exception ex)
            {
                Logger.CreateLog(Logger.Levels.ERROR, this, "Deletes(DeleteMasterDataParameter data)", ex);
                return null;
            }
        }

        public static void InsertMp(DataTable dt, int index, ref DataTable errorDT, ref string errorIndex)
        {
            SqlConnection connection;
            string constring = ConfigurationManager.ConnectionStrings["BoopAppContext"].ToString();
            connection = new SqlConnection(constring);

            using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(connection.ConnectionString))
            {
                try
                {
                    connection.Open();
                    var filesInserted = 0L;
                    //Set the database table name.
                    sqlBulkCopy.DestinationTableName = "dbo.Rec_Employees";
                    sqlBulkCopy.NotifyAfter = dt.Rows.Count;
                    sqlBulkCopy.SqlRowsCopied += (s, e) => filesInserted = e.RowsCopied;
                    //[OPTIONAL]: Map the Excel columns with that of the database table
                    sqlBulkCopy.ColumnMappings.Add("CandidateId", "CandidateId");
                    sqlBulkCopy.ColumnMappings.Add("FullName", "FullName");
                    sqlBulkCopy.ColumnMappings.Add("Email", "Email");
                    sqlBulkCopy.ColumnMappings.Add("Phone", "Phone");
                    sqlBulkCopy.ColumnMappings.Add("SocialLink", "SocialLink");
                    try
                    {
                        sqlBulkCopy.WriteToServer(dt);
                    }
                    catch (Exception e)
                    {
                        ///If an exception occurs, SqlBulkCopy will rollback, all records in the Table are not inserted into the database
                        ///At this point, half the Table, first insert one half, then insert the halves.
                        ///Recursion, until there is only one line, if exception is returned.
                        if (dt.Rows.Count == 1)
                        {
                            //Debug.Write(index);
                            errorIndex += (index.ToString() + "; ");
                            errorDT.ImportRow(dt.Rows[0]);
                            return;
                        }

                        int middle = dt.Rows.Count / 2;
                        DataTable table = dt.Clone();

                        for (int i = 0; i < middle; i++)
                            table.ImportRow(dt.Rows[i]);
                        InsertMp(table, index, ref errorDT, ref errorIndex);
                        table.Clear();

                        for (int i = middle; i < dt.Rows.Count; i++)
                            table.ImportRow(dt.Rows[i]);
                        InsertMp(table, index + middle, ref errorDT, ref errorIndex);
                        table.Clear();

                    }
                    finally
                    {
                        sqlBulkCopy.Close();
                    }
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    connection.Close();

                }
            }
        }
        #endregion
    }
}
