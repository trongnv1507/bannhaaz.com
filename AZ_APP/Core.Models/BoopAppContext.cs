using System.Collections.Generic;
using System.Reflection;
using Core.Models.EF;

namespace Core.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class BoopAppContext : DbContext
    {
        public BoopAppContext()
            : base("name=BoopAppContext")
        {
            this.Database.CommandTimeout = 0;
        }
        static BoopAppContext()
        {
            Database.SetInitializer<BoopAppContext>(null);
        }

        public BoopAppContext(string nameOfConnectionString)
            : base(nameOfConnectionString)
        {
            this.Database.CommandTimeout = 0;
        }
        private static IReadOnlyDictionary<Type, IReadOnlyCollection<PropertyInfo>> _ignoredProperties;
        public static IReadOnlyDictionary<Type, IReadOnlyCollection<PropertyInfo>> IgnoredProperties
        {
            get { return _ignoredProperties; }
        }

        #region + REC
        public virtual DbSet<RecFiles> RecFiles { get; set; }
        public virtual DbSet<RecEmployees> RecEmployees { get; set; }
        public virtual DbSet<RecEmployeeTechnology> RecEmployeeTechnologys { get; set; }
        public virtual DbSet<RecCallingInfo> RecCallingInfos { get; set; }

        #endregion
        public virtual DbSet<UrlConfig> UrlConfigs { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<Projects> Projects { get; set; }
        public virtual DbSet<SendLand> SendLands { get; set; }
        public virtual DbSet<Folder> Folders { get; set; }
        public virtual DbSet<Contents> Contents { get; set; }
        public virtual DbSet<Services> Services { get; set; }
        public virtual DbSet<Files> Files { get; set; }
        public virtual DbSet<Constant> Constant { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
