﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models.EF
{
    [Table("Rec_CallingInfo")]
    [Serializable]
    public class RecCallingInfo
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public string CandidateId { get; set; }

        [Required]
        [StringLength(200)]
        public string Notes { get; set; }
        [Required]
        public DateTime CreatedDate { get; set; }
    }
}
