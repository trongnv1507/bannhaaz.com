﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models.EF
{
    [Table("Rec_Employees")]
    [Serializable]
    public class RecEmployees
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        [StringLength(12)]
        public string CandidateId { get; set; }

        [Required]
        [StringLength(200)]
        public string FullName { get; set; }
        [Required]
        public string Phone { get; set; }
        [Required]
        public string Email { get; set; }
        public DateTime? DOB { get; set; }
        public string Technology { get; set; }
        public int Site { get; set; }
        public int Gender { get; set; }
        public string Address { get; set; }
        public string SocialLink { get; set; }
        public int? CV { get; set; }
        public int JobRank { get; set; }
        public bool? IsBlacklist { get; set; }

        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public string ModifiedBy { get; set; }
        public bool? IsActive { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? DeletedDate { get; set; }
        public string DeletedBy { get; set; }

    }
}
