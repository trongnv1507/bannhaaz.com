using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Core.Models.EF
{
    [Table("Rec_Files")]
    [Serializable]
    public partial class RecFiles
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Column(Order = 1)]
        [StringLength(500)]
        public string Name { get; set; }

        [StringLength(500)]
        public string Link { get; set; }
        public string Extension { get; set; }
        public DateTime CreatedDate { get; set; } = DateTime.Now;
        public int Size { get; set; }
    }

}

