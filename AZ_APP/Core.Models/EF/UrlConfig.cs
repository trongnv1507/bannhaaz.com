﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models.EF
{
    [Table("UrlConfig")]
    [Serializable]
    public partial class UrlConfig
    {
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Url { get; set; }
        public int? TransactionId { get; set; }
        public int? TypeId { get; set; }
        public int? City { get; set; }
        public int? District { get; set; }
        public int? Ward { get; set; }
        public int? Street { get; set; }
        public int? ProjectId { get; set; }
    }
}
