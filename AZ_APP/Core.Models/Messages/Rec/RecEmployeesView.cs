using Core.Models.EF;
using System;
using System.Collections.Generic;

namespace Core.Models.Messages
{
    public class RecEmployeesView
    {
        public string CandidateId { get; set; }

        public string FullName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public DateTime? DOB { get; set; }
        public string Technology { get; set; }
        public string ListTechnologyId { get; set; }
        public int? Site { get; set; }
        public string SiteName { get; set; }
        public int? Gender { get; set; }
        public string GenderName { get; set; }
        public int? JobRank { get; set; }
        public string JobRankName { get; set; }
        public string Address { get; set; }
        public string SocialLink { get; set; }
        public int? CV { get; set; }
        public bool? IsBlacklist { get; set; }

        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }

        public DateTime? ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }


        public bool? IsActive { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? DeletedDate { get; set; }
        public string DeletedBy { get; set; }
        public int Total { get; set; }

        public RecFiles CVs { get; set; } = null;
        public string CallingInfo { get; set; }
        public List<int> SelectedTechIds { get; set; } = null;

    }

}

