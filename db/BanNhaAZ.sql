USE [master]
GO
/****** Object:  Database [dathangtb_com_trong5]    Script Date: 2/24/2020 5:48:43 PM ******/
CREATE DATABASE [dathangtb_com_trong5]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'BoopApp', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MYSQL\MSSQL\DATA\dathangtb_com_trong5_9316b4ab8d8743ae869ea253b65650bd.mdf' , SIZE = 7168KB , MAXSIZE = 3145728KB , FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'BoopApp_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MYSQL\MSSQL\DATA\dathangtb_com_trong5_41d6e3159edc4959a080ad1d2c3127ff.ldf' , SIZE = 1792KB , MAXSIZE = 3145728KB , FILEGROWTH = 10%)
GO
ALTER DATABASE [dathangtb_com_trong5] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [dathangtb_com_trong5].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [dathangtb_com_trong5] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [dathangtb_com_trong5] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [dathangtb_com_trong5] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [dathangtb_com_trong5] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [dathangtb_com_trong5] SET ARITHABORT OFF 
GO
ALTER DATABASE [dathangtb_com_trong5] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [dathangtb_com_trong5] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [dathangtb_com_trong5] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [dathangtb_com_trong5] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [dathangtb_com_trong5] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [dathangtb_com_trong5] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [dathangtb_com_trong5] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [dathangtb_com_trong5] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [dathangtb_com_trong5] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [dathangtb_com_trong5] SET  DISABLE_BROKER 
GO
ALTER DATABASE [dathangtb_com_trong5] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [dathangtb_com_trong5] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [dathangtb_com_trong5] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [dathangtb_com_trong5] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [dathangtb_com_trong5] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [dathangtb_com_trong5] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [dathangtb_com_trong5] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [dathangtb_com_trong5] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [dathangtb_com_trong5] SET  MULTI_USER 
GO
ALTER DATABASE [dathangtb_com_trong5] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [dathangtb_com_trong5] SET DB_CHAINING OFF 
GO
ALTER DATABASE [dathangtb_com_trong5] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [dathangtb_com_trong5] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [dathangtb_com_trong5] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'dathangtb_com_trong5', N'ON'
GO
ALTER DATABASE [dathangtb_com_trong5] SET QUERY_STORE = OFF
GO
USE [dathangtb_com_trong5]
GO
/****** Object:  User [dathangtb_com_boop2]    Script Date: 2/24/2020 5:48:43 PM ******/
CREATE USER [dathangtb_com_boop2] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dathangtb_com_boop2]
GO
ALTER ROLE [db_ddladmin] ADD MEMBER [dathangtb_com_boop2]
GO
ALTER ROLE [db_backupoperator] ADD MEMBER [dathangtb_com_boop2]
GO
ALTER ROLE [db_datareader] ADD MEMBER [dathangtb_com_boop2]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [dathangtb_com_boop2]
GO
/****** Object:  Schema [dathangtb_com_boop2]    Script Date: 2/24/2020 5:48:43 PM ******/
CREATE SCHEMA [dathangtb_com_boop2]
GO
/****** Object:  UserDefinedTableType [dbo].[FilterType]    Script Date: 2/24/2020 5:48:43 PM ******/
CREATE TYPE [dbo].[FilterType] AS TABLE(
	[Field] [varchar](50) NOT NULL,
	[ValueString] [nvarchar](max) NULL,
	[ValueDateTimeFrom] [datetime] NULL,
	[ValueDateTimeTo] [datetime] NULL,
	[ValueDecimalFrom] [decimal](18, 0) NULL,
	[ValueDecimalTo] [decimal](18, 0) NULL,
	[ValueBit] [bit] NULL,
	[IsActive] [bit] NOT NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[SortType]    Script Date: 2/24/2020 5:48:43 PM ******/
CREATE TYPE [dbo].[SortType] AS TABLE(
	[Field] [varchar](250) NOT NULL,
	[Asc] [bit] NULL,
	[IsActive] [bit] NOT NULL
)
GO
/****** Object:  UserDefinedFunction [dbo].[Fn_Filter_GetStringValueField]    Script Date: 2/24/2020 5:48:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[Fn_Filter_GetStringValueField]
(
	@filter FilterType READONLY,
	@fieldName VARCHAR(50)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN

	DECLARE @result NVARCHAR(MAX) = NULL;
	
	IF EXISTS(SELECT TOP 1 f.Field FROM @filter AS f WHERE UPPER(f.Field) = UPPER(@fieldName))
	BEGIN
	  IF EXISTS(SELECT TOP 1 f.ValueString FROM @filter AS f WHERE UPPER(f.Field) = UPPER(@fieldName) AND NULLIF(f.ValueString, '') IS NULL)
	   BEGIN
	     SET @result = ''
	   END
	   ELSE
		BEGIN
		 SET @result = (SELECT TOP 1 f.ValueString FROM @filter AS f WHERE UPPER(f.Field) = UPPER(@fieldName) AND NULLIF(f.ValueString, '') IS NOT NULL)
		END
	END
	
	RETURN @result

END

GO
/****** Object:  UserDefinedFunction [dbo].[Fn_IsFilterField]    Script Date: 2/24/2020 5:48:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[Fn_IsFilterField]
(
	@filter FilterType READONLY,
	@fieldName VARCHAR(50)
)
RETURNS BIT
AS
BEGIN

	DECLARE @result BIT = (CASE  
									WHEN EXISTS(SELECT TOP 1 f.IsActive FROM @filter AS f WHERE UPPER(f.Field) = UPPER(@fieldName))
									THEN
										(SELECT TOP 1 f.IsActive FROM @filter AS f WHERE UPPER(f.Field) = UPPER(@fieldName))
									ELSE
										(SELECT 0)
								END)
	RETURN @result

END

GO
/****** Object:  UserDefinedFunction [dbo].[Fn_IsSortField]    Script Date: 2/24/2020 5:48:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[Fn_IsSortField]
(
	@sort SortType READONLY,
	@fieldName VARCHAR(50)
)
RETURNS BIT
AS
BEGIN

	DECLARE @result BIT = (CASE  
									WHEN EXISTS(SELECT TOP 1 f.IsActive FROM @sort AS f WHERE UPPER(f.Field) = UPPER(@fieldName))
									THEN
										(SELECT TOP 1 f.IsActive FROM @sort AS f WHERE UPPER(f.Field) = UPPER(@fieldName))
									ELSE
										(SELECT 0)
								END)
	RETURN @result

END



GO
/****** Object:  UserDefinedFunction [dbo].[Fn_IsSortFieldAsc]    Script Date: 2/24/2020 5:48:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[Fn_IsSortFieldAsc]
(
	@sort SortType READONLY,
	@fieldName VARCHAR(50)
)
RETURNS BIT
AS
BEGIN
	DECLARE @result BIT = (CASE  
									WHEN EXISTS(SELECT TOP 1 f.[Asc] FROM @sort AS f WHERE UPPER(f.Field) = UPPER(@fieldName))
									THEN
										(SELECT TOP 1 f.[Asc] FROM @sort AS f WHERE UPPER(f.Field) = UPPER(@fieldName))
									ELSE
										(SELECT 0)
								END)
	RETURN @result

END
GO
/****** Object:  UserDefinedFunction [dbo].[UF_StrToTable]    Script Date: 2/24/2020 5:48:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--select * from [UF_StrToTable] ('(Blanks);#2A',';#')

CREATE FUNCTION[dbo].[UF_StrToTable](@String NVARCHAR(MAX), @Delimiter CHAR(2))  
RETURNS @Temptable TABLE(Result VARCHAR(8000))  
AS  
BEGIN  
	DECLARE @INDEX int, @SLICE VARCHAR(8000)  
	SELECT @INDEX = 1  
	IF LEN(@String) < 1 OR @String IS NULL  
		return  
	WHILE @INDEX != 0  
		BEGIN  
			SET @INDEX = CHARINDEX(@Delimiter, @String)  
			IF @INDEX != 0  
			BEGIN  
				SET @SLICE = LEFT(@String, @INDEX - 1)  
			END  
			ELSE  
				BEGIN  
				SET @SLICE = @String  
				END  
			IF(LEN(@SLICE) > 0)  
			BEGIN  
			--Trongnv add : When Area is (Blanks)
				IF(@SLICE='(Blanks)')
					SET @SLICE=' ' 
			--
				INSERT INTO @Temptable(Result) VALUES(@SLICE)  
			END  
			SET @String = RIGHT(@String, LEN(@String) - @INDEX-1)  
			IF LEN(@String) = 0  
				break  
		END  
	RETURN  
END   
GO
/****** Object:  UserDefinedFunction [dbo].[UF_StrToTableInt]    Script Date: 2/24/2020 5:48:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--select * from [UF_StrToTable] ('(Blanks);#2A',';#')

Create FUNCTION[dbo].[UF_StrToTableInt](@String NVARCHAR(MAX), @Delimiter CHAR(2))  
RETURNS @Temptable TABLE(Result Int)  
AS  
BEGIN  
	DECLARE @INDEX int, @SLICE VARCHAR(8000)  
	SELECT @INDEX = 1  
	IF LEN(@String) < 1 OR @String IS NULL  
		return  
	WHILE @INDEX != 0  
		BEGIN  
			SET @INDEX = CHARINDEX(@Delimiter, @String)  
			IF @INDEX != 0  
			BEGIN  
				SET @SLICE = LEFT(@String, @INDEX - 1)  
			END  
			ELSE  
				BEGIN  
				SET @SLICE = @String  
				END  
			IF(LEN(@SLICE) > 0)  
			BEGIN  
			--Trongnv add : When Area is (Blanks)
				IF(@SLICE='(Blanks)')
					SET @SLICE=' ' 
			--
				INSERT INTO @Temptable(Result) VALUES(@SLICE)  
			END  
			SET @String = RIGHT(@String, LEN(@String) - @INDEX-1)  
			IF LEN(@String) = 0  
				break  
		END  
	RETURN  
END   
GO
/****** Object:  Table [dbo].[__MigrationHistory]    Script Date: 2/24/2020 5:48:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__MigrationHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ContextKey] [nvarchar](300) NOT NULL,
	[Model] [varbinary](max) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_dbo.__MigrationHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC,
	[ContextKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 2/24/2020 5:48:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 2/24/2020 5:48:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 2/24/2020 5:48:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](128) NOT NULL,
	[ProviderKey] [nvarchar](128) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC,
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 2/24/2020 5:48:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](128) NOT NULL,
	[RoleId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 2/24/2020 5:48:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](128) NOT NULL,
	[Email] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEndDateUtc] [datetime] NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[UserName] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AZ_Constant]    Script Date: 2/24/2020 5:48:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AZ_Constant](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Type] [nchar](20) NULL,
	[Name] [nvarchar](100) NULL,
	[ValueString] [nvarchar](100) NULL,
	[ValueInt] [int] NULL,
	[ParentId] [int] NULL,
 CONSTRAINT [PK_AZ_Constant] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AZ_Contents]    Script Date: 2/24/2020 5:48:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AZ_Contents](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NULL,
	[Content] [nvarchar](max) NULL,
 CONSTRAINT [PK_Content] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AZ_Files]    Script Date: 2/24/2020 5:48:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AZ_Files](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FolderId] [int] NULL,
	[Name] [nvarchar](500) NULL,
	[Link] [nchar](500) NULL,
	[Thumbail] [nchar](50) NULL,
	[Active] [bit] NULL,
	[CreatedDate] [datetime] NULL,
	[Type] [int] NULL,
 CONSTRAINT [PK_AZ_Files] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AZ_Folder]    Script Date: 2/24/2020 5:48:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AZ_Folder](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[ParentId] [int] NULL,
	[IsActive] [bit] NULL,
	[Orders] [int] NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_Folder] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AZ_Products]    Script Date: 2/24/2020 5:48:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AZ_Products](
	[Id] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](200) NOT NULL,
	[TypeTransaction] [int] NOT NULL,
	[TypeProperty] [int] NOT NULL,
	[ProjectId] [int] NULL,
	[City] [int] NOT NULL,
	[District] [int] NOT NULL,
	[Ward] [int] NULL,
	[Street] [int] NULL,
	[Area] [float] NULL,
	[Price] [float] NULL,
	[Unit] [int] NULL,
	[Address] [nvarchar](200) NULL,
	[HouseDirection] [int] NULL,
	[BalconyDirection] [int] NULL,
	[NumOfFloor] [int] NULL,
	[NumOfBedroom] [int] NULL,
	[NumOfWcs] [int] NULL,
	[ImageMain] [nvarchar](200) NULL,
	[ImageList] [nvarchar](max) NULL,
	[Summary] [nvarchar](max) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [nchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [nchar](50) NULL,
	[IsActive] [bit] NULL,
	[IsDelete] [bit] NULL,
	[DeletedDate] [datetime] NULL,
	[IsVIP] [bit] NULL,
 CONSTRAINT [PK_Products] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AZ_Projects]    Script Date: 2/24/2020 5:48:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AZ_Projects](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TypeId] [int] NULL,
	[Name] [nvarchar](200) NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[IsActive] [bit] NOT NULL,
	[IsDelete] [bit] NULL,
 CONSTRAINT [PK_AZ_Projects] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AZ_SendLand]    Script Date: 2/24/2020 5:48:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AZ_SendLand](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](250) NULL,
	[Time] [nvarchar](250) NULL,
	[Address] [nvarchar](500) NULL,
	[Area] [nvarchar](200) NULL,
	[Floor] [nvarchar](500) NULL,
	[Facade] [nvarchar](200) NULL,
	[Price] [nchar](100) NULL,
	[Email] [nchar](50) NULL,
	[Phone] [nchar](20) NULL,
	[BirthYear] [nchar](4) NULL,
	[IsRead] [bit] NULL,
	[ContentSent] [nvarchar](max) NULL,
	[IsConfirm] [bit] NULL,
	[ContentConfirm] [nvarchar](max) NULL,
	[ConfirmBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_AZ_SendLand] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AZ_Services]    Script Date: 2/24/2020 5:48:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AZ_Services](
	[Id] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](250) NULL,
	[Image] [varchar](250) NULL,
	[Title] [nvarchar](1000) NULL,
	[NumberView] [int] NULL,
	[Orders] [int] NULL,
	[Summary] [nvarchar](max) NULL,
	[Active] [bit] NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_AZ_Services] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Constant]    Script Date: 2/24/2020 5:48:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Constant](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NULL,
	[Value] [nvarchar](200) NULL,
 CONSTRAINT [PK_Constant] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Files]    Script Date: 2/24/2020 5:48:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Files](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](500) NULL,
	[Link] [nchar](500) NULL,
	[Active] [bit] NULL,
	[CreatedDate] [datetime] NULL,
	[Type] [int] NULL,
 CONSTRAINT [PK_Files] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderLink]    Script Date: 2/24/2020 5:48:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderLink](
	[Id] [uniqueidentifier] NOT NULL,
	[OrderId] [uniqueidentifier] NULL,
	[Link] [nchar](200) NULL,
	[Color] [nvarchar](100) NULL,
	[Size] [nchar](100) NULL,
	[Quantity] [int] NULL,
	[Price] [numeric](18, 0) NULL,
	[ImageLink] [nchar](200) NULL,
	[Note] [nvarchar](500) NULL,
 CONSTRAINT [PK_OrderLink] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Orders]    Script Date: 2/24/2020 5:48:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Orders](
	[Id] [uniqueidentifier] NOT NULL,
	[No] [nchar](20) NOT NULL,
	[Link] [nchar](200) NULL,
	[Name] [nvarchar](50) NULL,
	[Phone] [nchar](11) NULL,
	[Email] [nchar](100) NULL,
	[Address] [nvarchar](200) NULL,
	[Summary] [nvarchar](500) NULL,
	[CreatedDate] [datetime] NULL,
	[Status] [int] NULL,
	[Note] [nvarchar](max) NULL,
	[IsDelete] [bit] NULL,
 CONSTRAINT [PK_Orders] PRIMARY KEY CLUSTERED 
(
	[Id] ASC,
	[No] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderStatus]    Script Date: 2/24/2020 5:48:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderStatus](
	[Id] [int] NOT NULL,
	[Name] [nvarchar](100) NULL,
	[Active] [bit] NULL,
 CONSTRAINT [PK_OrderStatus] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Services]    Script Date: 2/24/2020 5:48:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Services](
	[Id] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](250) NULL,
	[Image] [varchar](250) NULL,
	[Title] [nvarchar](1000) NULL,
	[NumberView] [int] NULL,
	[Orders] [int] NULL,
	[Summary] [nvarchar](max) NULL,
	[Active] [bit] NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_Services] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[VN_City]    Script Date: 2/24/2020 5:48:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VN_City](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NULL,
	[Active] [bit] NULL,
 CONSTRAINT [PK_VN_City_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[VN_Districts]    Script Date: 2/24/2020 5:48:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VN_Districts](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NULL,
	[Active] [bit] NULL,
	[CityId] [int] NULL,
 CONSTRAINT [PK_VN_City] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[VN_Streets]    Script Date: 2/24/2020 5:48:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VN_Streets](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NULL,
	[Active] [bit] NULL,
	[DistrictId] [int] NULL,
 CONSTRAINT [PK_VN_Wards] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[VN_Wards]    Script Date: 2/24/2020 5:48:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VN_Wards](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NULL,
	[Active] [bit] NULL,
	[DistrictId] [int] NULL,
 CONSTRAINT [PK_District] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[__MigrationHistory] ([MigrationId], [ContextKey], [Model], [ProductVersion]) VALUES (N'201910290358084_InitialCreate', N'BoopApp.Models.ApplicationDbContext', 0x1F8B0800000000000400DD5C5B6FE3B6127E3FC0F90F829E7A0E522B97B38B6D60EF2275929EA09B0BD6D9A26F0B5AA21D61254A95A83441D15FD687FEA4FE850E254A166FBAD88AED140B2C2272F8CD70382487C3A1FFFAE3CFF187A730B01E7192FA1199D847A343DBC2C48D3C9F2C27764617DFBEB33FBCFFF7BFC6175EF864FD54D29D303A6849D289FD40697CEA38A9FB8043948E42DF4DA2345AD0911B850EF222E7F8F0F03BE7E8C8C100610396658D3F6584FA21CE3FE0731A1117C73443C175E4E120E5E55033CB51AD1B14E234462E9ED8DF47517C16C7A382D2B6CE021F8114331C2C6C0B1112514441C6D3CF299ED12422CB590C0528B87F8E31D02D5090622EFBE98ABC6B370E8F59379C55C312CACD521A853D018F4EB85E1CB9F95ADAB52BBD81E62E40C3F499F53AD7DEC4BEF2705EF4290A400132C3D3699030E2897D5DB1384BE31B4C4765C35101799900DCAF51F27554473CB03AB73BA8ECE87874C8FE1D58D32CA059822704673441C1817597CD03DFFD113FDF475F31999C1CCD1727EFDEBC45DEC9DBFFE19337F59E425F814E2880A2BB248A7102B2E145D57FDB72C4768EDCB06A566B5368056C09A6846D5DA3A78F982CE9034C96E377B675E93F61AF2CE1C6F599F83083A0114D32F8BCC98200CD035CD53B8D3CD9FF0D5C8FDFBC1D84EB0D7AF497F9D04BFC61E22430AF3EE120AF4D1FFCB8985EC2787FE164974914B26FD1BE8ADA2FB3284B5CD699C848728F9225A6A274636765BC9D4C9A410D6FD625EAFE9B369354356F2D29EBD03A33A164B1EDD950CAFBB27C3B5B1CEC3C3078B969318D34199CB8518DA4960716AF5F99CC51579321D0957FF20A7811223F186009ECC0053C8F859F84B8EA258C4A8011E92DF31D4A535801BCFFA3F4A14174F87300D167D8CD1230CC194561FCE2DCEE1E22826FB270CEEC7D7BBC061B9AFB5FA34BE4D228B920ACD5C6781F23F76B94D10BE29D238A3F53B704649FF77ED81D601071CE5C17A7E9251833F6A61138D625E015A127C7BDE1D8E2B46B17641A203FD4FB20D232FAA5245DF9217A0AC5173190E9FC9126513F464B9F7413B524358B5A50B48ACAC9FA8ACAC0BA49CA29CD82E604AD721654837978F9080DEFE2E5B0FBEFE36DB6799BD6829A1A67B042E21F30C1092C63DE1DA214276435025DD68D5D380BF9F031A62FBE37E59C7E42413634ABB56643BE080C3F1B72D8FD9F0DB99850FCE87BCC2BE970F0298901BE13BDFE4CD53EE724C9B63D1D846E6E9BF976D600D374394BD3C8F5F359A00979F18085283FF870567BF4A2E88D1C01818E81A1FB6CCB8312E89B2D1BD52D39C701A6D83A738B90E014A52EF254354287BC1E82953BAA46B055244414EEBF0A4FB0749CB046881D825298A93EA1EAB4F089EBC72868D592D4B2E316C6FA5EF1906BCE718C0963D8AA892ECCF5810F2640C5471A94360D8D9D9AC5351BA2C16B358D799B0BBB1A77251EB1159B6CF19D0D76C9FDB71731CC668D6DC1389B55D2450063106F1706CACF2A5D0D403EB8EC9B814A2726838172976A2B062A6A6C07062AAAE4D519687144ED3AFED27975DFCC533C286F7F5B6F54D70E6C53D0C79E9966E17B421B0A2D70A29AE7F99C55E227AA399C819CFC7C967257573611063EC3540CD9ACFC5DAD1FEA3483C846D404B832B416507EFDA7002913AA8770652CAF513AEE45F4802DE36E8DB07CED97606B36A062D7AF416B84E6CB52D9383B9D3EAA9E55D6A01879A7C3420D476310F2E22576BC83524C715955315D7CE13EDE70AD637C301A14D4E2B91A94547666702D95A6D9AE259D43D6C725DB484B92FB64D052D999C1B5C46DB45D491AA7A0875BB0918AC42D7CA0C956463AAADDA6AA1B3B4566142F183B8614AAF1358A639F2C6B2955BCC49A15F954D36F67FD938DC202C371534DCE51256DC58946095A62A9165883A4977E92D27344D11CB138CFD40B1532EDDE6A58FE4B96F5ED531DC4721F28A9D9DF450BE9D25ED867554784B7BF84DE85CC9BC943E89AB1D737B7587A1B0A50A289DA4FA3200B89D9B932B72EEEEEEAED8B121561EC48F22BCE93A229C5C515D5DE6950D40931C000557ECBFA83648630A9BAF43AEBCA3679A26694323055473105AB7636682607A6F340C97E61FF716A457899F9C49351EA00BCA827462D9F4101ABD5754715534EEA98624D774429AFA40E2955F590B29E3D220859AF580BCFA0513D45770E6ABE481D5DADED8EACC91CA9436BAAD7C0D6C82CD77547D52497D48135D5DDB1579926F202BAC73B96F1B4B2D696551C6637DBB30C182FB31A0EB3E5D5EEECEB40B5E29E58FC565E01E3E57B6949C613DD5A9654C42F36B324038679C5116EBAC505A7F17ADE8C295C5F0B8B7AD3F5BD19AF9FBDBEA85528873999A4E25E1DEAA4C3DB981FA4DA1FC92827AB82C4B64A35C286FE9C521C8E18C168F64B300D7CCC96EF92E01A117F81535AA46CD8C78747C7D25B9BFD79F7E2A4A917680EA2A6C72FE2986D21FB8A3CA2C47D40899A0BB1C1DB9015A81266BE221E7E9AD8BFE5AD4EF38805FB2B2F3EB0AED2CFC4FF25838AFB24C3D6EF6A6EE730B9F2CD07AB3D7DD9D05DAB573F7F299A1E58B709CC9853EB50D2E53A232CBE77E8254DD1740369D67E05F17A2794F0D4408B2A4D88F55F16CC7D3AC8AB8252CA6F42F4F49FBEA2695F0E6C84A8791D3014DE202A3465FFAF8365CCFCF7E093E699FFFD3AAB7F09B08E68C657003EE90F26BF01E8BE0C952D77B8D568CE43DB5892723DB7E6506F9450B9EBBD4949B5DE68A2ABE9D43DE03648995EC3325E59B6F160BBA326997830EC5D9AF68B6710EF4BD2F02A9D63B7B9C2DB4C0F6EB80AFA476505EF411E9B262F67F7B9BFDBB635530C77CF1328FB65F8EE99B1F16CADDDE7F16EDBD84C61DE3D37B65ED9BA7B666BBBDA3F776C699DB7D09DE7DEAA694486BB185D2CB82DB7B6089CC3097F1E8111141E65F124529FCCD59488DAC2704562666ACE2293192B1347E1AB5034B3EDD757BEE1377696D334B335E45E36F1E6EB7F236F4ED3CCDB90D1B88BAC606D4EA12E53BB651D6B4A7C7A4D59C0424F5A92CEDB7CD6C68BF5D794F43B885284D963B8237E3D39BE83A864C8A9D323A757BDEE85BDB3F6CB89B07FA7FE7205C17E47916057D8352B9A2BB288CACD5B92A824912234D798220FB6D4B384FA0BE452A86631E6FC4D771EB763371D73EC5D91DB8CC619852EE3701E08012FE60434F1CF13974599C7B771FEF324437401C4F4596CFE967C9FF98157C97DA9890919209877C123BA6C2C298BEC2E9F2BA49B887404E2EAAB9CA27B1CC60180A5B764861EF13AB281F97DC44BE43EAF22802690F68110D53E3EF7D1324161CA3156EDE1136CD80B9FDEFF0DD388C93140540000, N'6.1.0-30225')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'ff017997-dd2b-4012-9139-339e25bbcdfd', N'admin@gmail.com', 0, N'AA9J/fmpX9U2Fme4aUMKyjOfmswv+nSGzPuLLSusa5hqC5u4TxQ9PbBm329KqxgaEg==', N'b7b06c81-d719-43c3-b36e-2edec8544826', NULL, 0, 0, NULL, 0, 0, N'admin')
SET IDENTITY_INSERT [dbo].[AZ_Constant] ON 

INSERT [dbo].[AZ_Constant] ([Id], [Type], [Name], [ValueString], [ValueInt], [ParentId]) VALUES (1, N'Contact             ', N'Gmail', N'BannhaAZ@gmail.com', NULL, NULL)
INSERT [dbo].[AZ_Constant] ([Id], [Type], [Name], [ValueString], [ValueInt], [ParentId]) VALUES (2, N'Contact             ', N'Phone', N'0919.679.682 | Hotline:     0968.350.683', NULL, NULL)
INSERT [dbo].[AZ_Constant] ([Id], [Type], [Name], [ValueString], [ValueInt], [ParentId]) VALUES (3, N'Price               ', N'Price', N'3 45045', NULL, NULL)
INSERT [dbo].[AZ_Constant] ([Id], [Type], [Name], [ValueString], [ValueInt], [ParentId]) VALUES (4, N'Contact             ', N'Facebook', N'http://facebook.com', NULL, NULL)
INSERT [dbo].[AZ_Constant] ([Id], [Type], [Name], [ValueString], [ValueInt], [ParentId]) VALUES (5, N'TypeTransaction     ', N'Nhà đất bán', NULL, 1, NULL)
INSERT [dbo].[AZ_Constant] ([Id], [Type], [Name], [ValueString], [ValueInt], [ParentId]) VALUES (6, N'TypeTransaction     ', N'Nhà cho thuê', NULL, 2, NULL)
INSERT [dbo].[AZ_Constant] ([Id], [Type], [Name], [ValueString], [ValueInt], [ParentId]) VALUES (7, N'Unit                ', N'Triệu', N'', 2, 5)
INSERT [dbo].[AZ_Constant] ([Id], [Type], [Name], [ValueString], [ValueInt], [ParentId]) VALUES (8, N'Unit                ', N'Tỷ', N'', 3, 5)
INSERT [dbo].[AZ_Constant] ([Id], [Type], [Name], [ValueString], [ValueInt], [ParentId]) VALUES (9, N'Unit                ', N'Trăm nghìn/m2', N'', 4, 5)
INSERT [dbo].[AZ_Constant] ([Id], [Type], [Name], [ValueString], [ValueInt], [ParentId]) VALUES (10, N'Unit                ', N'Triệu/m2', N'', 5, 5)
INSERT [dbo].[AZ_Constant] ([Id], [Type], [Name], [ValueString], [ValueInt], [ParentId]) VALUES (11, N'Unit                ', N'Thỏa thuận', N'', 1, 5)
INSERT [dbo].[AZ_Constant] ([Id], [Type], [Name], [ValueString], [ValueInt], [ParentId]) VALUES (12, N'Unit                ', N'Thỏa thuận', N'', 1, 6)
INSERT [dbo].[AZ_Constant] ([Id], [Type], [Name], [ValueString], [ValueInt], [ParentId]) VALUES (13, N'Unit                ', N'Trăm nghìn/tháng', N'', 2, 6)
INSERT [dbo].[AZ_Constant] ([Id], [Type], [Name], [ValueString], [ValueInt], [ParentId]) VALUES (14, N'Unit                ', N'Triệu/tháng', N'', 3, 6)
INSERT [dbo].[AZ_Constant] ([Id], [Type], [Name], [ValueString], [ValueInt], [ParentId]) VALUES (15, N'Unit                ', N'Trăm nghìn/m2/tháng', N'', 4, 6)
INSERT [dbo].[AZ_Constant] ([Id], [Type], [Name], [ValueString], [ValueInt], [ParentId]) VALUES (16, N'Unit                ', N'Triệu/m2/tháng', N'', 5, 6)
INSERT [dbo].[AZ_Constant] ([Id], [Type], [Name], [ValueString], [ValueInt], [ParentId]) VALUES (17, N'Unit                ', N'Nghìn/m2/tháng', N'', 6, 6)
INSERT [dbo].[AZ_Constant] ([Id], [Type], [Name], [ValueString], [ValueInt], [ParentId]) VALUES (18, N'Direction           ', N'KXĐ', NULL, 1, NULL)
INSERT [dbo].[AZ_Constant] ([Id], [Type], [Name], [ValueString], [ValueInt], [ParentId]) VALUES (19, N'Direction           ', N'Đông', NULL, 2, NULL)
INSERT [dbo].[AZ_Constant] ([Id], [Type], [Name], [ValueString], [ValueInt], [ParentId]) VALUES (20, N'Direction           ', N'Tây', NULL, 3, NULL)
INSERT [dbo].[AZ_Constant] ([Id], [Type], [Name], [ValueString], [ValueInt], [ParentId]) VALUES (21, N'Direction           ', N'Nam', NULL, 4, NULL)
INSERT [dbo].[AZ_Constant] ([Id], [Type], [Name], [ValueString], [ValueInt], [ParentId]) VALUES (22, N'Direction           ', N'Bắc', NULL, 5, NULL)
INSERT [dbo].[AZ_Constant] ([Id], [Type], [Name], [ValueString], [ValueInt], [ParentId]) VALUES (23, N'Direction           ', N'Đông-Bắc', NULL, 6, NULL)
INSERT [dbo].[AZ_Constant] ([Id], [Type], [Name], [ValueString], [ValueInt], [ParentId]) VALUES (24, N'Direction           ', N'Tây-Bắc', NULL, 7, NULL)
INSERT [dbo].[AZ_Constant] ([Id], [Type], [Name], [ValueString], [ValueInt], [ParentId]) VALUES (25, N'Direction           ', N'Tây-Nam', NULL, 8, NULL)
INSERT [dbo].[AZ_Constant] ([Id], [Type], [Name], [ValueString], [ValueInt], [ParentId]) VALUES (26, N'Direction           ', N'Đông-Nam', NULL, 9, NULL)
INSERT [dbo].[AZ_Constant] ([Id], [Type], [Name], [ValueString], [ValueInt], [ParentId]) VALUES (27, N'TypeProperty        ', N'Bán căn hộ chung cư', NULL, 1, 5)
INSERT [dbo].[AZ_Constant] ([Id], [Type], [Name], [ValueString], [ValueInt], [ParentId]) VALUES (28, N'TypeProperty        ', N'Bán nhà riêng', NULL, 2, 5)
INSERT [dbo].[AZ_Constant] ([Id], [Type], [Name], [ValueString], [ValueInt], [ParentId]) VALUES (29, N'TypeProperty        ', N'Bán nhà biệt thự, liền kề', NULL, 3, 5)
INSERT [dbo].[AZ_Constant] ([Id], [Type], [Name], [ValueString], [ValueInt], [ParentId]) VALUES (30, N'TypeProperty        ', N'Bán nhà mặt phố', NULL, 4, 5)
INSERT [dbo].[AZ_Constant] ([Id], [Type], [Name], [ValueString], [ValueInt], [ParentId]) VALUES (31, N'TypeProperty        ', N'Bán đất nền dự án', NULL, 5, 5)
INSERT [dbo].[AZ_Constant] ([Id], [Type], [Name], [ValueString], [ValueInt], [ParentId]) VALUES (32, N'TypeProperty        ', N'Bán đất', NULL, 6, 5)
INSERT [dbo].[AZ_Constant] ([Id], [Type], [Name], [ValueString], [ValueInt], [ParentId]) VALUES (33, N'TypeProperty        ', N'Bán trang trại, khu nghỉ dưỡng', NULL, 7, 5)
INSERT [dbo].[AZ_Constant] ([Id], [Type], [Name], [ValueString], [ValueInt], [ParentId]) VALUES (34, N'TypeProperty        ', N'Bán kho, nhà xưởng', NULL, 8, 5)
INSERT [dbo].[AZ_Constant] ([Id], [Type], [Name], [ValueString], [ValueInt], [ParentId]) VALUES (35, N'TypeProperty        ', N'Bán loại bất động sản khác', NULL, 9, 5)
INSERT [dbo].[AZ_Constant] ([Id], [Type], [Name], [ValueString], [ValueInt], [ParentId]) VALUES (36, N'TypeProperty        ', N'Cho thuê căn hộ chung cư', NULL, 1, 6)
INSERT [dbo].[AZ_Constant] ([Id], [Type], [Name], [ValueString], [ValueInt], [ParentId]) VALUES (37, N'TypeProperty        ', N'Cho thuê nhà riêng', NULL, 2, 6)
INSERT [dbo].[AZ_Constant] ([Id], [Type], [Name], [ValueString], [ValueInt], [ParentId]) VALUES (38, N'TypeProperty        ', N'Cho thuê nhà mặt phố', NULL, 3, 6)
INSERT [dbo].[AZ_Constant] ([Id], [Type], [Name], [ValueString], [ValueInt], [ParentId]) VALUES (39, N'TypeProperty        ', N'Cho thuê nhà trọ, phòng trọ', N'', 4, 6)
INSERT [dbo].[AZ_Constant] ([Id], [Type], [Name], [ValueString], [ValueInt], [ParentId]) VALUES (40, N'TypeProperty        ', N'Cho thuê văn phòng', N'', 5, 6)
INSERT [dbo].[AZ_Constant] ([Id], [Type], [Name], [ValueString], [ValueInt], [ParentId]) VALUES (41, N'TypeProperty        ', N'Cho thuê cửa hàng, ki ốt', N'', 6, 6)
INSERT [dbo].[AZ_Constant] ([Id], [Type], [Name], [ValueString], [ValueInt], [ParentId]) VALUES (42, N'TypeProperty        ', N'Cho thuê kho, nhà xưởng, đất', N'', 7, 6)
INSERT [dbo].[AZ_Constant] ([Id], [Type], [Name], [ValueString], [ValueInt], [ParentId]) VALUES (43, N'TypeProperty        ', N'Cho thuê loại bất động sản khác', N'', 8, 6)
INSERT [dbo].[AZ_Constant] ([Id], [Type], [Name], [ValueString], [ValueInt], [ParentId]) VALUES (44, N'PriceTypeTransaction', N'Dưới 2 tỷ', N'', 1, 5)
INSERT [dbo].[AZ_Constant] ([Id], [Type], [Name], [ValueString], [ValueInt], [ParentId]) VALUES (45, N'PriceTypeTransaction', N'2 - 3 tỷ', N'', 2, 5)
INSERT [dbo].[AZ_Constant] ([Id], [Type], [Name], [ValueString], [ValueInt], [ParentId]) VALUES (46, N'PriceTypeTransaction', N'3 - 5 tỷ', N'', 3, 5)
INSERT [dbo].[AZ_Constant] ([Id], [Type], [Name], [ValueString], [ValueInt], [ParentId]) VALUES (47, N'PriceTypeTransaction', N'5 - 7 tỷ', N'', 4, 5)
INSERT [dbo].[AZ_Constant] ([Id], [Type], [Name], [ValueString], [ValueInt], [ParentId]) VALUES (48, N'PriceTypeTransaction', N'7 - 10 tỷ', N'', 5, 5)
INSERT [dbo].[AZ_Constant] ([Id], [Type], [Name], [ValueString], [ValueInt], [ParentId]) VALUES (49, N'PriceTypeTransaction', N'Trên 10 tỷ', N'', 6, 5)
INSERT [dbo].[AZ_Constant] ([Id], [Type], [Name], [ValueString], [ValueInt], [ParentId]) VALUES (50, N'PriceTypeTransaction', N'Dưới 3 triệu', N'', 1, 6)
INSERT [dbo].[AZ_Constant] ([Id], [Type], [Name], [ValueString], [ValueInt], [ParentId]) VALUES (51, N'PriceTypeTransaction', N'3 - 5 triệu/tháng', N'', 2, 6)
INSERT [dbo].[AZ_Constant] ([Id], [Type], [Name], [ValueString], [ValueInt], [ParentId]) VALUES (52, N'PriceTypeTransaction', N'5 - 7 triệu/tháng', N'', 3, 6)
INSERT [dbo].[AZ_Constant] ([Id], [Type], [Name], [ValueString], [ValueInt], [ParentId]) VALUES (53, N'PriceTypeTransaction', N'7 - 10 triệu/tháng', N'', 4, 6)
INSERT [dbo].[AZ_Constant] ([Id], [Type], [Name], [ValueString], [ValueInt], [ParentId]) VALUES (54, N'PriceTypeTransaction', N'10 - 15 triệu/tháng', N'', 5, 6)
INSERT [dbo].[AZ_Constant] ([Id], [Type], [Name], [ValueString], [ValueInt], [ParentId]) VALUES (55, N'PriceTypeTransaction', N'15 - 30 triệu/tháng', N'', 6, 6)
INSERT [dbo].[AZ_Constant] ([Id], [Type], [Name], [ValueString], [ValueInt], [ParentId]) VALUES (56, N'PriceTypeTransaction', N'30 - 50 triệu/tháng', N'', 7, 6)
INSERT [dbo].[AZ_Constant] ([Id], [Type], [Name], [ValueString], [ValueInt], [ParentId]) VALUES (57, N'PriceTypeTransaction', N'50 - 70 triệu/tháng', N'', 8, 6)
INSERT [dbo].[AZ_Constant] ([Id], [Type], [Name], [ValueString], [ValueInt], [ParentId]) VALUES (58, N'TypeTransaction     ', N'Sang nhượng', N'', 3, NULL)
INSERT [dbo].[AZ_Constant] ([Id], [Type], [Name], [ValueString], [ValueInt], [ParentId]) VALUES (59, N'TypeProperty        ', N'Mặt bằng kinh doanh', N'', 1, 58)
INSERT [dbo].[AZ_Constant] ([Id], [Type], [Name], [ValueString], [ValueInt], [ParentId]) VALUES (60, N'TypeProperty        ', N'Tòa nhà văn phòng', N'', 2, 58)
INSERT [dbo].[AZ_Constant] ([Id], [Type], [Name], [ValueString], [ValueInt], [ParentId]) VALUES (61, N'TypeProperty        ', N'Văn phòng', N'', 3, 58)
INSERT [dbo].[AZ_Constant] ([Id], [Type], [Name], [ValueString], [ValueInt], [ParentId]) VALUES (62, N'TypeProperty        ', N'Nhà trọ', N'', 4, 58)
INSERT [dbo].[AZ_Constant] ([Id], [Type], [Name], [ValueString], [ValueInt], [ParentId]) VALUES (63, N'Contact             ', N'Address', N'Số 102 C10 Ngõ 140 Giảng Võ, Ba Đình, Hà Nội', NULL, NULL)
SET IDENTITY_INSERT [dbo].[AZ_Constant] OFF
SET IDENTITY_INSERT [dbo].[AZ_Contents] ON 

INSERT [dbo].[AZ_Contents] ([Id], [Name], [Content]) VALUES (1, N'GioiThieu', N'<p style="text-align: center;"><img alt="" src="/Files/1501041344_75474000_1419573388193824_8024994346438754304_o.jpg" style="width: 500px; height: 334px;" /></p>

<p style="margin-bottom:10px"><span style="font-size:11pt"><span style="background:white"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;"><span style="color:#3b3b3b">Với kinh nghiệm trên 5 năm trong ngành “ Bất động sản Thổ Cư”, nắm trong tay thông tin tốt hàng ngàn ngôi nhà, phân loại theo chủng loại, khu vực, tài chính, hướng nhà, luôn được cập nhật thường xuyên. Cộng với sự am hiểu về pháp lý, luật Đất đai, đảm bảo UY TÍN với khách hàng.Tôi cam kết và đem lại những giá trị trên cả mong đợi cho khách hàng.</span></span></span></span></span></span></span></p>

<p style="margin-bottom:10px"><span style="font-size:11pt"><span style="background:white"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif"><b><span style="font-size:10.5pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;"><span style="color:#3b3b3b">9 CÁI ĐƯỢC CỦA NGƯỜI MUA NHÀ:</span></span></span></b></span></span></span></span></p>

<ol>
	<li style="margin-bottom:13px"><span style="font-size:11pt"><span style="background:white"><span style="color:#3b3b3b"><span style="line-height:normal"><span style="tab-stops:list 36.0pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Được tư vấn những căn nhà có Pháp lý rõ ràng, sổ đỏ chính chủ;</span></span></span></span></span></span></span></span></li>
	<li style="margin-bottom:13px"><span style="font-size:11pt"><span style="background:white"><span style="color:#3b3b3b"><span style="line-height:normal"><span style="tab-stops:list 36.0pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Được dẫn đi xem nhà Miễn Phí;</span></span></span></span></span></span></span></span></li>
	<li style="margin-bottom:13px"><span style="font-size:11pt"><span style="background:white"><span style="color:#3b3b3b"><span style="line-height:normal"><span style="tab-stops:list 36.0pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Được tư vấn những căn nhà phù hợp nhất với khách hàng căn cứ theo: Tài chính, Phong thủy, vị trí…</span></span></span></span></span></span></span></span></li>
	<li style="margin-bottom:13px"><span style="font-size:11pt"><span style="background:white"><span style="color:#3b3b3b"><span style="line-height:normal"><span style="tab-stops:list 36.0pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Được thương thảo giá trực tiếp với chủ nhà, không bị kênh giá;</span></span></span></span></span></span></span></span></li>
	<li style="margin-bottom:13px"><span style="font-size:11pt"><span style="background:white"><span style="color:#3b3b3b"><span style="line-height:normal"><span style="tab-stops:list 36.0pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Được tư vấn các loại thuế, phí liên quan đến chuyển nhượng nhà đất;</span></span></span></span></span></span></span></span></li>
	<li style="margin-bottom:13px"><span style="font-size:11pt"><span style="background:white"><span style="color:#3b3b3b"><span style="line-height:normal"><span style="tab-stops:list 36.0pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Được mua ngôi nhà đúng hoặc rẻ hơn với giá trị thực của thị trường;</span></span></span></span></span></span></span></span></li>
	<li style="margin-bottom:13px"><span style="font-size:11pt"><span style="background:white"><span style="color:#3b3b3b"><span style="line-height:normal"><span style="tab-stops:list 36.0pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Được miễn phí tư vấn thủ tục công chứng, sang tên sổ đỏ;</span></span></span></span></span></span></span></span></li>
	<li style="margin-bottom:13px"><span style="font-size:11pt"><span style="background:white"><span style="color:#3b3b3b"><span style="line-height:normal"><span style="tab-stops:list 36.0pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Được miễn phí tư vấn vay vốn ngân hàng;</span></span></span></span></span></span></span></span></li>
	<li style="margin-bottom:13px"><span style="font-size:11pt"><span style="background:white"><span style="color:#3b3b3b"><span style="line-height:normal"><span style="tab-stops:list 36.0pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Được miễn phí tư vấn cải tạo, sửa chữa nhà, làm nội thất;</span></span></span></span></span></span></span></span></li>
</ol>

<p style="margin-bottom:10px"><span style="font-size:11pt"><span style="background:white"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif"><b><span style="font-size:10.5pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;"><span style="color:#3b3b3b">7 CÁI ĐƯỢC CỦA NGƯỜI BÁN NHÀ:</span></span></span></b></span></span></span></span></p>

<ol>
	<li style="margin-bottom:10px; margin-left:8px"><span style="font-size:11pt"><span style="background:white"><span style="line-height:normal"><span style="tab-stops:list 36.0pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;"><span style="color:#3b3b3b">Được tư vấn giá bán theo thị trường, giá chào bán cho người cần mua;</span></span></span></span></span></span></span></span></li>
	<li style="margin-bottom:10px; margin-left:8px"><span style="font-size:11pt"><span style="background:white"><span style="line-height:normal"><span style="tab-stops:list 36.0pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;"><span style="color:#3b3b3b">Được tư vấn pháp lý liên quan tới chuyển nhượng nhà đất;</span></span></span></span></span></span></span></span></li>
	<li style="margin-bottom:10px; margin-left:8px"><span style="font-size:11pt"><span style="background:white"><span style="line-height:normal"><span style="tab-stops:list 36.0pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;"><span style="color:#3b3b3b">Được tư vấn trình tự, mức thuế phí liên quan tới chuyển nhượng nhà đất</span></span></span></span></span></span></span></span></li>
	<li style="margin-bottom:10px; margin-left:8px"><span style="font-size:11pt"><span style="background:white"><span style="line-height:normal"><span style="tab-stops:list 36.0pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;"><span style="color:#3b3b3b">Được trao đổi giá bán trực tiếp với người mua nhà;</span></span></span></span></span></span></span></span></li>
	<li style="margin-bottom:10px; margin-left:8px"><span style="font-size:11pt"><span style="background:white"><span style="line-height:normal"><span style="tab-stops:list 36.0pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;"><span style="color:#3b3b3b">Được quảng cáo ngôi nhà của mình trên rất nhiều phương tiện, kênh quảng cáo;</span></span></span></span></span></span></span></span></li>
	<li style="margin-bottom:10px; margin-left:8px"><span style="font-size:11pt"><span style="background:white"><span style="line-height:normal"><span style="tab-stops:list 36.0pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;"><span style="color:#3b3b3b">Được 01 đội ngũ bán hàng hùng hậu để giới thiệu khách hàng;</span></span></span></span></span></span></span></span></li>
	<li style="margin-bottom:10px; margin-left:8px"><span style="font-size:11pt"><span style="background:white"><span style="line-height:normal"><span style="tab-stops:list 36.0pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;"><span style="color:#3b3b3b">Được giới thiệu những khách hàng có như cầu, và tiêu chí mua nhà sát với ngôi nhà cần bán.</span></span></span></span></span></span></span></span></li>
</ol>

<p style="margin-bottom:10px"><span style="font-size:11pt"><span style="background:white"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;"><span style="color:#3b3b3b">Với kỹ năng và kinh nghiệm lâu năm của mình, tôi tự tin có thể giải quyết cho khách hàng trong mọi tình huống, mọi trường hợp, từ dễ đến khó. Là Cầu nối UY TÍN giữa người Bán và người Mua.</span></span></span></span></span></span></span></p>

<h3 style="color: rgb(170, 170, 170); font-style: italic; text-align: center;"><big><span style="color:#27ae60;"><strong>CÒN CHẦN CHỪ GÌ NỮA</strong></span></big></h3>

<h3 style="font-style: italic; text-align: center;"><font color="#27ae60"><span style="font-size: 18.252px;"><b>LIÊN HỆ NGAY HOTLINE </b></span></font></h3>

<h3 style="font-style: italic; text-align: center;"><span style="font-size:48px;"><span style="color:#e74c3c;"><b>0919 679 682&nbsp;</b></span></span></h3>

<h3 style="color: rgb(170, 170, 170); font-style: italic; text-align: center;"><big><span style="color:#27ae60;"><strong>ĐỂ ĐƯỢC TƯ VẤN MIỄN PHÍ</strong></span></big></h3>

<p style="text-align: center;">&nbsp;</p>

<p style="text-align: center;">&nbsp;</p>
')
SET IDENTITY_INSERT [dbo].[AZ_Contents] OFF
SET IDENTITY_INSERT [dbo].[AZ_Files] ON 

INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (23, 0, N'richie_hawtin_in_the_cube_at_omnia_-_photo_credit_jacob_riglin.jpg', N'/Files/01274a12-b94b-4e01-9757-494b65309678.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                     ', NULL, 0, CAST(N'2019-12-05T10:11:02.957' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (24, 1, N'swing.jpg', N'/Files/0512102811_swing.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         ', NULL, 0, CAST(N'2019-12-05T10:28:11.293' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (25, 1, N'richie_hawtin_in_the_cube_at_omnia_-_photo_credit_jacob_riglin.jpg', N'/Files/0512104434_richie_hawtin_in_the_cube_at_omnia_-_photo_credit_jacob_riglin.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                ', NULL, 0, CAST(N'2019-12-05T10:44:34.733' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (26, 1, N'choi-gi-o-ubud-bali-vietnamembassy-indonesia.org4_.jpg', N'/Files/0512104438_choi-gi-o-ubud-bali-vietnamembassy-indonesia.org4_.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                            ', NULL, 0, CAST(N'2019-12-05T10:44:38.870' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (27, 2, N'richie_hawtin_in_the_cube_at_omnia_-_photo_credit_jacob_riglin.jpg', N'/Files/0512105051_richie_hawtin_in_the_cube_at_omnia_-_photo_credit_jacob_riglin.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                ', NULL, 0, CAST(N'2019-12-05T10:50:51.433' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (28, 1, N'choi-gi-o-ubud-bali-vietnamembassy-indonesia.org4_.jpg', N'/Files/0512105113_choi-gi-o-ubud-bali-vietnamembassy-indonesia.org4_.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                            ', NULL, 1, CAST(N'2019-12-05T10:51:13.737' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (29, 1, N'richie_hawtin_in_the_cube_at_omnia_-_photo_credit_jacob_riglin.jpg', N'/Files/0512105113_richie_hawtin_in_the_cube_at_omnia_-_photo_credit_jacob_riglin.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                ', NULL, 1, CAST(N'2019-12-05T10:51:13.737' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (30, 1, N'choi-gi-o-ubud-bali-vietnamembassy-indonesia.org5_.jpg', N'/Files/0512105113_choi-gi-o-ubud-bali-vietnamembassy-indonesia.org5_.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                            ', NULL, 1, CAST(N'2019-12-05T10:51:13.740' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (31, 3, N'swing.jpg', N'/Files/0512105113_swing.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         ', NULL, 0, CAST(N'2019-12-05T10:51:13.763' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (32, 1, N'20200106175559-a816_wm.jpg', N'/Files/0601061833_20200106175559-a816_wm.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                        ', NULL, 1, CAST(N'2020-01-06T18:18:33.707' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (33, 2, N'20200106175559-6d9b_wm.jpg', N'/Files/0601061833_20200106175559-6d9b_wm.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                        ', NULL, 1, CAST(N'2020-01-06T18:18:33.887' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (34, 4, N'20200106175559-df21_wm.jpg', N'/Files/0601061833_20200106175559-df21_wm.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                        ', NULL, 1, CAST(N'2020-01-06T18:18:33.887' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (35, 5, N'20200106175559-f7ca_wm.jpg', N'/Files/0601061833_20200106175559-f7ca_wm.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                        ', NULL, 1, CAST(N'2020-01-06T18:18:33.887' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (36, 1, N'20200106175559-c179_wm.jpg', N'/Files/0601061833_20200106175559-c179_wm.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                        ', NULL, 1, CAST(N'2020-01-06T18:18:33.887' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (37, 1, N'20200106175559-2975_wm.jpg', N'/Files/0601061833_20200106175559-2975_wm.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                        ', NULL, 1, CAST(N'2020-01-06T18:18:33.890' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (38, 1, N'20200106175600-6b86_wm.jpg', N'/Files/0601061834_20200106175600-6b86_wm.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                        ', NULL, 1, CAST(N'2020-01-06T18:18:34.417' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (39, 1, N'75474000_1419573388193824_8024994346438754304_o.jpg', N'/Files/1501041344_75474000_1419573388193824_8024994346438754304_o.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                               ', NULL, 1, CAST(N'2020-01-15T16:13:44.413' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (40, 1, N'71484870_2392519194179438_7037044008845049856_n.jpg', N'/Files/1601031442_71484870_2392519194179438_7037044008845049856_n.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                               ', NULL, 1, CAST(N'2020-01-16T15:14:42.140' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (41, NULL, N'Capture.PNG', N'/Files/2001050759_Capture.PNG                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       ', NULL, 1, CAST(N'2020-01-20T17:07:59.057' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (42, NULL, N'Capture.PNG', N'/Files/2001054504_Capture.PNG                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       ', NULL, 1, CAST(N'2020-01-20T17:45:04.903' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (43, 1, N'Capture.PNG', N'/Files/2001054539_Capture.PNG                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       ', NULL, 1, CAST(N'2020-01-20T17:45:39.800' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (44, 2, N'Capture.PNG', N'/Files/2001054551_Capture.PNG                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       ', NULL, 0, CAST(N'2020-01-20T17:45:51.020' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (45, 4, N'9320f3e36800915ec811.jpg', N'/Files/0402114911_9320f3e36800915ec811.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-04T23:49:11.007' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (46, 4, N'3fc8150a8ee977b72ef8.jpg', N'/Files/0402114910_3fc8150a8ee977b72ef8.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-04T23:49:10.913' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (47, 4, N'd3e93f35a4d65d8804c7.jpg', N'/Files/0402114911_d3e93f35a4d65d8804c7.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-04T23:49:11.103' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (48, 4, N'20191220102359-7529_wm.jpg', N'/Files/0402114910_20191220102359-7529_wm.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                        ', NULL, 1, CAST(N'2020-02-04T23:49:10.807' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (49, 4, N'74d14d10d6f32fad76e2.jpg', N'/Files/0402114910_74d14d10d6f32fad76e2.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-04T23:49:10.960' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (50, 5, N'3fde69fd77ad8ff3d6bc.jpg', N'/Files/0502120214_3fde69fd77ad8ff3d6bc.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T00:02:14.757' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (51, 5, N'65a4efb4f1e409ba50f5.jpg', N'/Files/0502120215_65a4efb4f1e409ba50f5.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T00:02:15.337' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (52, 5, N'496cac91b5c14d9f14d0.jpg', N'/Files/0502120215_496cac91b5c14d9f14d0.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T00:02:15.633' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (53, 5, N'5847d04fce1f36416f0e.jpg', N'/Files/0502120215_5847d04fce1f36416f0e.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T00:02:15.823' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (54, 5, N'1430d027ce7736296f66.jpg', N'/Files/0502120215_1430d027ce7736296f66.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T00:02:15.900' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (55, 5, N'4377785666069e58c717.jpg', N'/Files/0502120216_4377785666069e58c717.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T00:02:16.120' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (56, 5, N'ac2fffd5e6851edb4794.jpg', N'/Files/0502120216_ac2fffd5e6851edb4794.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T00:02:16.587' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (57, 5, N'34e790e78eb776e92fa6.jpg', N'/Files/0502120216_34e790e78eb776e92fa6.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T00:02:16.887' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (58, 5, N'c206110b0f5bf705ae4a.jpg', N'/Files/0502120217_c206110b0f5bf705ae4a.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T00:02:17.230' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (59, 5, N'a8c81b270277fa29a366.jpg', N'/Files/0502120217_a8c81b270277fa29a366.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 0, CAST(N'2020-02-05T00:02:17.450' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (60, 5, N'ba5fdebec7ee3fb066ff.jpg', N'/Files/0502120217_ba5fdebec7ee3fb066ff.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T00:02:17.733' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (61, 5, N'd30d86229872602c3963.jpg', N'/Files/0502120217_d30d86229872602c3963.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T00:02:17.843' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (62, 5, N'f9bffea4e0f418aa41e5.jpg', N'/Files/0502120218_f9bffea4e0f418aa41e5.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T00:02:18.030' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (63, 5, N'eb3db338ad6855360c79.jpg', N'/Files/0502120218_eb3db338ad6855360c79.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T00:02:18.093' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (64, 5, N'c26d02731c23e47dbd32.jpg', N'/Files/0502120218_c26d02731c23e47dbd32.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T00:02:18.390' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (65, 5, N'e046e352fd02055c5c13.jpg', N'/Files/0502120219_e046e352fd02055c5c13.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T00:02:19.643' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (66, 5, N'496cac91b5c14d9f14d0.jpg', N'/Files/0502120546_496cac91b5c14d9f14d0.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T00:05:46.937' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (67, 5, N'f9bffea4e0f418aa41e5.jpg', N'/Files/0502120638_f9bffea4e0f418aa41e5.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T00:06:38.057' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (68, 5, N'4377785666069e58c717.jpg', N'/Files/0502120648_4377785666069e58c717.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T00:06:48.653' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (69, 5, N'1430d027ce7736296f66.jpg', N'/Files/0502120656_1430d027ce7736296f66.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T00:06:56.423' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (70, 2, N'4d2543f5a35c43021a4d.jpg', N'/Files/0502090022_4d2543f5a35c43021a4d.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T09:00:22.357' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (71, 2, N'9e6c22b3c21a22447b0b.jpg', N'/Files/0502090022_9e6c22b3c21a22447b0b.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T09:00:22.997' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (72, 2, N'30dfa87548dca882f1cd.jpg', N'/Files/0502090023_30dfa87548dca882f1cd.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T09:00:23.153' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (73, 2, N'45e5a61246bba6e5ffaa.jpg', N'/Files/0502090023_45e5a61246bba6e5ffaa.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T09:00:23.263' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (74, 2, N'7a5c32b6d21f32416b0e.jpg', N'/Files/0502090023_7a5c32b6d21f32416b0e.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T09:00:23.670' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (75, 2, N'92f6c31823b1c3ef9aa0.jpg', N'/Files/0502090024_92f6c31823b1c3ef9aa0.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T09:00:24.457' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (76, 2, N'4062efc40f6def33b67c.jpg', N'/Files/0502090026_4062efc40f6def33b67c.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T09:00:26.007' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (77, 2, N'0642b7d15778b726ee69.jpg', N'/Files/0502090026_0642b7d15778b726ee69.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T09:00:26.160' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (78, 2, N'dbe9560eb6a756f90fb6.jpg', N'/Files/0502090026_dbe9560eb6a756f90fb6.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T09:00:26.367' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (79, 2, N'aa9b8628668186dfdf90.jpg', N'/Files/0502090026_aa9b8628668186dfdf90.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T09:00:26.787' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (80, 2, N'e25a878f67268778de37.jpg', N'/Files/0502090027_e25a878f67268778de37.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T09:00:27.430' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (81, 2, N'eaf3382bd88238dc6193.jpg', N'/Files/0502090028_eaf3382bd88238dc6193.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T09:00:28.760' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (82, 2, N'1e62f4a65794aecaf785.jpg', N'/Files/0502090647_1e62f4a65794aecaf785.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T09:06:47.307' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (83, 2, N'9b084562f4a70df954b6.jpg', N'/Files/0502090708_9b084562f4a70df954b6.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T09:07:08.573' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (84, 2, N'1ee525e486d67f8826c7.jpg', N'/Files/0502090708_1ee525e486d67f8826c7.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T09:07:08.683' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (85, 2, N'35a0fc4f5f7da623ff6c.jpg', N'/Files/0502090708_35a0fc4f5f7da623ff6c.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T09:07:08.777' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (86, 2, N'79780a10bbd5428b1bc4.jpg', N'/Files/0502090708_79780a10bbd5428b1bc4.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T09:07:08.857' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (87, 2, N'b15d780f5ad8a386fac9.jpg', N'/Files/0502090708_b15d780f5ad8a386fac9.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T09:07:08.870' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (88, 2, N'7e7c89882abad3e48aab.jpg', N'/Files/0502090709_7e7c89882abad3e48aab.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T09:07:09.107' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (89, 2, N'f3f15c17ff25067b5f34.jpg', N'/Files/0502090709_f3f15c17ff25067b5f34.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T09:07:09.547' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (90, 2, N'312efb075835a16bf824.jpg', N'/Files/0502090709_312efb075835a16bf824.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T09:07:09.623' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (91, 2, N'3a74076da45f5d01044e.jpg', N'/Files/0502090709_3a74076da45f5d01044e.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T09:07:09.810' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (92, 2, N'2e4b6f5bcc6935376c78.jpg', N'/Files/0502090709_2e4b6f5bcc6935376c78.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T09:07:09.937' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (93, 2, N'7e7c89882abad3e48aab.jpg', N'/Files/0502091113_7e7c89882abad3e48aab.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T09:11:13.340' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (94, 1, N'3483e6fca9fb4aa513ea.jpg', N'/Files/0502123127_3483e6fca9fb4aa513ea.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T12:31:27.143' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (95, 1, N'4d5f51201e27fd79a436.jpg', N'/Files/0502123127_4d5f51201e27fd79a436.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T12:31:27.017' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (96, 1, N'8a2c935ddc5a3f04664b.jpg', N'/Files/0502123126_8a2c935ddc5a3f04664b.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T12:31:26.957' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (97, 1, N'639898ead7ed34b36dfc.jpg', N'/Files/0502123127_639898ead7ed34b36dfc.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T12:31:27.407' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (98, 1, N'02162668696f8a31d37e.jpg', N'/Files/0502123130_02162668696f8a31d37e.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T12:31:30.057' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (99, 1, N'b82745560a51e90fb040.jpg', N'/Files/0502123131_b82745560a51e90fb040.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T12:31:31.213' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (100, 1, N'9e766f042003c35d9a12.jpg', N'/Files/0502123132_9e766f042003c35d9a12.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T12:31:32.390' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (101, 1, N'2bf8c2868d816edf3790.jpg', N'/Files/0502123133_2bf8c2868d816edf3790.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T12:31:33.157' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (102, 1, N'd124ed59a25e4100184f.jpg', N'/Files/0502123136_d124ed59a25e4100184f.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T12:31:36.530' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (103, 1, N'f4c271bc3ebbdde584aa.jpg', N'/Files/0502123138_f4c271bc3ebbdde584aa.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T12:31:38.237' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (104, 1, N'38e5951fff5906075f48.jpg', N'/Files/0502010249_38e5951fff5906075f48.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T13:02:49.263' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (105, 1, N'3f141ee974af8df1d4be.jpg', N'/Files/0502010249_3f141ee974af8df1d4be.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T13:02:49.263' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (106, 1, N'50a3715d1b1be245bb0a.jpg', N'/Files/0502010249_50a3715d1b1be245bb0a.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T13:02:49.560' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (107, 1, N'54d19d2ef7680e365779.jpg', N'/Files/0502010249_54d19d2ef7680e365779.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T13:02:49.560' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (108, 1, N'3c70d93db37b4a25136a.jpg', N'/Files/0502010249_3c70d93db37b4a25136a.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T13:02:49.923' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (109, 1, N'0f0e1249780f8151d81e.jpg', N'/Files/0502010250_0f0e1249780f8151d81e.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T13:02:50.563' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (110, 1, N'84bd3e005746ae18f757.jpg', N'/Files/0502010251_84bd3e005746ae18f757.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T13:02:51.330' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (111, 1, N'798a6d340772fe2ca763.jpg', N'/Files/0502010251_798a6d340772fe2ca763.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T13:02:51.647' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (112, 1, N'b2ac40002a46d3188a57.jpg', N'/Files/0502010251_b2ac40002a46d3188a57.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T13:02:51.680' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (113, 1, N'6c0f719d18dbe185b8ca.jpg', N'/Files/0502010252_6c0f719d18dbe185b8ca.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T13:02:52.273' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (114, 1, N'b2ddb24fd80921577818.jpg', N'/Files/0502010252_b2ddb24fd80921577818.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T13:02:52.397' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (115, 1, N'8ea07b29126feb31b27e.jpg', N'/Files/0502010252_8ea07b29126feb31b27e.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T13:02:52.477' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (116, 1, N'bc8e151e7e588706de49.jpg', N'/Files/0502010252_bc8e151e7e588706de49.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T13:02:52.650' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (117, 1, N'c1bfb0dfda9923c77a88.jpg', N'/Files/0502010253_c1bfb0dfda9923c77a88.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 0, CAST(N'2020-02-05T13:02:53.463' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (118, 1, N'acfbeb258163783d2172.jpg', N'/Files/0502010253_acfbeb258163783d2172.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T13:02:53.510' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (119, 1, N'95558cd7e6911fcf4680.jpg', N'/Files/0502010253_95558cd7e6911fcf4680.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T13:02:53.670' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (120, 1, N'ddef4a002e46d7188e57.jpg', N'/Files/0502010253_ddef4a002e46d7188e57.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T13:02:53.717' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (121, 1, N'feb1eb4e810878562119.jpg', N'/Files/0502010253_feb1eb4e810878562119.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T13:02:53.827' AS DateTime), 1)
GO
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (122, 1, N'ec7dc9d0a2965bc80287.jpg', N'/Files/0502010254_ec7dc9d0a2965bc80287.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T13:02:54.013' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (123, 1, N'e9632ad44192b8cce183.jpg', N'/Files/0502010254_e9632ad44192b8cce183.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T13:02:54.153' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (124, 1, N'f011e96f83297a772338.jpg', N'/Files/0502010255_f011e96f83297a772338.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T13:02:55.407' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (125, 1, N'ff29e1538815714b2804.jpg', N'/Files/0502010257_ff29e1538815714b2804.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-05T13:02:57.100' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (126, 1, N'25765ca69c7e64203d6f.jpg', N'/Files/0802094642_25765ca69c7e64203d6f.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T09:46:42.280' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (127, 1, N'46d6d79275508d0ed441.jpg', N'/Files/0802095632_46d6d79275508d0ed441.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T09:56:32.587' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (128, 1, N'4c454002e2c01a9e43d1.jpg', N'/Files/0802095632_4c454002e2c01a9e43d1.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T09:56:32.627' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (129, 1, N'b96e372995eb6db534fa.jpg', N'/Files/0802095632_b96e372995eb6db534fa.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T09:56:32.650' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (130, 1, N'4ed4c59267509f0ec641.jpg', N'/Files/0802095633_4ed4c59267509f0ec641.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T09:56:33.203' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (131, 1, N'431a685cca9e32c06b8f.jpg', N'/Files/0802095633_431a685cca9e32c06b8f.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T09:56:33.260' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (132, 1, N'f224a8dcea12124c4b03.jpg', N'/Files/0802100939_f224a8dcea12124c4b03.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T10:09:39.053' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (133, 1, N'f431afcced02155c4c13.jpg', N'/Files/0802100939_f431afcced02155c4c13.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T10:09:39.130' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (134, 1, N'e7cd203b62f59aabc3e4.jpg', N'/Files/0802100939_e7cd203b62f59aabc3e4.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T10:09:39.303' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (135, 1, N'e3effc45a48b5cd5059a.jpg', N'/Files/0802100939_e3effc45a48b5cd5059a.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T10:09:39.383' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (136, 1, N'c92f9ad5d81b2045790a.jpg', N'/Files/0802100939_c92f9ad5d81b2045790a.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T10:09:39.430' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (137, 1, N'f615d5e3972d6f73363c.jpg', N'/Files/0802100939_f615d5e3972d6f73363c.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T10:09:39.470' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (138, 1, N'2883ae5df6930ecd5782.jpg', N'/Files/0802100939_2883ae5df6930ecd5782.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T10:09:39.607' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (139, 1, N'1.jpg', N'/Files/0802102937_1.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             ', NULL, 1, CAST(N'2020-02-08T10:29:37.507' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (140, 1, N'5c066f4d3c9dc4c39d8c.jpg', N'/Files/0802103003_5c066f4d3c9dc4c39d8c.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T10:30:03.613' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (141, 1, N'4c589c39cfe937b76ef8.jpg', N'/Files/0802103003_4c589c39cfe937b76ef8.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T10:30:03.730' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (142, 1, N'1e9f3aba696a9134c87b.jpg', N'/Files/0802103003_1e9f3aba696a9134c87b.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T10:30:03.950' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (143, 1, N'5bdcd08d835d7b03224c.jpg', N'/Files/0802103004_5bdcd08d835d7b03224c.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T10:30:04.217' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (144, 1, N'7ff22e797ea986f7dfb8.jpg', N'/Files/0802103004_7ff22e797ea986f7dfb8.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T10:30:04.340' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (145, 1, N'9cc0c9a69a7662283b67.jpg', N'/Files/0802103004_9cc0c9a69a7662283b67.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T10:30:04.470' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (146, 1, N'4714ca5a998a61d4389b.jpg', N'/Files/0802103005_4714ca5a998a61d4389b.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T10:30:05.373' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (147, 1, N'12d20a455a95a2cbfb84.jpg', N'/Files/0802103005_12d20a455a95a2cbfb84.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T10:30:05.783' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (148, 1, N'81f17fc42c14d44a8d05.jpg', N'/Files/0802103006_81f17fc42c14d44a8d05.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T10:30:06.113' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (149, 1, N'fef675992649de178758.jpg', N'/Files/0802103007_fef675992649de178758.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T10:30:07.613' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (150, 1, N'aa102c7d7fad87f3debc.jpg', N'/Files/0802103007_aa102c7d7fad87f3debc.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T10:30:07.630' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (151, 1, N'd184b48968589006c949.jpg', N'/Files/0802104008_d184b48968589006c949.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T10:40:08.973' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (152, 1, N'cd7d775dab8c53d20a9d.jpg', N'/Files/0802110111_cd7d775dab8c53d20a9d.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T11:01:11.820' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (153, 1, N'83eb19b0a3665b380277.jpg', N'/Files/0802111129_83eb19b0a3665b380277.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T11:11:29.693' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (154, 1, N'ebb819efa3395b670228.jpg', N'/Files/0802111128_ebb819efa3395b670228.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T11:11:28.950' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (155, 1, N'9fa1cfec753a8d64d42b.jpg', N'/Files/0802111132_9fa1cfec753a8d64d42b.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T11:11:32.947' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (156, 1, N'4c50a63ef4ee0cb055ff.jpg', N'/Files/0802111915_4c50a63ef4ee0cb055ff.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T11:19:15.327' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (157, 1, N'01bfb3d1e101195f4010.jpg', N'/Files/0802111935_01bfb3d1e101195f4010.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T11:19:35.400' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (158, 1, N'8e7e7d102fc0d79e8ed1.jpg', N'/Files/0802111935_8e7e7d102fc0d79e8ed1.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T11:19:35.957' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (159, 1, N'06a92fc67d168548dc07.jpg', N'/Files/0802111936_06a92fc67d168548dc07.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T11:19:36.150' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (160, 1, N'2f5d9b33c9e331bd68f2.jpg', N'/Files/0802111936_2f5d9b33c9e331bd68f2.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T11:19:36.483' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (161, 1, N'8010b57fe7af1ff146be.jpg', N'/Files/0802111936_8010b57fe7af1ff146be.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T11:19:36.533' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (162, 1, N'0db910e34233ba6de322.jpg', N'/Files/0802111936_0db910e34233ba6de322.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T11:19:36.620' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (163, 1, N'017bd41586c57e9b27d4.jpg', N'/Files/0802111937_017bd41586c57e9b27d4.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T11:19:37.267' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (164, 1, N'e2526c373ee7c6b99ff6.jpg', N'/Files/0802111938_e2526c373ee7c6b99ff6.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T11:19:38.780' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (165, 1, N'438198efca3f32616b2e.jpg', N'/Files/0802111938_438198efca3f32616b2e.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T11:19:38.790' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (166, 1, N'160ea48df55d0d03544c.jpg', N'/Files/0802112356_160ea48df55d0d03544c.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T11:23:56.017' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (167, 1, N'9a6459e10831f06fa920.jpg', N'/Files/0802112357_9a6459e10831f06fa920.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T11:23:57.377' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (168, 1, N'364ae5ceb41e4c40150f.jpg', N'/Files/0802112358_364ae5ceb41e4c40150f.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T11:23:58.017' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (169, 1, N'5dcb954fc49f3cc1658e.jpg', N'/Files/0802112358_5dcb954fc49f3cc1658e.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T11:23:58.157' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (170, 1, N'65ccd28c825c7a02234d.jpg', N'/Files/0802112358_65ccd28c825c7a02234d.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T11:23:58.387' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (171, 1, N'6c6abd39ede915b74cf8.jpg', N'/Files/0802112358_6c6abd39ede915b74cf8.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T11:23:58.643' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (172, 1, N'04297eac2f7cd7228e6d.jpg', N'/Files/0802112358_04297eac2f7cd7228e6d.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T11:23:58.790' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (173, 1, N'5487b803e9d3118d48c2.jpg', N'/Files/0802112358_5487b803e9d3118d48c2.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T11:23:58.977' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (174, 1, N'e1520fd65e06a658ff17.jpg', N'/Files/0802112401_e1520fd65e06a658ff17.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T11:24:01.420' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (175, 1, N'afc19d44cc9434ca6d85.jpg', N'/Files/0802112401_afc19d44cc9434ca6d85.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T11:24:01.683' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (176, 1, N'b501566806b8fee6a7a9.jpg', N'/Files/0802112401_b501566806b8fee6a7a9.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T11:24:01.900' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (177, 1, N'01c84d4d1c9de4c3bd8c.jpg', N'/Files/0802112714_01c84d4d1c9de4c3bd8c.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T11:27:14.177' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (178, 1, N'afc19d44cc9434ca6d85.jpg', N'/Files/0802112714_afc19d44cc9434ca6d85.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T11:27:14.350' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (179, 1, N'4640cda19b71632f3a60.jpg', N'/Files/0802112927_4640cda19b71632f3a60.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T11:29:27.270' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (180, 1, N'7022d22584f57cab25e4.jpg', N'/Files/0802112927_7022d22584f57cab25e4.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T11:29:27.290' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (181, 1, N'5e840a835c53a40dfd42.jpg', N'/Files/0802112927_5e840a835c53a40dfd42.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T11:29:27.420' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (182, 1, N'3904a403f2d30a8d53c2.jpg', N'/Files/0802112927_3904a403f2d30a8d53c2.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T11:29:27.823' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (183, 1, N'25d552d20402fc5ca513.jpg', N'/Files/0802112928_25d552d20402fc5ca513.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T11:29:28.030' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (184, 1, N'25ce46c91019e847b108.jpg', N'/Files/0802112932_25ce46c91019e847b108.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T11:29:32.317' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (185, 1, N'1330553703e7fbb9a2f6.jpg', N'/Files/0802112932_1330553703e7fbb9a2f6.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T11:29:32.707' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (186, 1, N'fd9caa9bfc4b04155d5a.jpg', N'/Files/0802112933_fd9caa9bfc4b04155d5a.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T11:29:33.563' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (187, 1, N'a6d708d15e01a65fff10.jpg', N'/Files/0802112933_a6d708d15e01a65fff10.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T11:29:33.660' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (188, 1, N'cd54a5a8f3780b265269.jpg', N'/Files/0802112933_cd54a5a8f3780b265269.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T11:29:33.733' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (189, 1, N'd06a866dd0bd28e371ac.jpg', N'/Files/0802112933_d06a866dd0bd28e371ac.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T11:29:33.887' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (190, 1, N'59161be24d32b56cec23.jpg', N'/Files/0802112934_59161be24d32b56cec23.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T11:29:34.217' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (191, 1, N'39e7c02c11efe9b1b0fe.jpg', N'/Files/0802113340_39e7c02c11efe9b1b0fe.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T11:33:40.903' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (192, 1, N'19788cbe5d7da523fc6c.jpg', N'/Files/0802113340_19788cbe5d7da523fc6c.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T11:33:40.933' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (193, 1, N'5dc9bf096eca9694cfdb.jpg', N'/Files/0802113340_5dc9bf096eca9694cfdb.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 0, CAST(N'2020-02-08T11:33:40.940' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (194, 1, N'2ea0f56524a6dcf885b7.jpg', N'/Files/0802113340_2ea0f56524a6dcf885b7.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T11:33:40.933' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (195, 1, N'23581292c3513b0f6240.jpg', N'/Files/0802113340_23581292c3513b0f6240.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T11:33:40.957' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (196, 1, N'3749cd821c41e41fbd50.jpg', N'/Files/0802113340_3749cd821c41e41fbd50.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T11:33:40.957' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (197, 1, N'b2c2b80469c79199c8d6.jpg', N'/Files/0802113340_b2c2b80469c79199c8d6.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T11:33:40.970' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (198, 1, N'd15241ce960d6e53371c.jpg', N'/Files/0802113340_d15241ce960d6e53371c.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T11:33:40.997' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (199, 1, N'dbb5937c42bfbae1e3ae.jpg', N'/Files/0802113341_dbb5937c42bfbae1e3ae.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T11:33:41.003' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (200, 1, N'3d94e41733d4cb8a92c5.jpg', N'/Files/0802113341_3d94e41733d4cb8a92c5.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T11:33:41.037' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (201, 1, N'be46b8c76f04975ace15.jpg', N'/Files/0802113341_be46b8c76f04975ace15.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T11:33:41.147' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (202, 1, N'5dc9bf096eca9694cfdb.jpg', N'/Files/0802113438_5dc9bf096eca9694cfdb.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T11:34:38.390' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (203, 1, N'giay-to-de-lam-so-do_1909135627.jpg', N'/Files/0802124957_giay-to-de-lam-so-do_1909135627.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                               ', NULL, 1, CAST(N'2020-02-08T12:49:57.750' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (204, 1, N'Thuế.jpg', N'/Files/0802010239_Thuế.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 0, CAST(N'2020-02-08T13:02:39.923' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (205, 1, N'Thue.jpg', N'/Files/0802010503_Thue.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-08T13:05:03.770' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (206, 1, N'note-1-1-750x294.gif', N'/Files/0802011006_note-1-1-750x294.gif                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', NULL, 1, CAST(N'2020-02-08T13:10:06.547' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (207, 1, N'Thưa đất 230m2.png', N'/Files/0802023748_Thưa đất 230m2.png                                                                                                                                                                                                                                                                                                                                                                                                                                                                                ', NULL, 1, CAST(N'2020-02-08T14:37:48.160' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (208, 1, N'screenshot_1581054924.png', N'/Files/0802023748_screenshot_1581054924.png                                                                                                                                                                                                                                                                                                                                                                                                                                                                         ', NULL, 1, CAST(N'2020-02-08T14:37:48.160' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (209, 0, N'e4050d19f0fa08a451eb.jpg', N'/Files/1002112618_e4050d19f0fa08a451eb.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-10T23:26:18.970' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (210, 0, N'f5e9b2cb4f28b776ee39.jpg', N'/Files/1002112635_f5e9b2cb4f28b776ee39.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-10T23:26:35.387' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (211, 0, N'3bc1414abca944f71db8.jpg', N'/Files/1002112702_3bc1414abca944f71db8.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-10T23:27:02.930' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (212, 0, N'44823a68c78b3fd5669a.jpg', N'/Files/1002112702_44823a68c78b3fd5669a.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-10T23:27:02.930' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (213, 0, N'6a6071f98c1a74442d0b.jpg', N'/Files/1002112702_6a6071f98c1a74442d0b.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-10T23:27:02.977' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (214, 0, N'e702d5f8281bd045890a.jpg', N'/Files/1002112703_e702d5f8281bd045890a.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-10T23:27:03.087' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (215, 0, N'005897836a60923ecb71.jpg', N'/Files/1002112703_005897836a60923ecb71.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-10T23:27:03.117' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (216, 0, N'0993c81235f1cdaf94e0.jpg', N'/Files/1002112703_0993c81235f1cdaf94e0.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-10T23:27:03.463' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (217, 0, N'd3f8716f8c8c74d22d9d.jpg', N'/Files/1002112703_d3f8716f8c8c74d22d9d.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-10T23:27:03.493' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (218, 1, N'Đất 115m2.jpg', N'/Files/1102104625_Đất 115m2.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     ', NULL, 1, CAST(N'2020-02-11T10:46:25.087' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (219, 0, N'754a2a550efae8a4b1eb.jpg', N'/Files/1102114756_754a2a550efae8a4b1eb.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T11:47:56.797' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (220, 0, N'd5df2a8f0e20e87eb131.jpg', N'/Files/1102114806_d5df2a8f0e20e87eb131.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T11:48:06.513' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (221, 0, N'0801cf0eeba10dff54b0.jpg', N'/Files/1102114807_0801cf0eeba10dff54b0.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T11:48:07.020' AS DateTime), 1)
GO
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (222, 0, N'708afabdd112374c6e03.jpg', N'/Files/1102114807_708afabdd112374c6e03.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T11:48:07.253' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (223, 0, N'3ae031cf1560f33eaa71.jpg', N'/Files/1102114807_3ae031cf1560f33eaa71.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T11:48:07.293' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (224, 0, N'eacafc35d99a3fc4668b.jpg', N'/Files/1102114807_eacafc35d99a3fc4668b.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T11:48:07.327' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (225, 0, N'1c0c530a78a59efbc7b4.jpg', N'/Files/1102114807_1c0c530a78a59efbc7b4.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T11:48:07.457' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (226, 0, N'eae9c1f1ea5e0c00554f.jpg', N'/Files/1102114808_eae9c1f1ea5e0c00554f.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T11:48:08.720' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (227, 0, N'7046ef05cbaa2df474bb.jpg', N'/Files/1102114808_7046ef05cbaa2df474bb.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T11:48:08.767' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (228, 0, N'0cf761dffa31026f5b20.jpg', N'/Files/1102115121_0cf761dffa31026f5b20.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T11:51:21.790' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (229, 0, N'9b38031e98f060ae39e1.jpg', N'/Files/1102115121_9b38031e98f060ae39e1.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T11:51:21.813' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (230, 0, N'0f726a56f1b809e650a9.jpg', N'/Files/1102115121_0f726a56f1b809e650a9.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T11:51:21.820' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (231, 0, N'928da7a93c47c4199d56.jpg', N'/Files/1102115121_928da7a93c47c4199d56.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T11:51:21.917' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (232, 0, N'73704754dcba24e47dab.jpg', N'/Files/1102115122_73704754dcba24e47dab.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T11:51:22.020' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (233, 0, N'078b59a2c24c3a12635d.jpg', N'/Files/1102115122_078b59a2c24c3a12635d.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T11:51:22.090' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (234, 0, N'a63a771fecf114af4de0.jpg', N'/Files/1102115122_a63a771fecf114af4de0.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T11:51:22.097' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (235, 0, N'3ebfe09a7b74832ada65.jpg', N'/Files/1102115122_3ebfe09a7b74832ada65.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T11:51:22.217' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (236, 0, N'ddf73fd2a43c5c62052d.jpg', N'/Files/1102115122_ddf73fd2a43c5c62052d.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T11:51:22.227' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (237, 0, N'36811da5864b7e15275a.jpg', N'/Files/1102115122_36811da5864b7e15275a.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T11:51:22.333' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (238, 0, N'13e375c7ee2916774f38.jpg', N'/Files/1102115122_13e375c7ee2916774f38.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T11:51:22.357' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (239, 0, N'0dc58fe1140fec51b51e.jpg', N'/Files/1102115122_0dc58fe1140fec51b51e.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T11:51:22.380' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (240, 0, N'a4091a2d81c3799d20d2.jpg', N'/Files/1102115122_a4091a2d81c3799d20d2.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T11:51:22.423' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (241, 0, N'Bán-nhà-ngõ-6-Vĩnh-Phúc-3.jpg', N'/Files/1102123239_Bán-nhà-ngõ-6-Vĩnh-Phúc-3.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                     ', NULL, 1, CAST(N'2020-02-11T12:32:39.197' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (242, 0, N'Bán-nhà-ngõ-6-Vĩnh-Phúc-5.jpg', N'/Files/1102123239_Bán-nhà-ngõ-6-Vĩnh-Phúc-5.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                     ', NULL, 1, CAST(N'2020-02-11T12:32:39.537' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (243, 0, N'Bán-nhà-ngõ-6-Vĩnh-Phúc.jpg', N'/Files/1102123239_Bán-nhà-ngõ-6-Vĩnh-Phúc.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                       ', NULL, 1, CAST(N'2020-02-11T12:32:39.560' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (244, 0, N'Bán-nhà-ngõ-6-Vĩnh-Phúc-6.jpg', N'/Files/1102123239_Bán-nhà-ngõ-6-Vĩnh-Phúc-6.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                     ', NULL, 1, CAST(N'2020-02-11T12:32:39.283' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (245, 0, N'Bán-nhà-ngõ-6-Vĩnh-Phúc-2.jpg', N'/Files/1102123239_Bán-nhà-ngõ-6-Vĩnh-Phúc-2.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                     ', NULL, 1, CAST(N'2020-02-11T12:32:39.297' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (246, 0, N'Bán-nhà-ngõ-6-Vĩnh-Phúc-4.jpg', N'/Files/1102123239_Bán-nhà-ngõ-6-Vĩnh-Phúc-4.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                     ', NULL, 1, CAST(N'2020-02-11T12:32:39.217' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (247, 0, N'Bán-nhà-ngõ-6-Vĩnh-Phúc-7.jpg', N'/Files/1102123240_Bán-nhà-ngõ-6-Vĩnh-Phúc-7.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                     ', NULL, 1, CAST(N'2020-02-11T12:32:40.727' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (248, 0, N'4caf2499fa201c7e4531.jpg', N'/Files/1102124836_4caf2499fa201c7e4531.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T12:48:36.727' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (249, 0, N'0e9e50b78e0e6850311f.jpg', N'/Files/1102124836_0e9e50b78e0e6850311f.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T12:48:36.713' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (250, 0, N'2d4a1172cfcb299570da.jpg', N'/Files/1102124836_2d4a1172cfcb299570da.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T12:48:36.777' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (251, 0, N'4e8502b8dc013a5f6310.jpg', N'/Files/1102124836_4e8502b8dc013a5f6310.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T12:48:36.807' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (252, 0, N'4ee335cbeb720d2c5463.jpg', N'/Files/1102124836_4ee335cbeb720d2c5463.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T12:48:36.850' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (253, 0, N'6cb2a792792b9f75c63a.jpg', N'/Files/1102124836_6cb2a792792b9f75c63a.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T12:48:36.967' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (254, 0, N'5ff316cac8732e2d7762.jpg', N'/Files/1102124836_5ff316cac8732e2d7762.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T12:48:36.970' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (255, 0, N'7f17231efda71bf942b6.jpg', N'/Files/1102124837_7f17231efda71bf942b6.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T12:48:37.017' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (256, 0, N'9a108f315188b7d6ee99.jpg', N'/Files/1102124837_9a108f315188b7d6ee99.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T12:48:37.050' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (257, 0, N'8a32d41c0aa5ecfbb5b4.jpg', N'/Files/1102124837_8a32d41c0aa5ecfbb5b4.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T12:48:37.057' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (258, 0, N'41bf159ccb252d7b7434.jpg', N'/Files/1102124837_41bf159ccb252d7b7434.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T12:48:37.140' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (259, 0, N'49bc209ffe2618784137.jpg', N'/Files/1102124837_49bc209ffe2618784137.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T12:48:37.167' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (260, 0, N'64e8f3cb2d72cb2c9263.jpg', N'/Files/1102124837_64e8f3cb2d72cb2c9263.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T12:48:37.177' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (261, 0, N'083bd5150bacedf2b4bd.jpg', N'/Files/1102124837_083bd5150bacedf2b4bd.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T12:48:37.180' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (262, 0, N'87af8684583dbe63e72c.jpg', N'/Files/1102124837_87af8684583dbe63e72c.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T12:48:37.267' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (263, 0, N'544cd86806d1e08fb9c0.jpg', N'/Files/1102124837_544cd86806d1e08fb9c0.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T12:48:37.323' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (264, 0, N'4d79655ebbe75db904f6.jpg', N'/Files/1102124837_4d79655ebbe75db904f6.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T12:48:37.333' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (265, 0, N'999e0fa4d11d37436e0c.jpg', N'/Files/1102124837_999e0fa4d11d37436e0c.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T12:48:37.333' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (266, 0, N'649ec8b3160af054a91b.jpg', N'/Files/1102124837_649ec8b3160af054a91b.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T12:48:37.347' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (267, 0, N'54496874b6cd509309dc.jpg', N'/Files/1102124837_54496874b6cd509309dc.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T12:48:37.523' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (268, 0, N'aa6ed2480cf1eaafb3e0.jpg', N'/Files/1102124837_aa6ed2480cf1eaafb3e0.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T12:48:37.550' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (269, 0, N'a8085724899d6fc3368c.jpg', N'/Files/1102124837_a8085724899d6fc3368c.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T12:48:37.570' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (270, 0, N'a5f813dacd632b3d7272.jpg', N'/Files/1102124837_a5f813dacd632b3d7272.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T12:48:37.630' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (271, 0, N'b4606a48b4f152af0be0.jpg', N'/Files/1102124837_b4606a48b4f152af0be0.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T12:48:37.733' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (272, 0, N'b4e0aadd7464923acb75.jpg', N'/Files/1102124837_b4e0aadd7464923acb75.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T12:48:37.773' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (273, 0, N'51491d72c3cb25957cda.jpg', N'/Files/1102124837_51491d72c3cb25957cda.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T12:48:37.773' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (274, 0, N'beb9e79f3926df788637.jpg', N'/Files/1102124837_beb9e79f3926df788637.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T12:48:37.827' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (275, 0, N'c0e53fc2e17b07255e6a.jpg', N'/Files/1102124837_c0e53fc2e17b07255e6a.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T12:48:37.887' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (276, 0, N'aaeed8d6066fe031b97e.jpg', N'/Files/1102124838_aaeed8d6066fe031b97e.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T12:48:38.027' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (277, 0, N'cac3cbfe1547f319aa56.jpg', N'/Files/1102124838_cac3cbfe1547f319aa56.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T12:48:38.027' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (278, 0, N'a74cda6c04d5e28bbbc4.jpg', N'/Files/1102124838_a74cda6c04d5e28bbbc4.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T12:48:38.040' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (279, 0, N'cafabddb6362853cdc73.jpg', N'/Files/1102124838_cafabddb6362853cdc73.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T12:48:38.083' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (280, 0, N'e96ef34a2df3cbad92e2.jpg', N'/Files/1102124838_e96ef34a2df3cbad92e2.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T12:48:38.093' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (281, 0, N'ed61734eadf74ba912e6.jpg', N'/Files/1102124838_ed61734eadf74ba912e6.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T12:48:38.240' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (282, 0, N'fede17f1c9482f167659.jpg', N'/Files/1102124838_fede17f1c9482f167659.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T12:48:38.313' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (283, 0, N'cb73b94a67f381add8e2.jpg', N'/Files/1102124838_cb73b94a67f381add8e2.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T12:48:38.400' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (284, 0, N'3fde69fd77ad8ff3d6bc.jpg', N'/Files/1102065310_3fde69fd77ad8ff3d6bc.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T18:53:10.057' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (285, 0, N'c206110b0f5bf705ae4a.jpg', N'/Files/1102065311_c206110b0f5bf705ae4a.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T18:53:11.593' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (286, 0, N'ac2fffd5e6851edb4794.jpg', N'/Files/1102065311_ac2fffd5e6851edb4794.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T18:53:11.767' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (287, 0, N'ba5fdebec7ee3fb066ff.jpg', N'/Files/1102065311_ba5fdebec7ee3fb066ff.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T18:53:11.893' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (288, 0, N'4377785666069e58c717.jpg', N'/Files/1102065311_4377785666069e58c717.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T18:53:11.940' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (289, 0, N'496cac91b5c14d9f14d0.jpg', N'/Files/1102065312_496cac91b5c14d9f14d0.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T18:53:12.080' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (290, 0, N'1430d027ce7736296f66.jpg', N'/Files/1102065312_1430d027ce7736296f66.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T18:53:12.327' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (291, 0, N'f9bffea4e0f418aa41e5.jpg', N'/Files/1102065312_f9bffea4e0f418aa41e5.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T18:53:12.483' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (292, 0, N'eb3db338ad6855360c79.jpg', N'/Files/1102065312_eb3db338ad6855360c79.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T18:53:12.517' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (293, 0, N'd30d86229872602c3963.jpg', N'/Files/1102065313_d30d86229872602c3963.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T18:53:13.377' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (294, 0, N'e046e352fd02055c5c13.jpg', N'/Files/1102065313_e046e352fd02055c5c13.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T18:53:13.830' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (295, 1, N'b96e372995eb6db534fa.jpg', N'/Files/1102065846_b96e372995eb6db534fa.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T18:58:46.730' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (296, 1, N'431a685cca9e32c06b8f.jpg', N'/Files/1102065846_431a685cca9e32c06b8f.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T18:58:46.877' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (297, 0, N'1b72dc3f5b79bc27e568.jpg', N'/Files/1102090017_1b72dc3f5b79bc27e568.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T21:00:17.987' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (298, 0, N'1a139676f4e516bb4ff4.jpg', N'/Files/1102090018_1a139676f4e516bb4ff4.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T21:00:18.083' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (299, 0, N'8aa6b39334d5d38b8ac4.jpg', N'/Files/1102090017_8aa6b39334d5d38b8ac4.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T21:00:18.003' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (300, 0, N'7dd2e0a167e780b9d9f6.jpg', N'/Files/1102090018_7dd2e0a167e780b9d9f6.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T21:00:18.473' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (301, 0, N'3a5cb33ed1ad33f36abc.jpg', N'/Files/1102090019_3a5cb33ed1ad33f36abc.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T21:00:19.820' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (302, 0, N'82d0bbb0d9233b7d6232.jpg', N'/Files/1102090021_82d0bbb0d9233b7d6232.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T21:00:21.577' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (303, 0, N'6cc3d7ae50e8b7b6eef9.jpg', N'/Files/1102090021_6cc3d7ae50e8b7b6eef9.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T21:00:21.733' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (304, 0, N'20190314103937-b9a6_wm.jpg', N'/Files/1102090021_20190314103937-b9a6_wm.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                        ', NULL, 1, CAST(N'2020-02-11T21:00:21.923' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (305, 0, N'15b65e2fd9693e376778.jpg', N'/Files/1102090021_15b65e2fd9693e376778.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T21:00:21.937' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (306, 0, N'98c801ab63388166d829.jpg', N'/Files/1102090022_98c801ab63388166d829.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T21:00:22.267' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (307, 0, N'a5d0b5c33285d5db8c94.jpg', N'/Files/1102090022_a5d0b5c33285d5db8c94.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T21:00:22.643' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (308, 0, N'272ef24f90dc72822bcd.jpg', N'/Files/1102090022_272ef24f90dc72822bcd.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T21:00:22.877' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (309, 0, N'0886d5dab74955170c58.jpg', N'/Files/1102090023_0886d5dab74955170c58.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T21:00:23.267' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (310, 0, N'Ảnh Phố Ô tô.png', N'/Files/1102090023_Ảnh Phố Ô tô.png                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  ', NULL, 1, CAST(N'2020-02-11T21:00:23.393' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (311, 0, N'329880fbe26800365979.jpg', N'/Files/1102090023_329880fbe26800365979.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T21:00:23.520' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (312, 0, N'a30d856ce7ff05a15cee.jpg', N'/Files/1102090025_a30d856ce7ff05a15cee.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T21:00:25.390' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (313, 0, N'eb3c215a43c9a197f8d8.jpg', N'/Files/1102090026_eb3c215a43c9a197f8d8.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T21:00:26.347' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (314, 0, N'8dde6e47dc2b25757c3a.jpg', N'/Files/1102102744_8dde6e47dc2b25757c3a.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T22:27:44.880' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (315, 0, N'407cbfe20d8ef4d0ad9f.jpg', N'/Files/1102102744_407cbfe20d8ef4d0ad9f.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T22:27:44.867' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (316, 0, N'774c7bd7788b81d5d89a.jpg', N'/Files/1102102744_774c7bd7788b81d5d89a.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T22:27:44.867' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (317, 0, N'00a8a432165eef00b64f.jpg', N'/Files/1102102745_00a8a432165eef00b64f.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T22:27:45.070' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (318, 0, N'7da8ce307c5c8502dc4d.jpg', N'/Files/1102102745_7da8ce307c5c8502dc4d.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T22:27:45.257' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (319, 0, N'20190528082742-6c1e_wm.jpg', N'/Files/1102102745_20190528082742-6c1e_wm.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                        ', NULL, 1, CAST(N'2020-02-11T22:27:45.320' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (320, 0, N'20190528082745-5c20_wm.jpg', N'/Files/1102102745_20190528082745-5c20_wm.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                        ', NULL, 1, CAST(N'2020-02-11T22:27:45.397' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (321, 0, N'20190528082743-3c3d_wm.jpg', N'/Files/1102102745_20190528082743-3c3d_wm.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                        ', NULL, 1, CAST(N'2020-02-11T22:27:45.447' AS DateTime), 1)
GO
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (322, 0, N'20190528082747-52a5_wm.jpg', N'/Files/1102102745_20190528082747-52a5_wm.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                        ', NULL, 1, CAST(N'2020-02-11T22:27:45.507' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (323, 0, N'20190529142634-754e.jpg', N'/Files/1102102745_20190529142634-754e.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                           ', NULL, 1, CAST(N'2020-02-11T22:27:45.587' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (324, 0, N'2381f53af6660f385677.jpg', N'/Files/1102102745_2381f53af6660f385677.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T22:27:45.617' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (325, 0, N'6d411e3b1d67e439bd76.jpg', N'/Files/1102102745_6d411e3b1d67e439bd76.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T22:27:45.617' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (326, 0, N'20190528082741-0ad1_wm.jpg', N'/Files/1102102745_20190528082741-0ad1_wm.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                        ', NULL, 1, CAST(N'2020-02-11T22:27:45.773' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (327, 0, N'5481f91c4b70b22eeb61.jpg', N'/Files/1102102746_5481f91c4b70b22eeb61.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T22:27:46.557' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (328, 0, N'e983fd194f75b62bef64.jpg', N'/Files/1102102746_e983fd194f75b62bef64.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T22:27:46.773' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (329, 0, N'a1fcb658b5044c5a1515.jpg', N'/Files/1102102746_a1fcb658b5044c5a1515.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T22:27:46.920' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (330, 0, N'7e0aeb23e4da1d8444cb.jpg', N'/Files/1102103335_7e0aeb23e4da1d8444cb.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T22:33:35.243' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (331, 0, N'30c156cd5834a16af825.jpg', N'/Files/1102103335_30c156cd5834a16af825.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T22:33:35.447' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (332, 0, N'6dfa0b2204dbfd85a4ca.jpg', N'/Files/1102103335_6dfa0b2204dbfd85a4ca.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T22:33:35.840' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (333, 0, N'2dcdecb4e24d1b13425c.jpg', N'/Files/1102103335_2dcdecb4e24d1b13425c.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T22:33:35.980' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (334, 0, N'43bb2c6d2394daca8385.jpg', N'/Files/1102103335_43bb2c6d2394daca8385.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T22:33:35.997' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (335, 0, N'4cccaf98a161583f0170.jpg', N'/Files/1102103336_4cccaf98a161583f0170.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T22:33:36.043' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (336, 0, N'6d4b0b3d05c4fc9aa5d5.jpg', N'/Files/1102103336_6d4b0b3d05c4fc9aa5d5.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T22:33:36.073' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (337, 0, N'62d0a593aa6a53340a7b.jpg', N'/Files/1102103336_62d0a593aa6a53340a7b.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T22:33:36.107' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (338, 0, N'ea515a3a54c3ad9df4d2.jpg', N'/Files/1102103336_ea515a3a54c3ad9df4d2.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T22:33:36.707' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (339, 0, N'25e6e12eeed717894ec6.jpg', N'/Files/1102103336_25e6e12eeed717894ec6.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T22:33:36.723' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (340, 0, N'f9152e1421edd8b381fc.jpg', N'/Files/1102103337_f9152e1421edd8b381fc.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T22:33:37.807' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (341, 0, N'5f20204f7031966fcf20.jpg', N'/Files/1102104110_5f20204f7031966fcf20.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T22:41:10.607' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (342, 0, N'7da4f159a12747791e36.jpg', N'/Files/1102104110_7da4f159a12747791e36.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T22:41:10.653' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (343, 0, N'79fbf8c2a8bc4ee217ad.jpg', N'/Files/1102104110_79fbf8c2a8bc4ee217ad.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T22:41:10.683' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (344, 0, N'70d6561b0665e03bb974.jpg', N'/Files/1102104110_70d6561b0665e03bb974.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T22:41:10.717' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (345, 0, N'445e9352c32c25727c3d.jpg', N'/Files/1102104110_445e9352c32c25727c3d.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T22:41:10.840' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (346, 0, N'6498ea9fbae15cbf05f0.jpg', N'/Files/1102104110_6498ea9fbae15cbf05f0.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T22:41:10.873' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (347, 0, N'98ee001b5065b63bef74.jpg', N'/Files/1102104110_98ee001b5065b63bef74.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T22:41:10.920' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (348, 0, N'db90974fc731216f7820.jpg', N'/Files/1102104111_db90974fc731216f7820.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T22:41:11.077' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (349, 0, N'72dabd1ded630b3d5272.jpg', N'/Files/1102104111_72dabd1ded630b3d5272.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T22:41:11.137' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (350, 0, N'a9de29c079be9fe0c6af.jpg', N'/Files/1102104111_a9de29c079be9fe0c6af.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T22:41:11.217' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (351, 0, N'f2376f2f3f51d90f8040.jpg', N'/Files/1102104111_f2376f2f3f51d90f8040.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-11T22:41:11.263' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (352, 0, N'd176a72ef7fe0fa056ef.jpg', N'/Files/1402093650_d176a72ef7fe0fa056ef.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-14T09:36:50.583' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (353, 0, N'afc19d44cc9434ca6d85.jpg', N'/Files/1402093650_afc19d44cc9434ca6d85.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-14T09:36:50.583' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (354, 0, N'20180928103805-2dac_wm.jpg', N'/Files/1702115503_20180928103805-2dac_wm.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                        ', NULL, 1, CAST(N'2020-02-17T11:55:03.930' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (355, 0, N'20180928103805-9d27_wm.jpg', N'/Files/1702115503_20180928103805-9d27_wm.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                        ', NULL, 1, CAST(N'2020-02-17T11:55:03.867' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (356, 0, N'20180928103805-7648_wm.jpg', N'/Files/1702115503_20180928103805-7648_wm.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                        ', NULL, 1, CAST(N'2020-02-17T11:55:03.930' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (357, 0, N'6050d43742d4a28afbc5.jpg', N'/Files/1702115505_6050d43742d4a28afbc5.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-17T11:55:05.233' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (358, 0, N'8d5c503dc6de26807fcf.jpg', N'/Files/1702115505_8d5c503dc6de26807fcf.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-17T11:55:05.297' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (359, 0, N'a6810aef9c0c7c52251d.jpg', N'/Files/1702115505_a6810aef9c0c7c52251d.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-17T11:55:05.923' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (360, 0, N'b8d2eeb9785a9804c14b.jpg', N'/Files/1702115507_b8d2eeb9785a9804c14b.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-17T11:55:07.250' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (361, 0, N'20200217103220-f593_wm.jpg', N'/Files/1702125607_20200217103220-f593_wm.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                        ', NULL, 1, CAST(N'2020-02-17T12:56:07.557' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (362, 0, N'20200217103221-bb47_wm.jpg', N'/Files/1702125607_20200217103221-bb47_wm.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                        ', NULL, 1, CAST(N'2020-02-17T12:56:07.810' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (363, 0, N'20200217103221-9b52_wm.jpg', N'/Files/1702125608_20200217103221-9b52_wm.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                        ', NULL, 1, CAST(N'2020-02-17T12:56:08.983' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (364, 0, N'20200217103222-a1cd_wm.jpg', N'/Files/1702125610_20200217103222-a1cd_wm.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                        ', NULL, 1, CAST(N'2020-02-17T12:56:10.737' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (365, 0, N'20200217103220-f593_wm.jpg', N'/Files/1702125610_20200217103220-f593_wm.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                        ', NULL, 1, CAST(N'2020-02-17T12:56:10.930' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (366, 0, N'20200217103222-f7be_wm.jpg', N'/Files/1702125611_20200217103222-f7be_wm.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                        ', NULL, 1, CAST(N'2020-02-17T12:56:11.387' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (367, 0, N'20200217103221-9fce_wm.jpg', N'/Files/1702125612_20200217103221-9fce_wm.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                        ', NULL, 1, CAST(N'2020-02-17T12:56:12.137' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (368, 0, N'20200217103221-bb47_wm.jpg', N'/Files/1702125612_20200217103221-bb47_wm.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                        ', NULL, 1, CAST(N'2020-02-17T12:56:12.213' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (369, 0, N'20200217103221-cee0_wm.jpg', N'/Files/1702125612_20200217103221-cee0_wm.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                        ', NULL, 1, CAST(N'2020-02-17T12:56:12.230' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (370, 0, N'20200217103220-1442_wm.jpg', N'/Files/1702125612_20200217103220-1442_wm.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                        ', NULL, 1, CAST(N'2020-02-17T12:56:12.487' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (371, 0, N'Edit BT2 22.jpg', N'/Files/1702094946_Edit BT2 22.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   ', NULL, 1, CAST(N'2020-02-17T21:49:46.307' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (372, 0, N'2c20677f4ff6b7a8eee7.jpg', N'/Files/1702100033_2c20677f4ff6b7a8eee7.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-17T22:00:33.967' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (373, 0, N'349977bc4e32b66cef23.jpg', N'/Files/1702100033_349977bc4e32b66cef23.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-17T22:00:33.967' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (374, 0, N'23b244ed6f64973ace75.jpg', N'/Files/1702100034_23b244ed6f64973ace75.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-17T22:00:34.577' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (375, 0, N'397b53227bab83f5daba.jpg', N'/Files/1702100034_397b53227bab83f5daba.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-17T22:00:34.577' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (376, 0, N'f1b01884210ad954801b.jpg', N'/Files/1702100034_f1b01884210ad954801b.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-17T22:00:34.607' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (377, 0, N'd8d123b5083cf062a92d.jpg', N'/Files/1702100035_d8d123b5083cf062a92d.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-17T22:00:35.093' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (378, 0, N'6af2662358ada0f3f9bc.jpg', N'/Files/1702100035_6af2662358ada0f3f9bc.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-17T22:00:35.263' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (379, 0, N'1556b5259eac66f23fbd.jpg', N'/Files/1702100035_1556b5259eac66f23fbd.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-17T22:00:35.830' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (380, 0, N'screenshot_1582106173.png', N'/Files/2002065626_screenshot_1582106173.png                                                                                                                                                                                                                                                                                                                                                                                                                                                                         ', NULL, 1, CAST(N'2020-02-20T06:56:26.547' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (381, 0, N'372208160c84f4daad95.jpg', N'/Files/2002065636_372208160c84f4daad95.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-20T06:56:36.703' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (382, 0, N'b8d84ae34e71b62fef60.jpg', N'/Files/2002065636_b8d84ae34e71b62fef60.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-20T06:56:36.690' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (383, 0, N'2eededdee94c1112485d.jpg', N'/Files/2002065636_2eededdee94c1112485d.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-20T06:56:36.703' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (384, 0, N'0a9493a9973b6f65362a.jpg', N'/Files/2002065636_0a9493a9973b6f65362a.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-20T06:56:36.703' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (385, 0, N'145e0f9f0a0df253ab1c.jpg', N'/Files/2002065637_145e0f9f0a0df253ab1c.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-20T06:56:37.067' AS DateTime), 1)
INSERT [dbo].[AZ_Files] ([Id], [FolderId], [Name], [Link], [Thumbail], [Active], [CreatedDate], [Type]) VALUES (386, 0, N'24f7db3cdeae26f07fbf.jpg', N'/Files/2002065637_24f7db3cdeae26f07fbf.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ', NULL, 1, CAST(N'2020-02-20T06:56:37.080' AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[AZ_Files] OFF
SET IDENTITY_INSERT [dbo].[AZ_Folder] ON 

INSERT [dbo].[AZ_Folder] ([Id], [Name], [ParentId], [IsActive], [Orders], [CreatedDate]) VALUES (1, N'Sản phẩm', NULL, 1, 1, CAST(N'2020-02-01T15:54:44.570' AS DateTime))
INSERT [dbo].[AZ_Folder] ([Id], [Name], [ParentId], [IsActive], [Orders], [CreatedDate]) VALUES (2, N'Dịch vụ', NULL, 1, 2, CAST(N'2020-02-02T15:54:44.570' AS DateTime))
INSERT [dbo].[AZ_Folder] ([Id], [Name], [ParentId], [IsActive], [Orders], [CreatedDate]) VALUES (3, N'Giới thiệu', NULL, 1, 3, CAST(N'2020-02-04T15:54:44.570' AS DateTime))
INSERT [dbo].[AZ_Folder] ([Id], [Name], [ParentId], [IsActive], [Orders], [CreatedDate]) VALUES (4, N'Sản phẩm 1', 1, 1, 1, CAST(N'2020-02-04T15:54:44.570' AS DateTime))
INSERT [dbo].[AZ_Folder] ([Id], [Name], [ParentId], [IsActive], [Orders], [CreatedDate]) VALUES (5, N'Sản phẩm 2', 1, 1, 2, CAST(N'2020-02-04T15:54:44.570' AS DateTime))
INSERT [dbo].[AZ_Folder] ([Id], [Name], [ParentId], [IsActive], [Orders], [CreatedDate]) VALUES (6, N'Sản phẩm 3', 4, 1, 1, CAST(N'2020-02-04T15:54:44.570' AS DateTime))
SET IDENTITY_INSERT [dbo].[AZ_Folder] OFF
INSERT [dbo].[AZ_Products] ([Id], [Name], [TypeTransaction], [TypeProperty], [ProjectId], [City], [District], [Ward], [Street], [Area], [Price], [Unit], [Address], [HouseDirection], [BalconyDirection], [NumOfFloor], [NumOfBedroom], [NumOfWcs], [ImageMain], [ImageList], [Summary], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsActive], [IsDelete], [DeletedDate], [IsVIP]) VALUES (N'c19e31c5-4a50-4984-99a1-049db1634786', N'[ BannhaAZ ] Bán nhà phân lô đường Phan Đình Giót, La Khê, Hà Đông, DT32m2x5T, giá 2,4 tỷ', 5, 28, NULL, 1, 8, 131, 719, 32, 2.4, 8, N'', 18, 18, 5, 3, 4, N'/Files/0802095632_b96e372995eb6db534fa_thumbail.jpg', N'[{"Id":0,"Name":"/Files/0802100939_f224a8dcea12124c4b03.jpg"}]', N'<p>&nbsp;</p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">B&aacute;n nh&agrave; ph&acirc;n l&ocirc; đường Phan Đ&igrave;nh Gi&oacute;t, La Kh&ecirc;, H&agrave; Đ&ocirc;ng, DT32m2x5T, gi&aacute; 2,4 tỷ</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Căn nh&agrave; ph&acirc;n l&ocirc;, thửa đất vu&ocirc;ng vắn: Diện t&iacute;ch 32m2, mặt tiền 3,8m, sổ vu&ocirc;ng h&igrave;nh hộp di&ecirc;m.</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Căn nh&agrave; thiết kế hiện đại, tối ưu h&oacute;a c&ocirc;ng năng sử dụng</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Tầng 1: Ph&ograve;ng kh&aacute;ch, gian bếp + b&agrave;n ăn, nh&agrave; vệ sinh phụ</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Tầng 2,3,4 Mỗi tầng 1 ph&ograve;ng ngủ rộng, nh&agrave; tắm vệ sinh từng tầng, cửa sổ tho&aacute;ng m&aacute;t.</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Tầng 5: Ph&ograve;ng thờ s&acirc;n phơi</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Nội thất: Thiết bị nội thất mới 100%, sang trọng, Hai lớp cửa, lớp cửa ch&iacute;nh gỗ Lim, tay vịn gỗ Lim, cửa sổ Nh&ocirc;m Việt Ph&aacute;p&hellip;</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Vị tr&iacute;: Trung t&acirc;m phường La Kh&ecirc;, H&agrave; Đ&ocirc;ng, giao th&ocirc;ng thuận tiện đi Quang Trung, Tố Hữu, L&ecirc; Trọng Tấn, thuận tiện c&aacute;c trường học từ Mầm non, Tiểu học, THCS của La Kh&ecirc;.</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Sổ đỏ ch&iacute;nh chủ, gi&aacute; 2,4 tỷ c&oacute; thương lượng</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Li&ecirc;n hệ 0919 679 682 (gặp Chức &ndash; Miễn TG, MG)</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Tư vấn hỗ trợ thủ tuc vay vốn ng&acirc;n h&agrave;ng, l&atilde;i xuất ưu đ&atilde;i tốt.</span></span></span></span></span></span></p>', CAST(N'2020-02-08T10:24:01.780' AS DateTime), NULL, CAST(N'2020-02-08T10:24:01.780' AS DateTime), NULL, 1, NULL, NULL, 0)
INSERT [dbo].[AZ_Products] ([Id], [Name], [TypeTransaction], [TypeProperty], [ProjectId], [City], [District], [Ward], [Street], [Area], [Price], [Unit], [Address], [HouseDirection], [BalconyDirection], [NumOfFloor], [NumOfBedroom], [NumOfWcs], [ImageMain], [ImageList], [Summary], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsActive], [IsDelete], [DeletedDate], [IsVIP]) VALUES (N'412477e5-9906-4813-a663-05a145826448', N'Bán nhà liền kề, đẹp – Hoàng Hoa Thám, P Vĩnh Phúc, Ba Đình, DT38m2x5T, giá 4,5 tỷ', 5, 28, NULL, 1, 2, 30, 168, 38, 4.5, 8, N'Phố Hoa', 18, 18, 5, 3, 4, N'/Files/1102114756_754a2a550efae8a4b1eb_thumbail.jpg', N'[{"Id":0,"Name":"/Files/1102114756_754a2a550efae8a4b1eb.jpg"},{"Id":1,"Name":"/Files/1102114807_708afabdd112374c6e03.jpg"},{"Id":2,"Name":"/Files/1102114807_eacafc35d99a3fc4668b.jpg"},{"Id":3,"Name":"/Files/1102114808_eae9c1f1ea5e0c00554f.jpg"},{"Id":4,"Name":"/Files/1102114807_1c0c530a78a59efbc7b4.jpg"},{"Id":5,"Name":"/Files/1102114806_d5df2a8f0e20e87eb131.jpg"}]', N'<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">B&aacute;n nh&agrave; liền kề, đẹp &ndash; Ho&agrave;ng Hoa Th&aacute;m, P Vĩnh Ph&uacute;c, Ba Đ&igrave;nh, DT38m2x5T, gi&aacute; 4,5 tỷ</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">B&aacute;n gấp 5 căn nh&agrave; ph&acirc;n l&ocirc; liền kề, gi&aacute; độc quyền kh&ocirc;ng qua trung gian, l&ocirc; nh&agrave; T&acirc;y tứ trạch.</span></span></span></span><br />
<br />
<span style="font-size:10.5pt"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black"><span style="background-color:white">+ Diện t&iacute;ch: 38m2, mặt tiền 4,5m, 5 tầng.</span><br />
<br />
<span style="background-color:white">+ Hướng: Đ&ocirc;ng Bắc, T&acirc;y Bắc hướng của học h&agrave;nh c&ocirc;ng danh.</span><br />
<br />
<span style="background-color:white">+ Nh&agrave; nằm gần phố Đội Cấn, Liễu Giai, v&agrave;i bước ch&acirc;n ra mặt Phố Ho&agrave;ng Hoa Th&aacute;m, đường trước nh&agrave; rộng, đoạn đường Đội Cấn &ocirc; t&ocirc; đi 2 chiều, gần Hồ T&acirc;y, ra Lăng B&aacute;c rất gần, khu đ&ocirc;ng người nước ngo&agrave;i sinh sống, nhất l&agrave; Người Nhật Bản.</span><br />
<br />
<span style="background-color:white">+ Nh&agrave; thiết kế khung BTCT rất chắc chắn, phong c&aacute;ch Ch&acirc;u &Acirc;u, thửa đất vu&ocirc;ng vắn, trước nh&agrave; cực k&igrave; th&ocirc;ng tho&aacute;ng, ph&aacute;p l&yacute; sạch sẽ kh&ocirc;ng lỗi phong thủy. Sổ đỏ ch&iacute;nh chủ.</span><br />
<br />
<span style="background-color:white">+ Nh&agrave; x&acirc;y 5 tầng, c&oacute; giấy ph&eacute;p x&acirc;y dựng, th&ocirc;ng tiền tho&aacute;ng hậu, &ocirc; t&ocirc; đỗ v&agrave;i bước ch&acirc;n.</span><br />
<br />
<span style="background-color:white">- Tầng 1: Để xe, ph&ograve;ng kh&aacute;ch, bếp, WC.</span><br />
<br />
<span style="background-color:white">- Tầng 2, 3, 4: Mỗi tầng 2 ph&ograve;ng ngủ rộng r&atilde;i v&agrave; WC.</span><br />
<br />
<span style="background-color:white">- Tầng 5: Ph&ograve;ng thờ v&agrave; s&acirc;n phơi.</span><br />
<br />
<span style="background-color:white">+ Trang thiết bị nội thất chất lượng cao, s&agrave;n ốp gỗ tự nhi&ecirc;n rất đẹp x&acirc;y để ở do vậy rất t&acirc;m huyết với căn nh&agrave;.</span><br />
<br />
<span style="background-color:white">+ Nh&agrave; ở khu vực d&acirc;n tr&iacute; cao an sinh x&atilde; hội cực tốt.</span><br />
<br />
<span style="background-color:white">+ Sổ đỏ ch&iacute;nh chủ.</span><br />
<br />
<span style="background-color:white">+ G&iacute;a: 4.5 tỷ kh&aacute;ch xem thiện ch&iacute;, gi&aacute; n&agrave;o cũng b&aacute;n. Cần b&aacute;n nhanh.</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Li&ecirc;n hệ xem nhanh, b&aacute;n nhanh:&nbsp; 0919 679 682 (Miễn TG, MG)</span></span></span></span></span></span></p>', CAST(N'2020-02-11T11:50:39.373' AS DateTime), NULL, CAST(N'2020-02-11T11:50:39.373' AS DateTime), NULL, 1, NULL, NULL, 0)
INSERT [dbo].[AZ_Products] ([Id], [Name], [TypeTransaction], [TypeProperty], [ProjectId], [City], [District], [Ward], [Street], [Area], [Price], [Unit], [Address], [HouseDirection], [BalconyDirection], [NumOfFloor], [NumOfBedroom], [NumOfWcs], [ImageMain], [ImageList], [Summary], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsActive], [IsDelete], [DeletedDate], [IsVIP]) VALUES (N'997ab5eb-66cc-4036-8854-0a83e6b2bd07', N'21', 5, 29, NULL, 1, 3, NULL, NULL, NULL, 0, NULL, N'', 18, 18, NULL, NULL, NULL, N'd', N'[{"Id":0,"Name":""}]', N'', CAST(N'2020-02-03T21:32:45.387' AS DateTime), NULL, CAST(N'2020-02-03T21:32:45.387' AS DateTime), NULL, 1, 1, NULL, 0)
INSERT [dbo].[AZ_Products] ([Id], [Name], [TypeTransaction], [TypeProperty], [ProjectId], [City], [District], [Ward], [Street], [Area], [Price], [Unit], [Address], [HouseDirection], [BalconyDirection], [NumOfFloor], [NumOfBedroom], [NumOfWcs], [ImageMain], [ImageList], [Summary], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsActive], [IsDelete], [DeletedDate], [IsVIP]) VALUES (N'7d23e1d1-3944-4d06-b62a-159031ce05e1', N'[ BannhaAZ ] Chính chủ bán nhà phố Bế Văn Đàn, Quang Trung, Hà Đông. Ô tô đỗ cửa. DT40m2x4T. Giá 3,2 tỷ', 5, 28, NULL, 1, 8, 142, 670, 40, 3.2, 8, N'', 18, 18, 4, 4, 3, N'/Files/0802095633_431a685cca9e32c06b8f_thumbail.jpg', N'[{"Id":0,"Name":"/Files/0802095633_431a685cca9e32c06b8f.jpg"}]', N'<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Ch&iacute;nh chủ b&aacute;n nh&agrave; phố Bế Văn Đ&agrave;n, Quang Trung, H&agrave; Đ&ocirc;ng. &Ocirc; t&ocirc; đỗ cửa. DT40m2x4T. Gi&aacute; 3,2 tỷ</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">CĐT cần b&aacute;n nh&agrave; l&ocirc; 2 căn, &Ocirc; t&ocirc; đỗ cửa ở ng&otilde; 6 phố Bế Văn Đ&agrave;n, P.Quang Trung, H&agrave; Đ&ocirc;ng. Nh&agrave; mới 100% được x&acirc;y dựng 4 tầng ki&ecirc;n cố, chắc chắn, thiết kế theo xu hướng hiện đại, nh&agrave; l&ocirc; g&oacute;c nến rất tho&aacute;ng.</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Tầng 1: ph&ograve;ng bếp, ph&ograve;ng kh&aacute;ch, WC phụ</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Tầng 2: 1 ph&ograve;ng ngủ rộng, WC</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Tầng 3: 2 ph&ograve;ng ngủ, WC</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Tầng 4: 1 ph&ograve;ng ngủ, 1 ph&ograve;ng thờ, WC</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Nh&agrave; được trang bị to&agrave;n bộ đồ nội thất cao cấp, s&agrave;n gỗ, thiết bị vệ sinh h&atilde;ng cao cấp</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Vị tr&iacute;: Nằm ở trung t&acirc;m quận H&agrave; Đ&Ocirc;ng, C&aacute;ch đường Quang Trung 100m, rất gần c&aacute;c bệnh viện v&agrave; trường học.</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Sổ đỏ ch&iacute;nh chủ, ph&aacute;p lỹ r&otilde; r&agrave;ng. Gi&aacute; 3,2 tỷ ( C&oacute; thương lượng )</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Li&ecirc;n Hệ: Anh Chức 0919 679 682 ( Miễn trung gian- Quảng C&aacute;o Mạng ).</span></span></span></span></span></span></p>', CAST(N'2020-02-08T10:21:29.790' AS DateTime), NULL, CAST(N'2020-02-08T10:21:29.790' AS DateTime), NULL, 1, NULL, NULL, 0)
INSERT [dbo].[AZ_Products] ([Id], [Name], [TypeTransaction], [TypeProperty], [ProjectId], [City], [District], [Ward], [Street], [Area], [Price], [Unit], [Address], [HouseDirection], [BalconyDirection], [NumOfFloor], [NumOfBedroom], [NumOfWcs], [ImageMain], [ImageList], [Summary], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsActive], [IsDelete], [DeletedDate], [IsVIP]) VALUES (N'86f45224-4c04-4b6d-abc2-1cc8a078768b', N'Cho thue nha az2 3', 6, 37, NULL, 1, 3, 31, 1, 2, 333, 14, N'So 123 Cau giay', 18, 18, 1, 3, 4, N'/Files/0601061833_20200106175559-c179_wm.jpg', N'[{"Id":0,"Name":"/Files/0512105113_choi-gi-o-ubud-bali-vietnamembassy-indonesia.org4_.jpg"},{"Id":1,"Name":"/Files/0512105113_swing.jpg"}]', N'', CAST(N'2020-01-06T18:41:32.960' AS DateTime), NULL, CAST(N'2020-01-06T18:41:32.960' AS DateTime), NULL, 0, 1, NULL, 1)
INSERT [dbo].[AZ_Products] ([Id], [Name], [TypeTransaction], [TypeProperty], [ProjectId], [City], [District], [Ward], [Street], [Area], [Price], [Unit], [Address], [HouseDirection], [BalconyDirection], [NumOfFloor], [NumOfBedroom], [NumOfWcs], [ImageMain], [ImageList], [Summary], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsActive], [IsDelete], [DeletedDate], [IsVIP]) VALUES (N'fa114dde-deda-4e33-8892-1e9cd7a4bb1a', N'[BannhaAZ] Nhà xây mới Vạn Phúc - Hà Đông. Ngõ thông, Ô tô đỗ cửa, DT35m2x4T, giá 2.8 tỷ', 5, 28, NULL, 1, 8, 139, 704, 35, 2.8, 8, N'Quyết Tiến', 26, 18, 5, 3, 4, N'/Files/1402093650_afc19d44cc9434ca6d85_thumbail.jpg', N'[{"Id":0,"Name":"/Files/1402093650_d176a72ef7fe0fa056ef.jpg"}]', N'<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">[BannhaAZ] Nh&agrave; x&acirc;y mới Vạn Ph&uacute;c - H&agrave; Đ&ocirc;ng. Ng&otilde; th&ocirc;ng, &Ocirc; t&ocirc; đỗ cửa, DT35m2x4T, gi&aacute; 2.8 tỷ</span></span></span></span><br />
<span style="font-size:10.5pt"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black"><span style="background-color:white">- Diện t&iacute;ch: 35m2.</span><br />
<span style="background-color:white">- Hướng: Đ&ocirc;ng Nam (C&oacute; hướng T&acirc;y tứ Trạch)<br />
<span style="background-color:white">- Mặt tiền: 3.8m.</span><br />
<span style="background-color:white">- Nh&agrave; thiết kế theo phong c&aacute;ch hiện đại, t&acirc;n cổ điển gồm 4 tầng.</span><br />
<span style="background-color:white">- Tầng 1: Thiết kế ph&ograve;ng kh&aacute;ch, chỗ để xe, bếp v&agrave; nh&agrave; vệ sinh.</span><br />
<span style="background-color:white">- Tầng 2,3,4: Mỗi tầng thiết kế một ph&ograve;ng ngủ, một nh&agrave; vệ sinh.</span><br />
<span style="background-color:white">- Tầng 5: Thiết kế gồm ph&ograve;ng thờ v&agrave; s&acirc;n phơi.</span><br />
<span style="background-color:white">- Nội thất: Được trang bị đầy đủ nội thất cơ bản gồm cửa ch&iacute;nh 2 lớp b&ecirc;n ngo&agrave;i gồm cửa sắt b&ecirc;n trong gỗ Lim Nam Phi. Cửa th&ocirc;ng ph&ograve;ng được thiết kế bằng gỗ Lim Nam Phi. Tay vịn cầu thang, tủ bếp được l&agrave;m bằng gỗ Sồi Nga, cửa nh&agrave; vệ sinh, cửa sổ l&agrave;m bằng nh&ocirc;m k&iacute;nh Việt Ph&aacute;p. Thiết bị vệ sinh được lắp đặt h&agrave;ng inox cao cấp. Hệ thống đ&egrave;n điện, trần thạch cao...</span><br />
<span style="background-color:white">- Vị tr&iacute; nằm trong khu d&acirc;n tr&iacute; cao, gần chợ, nh&agrave; văn h&oacute;a. C&aacute;ch trường cấp 1, 2 trong b&aacute;n k&iacute;nh 200m, đặc biệt c&aacute;ch khu chung cư Goldsilk, Phố Lụa 300m, giao th&ocirc;ng thuận tiện đi lại v&agrave;o trung t&acirc;m th&agrave;nh phố.</span></span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">B&atilde;i &ocirc; t&ocirc; c&aacute;ch nh&agrave; 50m, gửi ng&agrave;y đ&ecirc;m, c&oacute; m&aacute;i che đầy đủ.</span></span></span></span><br />
<span style="font-size:10.5pt"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black"><span style="background-color:white">- Ph&aacute;p l&yacute;: Sổ đỏ ch&iacute;nh chủ, bao sang t&ecirc;n<br />
<span style="background-color:white">- Gi&aacute;: 2.8 tỷ ( c&oacute; thương lượng )</span></span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Li&ecirc;n hệ 0919 679 682 (gặp A Chức &ndash; Miễn TG)</span></span></span></span></span></span></p>', CAST(N'2020-02-14T09:37:08.240' AS DateTime), NULL, CAST(N'2020-02-14T09:37:08.240' AS DateTime), NULL, 1, NULL, NULL, 0)
INSERT [dbo].[AZ_Products] ([Id], [Name], [TypeTransaction], [TypeProperty], [ProjectId], [City], [District], [Ward], [Street], [Area], [Price], [Unit], [Address], [HouseDirection], [BalconyDirection], [NumOfFloor], [NumOfBedroom], [NumOfWcs], [ImageMain], [ImageList], [Summary], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsActive], [IsDelete], [DeletedDate], [IsVIP]) VALUES (N'72b2009e-62f7-4f7c-89a3-21e063235fbf', N'Bán nhà mặt ngõ số 285 Đằng Hải, Hải An, Hải Phòng', 6, 37, NULL, 1, 3, 31, 1, 2, 333, 14, N'So 123 Cau giay', 18, 18, 1, 3, 4, N'https://nhadatvanminh.com.vn/images/raovat/146289.jpg', NULL, N'Nhà 4 tầng xây mới đẹp, kiên cố, chắc chắn, lô góc 2 mặt thoáng thiết kế trẻ trung, màu sắc hài hòa, tinh tế, khu dân cư đông đúc, an ninh tốt.', CAST(N'2020-01-06T16:40:15.907' AS DateTime), NULL, CAST(N'2020-01-10T09:56:37.857' AS DateTime), NULL, 1, 1, NULL, 0)
INSERT [dbo].[AZ_Products] ([Id], [Name], [TypeTransaction], [TypeProperty], [ProjectId], [City], [District], [Ward], [Street], [Area], [Price], [Unit], [Address], [HouseDirection], [BalconyDirection], [NumOfFloor], [NumOfBedroom], [NumOfWcs], [ImageMain], [ImageList], [Summary], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsActive], [IsDelete], [DeletedDate], [IsVIP]) VALUES (N'0a036a1a-c15b-4f2f-ae55-2ef6d09977cc', N'Bán dãy nhà trong ngõ phụ 4 Hoàng Mai, Đông Thái, An Dương, Hải phòng', 6, 37, NULL, 1, 3, 31, 1, 2, 333, 14, N'So 123 Cau giay', 18, 18, 1, 3, 4, N'/Files/0601061833_20200106175559-a816_wm.jpg', N'[{"Id":0,"Name":"/Files/0512105113_choi-gi-o-ubud-bali-vietnamembassy-indonesia.org4_.jpg"},{"Id":1,"Name":"/Files/0512105113_swing.jpg"}]', N'Nhà 3 tầng xây mới, đẹp, cấu trúc hiện đại, ngõ rộng, ô tô đỗ trước cửa, có sân trước, sân sau, có chỗ để ô tô, an ninh tốt, sổ đỏ chính chủ.', CAST(N'2020-01-06T10:33:16.123' AS DateTime), NULL, CAST(N'2020-01-10T09:49:14.607' AS DateTime), NULL, 1, 1, NULL, 1)
INSERT [dbo].[AZ_Products] ([Id], [Name], [TypeTransaction], [TypeProperty], [ProjectId], [City], [District], [Ward], [Street], [Area], [Price], [Unit], [Address], [HouseDirection], [BalconyDirection], [NumOfFloor], [NumOfBedroom], [NumOfWcs], [ImageMain], [ImageList], [Summary], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsActive], [IsDelete], [DeletedDate], [IsVIP]) VALUES (N'4fb63459-70a8-4d3b-8f62-31da32868b39', N'Bán nhà ngõ 86 Phố Hào Nam, Giang Văn Minh, Phường Cát Linh, Đống Đa, Hà Nội, Diện tích 31m2x5T, giá 3,25 tỷ', 5, 28, NULL, 1, 1, 1, 83, 31, 3.25, 8, N'86 Hào Nam, Đống Đa', 18, 18, 5, 3, 3, N'/Files/0502120216_ac2fffd5e6851edb4794_thumbail.jpg', N'[{"Id":0,"Name":"/Files/0502120216_ac2fffd5e6851edb4794.jpg"},{"Id":1,"Name":"/Files/0502120218_eb3db338ad6855360c79.jpg"},{"Id":2,"Name":"/Files/0502120546_496cac91b5c14d9f14d0.jpg"},{"Id":3,"Name":"/Files/0502120219_e046e352fd02055c5c13.jpg"},{"Id":4,"Name":"/Files/0502120217_c206110b0f5bf705ae4a.jpg"},{"Id":5,"Name":"/Files/0502120218_f9bffea4e0f418aa41e5.jpg"},{"Id":6,"Name":"/Files/0502120217_ba5fdebec7ee3fb066ff.jpg"}]', N'<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">B&aacute;n nh&agrave; ng&otilde; 86&nbsp;Phố H&agrave;o Nam, Giang Văn Minh, Phường C&aacute;t Linh, Đống Đa, H&agrave; Nội, Diện t&iacute;ch 31m2x5T, gi&aacute; 3,25 tỷ</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Th&ocirc;ng số: Diện t&iacute;ch 31m2, mặt tiền 4,3m, nở hậu 10cm, nh&agrave; x&acirc;y 4,5 tầng khung cột chắc chắn.</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Kh&aacute;ch mua về c&oacute; thể ở ngay kh&ocirc;ng cần sửa chữa nhiều.</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">C&ocirc;ng năng: C&oacute; Ph&ograve;ng kh&aacute;ch, bếp, ph&ograve;ng ăn ở tầng 1, 3 ph&ograve;ng ngủ kh&eacute;p k&iacute;n c&aacute;c tầng 2,3,4, Ph&ograve;ng thờ, khu m&aacute;y giặt, s&acirc;n phơi tầng 5 (đ&atilde; c&oacute; tum chống n&oacute;ng).</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Nh&agrave; tho&aacute;ng mặt sau, n&ecirc;n cầu thang, nh&agrave; vệ sinh đều c&oacute; cửa sổ th&ocirc;ng tho&aacute;ng kh&iacute; tự nhi&ecirc;n</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Vị tr&iacute;: MẶT NG&Otilde; TH&Ocirc;NG, C&aacute;ch mặt phố H&agrave;o Nam 20m, Ng&otilde; trước nh&agrave; gần 3m, thuận tiện chợ d&acirc;n sinh, trường Mẫu Gi&aacute;o, Tiểu học, THCS, Đường H&agrave;o Nam thuận tiện đi c&aacute;c tuyến phố huyết mạch của thủ đ&ocirc;.</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Nội thất: Hệ thống cửa gỗ Lim L&agrave;o, tủ bếp gỗ Tự Nhi&ecirc;n chắc chắn.</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Sổ đỏ ch&iacute;nh chủ, gi&aacute; 3,25 tỷ c&oacute; thương lượng</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Anh/chị quan t&acirc;m LH 0919 679 682 (Miễn TG, MG)</span></span></p>', CAST(N'2020-02-05T00:08:57.223' AS DateTime), NULL, CAST(N'2020-02-08T11:26:19.980' AS DateTime), NULL, 1, NULL, NULL, 1)
INSERT [dbo].[AZ_Products] ([Id], [Name], [TypeTransaction], [TypeProperty], [ProjectId], [City], [District], [Ward], [Street], [Area], [Price], [Unit], [Address], [HouseDirection], [BalconyDirection], [NumOfFloor], [NumOfBedroom], [NumOfWcs], [ImageMain], [ImageList], [Summary], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsActive], [IsDelete], [DeletedDate], [IsVIP]) VALUES (N'a0846c99-ea48-479b-a30d-33c9cdbb4f4d', N'[ BannhaAZ ] Bán nhà mới PL nhà dân, phố Vạn Phúc, quận Hà Đông, cách phố 8m, DT33m2x5T Giá 2,75 tỷ', 5, 28, NULL, 1, 8, 139, 664, 33, 2.75, 8, N'Gần Cty Len Hà Đông', 18, 18, NULL, 3, 4, N'/Files/0802100939_2883ae5df6930ecd5782_thumbail.jpg', N'[{"Id":0,"Name":"/Files/0802100939_2883ae5df6930ecd5782.jpg"},{"Id":1,"Name":"/Files/0802100939_e7cd203b62f59aabc3e4.jpg"},{"Id":2,"Name":"/Files/0802100939_c92f9ad5d81b2045790a.jpg"},{"Id":3,"Name":"/Files/0802100939_e3effc45a48b5cd5059a.jpg"},{"Id":4,"Name":"/Files/0802100939_f431afcced02155c4c13.jpg"}]', N'<p>&nbsp;</p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">B&aacute;n nh&agrave; mới PL nh&agrave; d&acirc;n, phố Vạn Ph&uacute;c, quận H&agrave; Đ&ocirc;ng, c&aacute;ch phố 8m, DT33m2x5T Gi&aacute; 2,75 tỷ</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Ch&iacute;nh chủ cần b&aacute;n nh&agrave; trong ng&otilde; phố Vạn Ph&uacute;c, nh&agrave; c&aacute;ch phố 8m, quận H&agrave; Đ&ocirc;ng, th&agrave;nh phố H&agrave; Nội. Nh&agrave; được x&acirc;y mới 5 tầng, thiết kế theo xu hướng hiện đại, m&oacute;ng b&egrave; khung cột chắc chắn. Ng&otilde; trước nh&agrave; rộng, th&ocirc;ng tho&aacute;ng.</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Bố cục nh&agrave; bao gồm :</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Tầng 1 : ph&ograve;ng kh&aacute;ch, ph&ograve;ng bếp, WC phụ</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Tầng 2, 3, 4 : Mỗi tầng 1 ph&ograve;ng ngủ rộng, 1 WC</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Tầng 5 : Ph&ograve;ng thờ, s&acirc;n phơi</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Nội thất : Nh&agrave; được trang bị to&agrave;n bộ nội thất cao cấp, s&agrave;n gỗ, thiết bị vệ sinh ch&iacute;nh h&atilde;ng&hellip;</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Vị tr&iacute;: Trung t&acirc;m quận H&agrave; Đ&ocirc;ng, nằm trong l&agrave;ng Lụa Vạn Ph&uacute;c nổi tiếng. Ng&otilde;&nbsp; rộng th&ocirc;ng tho&aacute;ng, gần phố, rất thuận tiện đi lại. Gần c&aacute;c trung t&acirc;m thương mại lớn như Hồ Gươm Plaza, AEMail H&agrave; Đ&ocirc;ng, gần c&aacute;c trường ĐH lớn như ĐH H&agrave; Nội, HV An Ninh&hellip;.</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Sổ đỏ ch&iacute;nh chủ, ph&aacute;p l&yacute; r&otilde; r&agrave;ng</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Gi&aacute;: 2,75 tỷ ( C&oacute; thương lượng )</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Li&ecirc;n hệ: 0919 679 682 ( Miễn trung gian, Quảng C&aacute;o Mạng ).</span></span></span></span></span></span></p>', CAST(N'2020-02-08T10:15:30.030' AS DateTime), NULL, CAST(N'2020-02-08T10:15:30.030' AS DateTime), NULL, 1, NULL, NULL, 0)
INSERT [dbo].[AZ_Products] ([Id], [Name], [TypeTransaction], [TypeProperty], [ProjectId], [City], [District], [Ward], [Street], [Area], [Price], [Unit], [Address], [HouseDirection], [BalconyDirection], [NumOfFloor], [NumOfBedroom], [NumOfWcs], [ImageMain], [ImageList], [Summary], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsActive], [IsDelete], [DeletedDate], [IsVIP]) VALUES (N'6bcdf61b-fb67-42ca-9ddd-37a5b9403a0c', N'Bán nhà riêng ngõ 612 Đê La Thành, P Giảng Võ, Quận Ba Đình. DT52m2, nhà 5 tầng LH 0919 679 682', 5, 28, NULL, 1, 2, 22, 199, 52, 3.55, 8, N'Phường Giảng Võ, Ba Đình, Hà Nội', 18, 18, 5, 3, 4, N'/Files/0502123127_4d5f51201e27fd79a436_thumbail.jpg', N'[{"Id":0,"Name":"/Files/0502123131_b82745560a51e90fb040.jpg"},{"Id":1,"Name":"/Files/0502123127_4d5f51201e27fd79a436.jpg"},{"Id":2,"Name":"/Files/0502123127_639898ead7ed34b36dfc.jpg"},{"Id":3,"Name":"/Files/0502123132_9e766f042003c35d9a12.jpg"},{"Id":4,"Name":"/Files/0502123136_d124ed59a25e4100184f.jpg"},{"Id":5,"Name":"/Files/0502123127_3483e6fca9fb4aa513ea.jpg"},{"Id":6,"Name":"/Files/0502123130_02162668696f8a31d37e.jpg"},{"Id":7,"Name":"/Files/0502123138_f4c271bc3ebbdde584aa.jpg"}]', N'<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Ch&iacute;nh chủ b&aacute;n nh&agrave; ng&otilde; 612 Đ&ecirc; La Th&agrave;nh, P Giảng V&otilde;, Quận Ba Đ&igrave;nh.</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Diện t&iacute;ch 52m2, với 32m2 x&acirc;y 5 tầng, 3 ph&ograve;ng ngủ, 01 ph&ograve;ng thờ, s&acirc;n phơi, nh&agrave; tắm, vệ sinh c&aacute;c tầng, c&ograve;n 20m2 l&agrave; s&acirc;n để xe chung.</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Thửa đất vu&ocirc;ng vắn, thiết kế hợp l&yacute;, mặt sau nh&agrave; c&oacute; khe tho&aacute;ng, n&ecirc;n kh&ocirc;ng kh&iacute; tự nhi&ecirc;n cực t&ocirc;t:</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Cụ thể:</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">-Tầng 1: l&agrave; Ph&ograve;ng kh&aacute;ch, ph&ograve;ng ăn, ph&ograve;ng bếp, wc phụ&nbsp;</span></span></span></span><br />
<span style="font-size:10.5pt"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black"><span style="background-color:white">-Tầng 2,3,4: Thiết kế mỗi tầng 01 ph&ograve;ng ngủ rộng wc kh&eacute;p k&iacute;n.&nbsp;</span><br />
<span style="background-color:white">-Tầng 5: l&agrave; ph&ograve;ng thờ, s&acirc;n phơi, đ&atilde; l&agrave;m h&agrave;ng r&agrave;o chuồng cọp đầy đủ</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Nội thất: Ho&agrave;n to&agrave;n mới 100%, chất lượng tốt, cửa cầu thang l&agrave;m bằng gỗ Lim, thiết bị vệ sinh Inax, hệ thống đ&egrave;n LED s&aacute;ng tiết kiệm điện..</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Vị tr&iacute;: trung t&acirc;m quận Ba Đ&igrave;nh, giao th&ocirc;ng thuận tiện, ng&otilde; th&ocirc;ng từ 112 Ngọc Kh&aacute;nh, th&ocirc;ng sang 612 Đ&ecirc; La Th&agrave;nh, &ocirc; t&ocirc; đỗ gần nh&agrave;. Tiện đi TTTM, phố lớn của H&agrave; Nội</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Sổ đỏ ch&iacute;nh chủ, ph&aacute;p l&yacute; OK, sẵn s&agrave;ng sang t&ecirc;n ngay.</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Li&ecirc;n hệ 0919 679 682 (Miễn TG, QC mạng)</span></span></span></span></span></span></p>', CAST(N'2020-02-05T13:00:12.100' AS DateTime), NULL, CAST(N'2020-02-08T11:26:35.730' AS DateTime), NULL, 1, NULL, NULL, 0)
INSERT [dbo].[AZ_Products] ([Id], [Name], [TypeTransaction], [TypeProperty], [ProjectId], [City], [District], [Ward], [Street], [Area], [Price], [Unit], [Address], [HouseDirection], [BalconyDirection], [NumOfFloor], [NumOfBedroom], [NumOfWcs], [ImageMain], [ImageList], [Summary], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsActive], [IsDelete], [DeletedDate], [IsVIP]) VALUES (N'b599033a-aadd-44a5-ae7d-3aca7daf83ea', N'a', 6, 38, 2, 3, 30, 510, 1436, NULL, 0, NULL, N'', 18, 18, NULL, NULL, NULL, N'', N'[{"Id":0,"Name":""}]', N'', CAST(N'2020-02-20T13:31:43.003' AS DateTime), NULL, CAST(N'2020-02-20T13:35:15.597' AS DateTime), NULL, 1, 1, NULL, 0)
INSERT [dbo].[AZ_Products] ([Id], [Name], [TypeTransaction], [TypeProperty], [ProjectId], [City], [District], [Ward], [Street], [Area], [Price], [Unit], [Address], [HouseDirection], [BalconyDirection], [NumOfFloor], [NumOfBedroom], [NumOfWcs], [ImageMain], [ImageList], [Summary], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsActive], [IsDelete], [DeletedDate], [IsVIP]) VALUES (N'83c75f16-be20-4aa7-b048-3b30d7fa19e5', N'[ BannhaAZ] Chính chủ bán nhà mới Khu PL Cán bộ, Dân trí cao, Khu Hà Trì – Phố Bà Triệu, Hà Đông, DT50m2x5T, giá 4.5 tỷ', 5, 28, NULL, 1, 8, 129, 668, 50, 4.5, 8, N'Khu Dân trí cao', 18, 18, 5, 5, 4, N'/Files/0802111129_83eb19b0a3665b380277_thumbail.jpg', N'[{"Id":0,"Name":"/Files/0802110111_cd7d775dab8c53d20a9d.jpg"},{"Id":1,"Name":"/Files/0802111129_83eb19b0a3665b380277.jpg"},{"Id":2,"Name":"/Files/0802111132_9fa1cfec753a8d64d42b.jpg"}]', N'<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Ch&iacute;nh chủ b&aacute;n nh&agrave; mới Khu PL C&aacute;n bộ, D&acirc;n tr&iacute; cao, Khu H&agrave; Tr&igrave; &ndash; Phố B&agrave; Triệu, H&agrave; Đ&ocirc;ng, DT50m2x5T, gi&aacute; 4.5 tỷ</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Căn nh&agrave; được thiết kế theo phong c&aacute;ch hiện đại, ph&acirc;n kh&uacute;c VIP, &ocirc; t&ocirc; c&oacute; thể để trong nh&agrave; thoải m&aacute;i.</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Mặt ng&otilde; trước nh&agrave; tr&ecirc;n 4,5m, gần ng&atilde; 3, xe &ocirc; t&ocirc; quay đầu thoải m&aacute;i.</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Tầng 1: Gara oto, xe m&aacute;y, Ph&ograve;ng bếp + ăn cực rộng, nh&agrave; vệ sinh phụ.</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Tầng 2: Ph&ograve;ng kh&aacute;ch rộng, nh&agrave; tắm rộng, ph&ograve;ng ngủ, c&oacute; ban c&ocirc;ng</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Tầng 3,4 Mỗi tầng 2 Ph&ograve;ng ngủ</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Tầng 5 Gian thờ, khu m&aacute;y giặt, s&acirc;n phơi rộng</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Nội thất: Được trang bị nội thất cao cấp, Cửa cuốn, tay vịn cầu thang l&agrave;m bằng gỗ Lim nguy&ecirc;n tấm, thiết bị vệ sinh Toto, cửa sổ nh&ocirc;m Việt Ph&aacute;p.</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Vị tr&iacute;: C&aacute;ch mặt phố v&agrave;i bước ch&acirc;n, ng&otilde; trước nh&agrave; rộng tr&ecirc;n 4m, ng&atilde; 3 ngay cạnh nh&agrave; n&ecirc;n &ocirc; t&ocirc; quay đầu thoải m&aacute;i, Khu d&acirc;n cư d&acirc;n tr&iacute; cao, khu hưu tr&iacute; &ndash; H&agrave; Tr&igrave;, giao th&ocirc;ng thuận tiện, đi c&aacute;c tuyến phố ch&iacute;nh như T&ocirc; Hiệu, Quang Trung, Ph&ugrave;ng Hưng&hellip;.</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Sổ đỏ ch&iacute;nh chủ, gi&aacute; 4,5 tỷ c&oacute; thương lượng</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Li&ecirc;n hệ ngay 09683 50 683 gặp Chức để được tư vấn.</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Hỗ trợ thủ tục vay vốn ng&acirc;n h&agrave;ng, ph&aacute;p l&yacute; mua b&aacute;n nh&agrave; đất.</span></span></span></span></span></span></p>', CAST(N'2020-02-08T11:13:47.253' AS DateTime), NULL, CAST(N'2020-02-08T11:13:47.253' AS DateTime), NULL, 1, NULL, NULL, 0)
INSERT [dbo].[AZ_Products] ([Id], [Name], [TypeTransaction], [TypeProperty], [ProjectId], [City], [District], [Ward], [Street], [Area], [Price], [Unit], [Address], [HouseDirection], [BalconyDirection], [NumOfFloor], [NumOfBedroom], [NumOfWcs], [ImageMain], [ImageList], [Summary], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsActive], [IsDelete], [DeletedDate], [IsVIP]) VALUES (N'1093e927-9873-4b6f-992f-43195ddb37b4', N'Chính chủ cần bán nhà mặt ngõ 465 Đội Cấn, P.Vĩnh Phúc, Quận Ba Đình, DT48m2x6T, giá 8,8 tỷ', 5, 28, NULL, 1, 2, 30, 160, 48, 8.8, 8, N'', 18, 18, 5, 5, 4, N'/Files/1102115122_0dc58fe1140fec51b51e_thumbail.jpg', N'[{"Id":0,"Name":"/Files/1102115122_3ebfe09a7b74832ada65.jpg"},{"Id":1,"Name":"/Files/1102115122_0dc58fe1140fec51b51e.jpg"},{"Id":2,"Name":"/Files/1102115122_13e375c7ee2916774f38.jpg"},{"Id":3,"Name":"/Files/1102115121_928da7a93c47c4199d56.jpg"},{"Id":4,"Name":"/Files/1102115122_73704754dcba24e47dab.jpg"},{"Id":5,"Name":"/Files/1102115122_36811da5864b7e15275a.jpg"},{"Id":6,"Name":"/Files/1102115122_a63a771fecf114af4de0.jpg"}]', N'<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Ch&iacute;nh chủ cần b&aacute;n nh&agrave; mặt ng&otilde; 465 Đội Cấn, Phường Vĩnh Ph&uacute;c, Quận Ba Đ&igrave;nh, TP H&agrave; Nội.</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Nh&agrave; mặt ng&otilde; vị tr&iacute; cực đẹp, &ocirc; t&ocirc; v&agrave;o nh&agrave; thoải m&aacute;i, trước nh&agrave; cực kỳ tho&aacute;ng đ&atilde;ng, thuận tiện để ở kết hợp l&agrave;m văn ph&ograve;ng. Diện t&iacute;ch 48m2, MT 4.8m, Gi&aacute; 8.8 tỷ.</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Thiết kế m&oacute;ng b&egrave; khung cột chắc chắn, nh&agrave; mới x&acirc;y được 2 năm, bố cục cực kỳ hợp l&yacute;:</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Tầng 1: Gara để xe &ocirc; t&ocirc;, xe m&aacute;y, wc phụ</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Tầng 2: Ph&ograve;ng kh&aacute;ch, ph&ograve;ng ăn, bếp, wc.</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Tầng 3: Ph&ograve;ng l&agrave;m việc, Ph&ograve;ng ngủ, wc kh&eacute;p k&iacute;n.</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Tầng 4,5: Mỗi tầng 02 ph&ograve;ng ngủ, wc ở giữa.</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Tầng 6: S&acirc;n phơi, khu trồng c&acirc;y xanh mướt.</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Nội thất: Nh&agrave; đang ở trang thiết bị nội thất hiện đại, chắc chắn, cầu thang, s&agrave;n nh&agrave; c&aacute;c tầng đều l&agrave;m bằng gỗ, b&igrave;nh n&oacute;ng lạnh, cửa gỗ lim chắc chắn, trần thạch cao Vĩnh Tường, đ&egrave;n led &acirc;m trần tiết kiệm điện. Chủ nh&agrave; thiện ch&iacute; để lại nhiều đồ đạc nội thất như tủ bếp, tủ lạnh, sofa, kệ tivi, tủ, giường 1 số ph&ograve;ng...</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Vị tr&iacute;: Nh&agrave; c&oacute; vị tr&iacute; cực đẹp. Ng&otilde; rộng &ocirc; t&ocirc; v&agrave; xe m&aacute;y tr&aacute;nh nhau thoải m&aacute;i, trước nh&agrave; l&agrave; s&acirc;n tennis n&ecirc;n cực kỳ tho&aacute;ng đ&atilde;ng. Nh&agrave; ngay cạnh UBND phường Vĩnh Ph&uacute;c v&agrave; khu doanh trại C&ocirc;ng Binh, Ph&aacute;o Binh an ninh th&igrave; khỏi phải b&agrave;n. Nh&agrave; c&aacute;ch mặt phố chưa đầy 50m c&aacute;ch đường v&agrave;nh đai 2 chưa đầy 100m giao th&ocirc;ng đi lại cực kỳ thuận tiện.</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Sổ đỏ ch&iacute;nh chủ, ph&aacute;p l&yacute; r&otilde; r&agrave;ng.</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Gi&aacute;: 8.8 tỷ (c&oacute; thương lượng)&nbsp; </span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Li&ecirc;n hệ : Anh Chức 0919 67 682&nbsp;( MTG-QCM)</span></span></p>', CAST(N'2020-02-11T11:54:33.963' AS DateTime), NULL, CAST(N'2020-02-11T11:54:33.963' AS DateTime), NULL, 1, NULL, NULL, 0)
INSERT [dbo].[AZ_Products] ([Id], [Name], [TypeTransaction], [TypeProperty], [ProjectId], [City], [District], [Ward], [Street], [Area], [Price], [Unit], [Address], [HouseDirection], [BalconyDirection], [NumOfFloor], [NumOfBedroom], [NumOfWcs], [ImageMain], [ImageList], [Summary], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsActive], [IsDelete], [DeletedDate], [IsVIP]) VALUES (N'64165e80-5d40-4ff6-83cf-4d51024f47ad', N'1', 6, 37, NULL, 1, 1, 3, 64, NULL, 0, 14, N'', 18, 18, NULL, NULL, NULL, N'2', N'[{"Id":0,"Name":""}]', N'<p>f</p>', CAST(N'2020-01-17T11:03:10.967' AS DateTime), NULL, CAST(N'2020-01-17T11:03:10.967' AS DateTime), NULL, 1, 1, NULL, 0)
INSERT [dbo].[AZ_Products] ([Id], [Name], [TypeTransaction], [TypeProperty], [ProjectId], [City], [District], [Ward], [Street], [Area], [Price], [Unit], [Address], [HouseDirection], [BalconyDirection], [NumOfFloor], [NumOfBedroom], [NumOfWcs], [ImageMain], [ImageList], [Summary], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsActive], [IsDelete], [DeletedDate], [IsVIP]) VALUES (N'8426bc22-a6a5-40c6-a03b-5417ccdd6274', N'Bán nhà ngõ 17 Đại Mỗ, Ngã tư Tố Hữu, nhà mới 100%, ngõ rộng thông thoáng. DT36m2x5T. Giá 2,4 tỷ', 5, 28, NULL, 1, 10, 144, 884, 36, 2.4, 8, N'Khu Phân Lô nhà dân', 18, 18, 4, 4, 3, N'/Files/0802095633_431a685cca9e32c06b8f_thumbail.jpg', N'[{"Id":0,"Name":"/Files/0802095633_431a685cca9e32c06b8f.jpg"},{"Id":1,"Name":"/Files/0802095633_4ed4c59267509f0ec641.jpg"},{"Id":2,"Name":"/Files/0802095632_b96e372995eb6db534fa.jpg"},{"Id":3,"Name":"/Files/0802095632_4c454002e2c01a9e43d1.jpg"},{"Id":4,"Name":"/Files/0802095632_46d6d79275508d0ed441.jpg"}]', N'<p>B&aacute;n nh&agrave; ng&otilde; 17 Đại Mỗ, Ng&atilde; tư Tố Hữu, nh&agrave; mới 100%, ng&otilde; rộng th&ocirc;ng tho&aacute;ng. DT36m2x5T. Gi&aacute; 2,4 tỷ.<br />
Ch&iacute;nh chủ cần b&aacute;n nh&agrave; ng&otilde; 17 Đại Mỗ, H&agrave; Đ&ocirc;ng. Nh&agrave; được x&acirc;y mới 4 tầng, thiết kế theo xu hướng hiện đại, tối ưu h&oacute;a c&ocirc;ng năng sử dụng. Ng&otilde; rộng th&ocirc;ng tho&aacute;ng, xe m&aacute;y, xe b&aacute; g&aacute;c tr&aacute;nh nhau thoải m&aacute;i.<br />
Bố cục nh&agrave; bao gồm:<br />
Tầng 1: ph&ograve;ng kh&aacute;ch, ph&ograve;ng bếp, WC phụ<br />
Tầng 2: 1 ph&ograve;ng ngủ rộng, WC<br />
Tầng 3,4: mỗi tầng 2 ph&ograve;ng,1 WC<br />
Nội thất: Nh&agrave; được trang bị to&agrave;n bộ nội thất cao cấp, s&agrave;n gỗ, thiết bị vệ sinh h&agrave;ng hiệu,&hellip;<br />
Vị tr&iacute;: Ng&otilde; rộng th&ocirc;ng tho&aacute;ng, gần vị tr&iacute; đỗ &ocirc; t&ocirc;, c&aacute;ch đường Đại Mỗ 100m, rất thuận tiện đi lại<br />
Sổ đỏ ch&iacute;nh chủ, ph&aacute;p l&yacute; r&otilde; r&agrave;ng<br />
Gi&aacute;: 2,4 tỷ ( C&oacute; thương lượng )<br />
Li&ecirc;n hệ: Anh Chức 0919 679 682&nbsp;( Miễn trung gian, Quảng C&aacute;o Mạng ).</p>', CAST(N'2020-02-08T10:08:29.937' AS DateTime), NULL, CAST(N'2020-02-08T10:08:29.937' AS DateTime), NULL, 1, NULL, NULL, 0)
INSERT [dbo].[AZ_Products] ([Id], [Name], [TypeTransaction], [TypeProperty], [ProjectId], [City], [District], [Ward], [Street], [Area], [Price], [Unit], [Address], [HouseDirection], [BalconyDirection], [NumOfFloor], [NumOfBedroom], [NumOfWcs], [ImageMain], [ImageList], [Summary], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsActive], [IsDelete], [DeletedDate], [IsVIP]) VALUES (N'4f9e4b9b-be8b-403e-916a-5ef4e5343013', N'[ BannhaAZ ] Bán nhà đẹp giá tốt, ngõ 12 Quang Trung, Hà Đông, DT30m2x4T, giá 2,1 tỷ', 5, 28, NULL, 1, 8, 138, 662, 30, 2.1, 8, N'Ngõ 12', 18, 18, NULL, 2, 3, N'/Files/0802111915_4c50a63ef4ee0cb055ff_thumbail.jpg', N'[{"Id":0,"Name":"/Files/0802111936_2f5d9b33c9e331bd68f2.jpg"},{"Id":1,"Name":"/Files/0802111935_8e7e7d102fc0d79e8ed1.jpg"},{"Id":2,"Name":"/Files/0802111936_06a92fc67d168548dc07.jpg"},{"Id":3,"Name":"/Files/0802111935_01bfb3d1e101195f4010.jpg"},{"Id":4,"Name":"/Files/0802111936_0db910e34233ba6de322.jpg"},{"Id":5,"Name":"/Files/0802111915_4c50a63ef4ee0cb055ff.jpg"}]', N'<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">B&aacute;n nh&agrave; đẹp gi&aacute; tốt, ng&otilde; 12 Quang Trung, H&agrave; Đ&ocirc;ng, DT30m2x4T, gi&aacute; 2,1 tỷ</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Vị tr&iacute;: C&aacute;ch trường chuy&ecirc;n Nguyễn Huệ 50m, c&aacute;ch mặt phố Quang Trung gần 100m, thuận tiện giao th&ocirc;ng, &ocirc; t&ocirc; quay đầu c&aacute;ch nh&agrave; 40m.</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Diện t&iacute;ch 30m2x4T, mặt tiền 3.94m, mặt hậu 4m, thửa đất nở hậu phong thủy tốt.</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Căn nh&agrave; thiết kế hiện đại, mỗi tầng 1 ph&ograve;ng, tối ưu h&oacute;a sử dụng.</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Nội thất: cửa tay vịn cầu thang gỗ Lim L&agrave;o, cửa sổ Nh&ocirc;m Việt Ph&aacute;p.</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Sổ đỏ ch&iacute;nh chủ, gi&aacute; 2,1 tỷ c&oacute; thương lượng</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Li&ecirc;n hệ 09683 50 683 gặp Chức (Miễn TG, MG)</span></span></span></span></span></span></p>', CAST(N'2020-02-08T11:21:59.737' AS DateTime), NULL, CAST(N'2020-02-08T11:21:59.737' AS DateTime), NULL, 1, NULL, NULL, 0)
INSERT [dbo].[AZ_Products] ([Id], [Name], [TypeTransaction], [TypeProperty], [ProjectId], [City], [District], [Ward], [Street], [Area], [Price], [Unit], [Address], [HouseDirection], [BalconyDirection], [NumOfFloor], [NumOfBedroom], [NumOfWcs], [ImageMain], [ImageList], [Summary], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsActive], [IsDelete], [DeletedDate], [IsVIP]) VALUES (N'e5b43c99-494b-420c-ae1e-62d44c9dba6a', N'[ BannhaAZ] Bán căn hộ tập thể K1 - Giảng Võ, Đống Đa, Hà Nội, DTSD 100m2, 3PN, 2 WC, LH 0919 679 682', 5, 27, NULL, 1, 1, 1, 81, 100, 2.05, 8, N'Cát Linh, Đống Đa', 18, 18, NULL, 3, 2, N'/Files/0502010254_ec7dc9d0a2965bc80287_thumbail.jpg', N'[{"Id":0,"Name":"/Files/0502010253_feb1eb4e810878562119.jpg"},{"Id":1,"Name":"/Files/0502010254_ec7dc9d0a2965bc80287.jpg"},{"Id":2,"Name":"/Files/0502010253_ddef4a002e46d7188e57.jpg"},{"Id":3,"Name":"/Files/0502010252_bc8e151e7e588706de49.jpg"},{"Id":4,"Name":"/Files/0502010254_e9632ad44192b8cce183.jpg"},{"Id":5,"Name":"/Files/0502010255_f011e96f83297a772338.jpg"},{"Id":6,"Name":"/Files/0502010257_ff29e1538815714b2804.jpg"}]', N'<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.0pt">B&aacute;n nh&agrave; căn hộ l&ocirc; g&oacute;c tập thể K1 &ndash; Giảng V&otilde;, Đống Đa, H&agrave; Nội, DT100m2, LH 0919 679 682</span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.0pt">Diện t&iacute;ch sử dụng căn hộ l&agrave; 100m2, 3 ph&ograve;ng ngủ, Ph&ograve;ng kh&aacute;ch, ph&ograve;ng ăn ri&ecirc;ng biệt, căn hộ l&ocirc; g&oacute;c n&ecirc;n cực kỳ th&ocirc;ng tho&aacute;ng, c&oacute; 2 nh&agrave; tắm v&agrave; nh&agrave; vệ sinh.</span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.0pt">Hiện căn hộ đ&atilde; được chủ nh&agrave; cải tạo, kh&aacute;ch mua về c&oacute; thể ở ngay, chủ nh&agrave; cũng để lại nhiều đồ nội thất.</span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.0pt">Vị tr&iacute;: Căn hộ thuộc tập thể của TỔNG CỤC Thống k&ecirc;, T&ograve;a K1, mặt phố Giảng V&otilde;, để xe m&aacute;y thoải m&aacute;i, an ninh tốt, Giao th&ocirc;ng thuận tiện đi c&aacute;c tuyến phố Giảng V&otilde;, C&aacute;t Linh, H&agrave;o Nam, L&aacute;ng hạ&hellip;thuận tiện c&aacute;c trường học cấp 1,2,3 &hellip;. Gần nhiều trường Đại Học như Mỹ Thuật, Văn H&oacute;a&hellip;</span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.0pt">Sổ đỏ ch&iacute;nh chủ, Gi&aacute; 2.05 tỷ</span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.0pt">Li&ecirc;n hệ để xem nh&agrave; 24/7: &nbsp;0919 679 682 (gặp Chức &ndash; Miễn TG)</span></span></span></p>', CAST(N'2020-02-05T13:12:14.627' AS DateTime), NULL, CAST(N'2020-02-08T11:26:42.140' AS DateTime), NULL, 1, NULL, NULL, 1)
INSERT [dbo].[AZ_Products] ([Id], [Name], [TypeTransaction], [TypeProperty], [ProjectId], [City], [District], [Ward], [Street], [Area], [Price], [Unit], [Address], [HouseDirection], [BalconyDirection], [NumOfFloor], [NumOfBedroom], [NumOfWcs], [ImageMain], [ImageList], [Summary], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsActive], [IsDelete], [DeletedDate], [IsVIP]) VALUES (N'0dae6a52-d339-41f0-bf28-631b7f3ea10f', N'Bán gấp!!! Nhà Phố Láng Hạ Diện Tích 45m Mặt tiền 4m xây 5 Tầng', 2, 3, NULL, 1, 1, 1, 1, 40, 23450000, 3, N'Ngõ 123 Trần Cung', NULL, NULL, 2, 4, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0)
INSERT [dbo].[AZ_Products] ([Id], [Name], [TypeTransaction], [TypeProperty], [ProjectId], [City], [District], [Ward], [Street], [Area], [Price], [Unit], [Address], [HouseDirection], [BalconyDirection], [NumOfFloor], [NumOfBedroom], [NumOfWcs], [ImageMain], [ImageList], [Summary], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsActive], [IsDelete], [DeletedDate], [IsVIP]) VALUES (N'363f0a8a-2317-4466-ab9c-6a7409defa6f', N'[ BannhaAZ ] Bán nhà phố Dịch Vọng, phân lô, xây mới, phường Dịch Vọng, Quận Cầu Giấy, DT32m2x5T, 3,55 tỷ LH 0919 679 682', 5, 28, NULL, 1, 3, 33, 4, 32, 3.55, 8, N'Phường Dịch Vọng, Quận Cầu Giấy', 18, 18, 5, 4, 4, N'/Files/0502090708_b15d780f5ad8a386fac9_thumbail.jpg', N'[{"Id":0,"Name":"/Files/0502090708_b15d780f5ad8a386fac9.jpg"},{"Id":1,"Name":"/Files/0502090708_35a0fc4f5f7da623ff6c.jpg"},{"Id":2,"Name":"/Files/0502090709_2e4b6f5bcc6935376c78.jpg"},{"Id":3,"Name":"/Files/0502090708_1ee525e486d67f8826c7.jpg"},{"Id":4,"Name":"/Files/0502090709_3a74076da45f5d01044e.jpg"},{"Id":5,"Name":"/Files/0502090709_f3f15c17ff25067b5f34.jpg"},{"Id":6,"Name":"/Files/0502090709_312efb075835a16bf824.jpg"}]', N'<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">B&aacute;n nh&agrave; x&acirc;y mới, phường Dịch Vọng, Quận Cầu Giấy, DT32m2x5T, 3,55 tỷ</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">X&acirc;y dựng theo phong c&aacute;ch hiện đại, tối ưu h&oacute;a c&ocirc;ng năng sử dụng của căn nh&agrave;.</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Tầng 1: Ph&ograve;ng kh&aacute;ch, ph&ograve;ng bếp, ph&ograve;ng ăn</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Tầng 2 Ph&ograve;ng ngủ rộng (hoặc bố tr&iacute; th&ecirc;m ph&ograve;ng kh&aacute;ch), nh&agrave; tắm vệ sinh. C&oacute; ban c&ocirc;ng</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Tầng 3, 4 Mỗi tầng 1 ph&ograve;ng ngủ rộng, nh&agrave; tắm vệ sinh, c&oacute; ban c&ocirc;ng</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Tầng 5: Ph&ograve;ng ngủ nhỏ, ph&ograve;ng thờ, khu m&aacute;y giặt, ban c&ocirc;ng s&acirc;n phơi th&ocirc;ng minh</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Nội thất: Thiết bị vệ sinh TOTO, cửa 2 lớp, lớp cửa cuốn v&agrave; lớp cửa gỗ Lim &ndash; K&iacute;nh, Gạch men cao cấp, s&agrave;n gỗ c&aacute;c tầng ph&ograve;ng ngủ, r&egrave;m cửa, b&igrave;nh n&oacute;ng lạnh, tủ bếp bền đẹp, Nh&agrave; 3,55 tỷ l&agrave;m nội thất ph&acirc;n kh&uacute;c 5 tỷ.</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Vị tr&iacute;: Phường Dịch Vọng, trung t&acirc;m của quận Cầu Giấy, c&aacute;ch b&atilde;i đỗ &ocirc; t&ocirc;, phố 60m, c&aacute;ch t&ograve;a HH FLC T&ograve;a TTTM Discovery mặt đường Cầu Giấy 200m, c&aacute;ch C&ocirc;ng vi&ecirc;n Cầu Giấy 600m, c&aacute;ch C&ocirc;ng vi&ecirc;n Nghĩa Đ&ocirc; 800m, c&aacute;ch Cầu Giấy 1.5km. Khu vực to&agrave;n c&ocirc;ng nh&acirc;n vi&ecirc;n chức d&acirc;n tr&iacute; cao, an ninh tốt, giao th&ocirc;ng đi lại thuận tiện.</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Sổ đỏ ch&iacute;nh chủ, kh&aacute;ch c&oacute; duy&ecirc;n sang t&ecirc;n ngay, gi&aacute; chỉ 3,55 tỷ c&oacute; thương lượng</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Li&ecirc;n hệ 0919 679 682 (gặp Chức &ndash; Miễn TG)</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Ngo&agrave;i ra Qu&yacute; kh&aacute;ch c&oacute; thể tham khảo c&aacute;c sản phẩm tương tự kh&aacute;c qua link dưới đ&acirc;y</span></span></span></span></span></span></p>', CAST(N'2020-02-05T09:12:39.723' AS DateTime), NULL, CAST(N'2020-02-08T11:26:27.220' AS DateTime), NULL, 1, NULL, NULL, 1)
INSERT [dbo].[AZ_Products] ([Id], [Name], [TypeTransaction], [TypeProperty], [ProjectId], [City], [District], [Ward], [Street], [Area], [Price], [Unit], [Address], [HouseDirection], [BalconyDirection], [NumOfFloor], [NumOfBedroom], [NumOfWcs], [ImageMain], [ImageList], [Summary], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsActive], [IsDelete], [DeletedDate], [IsVIP]) VALUES (N'b7212e63-2220-47e8-b469-6b27ac63a70d', N'Cho thue nha az2 3', 6, 40, NULL, 1, 3, 31, 1, 2, 333, 2, N'So 123 Cau giay', 18, 18, 1, 3, 4, N'/Files/0601061833_20200106175559-c179_wm.jpg', N'[{"Id":0,"Name":"/Files/0512105113_choi-gi-o-ubud-bali-vietnamembassy-indonesia.org4_.jpg"},{"Id":1,"Name":"/Files/0512105113_swing.jpg"}]', N'', CAST(N'2020-01-06T18:43:20.803' AS DateTime), NULL, CAST(N'2020-01-06T18:43:20.803' AS DateTime), NULL, 0, 1, NULL, 1)
INSERT [dbo].[AZ_Products] ([Id], [Name], [TypeTransaction], [TypeProperty], [ProjectId], [City], [District], [Ward], [Street], [Area], [Price], [Unit], [Address], [HouseDirection], [BalconyDirection], [NumOfFloor], [NumOfBedroom], [NumOfWcs], [ImageMain], [ImageList], [Summary], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsActive], [IsDelete], [DeletedDate], [IsVIP]) VALUES (N'ef61b2e2-cd28-46e1-8214-6f2209037acb', N'Bán nhà chính chủ Phố Thành Công, P Thành Công, Ba Đình, DT42m2x5T, giá 6,2 tỷ LH 0919 679 682', 5, 28, NULL, 1, 2, 28, 229, 42, 6.2, 8, N'', 18, 18, 5, 4, 4, N'/Files/1102102745_20190529142634-754e_thumbail.jpg', N'[{"Id":0,"Name":"/Files/1102102745_6d411e3b1d67e439bd76.jpg"},{"Id":1,"Name":"/Files/1102102745_20190528082747-52a5_wm.jpg"},{"Id":2,"Name":"/Files/1102102745_2381f53af6660f385677.jpg"},{"Id":3,"Name":"/Files/1102102745_20190528082741-0ad1_wm.jpg"},{"Id":4,"Name":"/Files/1102102745_20190528082745-5c20_wm.jpg"},{"Id":6,"Name":"/Files/1102102745_20190528082741-0ad1_wm.jpg"},{"Id":7,"Name":"/Files/1102102746_5481f91c4b70b22eeb61.jpg"},{"Id":8,"Name":"/Files/1102102745_6d411e3b1d67e439bd76.jpg"}]', N'<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">B&aacute;n nh&agrave; ch&iacute;nh chủ Phố Th&agrave;nh C&ocirc;ng, P Th&agrave;nh C&ocirc;ng, Ba Đ&igrave;nh, DT42m2x5T, gi&aacute; 6,2 tỷ</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Diện t&iacute;ch 42m2, mặt tiền 4.5m, ng&otilde; trước nh&agrave; 4m, ng&otilde; trước nh&agrave; trải nhựa, sạch đẹp</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Thiết kế hiện đại, phong c&aacute;ch ch&acirc;u &acirc;u, Kh&aacute;ch xem l&agrave; ưng</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Tầng 1: Ph&ograve;ng kh&aacute;ch, nh&agrave; vệ sinh phụ, nh&agrave; xe</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Tầng 2: Ph&ograve;ng bếp + ăn, nh&agrave; WC, ph&ograve;ng ngủ rộng</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Tầng 3,4 Mỗi tầng 2 ph&ograve;ng ngủ</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Tầng 5, Ph&ograve;ng thờ s&acirc;n phơi</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Nội thất: Căn nh&agrave; đ&atilde; được trang bị nội thất cao cấp: Cửa cuốn Nhật, của sổ Nh&ocirc;m Việt Ph&aacute;p, k&iacute;nh cường lực, đ&egrave;n trang tr&iacute;, r&egrave;m cửa, thiết bị vệ sinh Toto, trần thạch cao Vĩnh Tường.</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Vị tr&iacute;: Nằm phường Th&agrave;nh C&ocirc;ng, quận Ba Đ&igrave;nh, giao th&ocirc;ng thuận tiện đi c&aacute;c phố Nguy&ecirc;n hồng, Th&agrave;nh C&ocirc;ng, Đ&ecirc; La Th&agrave;nh, Huỳnh Th&uacute;c Kh&aacute;nh, Nguyễn Ch&iacute; Thanh&hellip;</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Sổ đỏ ch&iacute;nh chủ, gi&aacute; 6,2 tỷ c&oacute; thương lượng</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Li&ecirc;n hệ 0919 679 682 (gặp Chức - Miễn TG, B&aacute;o Mạng)</span></span></p>', CAST(N'2020-02-11T22:32:04.450' AS DateTime), NULL, CAST(N'2020-02-11T22:32:04.450' AS DateTime), NULL, 1, NULL, NULL, 0)
INSERT [dbo].[AZ_Products] ([Id], [Name], [TypeTransaction], [TypeProperty], [ProjectId], [City], [District], [Ward], [Street], [Area], [Price], [Unit], [Address], [HouseDirection], [BalconyDirection], [NumOfFloor], [NumOfBedroom], [NumOfWcs], [ImageMain], [ImageList], [Summary], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsActive], [IsDelete], [DeletedDate], [IsVIP]) VALUES (N'2d1fbbd4-e23a-4162-8da7-70f55405dafd', N'[ BannhaAZ ] Bán nhà Ngõ 36 Lê Trọng Tấn, Gần bể bơi La Khê, Hà Đông, DT50m2x4T, giá 2.8 tỷ', 5, 28, NULL, 1, 8, 131, 657, 50, 2.8, 8, N'Gần Bể Bơi La Khê', 18, 18, 5, 5, 4, N'/Files/0802113340_19788cbe5d7da523fc6c_thumbail.jpg', N'[{"Id":0,"Name":"/Files/0802113340_19788cbe5d7da523fc6c.jpg"},{"Id":1,"Name":"/Files/0802113340_d15241ce960d6e53371c.jpg"},{"Id":2,"Name":"/Files/0802113341_dbb5937c42bfbae1e3ae.jpg"},{"Id":3,"Name":"/Files/0802113341_3d94e41733d4cb8a92c5.jpg"},{"Id":4,"Name":"/Files/0802113340_23581292c3513b0f6240.jpg"},{"Id":5,"Name":"/Files/0802113438_5dc9bf096eca9694cfdb.jpg"}]', N'<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">B&aacute;n nh&agrave; Ng&otilde; 36 L&ecirc; Trọng Tấn, Gần bể bơi La Kh&ecirc;, H&agrave; Đ&ocirc;ng, DT50m2x4T, gi&aacute; 2.8 tỷ</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Diện t&iacute;ch 50m2 sổ, nh&agrave; x&acirc;y 4 tầng, khung b&ecirc; t&ocirc;ng chắc chắn, thiết kế đẹp c&oacute; tiểu cảnh, c&oacute; ban c&ocirc;ng, tho&aacute;ng trước sau, ph&iacute;a trước nh&agrave; rộng 3m, nh&agrave; đối diện l&agrave; vườn c&acirc;y n&ecirc;n rất tho&aacute;ng m&aacute;t.</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Thiết kế: cầu thang giữa chia tầng 2 ph&ograve;ng.</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Tầng 1: Ph&ograve;ng kh&aacute;ch, bếp+ nh&agrave; ăn, nh&agrave; vệ sinh phụ, khu tiểu cảnh</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Tầng 2,3 Mỗi tầng 2 ph&ograve;ng ngủ, nh&agrave; vệ sinh</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Tầng 4: Ph&ograve;ng thờ, khu m&aacute;y giặt, s&acirc;n phơi</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Vị tr&iacute;: đi ng&otilde; 36 L&ecirc; Trọng tấn, gần bể bơi La Kh&ecirc;, c&aacute;ch đường L&ecirc; Trọng Tấn chừng 150m, gần c&aacute;c trường Mẫu Gi&aacute;o, cấp 1,2 La Kh&ecirc;, giao th&ocirc;ng thuận tiện.</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Kh&aacute;ch thiện ch&iacute; mua, gia đ&igrave;nh để l&agrave; cho điều h&ograve;a , bếp nấu, r&egrave;m cửa, v&agrave; 1 số nội thất kh&aacute;ch</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Sổ đỏ ch&iacute;nh chủ, gi&aacute; 2,8 tỷ c&oacute; thương lượng, bao ph&iacute; sang t&ecirc;n.</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Li&ecirc;n hệ 0919 679 682 gặp Chức (Miễn TG, MG)</span></span></span></span></span></span></p>', CAST(N'2020-02-08T11:36:51.200' AS DateTime), NULL, CAST(N'2020-02-10T23:23:54.787' AS DateTime), NULL, 1, NULL, NULL, 0)
INSERT [dbo].[AZ_Products] ([Id], [Name], [TypeTransaction], [TypeProperty], [ProjectId], [City], [District], [Ward], [Street], [Area], [Price], [Unit], [Address], [HouseDirection], [BalconyDirection], [NumOfFloor], [NumOfBedroom], [NumOfWcs], [ImageMain], [ImageList], [Summary], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsActive], [IsDelete], [DeletedDate], [IsVIP]) VALUES (N'70d77af6-76ce-4241-868d-7301ca9164df', N'[ BannhaAZ ] Bán nhà Hà Trì 1, Phố Bà Triệu, Hà Đông, ô tô đỗ cổng, DT40m2x5T, giá 3.5 tỷ', 5, 28, NULL, 1, 8, 129, 668, 40, 3.5, 8, N'Khu PL Dân trí cao', 18, 18, 5, 4, 4, N'/Files/0802111132_9fa1cfec753a8d64d42b_thumbail.jpg', N'[{"Id":0,"Name":"/Files/0802111132_9fa1cfec753a8d64d42b.jpg"}]', N'<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Ch&iacute;nh chủ b&aacute;n nh&agrave; mới Khu PL C&aacute;n bộ, D&acirc;n tr&iacute; cao, Khu H&agrave; Tr&igrave; 1, H&agrave; Đ&ocirc;ng, DT40m2x5T</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Căn nh&agrave; được thiết kế theo phong c&aacute;ch hiện đại, tối ưu h&oacute;a c&ocirc;ng năng sử dụng, &ocirc; t&ocirc; quay đầu c&aacute;ch nh&agrave; 6m</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Tầng 1: Để&nbsp; xe, Ph&ograve;ng kh&aacute;ch, Ph&ograve;ng bếp + ăn cực rộng, nh&agrave; vệ sinh phụ.</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Tầng 2: Ph&ograve;ng kh&aacute;ch rộng (hoặc sinh hoạt chung), nh&agrave; tắm rộng, ph&ograve;ng ngủ, c&oacute; ban c&ocirc;ng</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Tầng 3,4 Mỗi tầng 2 Ph&ograve;ng ngủ</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Tầng 5 Gian thờ, khu m&aacute;y giặt, s&acirc;n phơi rộng</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Nội thất: Được trang bị nội thất cao cấp, Cửa cuốn, tay vịn cầu thang l&agrave;m bằng gỗ Lim nguy&ecirc;n tấm, thiết bị vệ sinh Toto, cửa sổ nh&ocirc;m Việt Ph&aacute;p.</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Vị tr&iacute;: C&aacute;ch mặt phố v&agrave;i bước ch&acirc;n, ng&otilde; trước nh&agrave; rộng tr&ecirc;n 4m, ng&atilde; 3 ngay cạnh nh&agrave; n&ecirc;n &ocirc; t&ocirc; quay đầu thoải m&aacute;i, Khu d&acirc;n cư d&acirc;n tr&iacute; cao, khu hưu tr&iacute; &ndash; H&agrave; Tr&igrave;, giao th&ocirc;ng thuận tiện, đi c&aacute;c tuyến phố ch&iacute;nh như T&ocirc; Hiệu, Quang Trung, Ph&ugrave;ng Hưng&hellip;.</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Sổ đỏ ch&iacute;nh chủ, gi&aacute; 3,5 tỷ c&oacute; thương lượng</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Li&ecirc;n hệ ngay 0919 679 682 gặp Chức để được tư vấn.</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Hỗ trợ thủ tục vay vốn ng&acirc;n h&agrave;ng, ph&aacute;p l&yacute; mua b&aacute;n nh&agrave; đất.</span></span></span></span></span></span></p>', CAST(N'2020-02-08T11:16:53.557' AS DateTime), NULL, CAST(N'2020-02-08T11:16:53.557' AS DateTime), NULL, 1, NULL, NULL, 0)
INSERT [dbo].[AZ_Products] ([Id], [Name], [TypeTransaction], [TypeProperty], [ProjectId], [City], [District], [Ward], [Street], [Area], [Price], [Unit], [Address], [HouseDirection], [BalconyDirection], [NumOfFloor], [NumOfBedroom], [NumOfWcs], [ImageMain], [ImageList], [Summary], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsActive], [IsDelete], [DeletedDate], [IsVIP]) VALUES (N'7270f5d2-5635-4c59-bf25-8969ef7c3413', N'[ BannhaAZ ] Bán nhà Ngõ 2 Vạn Phúc – Khu Ẩm thực Vạn Phúc, Hà Đông, DT35m2x5T, giá 2.6 tỷ', 5, 28, NULL, 1, 8, 139, 664, 35, 2.6, 8, N'Ngõ 2 - Làng Ẩm Thực', 18, 18, 5, 5, 4, N'/Files/0802094642_25765ca69c7e64203d6f_thumbail.jpg', N'[{"Id":0,"Name":""}]', N'<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">B&aacute;n nh&agrave; Ng&otilde; 2 Vạn Ph&uacute;c &ndash; Khu Ẩm thực Vạn Ph&uacute;c, H&agrave; Đ&ocirc;ng, DT35m2x5T, gi&aacute; nhỉnh 2,5 tỷ</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Căn nh&agrave; được x&acirc;y theo phong c&aacute;ch T&acirc;n cổ điển, ph&ugrave; hợp phong c&aacute;ch sống hiện đại</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Diện t&iacute;ch 35m2, mặt tiền gần 3,5m, cầu thang giữa chia 2 phần, c&oacute; s&acirc;n chung để xe m&aacute;y thoải m&aacute;i.</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Tầng 1: Ph&ograve;ng kh&aacute;ch, ph&ograve;ng bếp + ăn ri&ecirc;ng biệt, nh&agrave; vệ sinh</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Tầng 2,3,4 Mỗi tầng 2 ph&ograve;ng, nh&agrave; tắm vệ sinh</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Tầng 5: Khu m&aacute;y giặt s&acirc;n phơi.</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Nội thất mới 100%, h&agrave;ng cao cấp, Cửa cầu thang l&agrave;m bằng gỗ Lim nguy&ecirc;n tấm, cửa sổ gỗ Xingfa.</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Vị tr&iacute;: Thuộc khu d&acirc;n cư khu phố L&agrave;ng Ẩm Thực Vạn Ph&uacute;c, đầy đủ tiện &iacute;ch từ Trường học c1,2,3 cho c&aacute;c ch&aacute;u, Y tế, giao th&ocirc;ng thuận tiện.</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Sổ đỏ ch&iacute;nh chủ, gi&aacute; 2,6 tỷ c&oacute; thương lượng, hỗ trợ sang t&ecirc;n giấy tờ.</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Li&ecirc;n hệ ngay 0919 679 682 để được tư vấn.</span></span></p>', CAST(N'2020-02-08T09:48:48.537' AS DateTime), NULL, CAST(N'2020-02-08T11:26:49.027' AS DateTime), NULL, 1, NULL, NULL, 0)
INSERT [dbo].[AZ_Products] ([Id], [Name], [TypeTransaction], [TypeProperty], [ProjectId], [City], [District], [Ward], [Street], [Area], [Price], [Unit], [Address], [HouseDirection], [BalconyDirection], [NumOfFloor], [NumOfBedroom], [NumOfWcs], [ImageMain], [ImageList], [Summary], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsActive], [IsDelete], [DeletedDate], [IsVIP]) VALUES (N'4b00a897-f9d3-4935-a5b9-8a3c71fa4863', N'Bán nhà phân lô đường Phan Đình Giót, La Khê, Hà Đông, DT32m2x5T, giá 2,4 tỷ LH 0971 968 089', 5, 28, NULL, 1, 8, 131, 719, 32, 2.4, 8, N'', 18, 18, 5, 3, 4, N'/Files/1102065846_431a685cca9e32c06b8f_thumbail.jpg', N'[{"Id":0,"Name":"/Files/1102065846_b96e372995eb6db534fa.jpg"}]', N'<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.0pt">B&aacute;n nh&agrave; ph&acirc;n l&ocirc; đường Phan Đ&igrave;nh Gi&oacute;t, La Kh&ecirc;, H&agrave; Đ&ocirc;ng, DT32m2x5T, gi&aacute; 2,4 tỷ</span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.0pt">Căn nh&agrave; ph&acirc;n l&ocirc;, thửa đất vu&ocirc;ng vắn: Diện t&iacute;ch 32m2, mặt tiền 3,8m, sổ vu&ocirc;ng h&igrave;nh hộp di&ecirc;m.</span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.0pt">Căn nh&agrave; thiết kế hiện đại, tối ưu h&oacute;a c&ocirc;ng năng sử dụng</span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.0pt">Tầng 1: Ph&ograve;ng kh&aacute;ch, gian bếp + b&agrave;n ăn, nh&agrave; vệ sinh phụ</span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.0pt">Tầng 2,3,4 Mỗi tầng 1 ph&ograve;ng ngủ rộng, nh&agrave; tắm vệ sinh từng tầng, cửa sổ tho&aacute;ng m&aacute;t.</span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.0pt">Tầng 5: Ph&ograve;ng thờ s&acirc;n phơi</span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.0pt">Nội thất: Thiết bị nội thất mới 100%, sang trọng, Hai lớp cửa, lớp cửa ch&iacute;nh gỗ Lim, tay vịn gỗ Lim, cửa sổ Nh&ocirc;m Việt Ph&aacute;p&hellip;</span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.0pt">Vị tr&iacute;: Trung t&acirc;m phường La Kh&ecirc;, H&agrave; Đ&ocirc;ng, giao th&ocirc;ng thuận tiện đi Quang Trung, Tố Hữu, L&ecirc; Trọng Tấn, thuận tiện c&aacute;c trường học từ Mầm non, Tiểu học, THCS của La Kh&ecirc;.</span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.0pt">Sổ đỏ ch&iacute;nh chủ, gi&aacute; 2,4 tỷ c&oacute; thương lượng</span></span></span></p>

<p>Li&ecirc;n hệ Mr Th&aacute;i 0971 968 089<span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.0pt"> &ndash; Miễn TG, MG)</span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.0pt">Tư vấn hỗ trợ thủ tuc vay vốn ng&acirc;n h&agrave;ng, l&atilde;i xuất ưu đ&atilde;i tốt.</span></span></span></p>', CAST(N'2020-02-11T19:00:06.843' AS DateTime), NULL, CAST(N'2020-02-11T19:00:06.843' AS DateTime), NULL, 1, NULL, NULL, 0)
INSERT [dbo].[AZ_Products] ([Id], [Name], [TypeTransaction], [TypeProperty], [ProjectId], [City], [District], [Ward], [Street], [Area], [Price], [Unit], [Address], [HouseDirection], [BalconyDirection], [NumOfFloor], [NumOfBedroom], [NumOfWcs], [ImageMain], [ImageList], [Summary], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsActive], [IsDelete], [DeletedDate], [IsVIP]) VALUES (N'812681cb-cc76-47ae-b98a-90f05d809571', N'Bán nhà KINH DOANH NHÀ NGHỈ, DOANH THU LỚN, Hoàng Cầu, Đống Đa, DT50m2x5T, 6P, 5,6 tỷ', 5, 28, NULL, 1, 1, 9, 86, 50, 5.6, 8, N'Ngã 5 Hoàng Cầu, Ô chợ dừa', 18, 18, 5, 6, 4, N'/Files/1102103336_25e6e12eeed717894ec6_thumbail.jpg', N'[{"Id":0,"Name":"/Files/1102103336_4cccaf98a161583f0170.jpg"},{"Id":1,"Name":"/Files/1102103336_6d4b0b3d05c4fc9aa5d5.jpg"},{"Id":2,"Name":"/Files/1102103335_43bb2c6d2394daca8385.jpg"},{"Id":3,"Name":"/Files/1102103335_30c156cd5834a16af825.jpg"},{"Id":4,"Name":"/Files/1102103335_2dcdecb4e24d1b13425c.jpg"},{"Id":5,"Name":"/Files/1102103336_62d0a593aa6a53340a7b.jpg"},{"Id":6,"Name":"/Files/1102103336_ea515a3a54c3ad9df4d2.jpg"}]', N'<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">B&aacute;n nh&agrave; KINH DOANH NH&Agrave; NGHỈ, DOANH THU LỚN, Ho&agrave;ng Cầu, Đống Đa, DT50m2x5T, 6P, 5,6 tỷ</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Diện t&iacute;ch 50m2, x&acirc;y mới 5 tầng, c&oacute; 6 ph&ograve;ng ngủ kh&eacute;p k&iacute;n.</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Thiết kế hiện đại, theo ph&acirc;n kh&uacute;c gia đ&igrave;nh ở ph&ograve;ng kh&eacute;p k&iacute;n.</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Tầng 1: Sảnh, để xe, quầy, bếp ăn + ph&ograve;ng ăn</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Tầng 2,3,4 Mỗi tầng 2 ph&ograve;ng ngủ kh&eacute;p k&iacute;n</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Tầng 5: Ph&ograve;ng thờ s&acirc;n phơi.</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Nội thất: Chủ nh&agrave; trang bị đầy đủ v&agrave; cao cấp: Thiết bị vệ sinh INAX loại 1, Điều h&ograve;a c&aacute;c ph&ograve;ng, r&egrave;m cửa, đ&acirc;y điện Trần Ph&uacute;, trần Thạch Cao Vĩnh Tường&hellip;</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Vị tr&iacute;: gần ng&atilde; 3 &Ocirc; chợ Dừa, Ho&agrave;ng Cầu, khu trung t&acirc;m d&acirc;n cư, văn ph&ograve;ng c&ocirc;ng ty đ&ocirc;ng đ&uacute;c, thuận tiện giao th&ocirc;ng&hellip;.</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Ph&ugrave; hợp: Gia đ&igrave;nh c&oacute; nhu cầu c&oacute; ph&ograve;ng ngủ kh&eacute;p k&iacute;n, sạch sẽ, gần Phố (c&aacute;ch phố 12m), hoặc vừa ở vừa KINH DOANH NH&Agrave; NGHỈ, Nh&agrave; Trọ&hellip;</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Sổ đỏ ch&iacute;nh chủ, gi&aacute; 5,6 tỷ c&oacute; thương lượng</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Li&ecirc;n hệ 0919 679 682 (gặp Chức &ndash; Miễn TG)</span></span></span></span></span></span></p>', CAST(N'2020-02-11T22:36:12.553' AS DateTime), NULL, CAST(N'2020-02-11T22:36:12.553' AS DateTime), NULL, 1, NULL, NULL, 0)
INSERT [dbo].[AZ_Products] ([Id], [Name], [TypeTransaction], [TypeProperty], [ProjectId], [City], [District], [Ward], [Street], [Area], [Price], [Unit], [Address], [HouseDirection], [BalconyDirection], [NumOfFloor], [NumOfBedroom], [NumOfWcs], [ImageMain], [ImageList], [Summary], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsActive], [IsDelete], [DeletedDate], [IsVIP]) VALUES (N'87e09f07-fefa-4597-9c17-9d4bbb960863', N'Bán nhà PL Biệt thự sân vườn, Đường Đội Cấn, P.Cống Vị, Q. Ba Đình, DT đất 70m2, giá 6,5 tỷ', 5, 28, NULL, 1, 2, 30, 160, NULL, 6.5, 8, N'', 18, 18, 5, NULL, 5, N'/Files/1702125610_20200217103220-f593_wm_thumbail.jpg', N'[{"Id":0,"Name":"/Files/1702125610_20200217103220-f593_wm.jpg"},{"Id":1,"Name":"/Files/1702125610_20200217103222-a1cd_wm.jpg"},{"Id":2,"Name":"/Files/1702125608_20200217103221-9b52_wm.jpg"},{"Id":3,"Name":"/Files/1702125607_20200217103221-bb47_wm.jpg"},{"Id":4,"Name":"/Files/1702125612_20200217103221-9fce_wm.jpg"},{"Id":5,"Name":""},{"Id":6,"Name":""}]', N'<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">B&aacute;n nh&agrave; PL Biệt thự s&acirc;n vườn, Đường Đội Cấn, P.Cống Vị, Q. Ba Đ&igrave;nh, DT đất 70m2, gi&aacute; 6,5 tỷ</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Căn nh&agrave; thiết kế cực đẹp, x&acirc;y dựng chắc chắn 5 tầng thiết kế hiện đại, c&oacute; s&acirc;n vườn, ot&ocirc; v&agrave;o nh&agrave;</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Diện t&iacute;ch 70m2, mặt tiền nh&agrave; 6m, nh&agrave; x&acirc;y 5 tầng, c&oacute; s&acirc;n vườn, gara &ocirc; t&ocirc;</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">M&ocirc; tả thiết kế:</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Phần s&acirc;n c&ocirc;ng, l&agrave;m gara, xe m&aacute;y, khu vực trồng c&acirc;y cảnh, c&oacute; cổng sắt.</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Tầng 1: Ph&ograve;ng kh&aacute;ch, bếp, ph&ograve;ng ăn rộng r&atilde;i, nh&agrave; vệ sinh phụ</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Tầng 2,3: Mỗi tầng 1 ph&ograve;ng ngủ rộng vs kh&eacute;p k&iacute;n</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Tầng 4: Chia 2 ph&ograve;ng ngủ, vệ sinh chung</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Tầng 5: Gian thờ, s&acirc;n phơi</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Nội thất c&aacute;c ph&ograve;ng đều c&oacute; cửa sổ hệ Việt Ph&aacute;p an to&agrave;n v&agrave; rất tho&aacute;ng, trần thạch cao Vĩnh Tường, cầu thang bộ v&agrave; cửa ph&ograve;ng gỗ Lim Nam Phi, s&agrave;n gỗ Malaysia, hệ thống điện được thiết kế tiện &iacute;ch an to&agrave;n nhất. Thiết bị vệ sinh ph&ograve;ng tắm 100% TOTO,...</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Vị tr&iacute;: Địa chỉ thuộc phường Vĩnh Ph&uacute;c giao th&ocirc;ng thuận tiện đi c&aacute;c tuyến phố ĐỘi Cấn, Bưởi, Ho&agrave;ng Quốc Việt, V&otilde; Ch&iacute; c&ocirc;ng đi s&acirc;n bay Nội B&agrave;i, đầy đủ tiện &iacute;ch của trung t&acirc;m quận Ba Đ&igrave;nh.</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Sổ đỏ ch&iacute;nh chủ, gi&aacute; 6,5 tỷ c&oacute; thương lượng</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Li&ecirc;n hệ 0919 679 682 (gặp chủ nh&agrave; trực tiếp thương lượng gi&aacute;)</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Qu&yacute; kh&aacute;ch tham khảo th&ecirc;m sản phẩm c&ugrave;ng ph&acirc;n kh&uacute;c t&agrave;i ch&iacute;nh, khu vực theo link dưới đ&acirc;y:</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><a href="http://bannhaaz.com/?keySearch=&amp;type=5&amp;prop=28&amp;price=47&amp;city=1&amp;district=2" style="color:blue; text-decoration:underline">http://bannhaaz.com/?keySearch=&amp;type=5&amp;prop=28&amp;price=47&amp;city=1&amp;district=2</a></span></span></p>', CAST(N'2020-02-17T12:59:19.517' AS DateTime), NULL, CAST(N'2020-02-17T12:59:19.517' AS DateTime), NULL, 1, NULL, NULL, 0)
INSERT [dbo].[AZ_Products] ([Id], [Name], [TypeTransaction], [TypeProperty], [ProjectId], [City], [District], [Ward], [Street], [Area], [Price], [Unit], [Address], [HouseDirection], [BalconyDirection], [NumOfFloor], [NumOfBedroom], [NumOfWcs], [ImageMain], [ImageList], [Summary], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsActive], [IsDelete], [DeletedDate], [IsVIP]) VALUES (N'521be562-feab-4d28-8aee-a237fc2f76cd', N'Chính chủ gia đình cần bán căn hộ tập thể tầng 2 khu D Bắc Thành Công, Ngõ 9 Đường Nguyên Hồng, Quận Ba Đình.', 5, 27, NULL, 1, 2, 28, 205, 50, 1.4, 8, N'', 18, 18, 2, 2, 2, N'/Files/1702115507_b8d2eeb9785a9804c14b_thumbail.jpg', N'[{"Id":0,"Name":"/Files/1702115505_a6810aef9c0c7c52251d.jpg"},{"Id":1,"Name":"/Files/1702115505_8d5c503dc6de26807fcf.jpg"},{"Id":2,"Name":"/Files/1702115505_6050d43742d4a28afbc5.jpg"},{"Id":3,"Name":"/Files/1702115503_20180928103805-9d27_wm.jpg"},{"Id":4,"Name":"/Files/1702115503_20180928103805-9d27_wm.jpg"},{"Id":5,"Name":"/Files/1702115503_20180928103805-9d27_wm.jpg"}]', N'<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Ch&iacute;nh chủ gia đ&igrave;nh cần b&aacute;n căn hộ tập thể tầng 2 khu D Bắc Th&agrave;nh C&ocirc;ng, Quận Ba Đ&igrave;nh.</span></span></span></span><br />
<span style="font-size:10.5pt"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black"><span style="background-color:white">Căn hộ tập thể với diện t&iacute;ch sử dụng 50m2, kh&ocirc;ng gian tho&aacute;ng đ&atilde;ng</span></span></span></span></span></span></p>

<p><br />
<span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black"><span style="background-color:white">Thiết kế gồm: 1 ph&ograve;ng kh&aacute;ch, 1 ph&ograve;ng ngủ rất rộng (c&oacute; thể chia 2 ph&ograve;ng), 1 ph&ograve;ng bếp, 2 nh&agrave; tắm, vệ sinh rộng. Nh&agrave; đ&atilde; cải tạo rất đẹp như chung cư mới.</span></span></span></span></span></span></p>

<p><br />
<span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black"><span style="background-color:white">To&agrave;n bộ đồ cao cấp như tủ bếp An Cường, s&agrave;n gỗ Malaysia vệ sinh v&aacute;ch tắm đứng... Kh&aacute;ch chỉ việc đến ở lu&ocirc;n (ảnh thật của nh&agrave;).</span></span></span></span></span></span></p>

<p><br />
<span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black"><span style="background-color:white">T&ograve;a nh&agrave; nằm khu vực d&acirc;n tr&iacute; cao, khu vực trung t&acirc;m gần trường mầm non, tiểu học Th&agrave;nh C&ocirc;ng, THCS Th&agrave;nh C&ocirc;ng. Đi sang phố L&aacute;ng Hạ, Đ&ecirc; La Th&agrave;nh, trung t&acirc;m chiếu phim Quốc Gia 200m.</span></span></span></span></span></span></p>

<p><br />
<span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black"><span style="background-color:white">Sổ đỏ ch&iacute;nh chủ, gi&aacute; b&aacute;n 1.4 tỷ (c&oacute; thương lượng).</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Li&ecirc;n hệ để xem v&agrave; thương lượng trực tiếp chủ nh&agrave;: 0919 679 682 (gặp Chức &ndash; MTG)</span></span></span></span></span></span></p>', CAST(N'2020-02-17T11:58:37.530' AS DateTime), NULL, CAST(N'2020-02-17T22:10:19.593' AS DateTime), NULL, 1, NULL, NULL, 1)
INSERT [dbo].[AZ_Products] ([Id], [Name], [TypeTransaction], [TypeProperty], [ProjectId], [City], [District], [Ward], [Street], [Area], [Price], [Unit], [Address], [HouseDirection], [BalconyDirection], [NumOfFloor], [NumOfBedroom], [NumOfWcs], [ImageMain], [ImageList], [Summary], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsActive], [IsDelete], [DeletedDate], [IsVIP]) VALUES (N'ca481327-5e8c-4d32-888c-b1f2ebef0894', N'Chuyển nhượng lô đất mặt đường số 188 Trung Lực, Đằng Lâm, Hải An, Hải Phòng', 5, 29, NULL, 1, 2, 21, 145, 2, 123, 9, N'So 123 Cau giay', 19, 21, 2, 3, 4, N'/Files/0601061833_20200106175559-6d9b_wm.jpg', N'[{"Id":0,"Name":"/Files/0512105113_choi-gi-o-ubud-bali-vietnamembassy-indonesia.org4_.jpg"},{"Id":1,"Name":"/Files/0512105113_swing.jpg"}]', N'Đất mặt đường nằm tại vị trí đẹp, vuông vắn, kinh doanh buôn bán thuận lợi, đường rộng vỉa hè 2 bên, dân cư đông đúc, dân trí cao, an ninh tốt.', CAST(N'2020-01-06T10:34:20.407' AS DateTime), NULL, CAST(N'2020-01-10T09:48:54.830' AS DateTime), NULL, 1, 1, NULL, 0)
INSERT [dbo].[AZ_Products] ([Id], [Name], [TypeTransaction], [TypeProperty], [ProjectId], [City], [District], [Ward], [Street], [Area], [Price], [Unit], [Address], [HouseDirection], [BalconyDirection], [NumOfFloor], [NumOfBedroom], [NumOfWcs], [ImageMain], [ImageList], [Summary], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsActive], [IsDelete], [DeletedDate], [IsVIP]) VALUES (N'c29137b9-33d6-4e34-8c81-b53292435a48', N'Bán nhà phân lô xây mới, Ngõ 28 Trần Thái Tông, Cầu Giấy, DT30m2x5T, giá 3,4 tỷ', 5, 28, NULL, 1, 3, 31, 55, 30, 3.4, 8, N'Ngõ 28', 18, 18, 5, 3, 4, N'/Files/1002112618_e4050d19f0fa08a451eb_thumbail.jpg', N'[{"Id":0,"Name":"/Files/1002112703_d3f8716f8c8c74d22d9d.jpg"},{"Id":1,"Name":"/Files/1002112702_44823a68c78b3fd5669a.jpg"},{"Id":2,"Name":"/Files/1002112702_3bc1414abca944f71db8.jpg"},{"Id":3,"Name":"/Files/1002112703_e702d5f8281bd045890a.jpg"},{"Id":4,"Name":"/Files/1002112703_e702d5f8281bd045890a.jpg"}]', N'<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">B&aacute;n nh&agrave; ph&acirc;n l&ocirc; x&acirc;y mới, Ng&otilde; 28 Trần Th&aacute;i T&ocirc;ng, Cầu Giấy, DT30m2x5T, gi&aacute; 3,4 tỷ</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Diện t&iacute;ch 30m2, mặt tiền 4,5m, nở hậu.</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Căn nh&agrave; thiết kế phong c&aacute;ch T&acirc;n cổ điển, x&acirc;y dựng m&oacute;ng b&egrave;, khung cột chắc chắn.</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Thiết kế:</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Tầng 1: Ph&ograve;ng ăn, bếp, ph&ograve;ng kh&aacute;ch, nh&agrave; vệ sinh phụ</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Tầng 2,3,4 Mỗi tầng 1 ph&ograve;ng ngủ rộng, nh&agrave; tắm, vệ sinh</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Tầng 5 Ph&ograve;ng thờ s&acirc;n phơi, đ&atilde; c&oacute; chuồng cọp.</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Nội thất: Sử dụng thiết bị vệ sinh TOTO, đ&atilde; trang bị b&igrave;nh n&oacute;ng lạnh, Ph&ograve;ng ngủ s&agrave;n gỗ nhập khẩu, tủ bếp gỗ tự nhi&ecirc;n bền đẹp, Trần thạch cao c&aacute;c ph&ograve;ng, hệ thống &aacute;nh s&aacute;ng đẹp v&agrave; tiết kiệm điện.</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Vị tr&iacute;: Nằm khu đ&ocirc;ng d&acirc;n cư phường dịch Vọng Hậu, tiếp gi&aacute;p nhiều trường ĐH lớn như ĐH Quốc Gia, Ngoại Ngữ, Học Viện B&aacute;o tr&iacute; v&agrave; tuy&ecirc;n truyền, quận Trẻ đang ph&aacute;t triển mạnh về kinh tế.</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Sổ đỏ ch&iacute;nh chủ, gi&aacute; 3,4 tỷ c&oacute; thương lượng.</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Li&ecirc;n hệ EM Chức &nbsp;0919 679 682 (Miễn TG, MG)</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Qu&yacute; kh&aacute;ch tham khảo th&ecirc;m bằng link dưới đ&acirc;y: https://youtu.be/zktdiWDi8Oc</span></span></p>', CAST(N'2020-02-10T23:28:02.187' AS DateTime), NULL, CAST(N'2020-02-10T23:33:50.720' AS DateTime), NULL, 1, NULL, NULL, 1)
INSERT [dbo].[AZ_Products] ([Id], [Name], [TypeTransaction], [TypeProperty], [ProjectId], [City], [District], [Ward], [Street], [Area], [Price], [Unit], [Address], [HouseDirection], [BalconyDirection], [NumOfFloor], [NumOfBedroom], [NumOfWcs], [ImageMain], [ImageList], [Summary], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsActive], [IsDelete], [DeletedDate], [IsVIP]) VALUES (N'2e754051-1f7c-4059-b1fa-bb442d236a58', N'Bán đất Lô Biệt Thự - Khu Hành chính Phố Hoa, DT225m2, sổ đỏ riêng LH BannhaAZ.com 0919 679 682', 5, 32, NULL, 4, 12, 185, 1152, 225, 0, 11, N'', 18, 18, 1, 1, 1, N'/Files/1702094946_Edit BT2 22_thumbail.jpg', NULL, N'<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">B&aacute;n đất L&ocirc; Biệt Thự - Khu H&agrave;nh ch&iacute;nh Phố Hoa, DT225m2, sổ đỏ ri&ecirc;ng.</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Th&ocirc;ng số thửa đất: MT 12,5m, s&acirc;u 18m, thửa đất vu&ocirc;ng vắn, hệ thống tho&aacute;t nước, điện đầy đủ.</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Tiện &iacute;ch đầy đủ: chợ Phố Hoa c&aacute;ch 50m, Khu h&agrave;nh ch&iacute;nh cạnh nh&agrave;, trường cấp 3 Hiệp H&ograve;a 2 c&aacute;ch 100m đường chim bay.</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Sổ đổ ch&iacute;nh chủ, gi&aacute; thỏa thuận,</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">B&aacute;c n&agrave;o thiện ch&iacute; quan t&acirc;m alo 0919 679 682 để được tư vấn gi&aacute;.</span></span></p>

<p>&nbsp;</p>', CAST(N'2020-02-17T21:52:48.237' AS DateTime), NULL, CAST(N'2020-02-17T21:52:48.237' AS DateTime), NULL, 1, NULL, NULL, 0)
INSERT [dbo].[AZ_Products] ([Id], [Name], [TypeTransaction], [TypeProperty], [ProjectId], [City], [District], [Ward], [Street], [Area], [Price], [Unit], [Address], [HouseDirection], [BalconyDirection], [NumOfFloor], [NumOfBedroom], [NumOfWcs], [ImageMain], [ImageList], [Summary], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsActive], [IsDelete], [DeletedDate], [IsVIP]) VALUES (N'cd8bef08-5b9c-44f9-9fc6-c31fd70f9918', N'[ BannhaAZ ] Bán nhà trong ngõ, Phố Hà Trì – Bà Triệu, P. Hà Cầu, Hà Đông, DT33m2x5T, giá 2,45 tỷ', 5, 28, NULL, 1, 8, 129, 668, 33, 2.45, 8, N'TDP Hà Trì 4', 18, 18, 5, 3, 4, N'/Files/0802095633_431a685cca9e32c06b8f_thumbail.jpg', N'[{"Id":0,"Name":"/Files/0802095633_431a685cca9e32c06b8f.jpg"}]', N'<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">B&aacute;n nh&agrave; trong ng&otilde;, Phố H&agrave; Tr&igrave; &ndash; B&agrave; Triệu, P. H&agrave; Cầu, H&agrave; Đ&ocirc;ng, DT33m2x5T, gi&aacute; 2,45 tỷ</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Ch&iacute;nh chủ cần b&aacute;n nh&agrave; trong ng&otilde; phố H&agrave; Tr&igrave; &ndash; B&agrave; Triệu, phường H&agrave; Cầu, quận H&agrave; Đ&ocirc;ngm TP H&agrave; Nội. Ng&otilde; trước nh&agrave; rộng, th&ocirc;ng tho&aacute;ng, gần phố. Nh&agrave; x&acirc;y mới, thiết kế hiện đại, m&ograve;ng b&egrave;, khung cột b&ecirc; t&ocirc;ng chắc chắn.</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Bố tr&iacute; gồm:</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">+ Tầng 1: Ph&ograve;ng kh&aacute;ch + bếp + wc.</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">+ Tầng 2, 3,4 : Mỗi tầng 1 ph&ograve;ng + 1 WC.</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">+ Tầng 5: ph&ograve;ng thờ + s&acirc;n phơi</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">- Nội thất cơ bản: Gạch chống xước 60x60mm, cầu thang gỗ, tủ bếp gỗ Sồi Nga, thiết bị vệ sinh Inax, cửa sổ cửa nhựa l&otilde;i th&eacute;p, trần thạch cao, quạt trần, đầu chờ điều ho&agrave;, n&oacute;ng lạnh.</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">- Vị tr&iacute;: C&aacute;ch chợ H&agrave; Đ&ocirc;ng khoảng 400m. Gần ng&atilde; 5 H&agrave; Tr&igrave;, quanh nh&agrave; cơ sở hạ tầng tốt, d&acirc;n tr&iacute; cao, an ninh bảo đảm, gần trường học, bệnh viện, chợ lớn rất thuận lợi cho cuộc sống hiện đại.</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">- Sổ đỏ ch&iacute;nh chủ, ph&aacute;p l&yacute; r&otilde; r&agrave;ng</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">- Gi&aacute; b&aacute;n: 2,45 tỷ (c&oacute; thương lượng ch&iacute;nh chủ v&agrave; bao sang t&ecirc;n sổ đỏ).</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">- Li&ecirc;n hệ: Anh Chức 0919 679 682 (Miễn trung gian v&agrave; Quảng c&aacute;o mạng)</span></span></span></span></span></span></p>', CAST(N'2020-02-08T10:28:10.430' AS DateTime), NULL, CAST(N'2020-02-08T10:28:10.430' AS DateTime), NULL, 1, NULL, NULL, 0)
INSERT [dbo].[AZ_Products] ([Id], [Name], [TypeTransaction], [TypeProperty], [ProjectId], [City], [District], [Ward], [Street], [Area], [Price], [Unit], [Address], [HouseDirection], [BalconyDirection], [NumOfFloor], [NumOfBedroom], [NumOfWcs], [ImageMain], [ImageList], [Summary], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsActive], [IsDelete], [DeletedDate], [IsVIP]) VALUES (N'1fde6ec5-ab01-4e6c-98a3-c39dcac41428', N'Bán nhà số 8/1/522 Ngô Gia Tự, Hải An, Hải Phòng', 6, 37, NULL, 1, 3, 31, 1, 2, 333, 14, N'So 123 Cau giay', 18, 18, 1, 3, 4, N'https://nhadatvanminh.com.vn/images/raovat/p144926.jpg', NULL, N'Nhà 3 tầng, dân xây, độc lập, chắc chắn, khung cột, móng cọc khoan nhồi, thiết kế mang phong cách hiện đại, sang trọng, thoáng mát, an ninh tốt.', CAST(N'2020-01-06T17:14:49.057' AS DateTime), NULL, CAST(N'2020-01-10T09:51:51.597' AS DateTime), NULL, 1, 1, NULL, 1)
INSERT [dbo].[AZ_Products] ([Id], [Name], [TypeTransaction], [TypeProperty], [ProjectId], [City], [District], [Ward], [Street], [Area], [Price], [Unit], [Address], [HouseDirection], [BalconyDirection], [NumOfFloor], [NumOfBedroom], [NumOfWcs], [ImageMain], [ImageList], [Summary], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsActive], [IsDelete], [DeletedDate], [IsVIP]) VALUES (N'5c0ab141-6889-4d17-9fc2-c736f6b960cf', N'Cho thuê nhà mặt đường World bank, Vĩnh Niệm, Lê Chân, Hải Phòng', 6, 37, NULL, 1, 3, 31, 1, 2, 333, 14, N'So 123 Cau giay', 18, 18, 1, 3, 4, N'https://nhadatvanminh.com.vn/images/raovat/142155.jpg', NULL, N'Nhà xây mới 3 tầng, kiên cố, cách Bệnh viện Vinmec 50m, mặt đường đôi rộng rãi, phù hợp kinh doanh, tháng 6/2020 bắt đầu đưa vào sử dụng.', CAST(N'2020-01-06T16:38:31.917' AS DateTime), NULL, CAST(N'2020-01-16T10:54:32.787' AS DateTime), NULL, 1, 1, NULL, 1)
INSERT [dbo].[AZ_Products] ([Id], [Name], [TypeTransaction], [TypeProperty], [ProjectId], [City], [District], [Ward], [Street], [Area], [Price], [Unit], [Address], [HouseDirection], [BalconyDirection], [NumOfFloor], [NumOfBedroom], [NumOfWcs], [ImageMain], [ImageList], [Summary], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsActive], [IsDelete], [DeletedDate], [IsVIP]) VALUES (N'aff15acb-1d89-4ae5-a1bd-d0296563cfb6', N'Cho thuê nhà mặt đường số 35 TĐC Nam Cầu, Đằng Hải, Hải An, Hải Phòng', 6, 37, NULL, 1, 3, 31, 1, 2, 333, 14, N'So 123 Cau giay', 18, 18, 1, 3, 4, N'https://nhadatvanminh.com.vn/images/raovat/146557.jpg', N'[{"Id":0,"Name":"/Files/1601031442_71484870_2392519194179438_7037044008845049856_n.jpg"},{"Id":1,"Name":"/Files/0512105113_swing.jpg"},{"Id":2,"Name":"/Files/0512105113_swing.jpg"}]', N'<p>Nh&agrave; x&acirc;y 5 tầng mới đẹp, d&acirc;n x&acirc;y để ở, độc lập, ki&ecirc;n cố, kiến tr&uacute;c mang phong c&aacute;ch hiện đại, trẻ trung, nội thất cao cấp, sổ hồng ch&iacute;nh chủ. s</p>

<p>In this article we will learn how to display a number in its currency format with respect to a country. When we want to display a number in its respective country&#39;s currency format, we format the string in the currency format and get the currency symbol of a specific country using the &quot;CultureInfo&quot;&nbsp;class available in .Net. To use the CultureInfo class, we need to include the &quot;System.Globalization&quot; Namespace in our program.<br />
<br />
The &quot;C&quot; format specifier converts a number to a string that represents a currency amount. The precision specifier indicates the desired number of decimal places in the result string. If the precision specifier is omitted then the default precision is used (the default value is 2).<br />
<br />
If the value to be formatted has more than the specified or the default number of decimal places then the fractional value is rounded in the result string. If the value to the right of the number of the specified decimal places is 5 or greater then the last digit in the result string is rounded away from zero.<br />
<br />
&nbsp;</p>', CAST(N'2020-01-06T16:41:37.620' AS DateTime), NULL, CAST(N'2020-01-16T15:27:34.687' AS DateTime), NULL, 1, 1, NULL, 0)
INSERT [dbo].[AZ_Products] ([Id], [Name], [TypeTransaction], [TypeProperty], [ProjectId], [City], [District], [Ward], [Street], [Area], [Price], [Unit], [Address], [HouseDirection], [BalconyDirection], [NumOfFloor], [NumOfBedroom], [NumOfWcs], [ImageMain], [ImageList], [Summary], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsActive], [IsDelete], [DeletedDate], [IsVIP]) VALUES (N'a6d2f83d-ab1c-418d-a239-d040d1894858', N'BÁN NHÀ PL K80C, Làm Văn Phòng, Cửa hàng , CÓ THANG MÁY, ĐƯỜNG 4 LÀN, VỈA HÈ 6M, DT85M2X7T GIÁ 19,5 TỶ', 5, 28, NULL, 1, 2, 30, 241, 85, 19.5, 8, N'Khu PL K80C', 18, 18, 7, 5, 6, N'/Files/1102123239_Bán-nhà-ngõ-6-Vĩnh-Phúc-4_thumbail.jpg', N'[{"Id":0,"Name":"/Files/1102123239_Bán-nhà-ngõ-6-Vĩnh-Phúc-4.jpg"},{"Id":1,"Name":"/Files/1102123239_Bán-nhà-ngõ-6-Vĩnh-Phúc.jpg"},{"Id":2,"Name":"/Files/1102123239_Bán-nhà-ngõ-6-Vĩnh-Phúc-5.jpg"},{"Id":3,"Name":"/Files/1102123240_Bán-nhà-ngõ-6-Vĩnh-Phúc-7.jpg"},{"Id":4,"Name":"/Files/1102123239_Bán-nhà-ngõ-6-Vĩnh-Phúc-3.jpg"},{"Id":5,"Name":"/Files/1102123239_Bán-nhà-ngõ-6-Vĩnh-Phúc-6.jpg"},{"Id":6,"Name":"/Files/1102123239_Bán-nhà-ngõ-6-Vĩnh-Phúc-2.jpg"}]', N'<p>Gia đ&igrave;nh cần b&aacute;n nh&agrave; thuộc khu PL K80C trong ng&otilde; 6 phố Vĩnh Ph&uacute;c, phường Vĩnh Ph&uacute;c, quận Ba Đ&igrave;nh, th&agrave;nh phố H&agrave; Nội. Nh&agrave; mới ho&agrave;n thiện, nằm tr&ecirc;n mặt ng&otilde; to hơn phố c&oacute; vỉa h&egrave; rộng. Rất ph&ugrave; hợp để ở kết hợp kinh doanh hoặc l&agrave;m văn ph&ograve;ng. Diện t&iacute;ch 85m2 x&acirc;y mới 7 tầng c&oacute; thang m&aacute;y.</p>

<p>Nh&agrave; thiết kế hiện đại, kết cấu m&oacute;ng b&egrave; khung cột chắc chắn, tho&aacute;ng đ&atilde;ng bố tr&iacute; như sau:</p>

<p>Tầng 1,2: Th&ocirc;ng s&agrave;n kinh doanh, cầu thang cuối, thang m&aacute;y, wc</p>

<p>Tầng 3.4: Thiết kế mỗi tầng l&agrave; 02 ph&ograve;ng ngủ rộng c&oacute; wc master kh&eacute;p k&iacute;n.</p>

<p>Tầng 5: Thiết kế th&ocirc;ng s&agrave;n c&oacute; 01 wc master kh&eacute;p k&iacute;n.</p>

<p>Tầng 6: Ph&ograve;ng bếp, wc, nh&agrave; ăn cực rộng c&oacute; thể bố tr&iacute; bếp ăn văn ph&ograve;ng.</p>

<p>Tầng 7: Ph&ograve;ng thờ, s&acirc;n phơi rộng c&oacute; thể kinh doanh caf&eacute; view to&agrave;n cảnh khu 7.2 ha</p>

<p>Nội thất: Trang thiết bị nội thất cơ bản đều đầy đủ: Tủ bếp, n&oacute;ng lạnh, s&agrave;n gỗ, THANG M&Aacute;Y, trần thạch cao, đ&egrave;n led &acirc;m trần tiết kiệm điện&hellip;</p>

<p>Vị tr&iacute;: Nh&agrave; thuộc khu Ph&acirc;n l&ocirc; qu&acirc;n đội, d&acirc;n tr&iacute; cao, an ninh tốt. Nằm tr&ecirc;n mặt ng&otilde; rộng hơn cả phố, 4 &ocirc; t&ocirc; tr&aacute;nh nhau thoải m&aacute;i, vỉa h&egrave; rộng 6m kinh doanh sầm uất. Nh&agrave; gần nhiều trường học, chợ d&acirc;n sinh n&ecirc;n an sinh rất tốt.</p>

<p>Sổ đỏ ch&iacute;nh chủ,</p>

<p>Gi&aacute; b&aacute;n: 19,5 tỷ (c&oacute; thương lượng)</p>

<p>Li&ecirc;n hệ 0919 679 682 gặp Chức MTG</p>', CAST(N'2020-02-11T12:36:38.617' AS DateTime), NULL, CAST(N'2020-02-11T12:36:38.617' AS DateTime), NULL, 1, NULL, NULL, 0)
INSERT [dbo].[AZ_Products] ([Id], [Name], [TypeTransaction], [TypeProperty], [ProjectId], [City], [District], [Ward], [Street], [Area], [Price], [Unit], [Address], [HouseDirection], [BalconyDirection], [NumOfFloor], [NumOfBedroom], [NumOfWcs], [ImageMain], [ImageList], [Summary], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsActive], [IsDelete], [DeletedDate], [IsVIP]) VALUES (N'15a4f076-c2f4-4ca6-a704-d477655d9c65', N'Bán nhà ngõ 371 Kim Mã, Phường Ngọc Khánh, Ba Đình, DT34m2x5 tầng, giá 3,5 tỷ LH 0919 679 682', 5, 28, NULL, 1, 2, 26, 176, 34, 3.5, 8, N'Ngõ 371 Kim Mã, Phường Ngọc Khánh, Quận Ba Đình, Hà Nội', 18, 18, 5, 4, 5, N'/Files/0402114910_74d14d10d6f32fad76e2_thumbail.jpg', N'[{"Id":0,"Name":"/Files/0402114910_20191220102359-7529_wm.jpg"},{"Id":1,"Name":"/Files/0402114910_74d14d10d6f32fad76e2.jpg"},{"Id":2,"Name":"/Files/0402114911_d3e93f35a4d65d8804c7.jpg"},{"Id":3,"Name":"/Files/0402114911_9320f3e36800915ec811.jpg"}]', N'<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">B&aacute;n nh&agrave; ng&otilde; 371 Kim M&atilde;, Phường Ngọc Kh&aacute;nh, Ba Đ&igrave;nh, DT34m2x5 tầng, gi&aacute; 3,5 tỷ</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Diện t&iacute;ch 34m2, nh&agrave; x&acirc;y 5 tầng, cầu thang cuối, </span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Thiết kế hiện đại, mỗi tầng 1 ph&ograve;ng, tổng c&oacute; : Ph&ograve;ng kh&aacute;ch, 3 ph&ograve;ng ngủ, ph&ograve;ng thờ, ph&ograve;ng ăn + bếp, khu m&aacute;y giặt s&acirc;n phơi.</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Nội thất: Cửa, tay vịn cầu thang bằng gỗ Lim, bậc s&agrave;n đ&aacute;, tủ bếp gỗ tự nhi&ecirc;n, kh&aacute;ch mua về c&oacute; thể ở được ngay.</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Vị tr&iacute;: nằm trong ng&otilde; 371 Kim M&atilde;, th&ocirc;ng sang 409 Kim M&atilde;, khu d&acirc;n cư, gần Lotte, Metroplis&hellip;.</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Sổ đỏ ch&iacute;nh chủ, gi&aacute; 3,5 tỷ c&oacute; thương lượng.</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Li&ecirc;n hệ 0919 679 682 (Miễn TG- MG)</span></span></span></span></span></span></p>', CAST(N'2020-02-04T23:54:10.680' AS DateTime), NULL, CAST(N'2020-02-08T11:26:12.287' AS DateTime), NULL, 1, NULL, NULL, 1)
INSERT [dbo].[AZ_Products] ([Id], [Name], [TypeTransaction], [TypeProperty], [ProjectId], [City], [District], [Ward], [Street], [Area], [Price], [Unit], [Address], [HouseDirection], [BalconyDirection], [NumOfFloor], [NumOfBedroom], [NumOfWcs], [ImageMain], [ImageList], [Summary], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsActive], [IsDelete], [DeletedDate], [IsVIP]) VALUES (N'49a0d6f5-6062-4fa5-98be-d58ca3435dec', N'Bán nhà AZ', 6, 40, NULL, 1, 2, 22, 145, 3, 44, 14, N'Ngach 123/3/4', 19, 20, NULL, 4, 5, NULL, NULL, N'Mô tả đây', CAST(N'2020-01-06T16:17:39.097' AS DateTime), NULL, CAST(N'2020-01-06T16:17:41.190' AS DateTime), NULL, 1, 1, NULL, 0)
INSERT [dbo].[AZ_Products] ([Id], [Name], [TypeTransaction], [TypeProperty], [ProjectId], [City], [District], [Ward], [Street], [Area], [Price], [Unit], [Address], [HouseDirection], [BalconyDirection], [NumOfFloor], [NumOfBedroom], [NumOfWcs], [ImageMain], [ImageList], [Summary], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsActive], [IsDelete], [DeletedDate], [IsVIP]) VALUES (N'cbe99980-01f0-4be0-b260-d61ab4a0848d', N'Bán nhà ngõ 173/63 Hoàng Hoa Thám thông sang ngõ 279 Đội Cấn, phường Ngọc Hà, quận Ba Đình, Hà Nội', 5, 28, NULL, 1, 2, 25, 168, 50, 4.3, 8, N'Phường Ngọc Hà, Ba Đình, Hà Nội', 18, 18, 4, 5, 3, N'/Files/0502090023_30dfa87548dca882f1cd_thumbail.jpg', N'[{"Id":0,"Name":"/Files/0502090023_7a5c32b6d21f32416b0e.jpg"},{"Id":1,"Name":"/Files/0502090026_dbe9560eb6a756f90fb6.jpg"},{"Id":2,"Name":"/Files/0502090024_92f6c31823b1c3ef9aa0.jpg"},{"Id":3,"Name":"/Files/0502090027_e25a878f67268778de37.jpg"},{"Id":4,"Name":"/Files/0502090023_30dfa87548dca882f1cd.jpg"},{"Id":5,"Name":"/Files/0502090026_aa9b8628668186dfdf90.jpg"},{"Id":6,"Name":"/Files/0502090023_45e5a61246bba6e5ffaa.jpg"}]', N'<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">B&aacute;n nh&agrave; ng&otilde; 173/63 Ho&agrave;ng Hoa Th&aacute;m th&ocirc;ng sang ng&otilde; 279 Đội Cấn, phường Ngọc H&agrave;, quận Ba Đ&igrave;nh, H&agrave; Nội. Diện t&iacute;ch 50m2 x&acirc;y 4 tầng. Nh&agrave; l&ocirc; g&oacute;c 2 mặt tho&aacute;ng trước v&agrave; b&ecirc;n n&ecirc;n c&aacute;c ph&ograve;ng đều c&oacute; cửa sổ, nhiều &aacute;nh s&aacute;ng. Thiết kế c&ocirc;ng năng sử dụng hợp l&yacute;:</span></span></span></span><br />
<span style="font-size:10.5pt"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black"><span style="background-color:white">- Tầng 1: Chỗ để xe, Ph&ograve;ng kh&aacute;ch, bếp, ph&ograve;ng ăn, vệ sinh.</span><br />
<span style="background-color:white">- Tầng 2,3: Mỗi tầng 2 ph&ograve;ng ngủ cửa sổ tho&aacute;ng rộng, vệ sinh giữa.</span><br />
<span style="background-color:white">- Tầng 4: Ph&ograve;ng ngủ, Ph&ograve;ng thờ, s&acirc;n phơi</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">+ Nội thất: Cửa th&ocirc;ng ph&ograve;ng gỗ Lim, cầu thang mặt đ&aacute;, tay vịn gỗ Lim, s&agrave;n gỗ, trần thạch cao kết hợp đ&egrave;n Led tiết kiệm điện.</span></span></span></span><br />
<span style="font-size:10.5pt"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black"><span style="background-color:white">+ Vị tr&iacute; trung t&acirc;m quận Ba Đ&igrave;nh, d&acirc;n tr&iacute; cao, an ninh tốt. Gần trường học cấp 1, 2, 3 Chu Văn An, Ba Đ&igrave;nh, chợ Ngọc H&agrave;... Giao th&ocirc;ng thuận tiện kết nối dễ d&agrave;ng với c&aacute;c tuyến phố ch&iacute;nh như Ho&agrave;ng Hoa Th&aacute;m, Liễu Giai, Đội Cấn...</span><br />
<span style="background-color:white">Sổ đỏ ch&iacute;nh chủ, ph&aacute;p l&yacute; r&otilde; r&agrave;ng, mua b&aacute;n nhanh gọn.</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Quan trọng l&agrave;: Chủ nh&agrave; thiện ch&iacute; b&aacute;n</span></span></span></span><br />
<span style="font-size:10.5pt"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black"><span style="background-color:white">Gi&aacute; 4.3 tỷ (c&oacute; thương lượng, bao sang t&ecirc;n cho người mua).</span><br />
<span style="background-color:white">Li&ecirc;n hệ anh Chức 0919 679 682 (Miễn m&ocirc;i giới - trung gian)</span></span></span></span></span></span></p>', CAST(N'2020-02-05T09:04:30.850' AS DateTime), NULL, CAST(N'2020-02-08T11:26:23.847' AS DateTime), NULL, 1, NULL, NULL, 1)
INSERT [dbo].[AZ_Products] ([Id], [Name], [TypeTransaction], [TypeProperty], [ProjectId], [City], [District], [Ward], [Street], [Area], [Price], [Unit], [Address], [HouseDirection], [BalconyDirection], [NumOfFloor], [NumOfBedroom], [NumOfWcs], [ImageMain], [ImageList], [Summary], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsActive], [IsDelete], [DeletedDate], [IsVIP]) VALUES (N'b3f5862a-48b2-4307-ae6f-dbc819321036', N'[ BannhaAZ ] Bán nhà ngõ 58 Cầu Am, Vạn Phúc, Hà Đông, DT33m2x5T, giá 2,85 tỷ', 5, 28, NULL, 1, 8, 139, 673, NULL, 2.85, NULL, N'Khu Làng Ẩm Thực', 18, 18, NULL, NULL, NULL, N'/Files/0802094642_25765ca69c7e64203d6f_thumbail.jpg', N'[{"Id":0,"Name":"/Files/0802094642_25765ca69c7e64203d6f.jpg"}]', N'<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">B&aacute;n nh&agrave; ng&otilde; L&agrave;ng Văn H&oacute;a Ẩm Thực, Vạn Ph&uacute;c, H&agrave; Đ&ocirc;ng, DT33m2x5T, gi&aacute; 2,85 tỷ</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Diện t&iacute;ch 33m2, nh&agrave; x&acirc;y mới 5 tầng, mặt tiền rộng, l&ocirc; g&oacute;c 2 mặt tho&aacute;ng, mặt ng&otilde; th&ocirc;ng KINH DOANH tốt.</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Thiết kế hiện đại</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Tầng 1: Mặt bằng kinh doanh, Ph&ograve;ng bếp ăn, nh&agrave; vệ sinh phụ</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Tầng 2,3,4 Mỗi tầng 1 ph&ograve;ng ngủ, ph&ograve;ng tắm vệ sinh</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Tầng 5 Ph&ograve;ng thờ s&acirc;n phơi.</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Nội thất cao cấp như d&acirc;y điện Trần Ph&uacute;, trần thạch cao Vĩnh Tường&hellip;.</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Vị tr&iacute;: Nằm trong khu phố L&agrave;ng Ẩm Thực &ndash; Vạn Ph&uacute;c, c&aacute;ch mặt phố 60m, giao th&ocirc;ng thuận tiện, giao th&ocirc;ng thuận tiện.</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Sổ đỏ ch&iacute;nh chủ, gi&aacute; 2,85 tỷ c&oacute; thương lượng</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Li&ecirc;n hệ 09 683 50 683 gặp Chức (Miễn TG, MG)</span></span></p>', CAST(N'2020-02-08T09:52:35.380' AS DateTime), NULL, CAST(N'2020-02-08T11:26:53.123' AS DateTime), NULL, 1, NULL, NULL, 0)
INSERT [dbo].[AZ_Products] ([Id], [Name], [TypeTransaction], [TypeProperty], [ProjectId], [City], [District], [Ward], [Street], [Area], [Price], [Unit], [Address], [HouseDirection], [BalconyDirection], [NumOfFloor], [NumOfBedroom], [NumOfWcs], [ImageMain], [ImageList], [Summary], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsActive], [IsDelete], [DeletedDate], [IsVIP]) VALUES (N'67a18f26-d2b0-40f9-be60-ddd0cde6b2e6', N'[ BannhaAZ ] Bán nhà riêng Sông Nhuệ, Khu Tập thể Sông Nhuệ, P Nguyễn Trãi, HĐ, DT34m2x5T, giá 2,98 tỷ', 5, 28, NULL, 1, 8, 133, 711, 34, 2.98, 8, N'Khu TT Nhuệ Giang', 18, 18, 5, 5, 4, N'/Files/0802112932_25ce46c91019e847b108_thumbail.jpg', N'[{"Id":0,"Name":"/Files/0802112934_59161be24d32b56cec23.jpg"},{"Id":1,"Name":"/Files/0802112933_cd54a5a8f3780b265269.jpg"},{"Id":2,"Name":"/Files/0802112933_fd9caa9bfc4b04155d5a.jpg"},{"Id":3,"Name":"/Files/0802112932_1330553703e7fbb9a2f6.jpg"},{"Id":4,"Name":"/Files/0802112928_25d552d20402fc5ca513.jpg"},{"Id":5,"Name":"/Files/0802112933_a6d708d15e01a65fff10.jpg"}]', N'<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">B&aacute;n nh&agrave; ri&ecirc;ng S&ocirc;ng Nhuệ, Khu Tập thể S&ocirc;ng Nhuệ, P Nguyễn Tr&atilde;i, HĐ, DT34m2x5T, gi&aacute; 2,98 tỷ</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Diện t&iacute;ch l&agrave; 34m2, nh&agrave; l&ocirc; g&oacute;c 3 mặt tho&aacute;ng, &aacute;nh s&aacute;ng tự nhi&ecirc;n tr&agrave;n ngập.</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Tầng 1: Nh&agrave; để xe, ph&ograve;ng bếp + ăn, nh&agrave; vệ sinh phụ</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Tầng 2: Tầng Lửng: Ph&ograve;ng kh&aacute;ch</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Tầng 3,4 Mỗi tầng chia 2 ph&ograve;ng ngủ, nh&agrave; tắm vệ sinh</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Tầng 5: Ph&ograve;ng thờ s&acirc;n phơi</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Nội thất: Đ&atilde; được trang bị nội thất cao cấp, từ Thiết bị vệ sinh, r&egrave;m cửa, đ&egrave;n chiếu s&aacute;ng, cửa, cầu thang&hellip;</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Vị tr&iacute;: c&aacute;ch một nh&agrave; l&agrave; ra mặt phố S&ocirc;ng Nhuệ, nh&agrave; th&ocirc;ng sang phố Nguyễn Tr&atilde;i, giao th&ocirc;ng thuận tiện, thuộc khu thủ phủ H&agrave; T&acirc;y xưa.</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Sổ đỏ ch&iacute;nh chủ, gi&aacute; 2,98 tỷ c&oacute; thương lượng, bao sang t&ecirc;n cho người mua</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Li&ecirc;n hệ 0919 679 682 gặp Chức (Miễn TG, MG)</span></span></span></span></span></span></p>', CAST(N'2020-02-08T11:31:57.340' AS DateTime), NULL, CAST(N'2020-02-08T11:31:57.340' AS DateTime), NULL, 1, NULL, NULL, 0)
INSERT [dbo].[AZ_Products] ([Id], [Name], [TypeTransaction], [TypeProperty], [ProjectId], [City], [District], [Ward], [Street], [Area], [Price], [Unit], [Address], [HouseDirection], [BalconyDirection], [NumOfFloor], [NumOfBedroom], [NumOfWcs], [ImageMain], [ImageList], [Summary], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsActive], [IsDelete], [DeletedDate], [IsVIP]) VALUES (N'c944cee7-515b-4215-82e0-e04c8164a4ad', N'Bán nhà khu PL Quân Đội, P Điện Biên, Ba Đình, DT46m2x4T, 8,35 tỷ LH 0919 679 682', 5, 28, NULL, 1, 2, 20, 216, 48, 8.35, 8, N'Nhà Ngõ', 18, 18, 4, 4, 3, N'/Files/1102090021_15b65e2fd9693e376778_thumbail.jpg', N'[{"Id":0,"Name":"/Files/1102090022_272ef24f90dc72822bcd.jpg"},{"Id":1,"Name":"/Files/1102090022_98c801ab63388166d829.jpg"},{"Id":2,"Name":"/Files/1102090023_0886d5dab74955170c58.jpg"},{"Id":3,"Name":"/Files/1102090023_329880fbe26800365979.jpg"},{"Id":4,"Name":"/Files/1102090025_a30d856ce7ff05a15cee.jpg"},{"Id":5,"Name":"/Files/1102090025_a30d856ce7ff05a15cee.jpg"},{"Id":6,"Name":"/Files/1102090026_eb3c215a43c9a197f8d8.jpg"},{"Id":7,"Name":"/Files/1102090022_a5d0b5c33285d5db8c94.jpg"},{"Id":8,"Name":"/Files/1102090023_Ảnh Phố Ô tô.png"}]', N'<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">B&aacute;n nh&agrave; khu PL Qu&acirc;n Đội, P Điện Bi&ecirc;n, Ba Đ&igrave;nh, DT46m2x4T, 8,35 tỷ LH 0919 679 682</span></span></span></span></span></span></p>

<ul>
	<li><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Diện t&iacute;ch 46m2 nh&agrave; x&acirc;y 4 tầng, thửa đất ph&acirc;n l&ocirc; h&igrave;nh chữ nhật, mặt sau c&oacute; khe tho&aacute;ng 1m giữa 2 nh&agrave;</span></span></span></span></span></span></li>
	<li><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Căn nh&agrave; thiết kế cầu thang giữa, chia tầng 2 ph&ograve;ng trước sau.</span></span></span></span></span></span></li>
	<li><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Nội thất: S&agrave;n gỗ tự nhi&ecirc;n, tủ bếp gỗ sồi, Tay Vịn cầu thang, cửa đi gỗ Lim Nghệ An, cửa sổ, cửa đi ban c&ocirc;ng euro window chống ồn&hellip;</span></span></span></span></span></span></li>
	<li><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Vị tr&iacute;: mặt ng&otilde; trươc nh&agrave; hơn 4m, c&aacute;ch mặt phố 15m, c&aacute;ch nh&agrave; 10m c&oacute; s&acirc;n vui chơi, khu tập thể dục của c&aacute;c B&aacute;c, c&aacute;c ch&aacute;u nhỏ. Vị tr&iacute; gửi &ocirc; t&ocirc; tại Sở Y Tế ngay đ&oacute;, Văn Ph&ograve;ng ch&iacute;nh phủ vế b&ecirc;n đối diện.</span></span></span></span></span></span></li>
</ul>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Chủ nh&agrave; l&agrave; c&aacute;n bộ ng&agrave;nh qu&acirc;n đội nghỉ hưu, nay về ở với con c&aacute;i n&ecirc;n cần b&aacute;n ngay, rất mong gặp kh&aacute;ch thiện ch&iacute;, c&oacute; gia lộc, v&agrave; để lại nhiều đồ nội thất cho kh&aacute;ch h&agrave;ng may mắn v&agrave; thiện ch&iacute; mua.</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Sổ đỏ ch&iacute;nh chủ, để ở nh&agrave;, gi&aacute; 8,35 tỷ c&oacute; thương lượng</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Li&ecirc;n hệ 0919 679 682&nbsp; (gặp Chủ nh&agrave; trao đổi trực tiếp gi&aacute;) (Miễn TG, b&aacute;o mạng)</span></span></span></span></span></span></p>', CAST(N'2020-02-11T21:09:32.040' AS DateTime), NULL, CAST(N'2020-02-11T21:09:32.040' AS DateTime), NULL, 1, NULL, NULL, 1)
INSERT [dbo].[AZ_Products] ([Id], [Name], [TypeTransaction], [TypeProperty], [ProjectId], [City], [District], [Ward], [Street], [Area], [Price], [Unit], [Address], [HouseDirection], [BalconyDirection], [NumOfFloor], [NumOfBedroom], [NumOfWcs], [ImageMain], [ImageList], [Summary], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsActive], [IsDelete], [DeletedDate], [IsVIP]) VALUES (N'84f7b31d-ea83-44d0-97f0-e0ffc442a4c2', N'[Bannhaaz] Bán nhà đường Phan Đình Giót, Phường La Khê, Hà Đông, DT30m2x4T, 2.15 tỷ', 5, 28, NULL, 1, 8, 131, 719, 30, 2.15, 8, N'Nhà số 2', 18, 18, 4, 3, 3, N'/Files/2002065626_screenshot_1582106173_thumbail.png', N'[{"Id":0,"Name":"/Files/2002065636_2eededdee94c1112485d.jpg"},{"Id":1,"Name":"/Files/2002065636_0a9493a9973b6f65362a.jpg"},{"Id":2,"Name":"/Files/2002065636_372208160c84f4daad95.jpg"},{"Id":3,"Name":"/Files/2002065637_24f7db3cdeae26f07fbf.jpg"}]', N'<p>&nbsp;</p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">[Bannhaaz] B&aacute;n nh&agrave; đường Phan Đ&igrave;nh Gi&oacute;t, Phường La Kh&ecirc;, H&agrave; Đ&ocirc;ng, DT30m2x4T, 2.15 tỷ</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Diện t&iacute;ch sổ 30m2, mặt tiền 3,8m, &nbsp;nh&agrave; x&acirc;y 4 tầng mới đẹp, nh&agrave; x&acirc;y dựng 30m2x4T</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Thiết kế hiện đại, theo phong c&aacute;ch t&acirc;n cổ điển.</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Tầng 1: Ph&ograve;ng kh&aacute;ch, bếp, nh&agrave; vệ sinh</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Tầng 2,3: Mỗi tầng 01 ph&ograve;ng ngủ, nh&agrave; tắm</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Tầng 4: Ph&ograve;ng ngủ nhỏ, gia thờ, s&acirc;n phơi</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Nội thất: thiết bị nội thất cơ bản như hệ thống cửa, thiết bị vệ sinh cao cấp</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Vị tr&iacute;: thuận tiện giao th&ocirc;ng đi L&ecirc; Trọng Tấn, Phan Đ&igrave;nh Gi&oacute;t&hellip;.đầy đủ c&aacute;c tiện &iacute;ch về trường học, y tế&hellip;..</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Sổ đỏ ch&iacute;nh chủ, gi&aacute; 2,15 tỷ ( c&oacute; thương lượng, thương lượng trực tiếp với chủ nh&agrave;)</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Li&ecirc;n hệ 0919 679 682 (Gặp Chức &ndash; Miễn TG)</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Ngo&agrave;i ra qu&yacute; kh&aacute;ch tham khảo c&aacute;c sản phẩm kh&aacute;c c&ugrave;ng ph&acirc;n kh&uacute;c, khu vực theo link dưới đ&acirc;y:</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><a href="http://bannhaaz.com/?keySearch=&amp;t=1&amp;type=5&amp;price=45&amp;city=1&amp;district=8&amp;order=2" style="color:blue; text-decoration:underline">http://bannhaaz.com/?keySearch=&amp;t=1&amp;type=5&amp;price=45&amp;city=1&amp;district=8&amp;order=2</a></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">[Bannhaaz] B&aacute;n nh&agrave; đường Phan Đ&igrave;nh Gi&oacute;t, Phường La Kh&ecirc;, H&agrave; Đ&ocirc;ng, DT30m2x4T, 2.15 tỷ</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Diện t&iacute;ch sổ 30m2, mặt tiền 3,8m, &nbsp;nh&agrave; x&acirc;y 4 tầng mới đẹp, nh&agrave; x&acirc;y dựng 30m2x4T</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Thiết kế hiện đại, theo phong c&aacute;ch t&acirc;n cổ điển.</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Tầng 1: Ph&ograve;ng kh&aacute;ch, bếp, nh&agrave; vệ sinh</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Tầng 2,3: Mỗi tầng 01 ph&ograve;ng ngủ, nh&agrave; tắm</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Tầng 4: Ph&ograve;ng ngủ nhỏ, gia thờ, s&acirc;n phơi</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Nội thất: thiết bị nội thất cơ bản như hệ thống cửa, thiết bị vệ sinh cao cấp</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Vị tr&iacute;: thuận tiện giao th&ocirc;ng đi L&ecirc; Trọng Tấn, Phan Đ&igrave;nh Gi&oacute;t&hellip;.đầy đủ c&aacute;c tiện &iacute;ch về trường học, y tế&hellip;..</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Sổ đỏ ch&iacute;nh chủ, gi&aacute; 2,15 tỷ ( c&oacute; thương lượng, thương lượng trực tiếp với chủ nh&agrave;)</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Li&ecirc;n hệ 0919 679 682 (Gặp Chức &ndash; Miễn TG)</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Ngo&agrave;i ra qu&yacute; kh&aacute;ch tham khảo c&aacute;c sản phẩm kh&aacute;c c&ugrave;ng ph&acirc;n kh&uacute;c, khu vực theo link dưới đ&acirc;y:</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><a href="http://bannhaaz.com/?keySearch=&amp;t=1&amp;type=5&amp;price=45&amp;city=1&amp;district=8&amp;order=2" style="color:blue; text-decoration:underline">http://bannhaaz.com/?keySearch=&amp;t=1&amp;type=5&amp;price=45&amp;city=1&amp;district=8&amp;order=2</a></span></span></p>

<p>&nbsp;</p>', CAST(N'2020-02-20T06:59:48.130' AS DateTime), NULL, CAST(N'2020-02-20T07:00:25.323' AS DateTime), NULL, 1, NULL, NULL, 1)
INSERT [dbo].[AZ_Products] ([Id], [Name], [TypeTransaction], [TypeProperty], [ProjectId], [City], [District], [Ward], [Street], [Area], [Price], [Unit], [Address], [HouseDirection], [BalconyDirection], [NumOfFloor], [NumOfBedroom], [NumOfWcs], [ImageMain], [ImageList], [Summary], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsActive], [IsDelete], [DeletedDate], [IsVIP]) VALUES (N'2d677efb-ace0-48c1-b3e8-e1bdd4249403', N'Bán nhà gần ĐH Ngoại Thương, ĐH GTVT, Phường Láng Thượng, Đống Đa, DT36m2x5T, còn mới', 5, 28, NULL, 1, 1, 7, 69, 32, 3.28, 8, N'185', 18, 18, NULL, 3, 3, N'/Files/1102104110_70d6561b0665e03bb974_thumbail.jpg', N'[{"Id":0,"Name":"/Files/1102104110_70d6561b0665e03bb974.jpg"},{"Id":1,"Name":"/Files/1102104110_7da4f159a12747791e36.jpg"},{"Id":2,"Name":"/Files/1102104110_98ee001b5065b63bef74.jpg"},{"Id":3,"Name":"/Files/1102104111_f2376f2f3f51d90f8040.jpg"},{"Id":4,"Name":"/Files/1102104111_a9de29c079be9fe0c6af.jpg"},{"Id":5,"Name":"/Files/1102104110_79fbf8c2a8bc4ee217ad.jpg"},{"Id":6,"Name":"/Files/1102104111_db90974fc731216f7820.jpg"},{"Id":7,"Name":"/Files/1102104111_72dabd1ded630b3d5272.jpg"}]', N'<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">B&aacute;n nh&agrave; gần ĐH Ngoại Thương, ĐH GTVT, Phường L&aacute;ng Thượng, Đống Đa, DT36m2x5T, c&ograve;n mới</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Diện t&iacute;ch 30m2 sổ, thực tế x&acirc;y dựng 32m2, cầu thang cuối, mỗi tầng 1 ph&ograve;ng</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Vị tr&iacute;: Căn nh&agrave; l&ocirc; g&oacute;c, mở cửa sổ tho&aacute;n m&aacute;t, cầu thang, WC đều c&oacute; cửa th&ocirc;ng tho&aacute;ng.</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Khu vực d&acirc;n cư đ&ocirc;ng đ&uacute;c, gần c&aacute;c trường ĐH lớn, BV GTVT, Phụ Sản, Nhi&hellip;.</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Giao th&ocirc;ng cực kỳ thuận lợi.</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Nội thất: Nước sơn nh&agrave; vẫn c&ograve;n rất mới, kh&aacute;ch thiện ch&iacute;, chủ nh&agrave; để lại nhiều đồ đạc c&oacute; thể ở được ngay.</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Sổ đỏ ch&iacute;nh chủ, gi&aacute; 3.28 tỷ c&oacute; thương lượng</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">LH A Chức: 0919 679 682 (Miễn TG)</span></span></span></span></span></span></p>', CAST(N'2020-02-11T22:44:19.597' AS DateTime), NULL, CAST(N'2020-02-11T22:44:19.597' AS DateTime), NULL, 1, NULL, NULL, 0)
INSERT [dbo].[AZ_Products] ([Id], [Name], [TypeTransaction], [TypeProperty], [ProjectId], [City], [District], [Ward], [Street], [Area], [Price], [Unit], [Address], [HouseDirection], [BalconyDirection], [NumOfFloor], [NumOfBedroom], [NumOfWcs], [ImageMain], [ImageList], [Summary], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsActive], [IsDelete], [DeletedDate], [IsVIP]) VALUES (N'f76a39af-6174-45f5-be65-e271a42bf3c2', N'Chuyển nhượng đất mặt đường số 766 Thiên Lôi, Kênh Dương, Lê Chân, Hải Phòng', 6, 37, NULL, 1, 3, 31, 1, 2, 333, 14, N'So 123 Cau giay', 18, 18, 1, 3, 4, N'https://nhadatvanminh.com.vn/images/raovat/146521.jpg', NULL, N'Lô đất mặt đường vuông vắn, nằm tại vị trí cực đẹp, thoáng mát, giao thông thuận tiện, an ninh tốt, phù hợp kinh doanh, buôn bán, mở văn phòng.', CAST(N'2020-01-06T17:13:43.633' AS DateTime), NULL, CAST(N'2020-01-10T09:55:04.953' AS DateTime), NULL, 1, 1, NULL, 1)
INSERT [dbo].[AZ_Products] ([Id], [Name], [TypeTransaction], [TypeProperty], [ProjectId], [City], [District], [Ward], [Street], [Area], [Price], [Unit], [Address], [HouseDirection], [BalconyDirection], [NumOfFloor], [NumOfBedroom], [NumOfWcs], [ImageMain], [ImageList], [Summary], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsActive], [IsDelete], [DeletedDate], [IsVIP]) VALUES (N'31d767bb-1af1-4eae-ab05-e4bc118e2273', N'test', 58, 60, NULL, 1, 3, 31, 3, NULL, 0, NULL, N'', 18, 18, NULL, NULL, NULL, N'/Files/0502123127_4d5f51201e27fd79a436_thumbail.jpg', N'[{"Id":0,"Name":"/Files/0502010253_95558cd7e6911fcf4680.jpg"}]', N'', CAST(N'2020-02-07T14:02:38.420' AS DateTime), NULL, CAST(N'2020-02-07T14:05:51.587' AS DateTime), NULL, 1, 1, NULL, 1)
INSERT [dbo].[AZ_Products] ([Id], [Name], [TypeTransaction], [TypeProperty], [ProjectId], [City], [District], [Ward], [Street], [Area], [Price], [Unit], [Address], [HouseDirection], [BalconyDirection], [NumOfFloor], [NumOfBedroom], [NumOfWcs], [ImageMain], [ImageList], [Summary], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsActive], [IsDelete], [DeletedDate], [IsVIP]) VALUES (N'42c11ad0-5935-49c7-8c68-e509106546e3', N'[ BannhaAZ ] Bán nhà lô góc 3 mặt thoáng, Hà Trì 1, P. Hà Trì, Hà Đông, DT55m2x3T, giá 3,15 tỷ', 5, 28, NULL, 1, 8, 129, 668, 55, 3.15, 8, N'Ngõ Hà Trì 1', 18, 18, 3, 3, 3, N'/Files/0802103007_aa102c7d7fad87f3debc_thumbail.jpg', N'[{"Id":0,"Name":"/Files/0802103007_aa102c7d7fad87f3debc.jpg"},{"Id":1,"Name":"/Files/0802103005_12d20a455a95a2cbfb84.jpg"},{"Id":2,"Name":"/Files/0802103004_7ff22e797ea986f7dfb8.jpg"},{"Id":3,"Name":"/Files/0802103004_9cc0c9a69a7662283b67.jpg"},{"Id":4,"Name":"/Files/0802103005_4714ca5a998a61d4389b.jpg"},{"Id":5,"Name":"/Files/0802103004_5bdcd08d835d7b03224c.jpg"},{"Id":6,"Name":"/Files/0802103003_4c589c39cfe937b76ef8.jpg"}]', N'<p>&nbsp;</p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">B&aacute;n nh&agrave; l&ocirc; g&oacute;c 3 mặt tho&aacute;ng, H&agrave; Tr&igrave; 1, P. H&agrave; Tr&igrave;, H&agrave; Đ&ocirc;ng, DT55m2x3T, gi&aacute; 3,15 tỷ</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Vị tr&iacute;: Thuộc khu H&agrave; Tr&igrave; 1, gần c&aacute;c trường học từ cấp 1,2,3 L&ecirc; Lợi, vị tr&iacute; trung t&acirc;m của quận H&agrave; Đ&ocirc;ng, giao th&ocirc;ng thuận tiện, tiện &iacute;ch đầy đủ.</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Th&ocirc;ng số: DT55m2, 3 tầng l&ocirc; g&oacute;c 3 mặt, cửa sổ th&ocirc;ng tho&aacute;ng, s&acirc;n chơi cạch nh&agrave;.</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Tầng 1: S&acirc;n để xe - ph&ograve;ng kh&aacute;ch + Bếp + 1wc th&ocirc;ng thuỷ.</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Tầng 2: C&oacute; 2 ph&ograve;ng ngủ + 1 wc, ban c&ocirc;ng.</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Tầng 3: C&oacute; 1 ph&ograve;ng ngủ master + 1 ph&ograve;ng thờ + 1 Wc + 1 h&agrave;nh lang l&ocirc;gia s&acirc;n phơi (gi&agrave;n s&acirc;n phơi th&ocirc;ng minh, rất tiện lợi.</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Nh&agrave; được thiết kế hiện đại với thiết bị xịn cửa gỗ Lim v&agrave; nh&ocirc;m Xingfa...</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Sổ đỏ ch&iacute;nh chủ, kh&ocirc;ng cầm cố.</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Gi&aacute; 3,15 tỷ (c&oacute; thương lượng, bao sang t&ecirc;n cho kh&aacute;ch mua).</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Li&ecirc;n hệ 0919. 679. 682 gặp Chức miễn TG, MG</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Sổ đỏ ch&iacute;nh chủ hỗ trợ ng&acirc;n h&agrave;ng 3 b&ecirc;n.</span></span></span></span></span></span></p>', CAST(N'2020-02-08T10:34:17.443' AS DateTime), NULL, CAST(N'2020-02-08T10:34:17.443' AS DateTime), NULL, 1, NULL, NULL, 0)
INSERT [dbo].[AZ_Products] ([Id], [Name], [TypeTransaction], [TypeProperty], [ProjectId], [City], [District], [Ward], [Street], [Area], [Price], [Unit], [Address], [HouseDirection], [BalconyDirection], [NumOfFloor], [NumOfBedroom], [NumOfWcs], [ImageMain], [ImageList], [Summary], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsActive], [IsDelete], [DeletedDate], [IsVIP]) VALUES (N'b5447148-8f99-4bb5-80ae-e52cefe9246d', N'Bán nhà số 16 Cầu Đất, Ngô Quyền, Hải Phòng 112', 6, 37, NULL, 1, 3, 33, 2, 2, NULL, 1, N'FLC Cau IGat', 20, 21, 3, 3, 1, N'/Files/0601061834_20200106175600-6b86_wm.jpg', N'[{"Id":0,"Name":"/Files/0601061834_20200106175600-6b86_wm.jpg"},{"Id":1,"Name":"/Files/0601061833_20200106175559-c179_wm.jpg"},{"Id":2,"Name":"/Files/0601061833_20200106175559-f7ca_wm.jpg"},{"Id":3,"Name":"/Files/0601061833_20200106175559-6d9b_wm.jpg"}]', N'<p>Nh&agrave; 2 tầng, vị tr&iacute; đắc địa, l&ocirc; g&oacute;c 2 mặt tiền, thuộc tuyến đường trung t&acirc;m số 1 của th&agrave;nh phố, giao th&ocirc;ng, sinh hoạt thuận tiện, an ninh tốt.</p>', CAST(N'2020-01-06T18:26:17.037' AS DateTime), NULL, CAST(N'2020-01-16T15:10:28.263' AS DateTime), NULL, 1, 1, NULL, 1)
INSERT [dbo].[AZ_Products] ([Id], [Name], [TypeTransaction], [TypeProperty], [ProjectId], [City], [District], [Ward], [Street], [Area], [Price], [Unit], [Address], [HouseDirection], [BalconyDirection], [NumOfFloor], [NumOfBedroom], [NumOfWcs], [ImageMain], [ImageList], [Summary], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsActive], [IsDelete], [DeletedDate], [IsVIP]) VALUES (N'c45869a7-1274-4582-9274-e62317c6f928', N'Bán đất Phường Phúc Diễn, Bắc Từ Liêm, Hà Nội, DT115m2, giá 50tr/m2, có thương lượng', 5, 32, NULL, 1, 10, 143, 973, 115, 50, 10, N'', 18, 18, 1, NULL, NULL, N'/Files/1102104625_Đất 115m2_thumbail.jpg', N'[{"Id":0,"Name":"/Files/1102104625_Đất 115m2.jpg"}]', N'<p>B&aacute;n nh&agrave; đất Phường Ph&uacute;c Diễn, Từ Li&ecirc;m, H&agrave; Nội</p>

<p>Diện t&iacute;ch 115m2, mặt tiền 7,6m, thửa đất vu&ocirc;ng vắn.</p>

<p>Ph&ugrave; hợp x&acirc;y chung cư MINI, ph&acirc;n l&ocirc; chia 3 căn v&ocirc; c&ugrave;ng đẹp</p>

<p>Sổ đỏ ch&iacute;nh chủ, gi&aacute; 50tr/m2 c&oacute; thương lượng th&ecirc;m</p>

<p>Li&ecirc;n hệ 0919 679 682 (gặp Chức - trao đổi gi&aacute; chốt với chủ nh&agrave;)</p>', CAST(N'2020-02-11T10:50:25.187' AS DateTime), NULL, CAST(N'2020-02-11T10:50:25.187' AS DateTime), NULL, 1, NULL, NULL, 1)
INSERT [dbo].[AZ_Products] ([Id], [Name], [TypeTransaction], [TypeProperty], [ProjectId], [City], [District], [Ward], [Street], [Area], [Price], [Unit], [Address], [HouseDirection], [BalconyDirection], [NumOfFloor], [NumOfBedroom], [NumOfWcs], [ImageMain], [ImageList], [Summary], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsActive], [IsDelete], [DeletedDate], [IsVIP]) VALUES (N'becfa373-564d-46f1-b7e4-f2a1398bc8f6', N'[ BannhaAZ ] Bán đất ngõ đường lớn Tỉnh Lộ 295, Bắc Lý, Hiệp Hòa Bắc Giang, DT232m2, MT8,5m', 5, 32, NULL, 4, 12, 185, 1166, 232, 0, 11, N'Khu Dân cư - Xóm 3, Lý Viên', 18, 18, 1, 1, 1, N'/Files/0802023748_screenshot_1581054924_thumbail.png', N'[{"Id":1,"Name":"/Files/0802023748_Thưa đất 230m2.png"}]', N'<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">B&aacute;n đất ng&otilde; đường lớn Tỉnh Lộ 295, Bắc L&yacute;, Hiệp H&ograve;a Bắc Giang, DT232m2, MT8,5m</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Th&ocirc;ng số: Diện t&iacute;ch 232m2, mặt tiền 8,5m vu&ocirc;ng vắn, c&oacute; thể chia 2 l&ocirc; đẹp, hoặc l&agrave;m kho xưởng.</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Vị tr&iacute;: Thuộc x&oacute;m 3 th&ocirc;n L&yacute; Vi&ecirc;n, Bắc L&yacute;, Hiệp H&ograve;a, Bắc giang.</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Ph&aacute;p l&yacute;: SĐCC, 100% đất ở (đất thổ cư), giao dịch nhanh gọn</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Gi&aacute;: thỏa thuận</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Li&ecirc;n hệ ngay để được tư vấn: 0919 679 682 Miễn TG, MG</span></span></p>', CAST(N'2020-02-08T14:58:13.427' AS DateTime), NULL, CAST(N'2020-02-08T14:58:13.427' AS DateTime), NULL, 1, NULL, NULL, 0)
INSERT [dbo].[AZ_Products] ([Id], [Name], [TypeTransaction], [TypeProperty], [ProjectId], [City], [District], [Ward], [Street], [Area], [Price], [Unit], [Address], [HouseDirection], [BalconyDirection], [NumOfFloor], [NumOfBedroom], [NumOfWcs], [ImageMain], [ImageList], [Summary], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsActive], [IsDelete], [DeletedDate], [IsVIP]) VALUES (N'6c6d7847-7ffa-42a8-ba0d-f44a64ada6db', N'[ BannhaAZ ] Bán nhà ngõ Tổ dân phố Đoàn kết, Phố Ngô Thì Sỹ, Vạn Phúc, DT51m2x3T, giá 2,9 tỷ', 5, 28, NULL, 1, 8, 139, 704, 51, 2.9, 8, N'Gần Nhà Văn Hóa Độc Lập', 18, 18, 3, 3, 3, N'/Files/0802112358_6c6abd39ede915b74cf8_thumbail.jpg', N'[{"Id":0,"Name":"/Files/0802112358_04297eac2f7cd7228e6d.jpg"},{"Id":1,"Name":"/Files/0802112358_5dcb954fc49f3cc1658e.jpg"},{"Id":2,"Name":"/Files/0802112358_364ae5ceb41e4c40150f.jpg"},{"Id":3,"Name":"/Files/0802112357_9a6459e10831f06fa920.jpg"},{"Id":4,"Name":"/Files/0802112358_5487b803e9d3118d48c2.jpg"},{"Id":5,"Name":"/Files/0802112401_e1520fd65e06a658ff17.jpg"},{"Id":6,"Name":"/Files/0802112401_b501566806b8fee6a7a9.jpg"},{"Id":7,"Name":"/Files/0802112714_01c84d4d1c9de4c3bd8c.jpg"},{"Id":8,"Name":"/Files/0802112714_afc19d44cc9434ca6d85.jpg"}]', N'<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">B&aacute;n nh&agrave; ng&otilde; Tổ d&acirc;n phố Đo&agrave;n kết, Phố Ng&ocirc; Th&igrave; Sỹ, Vạn Ph&uacute;c, DT51m2x3T, gi&aacute; 2,9 tỷ</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Vị tr&iacute;: Thuộc khu d&acirc;n cư phường Vạn Ph&uacute;c, gần nh&agrave; văn h&oacute;a, khu vui chơi của phường, c&aacute;ch vị tr&iacute; đỗ &ocirc; t&ocirc; ng&agrave;y đ&ecirc;m 20m, v&ocirc; c&ugrave;ng thuận ti&ecirc;n</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Thuận tiện cho gia đ&igrave;nh c&oacute; &ocirc; t&ocirc; gửi gần nh&agrave;, hoặc c&oacute; nhu cầu bắt xe &ocirc; t&ocirc; taxi di chuyển.</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Diện t&iacute;ch: 51m2 nh&agrave; x&acirc;y 3 tầng mới, thửa đất nở hậu, th&ocirc;ng thủy cực tốt.</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Tầng 1: Nh&agrave; để xe, Ph&ograve;ng kh&aacute;ch, cầu thang giữa, gian bếp b&ecirc;n trong, nh&agrave; vệ sinh</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Tầng 2: Hai ph&ograve;ng ngủ, nh&agrave; tắm, nh&agrave; vệ sinh</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Tầng 3: Ph&ograve;ng ngủ rộng, ph&ograve;ng thờ, nh&agrave; tắm, khu m&aacute;y giặt, tum s&acirc;n phơi, t&eacute;c nước&hellip;</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Nội thất: D&ugrave;ng thiết bị cao cấp, gỗ Lim, kết hợp hoa kim l&agrave;m cầu thang, cửa nh&ocirc;m Việt Ph&aacute;p.</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Sổ đỏ ch&iacute;nh chủ, gi&aacute; 2,9 tỷ c&oacute; thương lượng.</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Li&ecirc;n hệ 0919 679 682 gặp Chức (Miễn TG, MG)</span></span></span></span></span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:10.5pt"><span style="background-color:white"><span style="font-family:&quot;Tahoma&quot;,&quot;sans-serif&quot;"><span style="color:black">Hỗ trợ thủ tuch vay vốn ng&acirc;n h&agrave;ng, 70% gi&aacute; trị, l&atilde;i suất ưu đ&atilde;i.</span></span></span></span></span></span></p>', CAST(N'2020-02-08T11:27:34.103' AS DateTime), NULL, CAST(N'2020-02-08T11:27:34.103' AS DateTime), NULL, 1, NULL, NULL, 0)
INSERT [dbo].[AZ_Products] ([Id], [Name], [TypeTransaction], [TypeProperty], [ProjectId], [City], [District], [Ward], [Street], [Area], [Price], [Unit], [Address], [HouseDirection], [BalconyDirection], [NumOfFloor], [NumOfBedroom], [NumOfWcs], [ImageMain], [ImageList], [Summary], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsActive], [IsDelete], [DeletedDate], [IsVIP]) VALUES (N'389d697f-0a08-455a-b359-f84ee3ff739a', N'Bán nhà Phố Hào Nam, Phường Cát Linh, Đống Đa, Hà Nội, Diện tích 32m2x5T, giá 3,25 tỷ', 5, 28, NULL, 1, 1, 9, 83, 32, 3.25, 8, N'Ngõ 86', 18, 18, 5, 3, 4, N'/Files/1102065311_ac2fffd5e6851edb4794_thumbail.jpg', N'[{"Id":0,"Name":"/Files/1102065312_eb3db338ad6855360c79.jpg"},{"Id":1,"Name":"/Files/1102065312_496cac91b5c14d9f14d0.jpg"},{"Id":2,"Name":"/Files/1102065312_1430d027ce7736296f66.jpg"},{"Id":3,"Name":"/Files/1102065311_4377785666069e58c717.jpg"},{"Id":4,"Name":"/Files/1102065311_c206110b0f5bf705ae4a.jpg"},{"Id":5,"Name":"/Files/1102065312_f9bffea4e0f418aa41e5.jpg"},{"Id":6,"Name":"/Files/1102065313_d30d86229872602c3963.jpg"},{"Id":7,"Name":"/Files/1102065313_e046e352fd02055c5c13.jpg"}]', N'<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">B&aacute;n nh&agrave; Phố H&agrave;o Nam, Phường C&aacute;t Linh, Đống Đa, H&agrave; Nội, Diện t&iacute;ch 32m2x5T, gi&aacute; 3,25 tỷ</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Th&ocirc;ng số: Diện t&iacute;ch 32m2, mặt tiền 4,3m, nở hậu 10cm, nh&agrave; x&acirc;y 4,5 tầng khung cột chắc chắn.</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Kh&aacute;ch mua về c&oacute; thể ở ngay kh&ocirc;ng cần sửa chữa nhiều.</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">C&ocirc;ng năng: C&oacute; Ph&ograve;ng kh&aacute;ch, bếp, ph&ograve;ng ăn ở tầng 1, 3 ph&ograve;ng ngủ kh&eacute;p k&iacute;n c&aacute;c tầng 2,3,4, Ph&ograve;ng thờ, khu m&aacute;y giặt, s&acirc;n phơi tầng 5 (đ&atilde; c&oacute; tum chống n&oacute;ng).</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Nh&agrave; tho&aacute;ng mặt sau, n&ecirc;n cầu thang, nh&agrave; vệ sinh đều c&oacute; cửa sổ th&ocirc;ng tho&aacute;ng kh&iacute; tự nhi&ecirc;n</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Vị tr&iacute;: MẶT NG&Otilde; TH&Ocirc;NG, C&aacute;ch mặt phố H&agrave;o Nam 20m, Ng&otilde; trước nh&agrave; gần 3m, thuận tiện chợ d&acirc;n sinh, trường Mẫu Gi&aacute;o, Tiểu học, THCS, Đường H&agrave;o Nam thuận tiện đi c&aacute;c tuyến phố huyết mạch của thủ đ&ocirc;.</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Nội thất: Hệ thống cửa gỗ Lim L&agrave;o, tủ bếp gỗ Tự Nhi&ecirc;n chắc chắn.</span></span></p>

<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Sổ đỏ ch&iacute;nh chủ, gi&aacute; 3,25 tỷ c&oacute; thương lượng</span></span></p>

<p>Li&ecirc;n hệ Mr Th&aacute;i 0971 968 089 (Miễn TG, QC mạng)</p>', CAST(N'2020-02-11T18:55:45.677' AS DateTime), NULL, CAST(N'2020-02-11T18:56:46.447' AS DateTime), NULL, 1, 1, NULL, 0)
INSERT [dbo].[AZ_Products] ([Id], [Name], [TypeTransaction], [TypeProperty], [ProjectId], [City], [District], [Ward], [Street], [Area], [Price], [Unit], [Address], [HouseDirection], [BalconyDirection], [NumOfFloor], [NumOfBedroom], [NumOfWcs], [ImageMain], [ImageList], [Summary], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsActive], [IsDelete], [DeletedDate], [IsVIP]) VALUES (N'e5862782-e50b-49ff-9d68-fd3faca88545', N'BÁN TÒA NHÀ CĂN HỘ DỊCH VỤ NGÕ 535 KIM MÃ, 70M2X8T THANG MÁY, Ô TÔ ĐỖ CỬA, GIÁ 19,9 TỶ', 5, 28, NULL, 1, 2, 26, 176, 70, 19.5, 8, N'575', NULL, NULL, 8, 8, 8, N'/Files/1102124838_ed61734eadf74ba912e6_thumbail.jpg', N'[{"Id":0,"Name":"/Files/1102124837_c0e53fc2e17b07255e6a.jpg"},{"Id":1,"Name":"/Files/1102124837_beb9e79f3926df788637.jpg"},{"Id":2,"Name":"/Files/1102124838_aaeed8d6066fe031b97e.jpg"},{"Id":3,"Name":"/Files/1102124838_a74cda6c04d5e28bbbc4.jpg"},{"Id":4,"Name":"/Files/1102124838_e96ef34a2df3cbad92e2.jpg"},{"Id":5,"Name":"/Files/1102124838_e96ef34a2df3cbad92e2.jpg"},{"Id":6,"Name":"/Files/1102124838_cb73b94a67f381add8e2.jpg"}]', N'<p>Ch&iacute;nh chủ cần b&aacute;n t&ograve;a nh&agrave; căn hộ dịch vụ tại ng&otilde; 535 Kim M&atilde; th&ocirc;ng sang phố Phạm Huy Th&ocirc;ng, phường Kim M&atilde;, quận Ba Đ&igrave;nh, TP H&agrave; Nội. Khu vực c&oacute; rất nhiều người Nhật v&agrave; H&agrave;n sinh sống, l&agrave;m việc n&ecirc;n l&uacute;c n&agrave;o cũng k&iacute;n ph&ograve;ng.</p>

<p>T&ograve;a nh&agrave; gồm 8 ph&ograve;ng, mỗi ph&ograve;ng đều c&oacute; 1 ph&ograve;ng ngủ, 1 ph&ograve;ng kh&aacute;ch, 1 bếp, 1 nh&agrave; vệ sinh ri&ecirc;ng biệt.</p>

<p>Nh&agrave; 2 mặt tho&aacute;ng trước sau n&ecirc;n ph&ograve;ng n&agrave;o cũng ngập tr&agrave;n &aacute;nh s&aacute;ng, rất được kh&aacute;ch thu&ecirc; ưa chuộng.</p>

<p>Ph&ugrave; hợp với những người c&oacute; mục đ&iacute;ch đầu tư bất động sản dưới dạng cho thu&ecirc;, vừa giữ tiền rất tốt, gi&aacute; trị nh&agrave; đất tăng theo thời gian m&agrave; vẫn c&oacute; d&ograve;ng tiền cho thu&ecirc; h&agrave;ng th&aacute;ng chảy v&agrave;o t&agrave;i khoản.</p>

<p>Nh&agrave; đang cho kh&aacute;ch Nhật thu&ecirc;, nếu người mua c&oacute; nhu cầu cho thu&ecirc; th&igrave; sẽ chuyển nhượng lu&ocirc;n cả hợp đồng của kh&aacute;ch thu&ecirc; cho người mua. Người mua sẽ kh&ocirc;ng mất thời gian trống để t&igrave;m kh&aacute;ch thu&ecirc; mới m&agrave; c&oacute; d&ograve;ng tiền thu về lu&ocirc;n.</p>

<p>Sổ đỏ ch&iacute;nh chủ, ph&aacute;p l&yacute; r&otilde; r&agrave;ng</p>

<p>Gi&aacute; b&aacute;n 19,9 tỷ (c&oacute; thương lượng)</p>

<p>Li&ecirc;n hệ ngay 0919 679 682 gặp Chức</p>', CAST(N'2020-02-11T12:50:08.130' AS DateTime), NULL, CAST(N'2020-02-11T12:58:13.380' AS DateTime), NULL, 1, NULL, NULL, 0)
SET IDENTITY_INSERT [dbo].[AZ_Projects] ON 

INSERT [dbo].[AZ_Projects] ([Id], [TypeId], [Name], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsActive], [IsDelete]) VALUES (1, NULL, N'ABCxxx', CAST(N'2020-02-20T11:24:14.133' AS DateTime), 0, CAST(N'2020-02-20T11:24:45.440' AS DateTime), 0, 1, NULL)
INSERT [dbo].[AZ_Projects] ([Id], [TypeId], [Name], [CreatedDate], [CreatedBy], [ModifiedDate], [ModifiedBy], [IsActive], [IsDelete]) VALUES (2, NULL, N'123123', CAST(N'2020-02-20T11:24:52.997' AS DateTime), 0, CAST(N'2020-02-20T11:24:52.997' AS DateTime), 0, 1, NULL)
SET IDENTITY_INSERT [dbo].[AZ_Projects] OFF
SET IDENTITY_INSERT [dbo].[AZ_SendLand] ON 

INSERT [dbo].[AZ_SendLand] ([Id], [Name], [Time], [Address], [Area], [Floor], [Facade], [Price], [Email], [Phone], [BirthYear], [IsRead], [ContentSent], [IsConfirm], [ContentConfirm], [ConfirmBy], [CreatedDate], [ModifiedDate]) VALUES (1, NULL, N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, 0, CAST(N'2020-02-02T00:11:16.407' AS DateTime), CAST(N'2020-02-02T00:11:16.407' AS DateTime))
INSERT [dbo].[AZ_SendLand] ([Id], [Name], [Time], [Address], [Area], [Floor], [Facade], [Price], [Email], [Phone], [BirthYear], [IsRead], [ContentSent], [IsConfirm], [ContentConfirm], [ConfirmBy], [CreatedDate], [ModifiedDate]) VALUES (2, N'Họ và tên ', N'0', N'2', N'2', N'1', N'2', N'2                                                                                                   ', N'trongnv@gmail.com                                 ', N'21                  ', N'1998', 0, N'do bán, mô tả thêm về ngôi nhà, kỳ vọng khi làmdo bán, mô tả thêm về ngôi nhà, kỳ vọng khi làm', 0, NULL, 0, CAST(N'2020-02-02T00:12:38.240' AS DateTime), CAST(N'2020-02-02T00:12:38.240' AS DateTime))
INSERT [dbo].[AZ_SendLand] ([Id], [Name], [Time], [Address], [Area], [Floor], [Facade], [Price], [Email], [Phone], [BirthYear], [IsRead], [ContentSent], [IsConfirm], [ContentConfirm], [ConfirmBy], [CreatedDate], [ModifiedDate]) VALUES (3, N'Họ và tên ', N'6 tháng - 1 năm', N'3', N'3', N'2', N'4', N'4                                                                                                   ', N'trongnv@gmail.com                                 ', N'21                  ', N'1998', 0, N'hắn (lý do bán, mô tả thêm về ngôi nhà, kỳ vọng khi làm việc với Công t', 0, NULL, 0, CAST(N'2020-02-02T00:15:05.847' AS DateTime), CAST(N'2020-02-02T00:15:05.847' AS DateTime))
INSERT [dbo].[AZ_SendLand] ([Id], [Name], [Time], [Address], [Area], [Floor], [Facade], [Price], [Email], [Phone], [BirthYear], [IsRead], [ContentSent], [IsConfirm], [ContentConfirm], [ConfirmBy], [CreatedDate], [ModifiedDate]) VALUES (4, N'Họ và tên ', N'Dưới 1 tháng', N'2', N'1', N'1', N'45', N'1                                                                                                   ', N'trongnv@gmail.com                                 ', N'21                  ', N'2020', 0, N'án, mô tả thêm về ngôi nhà, kỳ vọng khi làm việc với Công ty Godaco...', 0, NULL, 0, CAST(N'2020-02-02T00:19:03.407' AS DateTime), CAST(N'2020-02-02T00:19:03.407' AS DateTime))
INSERT [dbo].[AZ_SendLand] ([Id], [Name], [Time], [Address], [Area], [Floor], [Facade], [Price], [Email], [Phone], [BirthYear], [IsRead], [ContentSent], [IsConfirm], [ContentConfirm], [ConfirmBy], [CreatedDate], [ModifiedDate]) VALUES (5, N'22', N'Dưới 1 tháng', N'11', NULL, NULL, NULL, NULL, NULL, N'22                  ', N'2020', 0, NULL, 0, NULL, 0, CAST(N'2020-02-03T21:30:21.347' AS DateTime), CAST(N'2020-02-03T21:30:21.347' AS DateTime))
SET IDENTITY_INSERT [dbo].[AZ_SendLand] OFF
INSERT [dbo].[AZ_Services] ([Id], [Name], [Image], [Title], [NumberView], [Orders], [Summary], [Active], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (N'00000000-0000-0000-0000-000000000000', NULL, NULL, NULL, 0, 0, NULL, 0, NULL, CAST(N'2020-02-01T23:48:54.163' AS DateTime), NULL, CAST(N'2020-02-01T23:48:54.167' AS DateTime))
INSERT [dbo].[AZ_Services] ([Id], [Name], [Image], [Title], [NumberView], [Orders], [Summary], [Active], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (N'6b17c5ca-32d0-4408-a994-130dd69f886d', N'1', N'/Files/b416ba40-703f-4e71-9e8e-227dfce11019.jpg', N'2', 0, 4, N'5', 0, NULL, CAST(N'2019-11-29T22:18:11.303' AS DateTime), NULL, CAST(N'2019-11-29T00:00:00.000' AS DateTime))
INSERT [dbo].[AZ_Services] ([Id], [Name], [Image], [Title], [NumberView], [Orders], [Summary], [Active], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (N'6b17c5ca-32d0-4408-a994-130dd69f886f', N'Dịch vụ đặt mua hàng 1688', N'https://www.thuongdo.com/sites/default/files/dichvu/nhap-hang-1688.jpg', N'', 2, 2, N'<h3>2. Bảng giá dịch vụ mua hàng</h3>

<table>
	<thead>
		<tr>
			<th>GIÁ TRỊ ĐƠN HÀNG</th>
			<th>% PHÍ DỊCH VỤ</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>0 đến 30triệu</td>
			<td>5%</td>
		</tr>
		<tr>
			<td>30 đến 50triệu</td>
			<td>4%</td>
		</tr>
		<tr>
			<td>50 đến 100triệu</td>
			<td>3%</td>
		</tr>
		<tr>
			<td>100 đến 300triệu</td>
			<td>2%</td>
		</tr>
		<tr>
			<td>Trên 300tr</td>
			<td>%</td>
		</tr>
	</tbody>
</table>

<h3>3. Phí ship Trung Quốc</h3>

<table>
	<thead>
		<tr>
			<th>LOẠI HÌNH</th>
			<th colspan="2">GIẢI THÍCH</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Chuyển phát nhanh thông thường</td>
			<td>Kg đầu dựa vào quy định của nhà cung cấp trên trang Taobao hoặc 1688</td>
			<td>Kg tiếp theo nếu nhà cung cấp thuộc tỉnh Quảng Đông là 4 tệ, tỉnh khác là 8 tệ</td>
		</tr>
		<tr>
			<td>Chuyển phát nhanh siêu tốc</td>
			<td>Kg đầu dựa vào quy định của nhà cung cấp trên trang Taobao hoặc 1688</td>
			<td>Mỗi 0.5kg tiếp theo là 5 tệ/kg</td>
		</tr>
		<tr>
			<td>Chuyển phát thường bằng oto tải</td>
			<td colspan="2">Mỗi kg 1 tệ/kg + 70 tệ/đơn hàng</td>
		</tr>
	</tbody>
</table>

<h3>4. Phí vận chuyển theo cân nặng</h3>

<table border="1" cellpadding="1" cellspacing="1">
	<thead>
		<tr>
			<th scope="col">Tên</th>
			<th scope="col">Loại</th>
			<th scope="col">Hà Nội</th>
			<th scope="col">TP.HCM</th>
			<th scope="col">Hải Phòng</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>≤ 20kg</td>
			<td>kg</td>
			<td>23,000<sup>đ</sup></td>
			<td>31,000<sup>đ</sup></td>
			<td>26,000<sup>đ</sup></td>
		</tr>
		<tr>
			<td>21 → 100kg</td>
			<td>kg</td>
			<td>20,000<sup>đ</sup></td>
			<td>27,000<sup>đ</sup></td>
			<td>23,000<sup>đ</sup></td>
		</tr>
		<tr>
			<td>101 → 500kg</td>
			<td>kg</td>
			<td>17,000<sup>đ</sup></td>
			<td>23,000<sup>đ</sup></td>
			<td>20,000<sup>đ</sup></td>
		</tr>
		<tr>
			<td>&gt; 500kg</td>
			<td>kg</td>
			<td>14,000<sup>đ</sup></td>
			<td>20,000<sup>đ</sup></td>
			<td>17,000<sup>đ</sup></td>
		</tr>
		<tr>
			<td>≤ 5m3</td>
			<td>m3</td>
			<td>2,400,000<sup>đ</sup></td>
			<td>2,700,000<sup>đ</sup></td>
			<td>2,500,000<sup>đ</sup></td>
		</tr>
		<tr>
			<td>5 → 10m3</td>
			<td>m3</td>
			<td>2,100,000<sup>đ</sup></td>
			<td>2,500,000<sup>đ</sup></td>
			<td>2,200,000<sup>đ</sup></td>
		</tr>
		<tr>
			<td>10 → 20m3</td>
			<td>m3</td>
			<td>1,800,000<sup>đ</sup></td>
			<td>2,400,000<sup>đ</sup></td>
			<td>1,900,000<sup>đ</sup></td>
		</tr>
		<tr>
			<td>&gt; 20m3</td>
			<td>m3</td>
			<td>1,700,000<sup>đ</sup></td>
			<td>2,300,000<sup>đ</sup></td>
			<td>1,800,000<sup>đ</sup></td>
		</tr>
	</tbody>
</table>

<p><strong>Lưu ý:</strong></p>

<p><em>- Mọi mặt hàng đều có cân nặng thực tế và cân nặng quy đổi.</em></p>

<p><em>- Đối với những mặt hàng cồng kềnh thì khối lượng đơn hàng sẽ được tính bằng công thức quy đổi.</em></p>

<p><em>- Khối lượng quy đổi được tính theo công thức:</em>&nbsp;<strong>Chiều dài * chiều rộng * chiều cao / 6000 = Cân nặng đơn hàng</strong></p>

<p><em>- Cân nặng được áp dụng là mức cân nặng cao hơn của trọng lượng thực và khối lượng quy đổi.</em></p>

<p>-&nbsp;<em>Khối lượng đơn hàng sẽ được làm tròn theo 0.5kg</em></p>

<p><strong><em>VD: Đơn hàng có trọng lượng: 1.3kg thì sẽ được làm tròn lên 1.5kg, Đơn hàng có trọng lượng: 58.8kg sẽ được làm tròn lên: 59kg…</em></strong></p>

<p>Khách hàng chú ý, với đơn hàng của quý khách gồm nhiều sản phẩm, và về làm nhiều đợt thì hàng về tới đâu công ty sẽ tính phí tới đó. Phí vận chuyển sẽ tính theo số hàng về của khách trong một thời điểm chứ không tính theo tổng đơn hàng.</p>

<ol>
	<li>Cân nặng quy đổi</li>
	<li>Quy tắc làm tròn</li>
	<li>Tính giá vận chuyển khi hàng về</li>
</ol>

<h3>5. Phí kiểm đếm sản phẩm</h3>

<table>
	<thead>
		<tr>
			<th>SỐ LƯỢNG</th>
			<th>MỨC GIÁ (VNĐ)/ 1 sản phẩm</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>1-2 sản phẩm</td>
			<td>5,000<sup>đ</sup></td>
		</tr>
		<tr>
			<td>3-10 sản phẩm</td>
			<td>3,500<sup>đ</sup></td>
		</tr>
		<tr>
			<td>11-100 sản phẩm</td>
			<td>2,000<sup>đ</sup></td>
		</tr>
		<tr>
			<td>101-500 sản phẩm</td>
			<td>1,500<sup>đ</sup></td>
		</tr>
		<tr>
			<td>501-10000 sản phẩm</td>
			<td>1,000<sup>đ</sup></td>
		</tr>
	</tbody>
</table>

<h3>6. Phí đóng gỗ</h3>

<table>
	<thead>
		<tr>
			<th>&nbsp;</th>
			<th>Kg đầu tiên</th>
			<th>Kg tiếp theo</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Phí đóng kiện</td>
			<td>20&nbsp;<sup>tệ</sup></td>
			<td>1&nbsp;<sup>tệ</sup></td>
		</tr>
	</tbody>
</table>

<h3>7. Cấp độ thành viên</h3>

<table>
	<thead>
		<tr>
			<th>Tên cấp độ</th>
			<th>Tổng giá trị giao dịch</th>
			<th>Chiết khấu phí giao dịch</th>
			<th>Chiết khấu phí vận chuyển</th>
			<th>% đặt cọc</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Cấp độ 1 sao</td>
			<td>0<sup>đ</sup>&nbsp;- 100,000,000<sup>đ</sup></td>
			<td>%</td>
			<td>%</td>
			<td>90 %</td>
		</tr>
		<tr>
			<td>Cấp độ 2 sao</td>
			<td>100,000,000<sup>đ</sup>&nbsp;- 500,000,000<sup>đ</sup></td>
			<td>5 %</td>
			<td>1 %</td>
			<td>85 %</td>
		</tr>
		<tr>
			<td>Cấp độ 3 sao</td>
			<td>500,000,000<sup>đ</sup>&nbsp;- 1,000,000,000<sup>đ</sup></td>
			<td>8 %</td>
			<td>3 %</td>
			<td>75 %</td>
		</tr>
		<tr>
			<td>Cấp độ 4 sao</td>
			<td>1,000,000,000<sup>đ</sup>&nbsp;- 5,000,000,000<sup>đ</sup></td>
			<td>10 %</td>
			<td>5 %</td>
			<td>60 %</td>
		</tr>
		<tr>
			<td>Cấp độ 5 sao</td>
			<td>5,000,000,000<sup>đ</sup>&nbsp;- 50,000,000,000<sup>đ</sup></td>
			<td>15 %</td>
			<td>10 %</td>
			<td>50 %</td>
		</tr>
	</tbody>
</table>', 0, NULL, CAST(N'2012-02-02T00:00:00.000' AS DateTime), NULL, CAST(N'2018-05-03T00:04:13.567' AS DateTime))
INSERT [dbo].[AZ_Services] ([Id], [Name], [Image], [Title], [NumberView], [Orders], [Summary], [Active], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (N'8e09649b-257f-425b-b509-1e1a84457830', N'3', N'/Files/11ca59b4-0217-4531-ba7d-da36188f606e.jpg', N'4xx', 0, 2, N'xx', 0, NULL, CAST(N'2019-11-29T22:35:39.770' AS DateTime), NULL, CAST(N'2019-11-29T00:00:00.000' AS DateTime))
INSERT [dbo].[AZ_Services] ([Id], [Name], [Image], [Title], [NumberView], [Orders], [Summary], [Active], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (N'be1ca402-4d34-461b-8ff1-297ed8999da7', N'2', N'3', N'2', 0, 0, N'', 0, NULL, CAST(N'2020-01-17T11:24:43.033' AS DateTime), NULL, CAST(N'2020-01-17T11:26:22.950' AS DateTime))
INSERT [dbo].[AZ_Services] ([Id], [Name], [Image], [Title], [NumberView], [Orders], [Summary], [Active], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (N'f67cb5ae-d002-4a11-81a5-2fc5571278a6', N'NOTE', N'/Files/0802011006_note-1-1-750x294.gif', N'LƯU Ý KHI ĐẶT CỌC MUA BÁN NHÀ ĐẤT - NẾU NHƯ KHÔNG MUỐN MẤT TIỀN OAN', 0, 3, N'<h1>LƯU Ý KHI ĐẶT CỌC MUA BÁN NHÀ ĐẤT - NẾU NHƯ KHÔNG MUỐN MẤT TIỀN OAN</h1>

<p>Dưới đây là những khuyến cáo của chuyên gia bất động sản về những điều bạn cần lưu ý khi đặt cọc mua bất động sản.</p>

<p>Sau khi xem xong bất động sản, nếu thoả thuận được giá cả, đây là danh sách những việc cần kiểm tra:</p>

<p><strong><em>1. Kiểm tra tính chính danh của chủ nhà: chủ nhà có phải là chính chủ không?</em></strong></p>

<p>Đối chiếu thông tin chủ nhà: tên, ảnh, số chứng minh nhân dân hoặc căn cước công dân trên sổ đỏ có trùng khớp với thông tin trên chứng minh nhân dân hoặc sổ đỏ không.</p>

<p>Xin một bản photo sổ hồng đem lên phường hoặc tổ dân phố để hỏi. Thường thì tổ trưởng dân phố hoặc Uỷ ban phường sẽ nắm rất rõ chủ nhà đó có phải là chính chủ hay không.</p>

<p><em><strong>2. Kiểm tra xem nhà có bị quy hoạch không?</strong></em></p>

<p>Thông tin này có thể&nbsp;kiểm tra tại Phòng Quản lý đô thị hoặc bộ phận kiểm tra quy hoạch tại các Uỷ ban nhân dân Quận/Huyện nơi bất động sản toạ lạc.</p>

<p><em><strong>3. Kiểm tra xem nhà có bị ngăn chặn giao dịch không?</strong></em></p>

<p>Hãy mang giấy photo chủ quyền nhà đến Phòng Công chứng hoặc Văn phòng công chứng để hỏi. Một số căn nhà vướng các vụ kiện tụng về tranh chấp tài sản, kê biên thi hành án... sẽ bị ngăn chặn không công chứng được. Phải cẩn thận kẻo mất cọc nếu không công chứng được do vướng trường hợp này.</p>

<p><em><strong>4. Soạn thảo hợp đồng đặt cọc?</strong></em></p>

<p>Bên nào soạn thảo hợp đồng đặt cọc là bên có lợi. Hợp đồng đặt cọc là văn bản quan trọng nhất vì nó là văn bản được ký đầu tiên giữa hai bên giao dịch. Nên nhờ một chuyên gia có kinh nghiệm hoặc một luật sư chuyên về nhà đất giúp khâu này.</p>

<p>Những điều cần chú ý trước khi ký hợp đồng đặt cọc:</p>

<p>Hợp đồng đặt cọc có thể công chứng hoặc không. Nếu không công chứng, hợp đồng vẫn có hiệu lực pháp luật theo điều 328, Bộ luật Dân sự năm 2015 về hợp đồng đặt cọc.&nbsp;</p>

<p>Kiểm tra toàn bộ các điều khoản liên quan: thông tin nhân thân, địa chỉ nhà, số tờ, số thửa, bản đồ vị trí, giá mua bán, các đợt thanh toán, ngày bàn giao nhà, thuế, lệ phí...</p>

<p>Khi ký phải có đủ vợ và chồng của bên bán, không để trường hợp chỉ một người ký sau này rất rắc rối.</p>

<p>Phải chắc chắn có biên bản xác nhận giao nhận tiền hoặc uỷ nhiệm chi của ngân hàng sau khi đã giao tiền cho bên bán.</p>

<p><strong>Những việc cần làm sau khi ký hợp đồng đặt cọc:</strong></p>

<p>- Nếu phải vay ngân hàng nên liên lạc ngay để hỏi thủ tục ở các ngân hàng. Chọn ngân hàng cho vay tốt nhất phù hợp với điều kiện tài chính của mình.</p>

<p>- Giải quyết với người thuê trong trường hợp căn nhà đang có người thuê hiện tại.</p>

<p>- Chuẩn bị tài chính.</p>

<p>- Những lưu ý này đều vô cùng quan trọng, bỏ qua một bước đều tiềm ẩn rủi ro cho bạn.</p>

<p>Tổng hợp bởi BannhaAZ.com</p>', 1, NULL, CAST(N'2020-02-08T13:11:13.193' AS DateTime), NULL, CAST(N'2020-02-08T13:11:13.193' AS DateTime))
INSERT [dbo].[AZ_Services] ([Id], [Name], [Image], [Title], [NumberView], [Orders], [Summary], [Active], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (N'd19c45a8-eac5-46bc-91c1-3f064d4cbc60', N'Thuế, lệ phí', N'/Files/0802010503_Thue.jpg', N'Các loại thuế phí, lệ phí phải nộp khi mua bán nhà, đất', 0, 2, N'<h1>Các khoản thuế, phí, lệ phí phải nộp khi mua bán nhà, đất ở</h1>

<p>&nbsp;</p>

<p>Theo quy định của pháp luật hiện nay thì các khoản phí, lệ phí khi thực hiện thủ tục chuyển quyền sử dụng đất bao gồm:</p>

<p>Thứ nhất, về Thuế thu nhập cá nhân.</p>

<p>Căn cứ vào công văn số 17526/BTC-TCT về Triển khai thực hiện một số nội dung tại Luật sửa đổi, bổ sung một số điều của các Luật về thuế. Theo Điểm c Tiểu mục 1.1 Mục 1 của Công văn này quy định về Thuế thu nhập cá nhân trong trường hợp chuyển nhượng bất động sản của cá nhân như sau:“Từ 01/01/2015, áp dụng một mức thuế suất 2 trên giá chuyển nhượng từng lần đối với chuyển nhượng bất động sản thay cho thực hiện 02 phương pháp tính thuế trước đây”.</p>

<p>Như vậy, Thuế thu nhập cá nhân = 2 x (Giá chuyển nhượng)</p>

<p>Ngoài ra, căn cứ theo Khoản 2 Điều 4 Luật thuế thu nhập cá nhân 2007: “Thu nhập từ chuyển nhượng nhà ở, quyền sử dụng đất ở và tài sản gắn liền với đất ở của cá nhân trong trường hợp cá nhân chỉ có một nhà ở, đất ở duy nhất” sẽ được miễn thuế.</p>

<p>Thứ hai, Lệ phí trước bạ.</p>

<p>Căn cứ quy định tại khoản 1 Điều 7 Nghị định số 45/2011/NĐ-CP thì: “Mức lệ phí trước bạ đối với nhà, đất là 0,5”</p>

<p>Căn cứ theo Khoản 3 Điều Thông tư 34/2013/TT-BTC thì Số tiền lệ phí trước bạ phải nộp được xác định như sau:</p>

<p>Số tiền lệ phí trước bạ phải nộp = (Giá trị tài sản tính lệ phí trước bạ) x (Mức lệ phí trước bạ)</p>

<p>Giá trị đất = (diện tích đất) x (giá đất). Mỗi địa phương quy định giá đất khác nhau</p>

<p>Như vậy, Số tiền lệ phí trước bạ của bạn = (Diện tích đất) x ( Giá đất) x 0,5</p>

<p>Thứ ba, các lệ phí khác.</p>

<p>Lệ phí công chứng: Mức thu lệ phí chứng thực hợp đồng, giao dịch liên quan đến bất động sản được xác định theo giá trị tài sản hoặc giá trị hợp đồng, giao dịch được quy định chi tiết tại Khoản 1 Điều 3 Thông tư liên tịch 62/2013/TTLT-BTC-BTP</p>

<p>Lệ phí địa chính: 15.000 đồng</p>

<p>Lệ phí thẩm định: 0,15 giá trị chuyển nhượng (tối thiểu 100.000 đồng và tối đa 5.000.000 đồng).</p>

<p>Tổng 3 loại trên là: 2,65% giá trị giao dịch.</p>

<p><br />
Tổng hợp bởi BannhaAZ.com</p>', 1, NULL, CAST(N'2020-02-08T13:03:55.640' AS DateTime), NULL, CAST(N'2020-02-08T13:06:32.787' AS DateTime))
INSERT [dbo].[AZ_Services] ([Id], [Name], [Image], [Title], [NumberView], [Orders], [Summary], [Active], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (N'7cc275a4-7373-4f88-b1d7-57639340f066', N'Gửi các bạn mới vào nghề hoặc đang có ý định làm nghề môi giới BĐS thổ cư!', N'/Files/0802124957_giay-to-de-lam-so-do_1909135627.jpg', N'Gửi các bạn mới vào nghề hoặc đang có ý định làm nghề môi giới BĐS thổ cư!', 0, 2, N'<p><span style="font-size:14px;">Gửi các bạn mới vào nghề hoặc đang có ý định làm nghề môi giới BĐS thổ cư!<br />
Bạn mới vào nghề môi giới bất động sản thổ cư? Bạn đang có ý định làm nghề môi giới bất động sản thổ cư? Bạn muốn tìm hiểu thêm về lĩnh vực môi giới bất động sản?<br />
&nbsp; &nbsp; &nbsp; &nbsp; Trong những năm gần đây, với sự phát triển mạnh của ngành bất động sản thì nghề môi giới bất động sản đang trở thành một trong những ngành nghề ‘hot’ nhất và thu hút nhất hiện nay. Ngành nghề mà khi nghe đến người ta liên tưởng tới những người có vẻ ngoài hào nhoáng “đồ hiệu,áo trắng cổ cồn”, check in sang chảnh, “du lịch Châu Âu, nghỉ Resot”, rồi có thu nhập lớn “ mua xe, đổi nhà”... Chính vì sức hút đó mà trong mấy năm gần đây mọi người đổ xô đi làm môi giới BĐS, có thể nói người người làm môi giới, nhà nhà làm môi giới từ sinh viên mới ra trường cho tới cử nhân, công nhân, giao viên, công chức, kỹ sư...<br />
&nbsp; &nbsp; &nbsp; &nbsp;<br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Bản thân tôi là một kỹ sư xây dựng giao thông ra trường và đi làm đã 7 năm. Với tính chất công việc thường xuyên phải xa gia đình, một năm số ngày ở nhà đếm trên đầu ngón tay, áp lực công việc cực lớn đặc biệt trong thời gian công trình đi vào đường “gantt” tiến độ. Mức lương ổn định và thuộc loại khá tuy nhiên với cuộc sống công trường thì việc tiết kiệm gửi về cho gia đình cũng không đáng là bao, chưa kể việc các công ty nợ lương, cắt thưởng. Bởi vậy khi nghe bạn tôi giới thiệu về việc làm nghề môi giới bất động sản thổ cư tôi cũng rất hứng thú và muốn thử sức. Bản thân tôi nghĩ việc làm nghề môi giới BĐS không phải là nghề xấu không lừa lọc ai, chủ động thời gian cho công việc gia đình, có thu nhập tốt và đột biến nếu có ‘duyên’ với nghề, đặc biệt là mình sẽ biết thêm một lĩnh vực mới mà với một người đàn ông trước sau gì chúng ta cũng sẽ va chạm tới đó là việc mua đất, mua nhà. Vì vậy, tôi nộp đơn xin nghỉ việc 1 tuần sau buổi nói chuyện với bạn minh.<br />
&nbsp; &nbsp; &nbsp; &nbsp;<br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Tuy nhiên, sau gần 1 năm làm trong lĩnh vực này tôi nhận ra rằng nghề môi giới BĐS không phải là một nghề nghiệp dễ dàng nếu không muốn nói là nghề khó. Tỷ lệ người trụ lại được với nghề rất ít, mức độ đào thải của ngành này rất cao. Nhưng những người tồn tại lâu năm được với nghề thì hầu hết là những người rất thành công, họ có được bản lĩnh của người đầu sóng ngọn gió, có kiến thức, kỹ năng tốt. Bài viết này tôi không hứa là sẽ giúp bạn trở thành những người giỏi như vậy, nhưng tôi sẽ giúp bạn hiểu hơn về nghề môi giới BĐS thổ cư, giúp các bạn mới bước vào nghề hoặc đang có ý định chuyển nghề sang làm môi giới BĐS thổ cư nắm được những điều cốt yếu để bắt đầu với nghề được hiệu quả hơn.</span></p>

<p><span style="font-size:14px;">1. Hiểu rõ về nghề và vững bước trên con đường mình đã chọn.<br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Ngay từ đầu bạn phải trả lời được rằng nghề môi giới bất động sản là gì? tại sao bạn chọn nghề môi giới bất động sản? Ngành nghề này mang lại cho bạn được những gì sau 1 năm, 2 năm, 3 năm và lâu hơn nữa? Bạn có thể gắn bó được lâu dài với nghề hay không? Nếu không có thu nhập thì bạn có thể trụ được bao lâu? Khi bạn trả lời được các câu hỏi trên thì bạn sẽ xác định được hướng đi rõ ràng hơn cho bản thân và vững bước trên con đường mình đã chọn.<br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<br />
&nbsp; &nbsp; &nbsp; &nbsp; Khi nhắc tới nghề môi giới bất động sản điều đầu tiên mọi người nghĩ tới “cò đất” chuyên lừa đào, là người sống bằng tiền hoa hồng cao ngất ngưởng, là những người chỉ chuyên mặc đồ đẹp, uốn ba tấc lưỡi để chèo kéo khách mua nhà hay là nghề dành cho sinh viên mới ra trường chưa có kinh nghiệm thực tế chấp nhận làm để kiếm thêm kinh nghiệm viết vào CV. Tuy nhiên thực tế không phải vậy, nhân viên môi giới là đội ngũ không thể thiếu trong kinh doanh bất động sản vì nhờ có họ mà các giao dịch nhà đất trở nên thuận lợi hơn. Bởi vì họ là những người giúp cho người mua chọn được căn nhà phù hợp với tài chính và nhu cầu cuộc sống; giúp cho người bán bán được với mức giá tốt nhất; ngoài ra họ còn hỗ trợ khách hàng trong các thủ tục giấy tờ giao dịch nhà đất. Nghề môi giới bất động sản ở Việt Nam ban đầu mang tính tự phát và thường bị hiểu nhầm với nghề cò đất, do đó nghề này chưa được coi trọng. Nếu như cò đất là những người sử dụng các mánh khóe để làm ăn, thì những người môi giới nhà đất lại sử dụng kiến thức chuyên môn về bất động sản, tầm nhìn và khả năng đánh giá thị trường để có thể tư vấn tốt nhất cho khách hàng của mình. Hai nghề này không phải là một như mọi người vẫn nghĩ.&nbsp;<br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Khi làm nghề môi giới bất động sản thu nhập chính của bạn không phải là từ lương cứng từ các sàn giao dịch trả cho bạn mà là từ việc bán hàng nên bạn cần xác định rõ ràng và có kế hoạch chuẩn bị cho cuộc sống của bạn trong trường hợp không bán được hàng. Bán hàng quần áo, rau củ quả, thịt cá đã khó, bán bất động sản còn khó gấp vạn lần. Bán rau, củ, quả, thịt, cá, quần áo bạn không bán bạn có thể tặc lưỡi thôi thi mang về dùng vậy. Nhưng bán bất động sản bạn không thể làm như vậy được, bạn không bán được hàng đồng nghĩa với bạn không có thu nhập, không có đủ tiền để sinh hoạt và bạn sẽ tự bị đào thải khỏi nghề. Tuy nhiên nếu bạn bán được hàng thì thu nhập của bạn sẽ rất cao bởi vì giao dịch bất động sản có giá trị cao do đó hoa hồng mà nhân viên môi giới có được cũng rất hấp dẫn.<br />
&nbsp; &nbsp; &nbsp; &nbsp;<br />
&nbsp; &nbsp; &nbsp; &nbsp; Thời gian làm việc rất linh động và không gò bó. Bạn sẽ không phải ngồi 8 tiếng trong văn phòng hàng ngày, mà được di chuyển tới nhiều địa điểm khác nhau. Nơi làm việc của bạn có thể là các quán trà đá vỉa hè, các quán café, là lang thang dong duổi khắp đường cùng ngõ hẻm trên phố (dẫn khách đi xem nhà, gặp gỡ khách hàng, tìm kiếm nguồn nhà, đánh giá thị trường...). Nhưng bạn cũng phải làm quen với việc phải làm việc vào ngày nghỉ và ngoài giờ hành chính. Bởi vào thời điểm đó khách hàng của bạn mới rảnh và có thời gian dành cho việc đi xem nhà.<br />
&nbsp; &nbsp; &nbsp; &nbsp;<br />
&nbsp; &nbsp; &nbsp; &nbsp; Với tính chất công việc thường xuyên phải giao tiếp với nhiều người đến từ nhiều lĩnh vực khác nhau và đa phần đều là những người có tiền nên bạn sẽ cải thiện được nhiều kỹ năng mềm cũng như mở rộng thêm được nhiều mối quan hệ tốt giúp bạn phát triển bản thân sau này.<br />
&nbsp; &nbsp; &nbsp; &nbsp;<br />
&nbsp; &nbsp; &nbsp; &nbsp; Rèn luyện mình một tinh thần thép để đối mặt với những tĩnh huống như: mọi thỏa thuận đàm phán đã xong xuôi nhưng lại khách hàng lại hủy giao dịch vào phút chót; hoặc khách hàng hẹn bạn nhưng khi đến nơi họ lại không tới...</span></p>

<p><span style="font-size:14px;">2. Hãy tìm cho mình một người hướng dẫn và một môi trường làm việc tốt phù hợp với bản thân.<br />
&nbsp; &nbsp; &nbsp; &nbsp;<br />
&nbsp; &nbsp; &nbsp; &nbsp; Nhiều bạn tự mò mẫm vào nghề, làm một môi giới bất động sản tự do tự tại. Đây cũng có thể là cách hay nếu bạn am hiểu và luôn sẵn sàng làm leader cho chính mình. Số khác chọn con đường tham gia vào các công ty lớn với mong muốn có được danh tiếng, thương hiệu sau thời gian làm việc. Thực tế thì cả hai đều có thể mắc sai lầm do thiếu người dẫn dắt đủ tâm và tầm.<br />
&nbsp; &nbsp; &nbsp; &nbsp;<br />
&nbsp; &nbsp; &nbsp; &nbsp; Khi bạn mới vào nghề hãy tìm cho mình một người dẫn dắt có đủ tâm và đủ tầm điều này sẽ giúp bạn hiểu hơn về nghề, cải thiện được nhiều kỹ năng để có thể vững bước với nghề. Môi trường làm việc cũng là yếu tố sẽ giúp bạn làm việc hiệu quả và cảm thấy yêu công việc mình lựa chọn hơn. Tôi khuyên bạn không nên làm môi giới tự do cũng không nhất thiết phải vào công ty lớn; bởi vì khi vào công ty bạn sẽ phải chia sẻ phần hoa hồng thu được từ một giao dịch tuy nhiên bạn sẽ nhận được nhiều người hỗ trợ bạn trong việc bán sản phẩm của mình và giải quyết các tình huống khó khăn khi gặp phải; nếu làm trong công ty lớn việc thu phí từ mỗi giao dịch sẽ lớn để đáp ứng cho hoạt động của công ty cũng như đủ chí phí chi trả cho nhân viên điều này sẽ tác động đến giá bán cuối cùng của sản phẩm dẫn đến việc khó chốt được giao dịch. Jack Ma có câu nói nổi tiếng: “Trước 20 tuổi, hãy học hành cho tốt. Trước 30 tuổi, hãy theo một ai đấy, vào một công ty nhỏ mà làm, vì ở công ty nhỏ, bạn sẽ học được cách đam mê, học cách khao khát, học được cách làm nhiều việc cùng một lúc.” Và “Trước 30 tuổi, quan trọng không phải theo công ty nào mà theo người sếp nào. Người sếp giỏi dạy chúng ta rất khác”.<br />
&nbsp; &nbsp; &nbsp; &nbsp;<br />
&nbsp; &nbsp; &nbsp; &nbsp; Công ty CP địa ốc Godaco không hẳn phải là một công ty lớn nhưng anh em trong công ty rất đoàn kết gắn bó như anh em trong nhà, hàng năm công ty có những đợt tuyển dụng nhân viên kinh doanh, bằng kinh nghiệm thự tế quý báu, những người đã thành công trong nghề sẽ dẫn dắt theo hình thức 1 kèm 1 với mục đích đào tạo ra những chuyên viên bất động sản chuyên nghiệp, có kiến thức sâu rộng và đam mê và có tinh thần trách nhiệm cao với nghề hy vọng sẽ là một môi trường làm việc tốt cho các bạn.</span></p>

<p><span style="font-size:14px;">3. Trau dồi kỹ năng và kiến thức về mọi lĩnh vực.<br />
&nbsp; &nbsp; &nbsp; &nbsp;<br />
&nbsp; &nbsp; &nbsp; &nbsp; Thông thường khi mới vào nghề môi giới BĐS thổ cư các bạn có rất nhiều thời gian để học hỏi trau dồi kiến thức bởi khi mới vào nghề thời gian của bạn chủ yếu sử dụng cho việc xem nhà để hiểu rõ sản phẩm và đăng tin tìm kiếm khách hàng. Đây là thời gian bạn nên tìm hiểu thêm về các kiến thức về lĩnh vực bất động sản như các loại thuế, phí khi giao dịch bất động sản thổ cư, cách xác định hướng nhà phù hợp với tuổi khách hàng, các kiểu nhà bị lỗi phong thủy... Tuy nhiên đa phần các bạn không dành thời gian cho công việc này mà tranh thủ lướt web, facebook, chat với bạn bè nên kiến thức chuyên môn bị thiếu và hổng nhiều.<br />
&nbsp; &nbsp; &nbsp; &nbsp;<br />
&nbsp; &nbsp; &nbsp; &nbsp; Bất kể nghề nào cũng cần phải được đào tạo, huấn luyện thì mới có thể làm tốt được. Hãy tham gia các khóa đào tạo nghiệp vụ để được trang bị các kiến thức cần thiết và các bí quyết hữu ích để biết cách tìm kiếm được khách hàng tiềm năng, làm việc với khách hàng và chốt sale thành công. Ngoài kiến thức, kỹ năng, bạn còn có cơ hội được làm quen nói chuyện với những người đã thành công trong nghề.<br />
&nbsp; &nbsp; &nbsp; &nbsp;<br />
&nbsp; &nbsp; &nbsp; &nbsp; Bạn sẽ phải tiếp xúc nói chuyện với rất nhiều khách hàng với những thế giới quan khác nhau. Làm thế nào để cuộc nói chuyện của bạn không bị tẻ nhạt bởi các thông tin nhà đất, và quan trọng là để lại ấn tượng với khách hàng, giúp họ nhớ bạn mà không phải vô số các nhân viên nhà đất khác mà họ tiếp xúc. Hãy thêm muối vào câu chuyện của mình. Cuộc sống muôn màu giúp bạn có nhiều thứ để nói, vì vậy hãy trang bị cho mình kiến thức về nhiều lĩnh vực. Để có được một vốn hiểu biết phong phú, đòi hỏi bạn cần phải tự trau dồi cho bản thân mình. Hãy đọc sách, tìm hiểu về tình hình thời sự,…đừng chỉ chăm chăm vào các thông tin dự án hay lướt Facebook, Instagram một cách vô thức, lãng phí thời gian.<br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<br />
&nbsp; &nbsp; &nbsp; &nbsp; Nghề môi giới bất động sản là nghề cần thiết trong xã hội hiện nay vì nhờ có nó mà các giao dịch nhà đất trở nên thuận lợi hơn. Hy vọng qua bài viết này, các bạn sẽ hiểu rõ hơn về nghề môi giới bất động sản thổ cư nói riêng và nghề môi giới bất động sản nói chung. Chúc các bạn đã, đang và sẽ làm nghề này có đủ bản lĩnh để vượt qua các thử thách để thành công</span></p>

<p><span style="font-size:14px;">Tác giả : Chính Hạnh&nbsp;</span></p>', 1, NULL, CAST(N'2020-02-08T12:50:21.053' AS DateTime), NULL, CAST(N'2020-02-08T12:50:21.053' AS DateTime))
INSERT [dbo].[AZ_Services] ([Id], [Name], [Image], [Title], [NumberView], [Orders], [Summary], [Active], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (N'16efc728-169e-4037-9b38-6fa932e67a73', N'3', N'/Files/de4698ee-ebd2-4c7f-9265-088496a5fe87.jpg', N'3', 0, 2, N'3', 0, NULL, CAST(N'2019-11-29T22:33:25.923' AS DateTime), NULL, CAST(N'2019-11-29T00:00:00.000' AS DateTime))
INSERT [dbo].[AZ_Services] ([Id], [Name], [Image], [Title], [NumberView], [Orders], [Summary], [Active], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (N'7f1738ca-269b-4288-8041-881618f7f203', N'8 ĐIỀU CẦN BIẾT KHI ĐI MUA NHÀ ĐẤT THỔ CƯ', N'https://blog.rever.vn/hubfs/luu-y-khi-mua-nha-4.jpg', N'Với đa số người dân Việt Nam, mua nhà là một việc lớn trong đời, do đó cần hết sức thận trọng để mua được căn nhà như ý. Dưới đây là 8 vấn đề hàng đầu mà người mua nhà cần lưu ý.', 0, 1, N'<h2><strong>1. Kiểm tra kĩ pháp lý căn nhà</strong></h2>

<p>Pháp lý là nhân tố quan trọng hàng đầu mà người mua nhà cần kiểm tra trước khi “xuống tiền”. Với những người không làm trong lĩnh vực nhà đất thường thiếu kinh nghiệm và khả năng&nbsp;kiểm tra tính minh bạch của giấy tờ, do đó bạn cần tìm đến người quen có hiểu biết, hay sự giúp đỡ của môi giới, sàn giao dịch uy tín để tránh việc rơi vào bẫy của những kẻ lừa đảo.</p>

<p>Bên cạnh đó, nhiều người vì ham giá rẻ nên mua nhà giấy tay hay vi bằng, dẫn đến những rắc rối nhiêu khê, thậm chí là mất trắng. Mua nhà có sổ đỏ hay sổ hồng, dù giá có đắt hơn nhưng bạn có thể kê cao gối ngủ. Đồng thời, với những căn nhà có giấy tờ pháp lý đầy đủ sau này sẽ dễ dàng mua bán, giao dịch hơn.</p>

<p>&nbsp;</p>

<p><img onload="NcodeImageResizer.createOn(this);" src="https://blog.rever.vn/hubfs/luu-y-khi-mua-nha-2-1.jpg" /></p>

<p><strong><em>Đừng vì ham rẻ mà mua nhà có giấy tờ pháp lý không minh bạch, khiến "tiền mất tật mang".</em></strong></p>

<h2><strong>2. Kiểm tra quy hoạch, tránh mua nhà nằm trong khu quy hoạch treo</strong></h2>

<p>Việc kiểm tra quy hoạch khá mất thời gian nhưng là một việc vô cùng cần thiết khi mua nhà. Đừng vì giá rẻ mà mua nhà nằm trong khu quy hoạch treo, đây là cái bẫy đưa bạn vào thế “tiến thoái lưỡng nan”. Bởi khi mua phải những căn nhà nằm trong khu vực này thuộc dự án triển khai chậm triển khai hay không triển khai, bạn không được tiến hành xây mới, thậm chí là sửa chữa, cải tạo, phải giữ nguyên hiện trạng căn nhà, sau này muốn bán cũng không ai mua.</p>

<p>Thông thường, muốn kiểm tra quy hoạch đất, bạn phải đến Phòng Tài nguyên môi trường ở quận/huyện nơi bất động sản toạ lạc. Để kiểm tra tầng cao, mật độ xây dựng, khoảng lùi, hệ số sử dụng đất, bạn cần đến Phòng Quản lý đô thị ở quận/huyện nơi bất động sản tọa lạc. Bạn nên nhớ, hỏi đúng việc ở đúng chỗ là rất quan trọng.</p>

<h2><strong>3. Kiểm tra kĩ tường nhà</strong></h2>

<p>Khi mua nhà, tốt nhất là chọn căn có tường riêng để thuận tiện trong việc xây lại hay sửa chữa sau này. Với những trường hợp nhà tường chung hay tường mượn thì bạn cũng phải chịu thiệt thòi dù bạn hay nhà hàng xóm tiến hành xây mới, sửa chữa.</p>

<p>Ngoài ra, khi đi xem nhà, bạn cũng nên lưu ý bức tường xem có rong rêu bám dưới chân hoặc xuất hiện vết ố ngang tường hay không, nếu có thì đó là điểm mà nước dâng đến, chứng tỏ căn nhà này thường rơi vào tình trạng ngập nước vào mùa mưa.</p>

<h2><strong>4. Lưu tâm về hàng xóm xung quanh nhà</strong></h2>

<p>Lối sống của hàng xóm có thể ảnh hưởng đến cuộc sống thường ngày của bạn. Liệu bạn có thể nào chịu nổi một hàng xóm mở karaoke 24/7 khiến những giờ phút thư giãn hiếm hoi cũng hóa thành buổi tra tấn âm thanh. Hay mua phải nhà nằm trong khu dân cư vắng vẻ, thiếu an ninh, thường xuyên xảy ra trộm cướp cũng khiến bạn và gia đình phải sống trong lo lắng, đề phòng.</p>

<p>&nbsp;</p>

<p><img onload="NcodeImageResizer.createOn(this);" src="https://blog.rever.vn/hubfs/luu-y-khi-mua-nha-4.jpg" /></p>

<p><em>Hàng xóm xung quanh cũng ảnh hưởng lớn đến cuộc sống thường ngày của gia đình bạn.</em></p>

<h2><strong>5. Tìm hiểu thông tin mua nhà từ những người xung quanh</strong></h2>

<p>Nguồn cung thông tin đáng tin cậy nhất có lẽ là từ người thân hay hàng xóm của bạn, bởi thông tin nhà đất ngày nay tràn lan trên internet, khó lòng phân biệt đâu là thật, đâu là giả, dễ rơi vào bẫy lừa đảo. Bên cạnh đó, bạn cũng có thể tìm đến những sàn giao dịch lớn, uy tín để được hỗ trợ tốt nhất từ khâu tìm căn nhà ưng ý cho đến các vấn đề giấy tờ phát sinh.</p>

<h2><strong>6. Mua nhà phù hợp với nhu cầu và khả năng tài chính</strong></h2>

<p>Nếu bạn là tiểu thương, mua bán nhỏ hay có các dự định kết hợp vừa ở, vừa cho thuê văn phòng tầng trệt thì nên tìm đến những căn nhà có mặt tiền rộng, đường lớn. Nếu bạn chỉ có nhu cầu an cư, cũng như tình hình tài chính chưa cho phép thì không nên bỏ ra một số tiền lớn để mua nhà mặt tiền.</p>

<p>Song song đó, nếu bạn mua nhà theo phương thức thanh toán theo đợt hay&nbsp;vay mượn ngân hàng để thanh toán&nbsp;thì không nên vay quá 40% giá trị căn nhà. Điều này giúp hạn chế gánh nặng kinh tế cho bạn và gia đình. Số tiền lãi hàng tháng là một vấn đề lớn mà bạn cần bàn bạc kĩ với gia đình mình.</p>

<p>&nbsp;</p>

<p><img onload="NcodeImageResizer.createOn(this);" src="https://blog.rever.vn/hubfs/luu-y-khi-mua-nha-3-1.jpg" /></p>

<p><strong><em>Đừng để việc mua nhà thành một gánh nặng đè trên lưng hàng chục năm ròng rã.</em></strong></p>

<h2><strong>7. Tìm hiểu mức giá nhà đất trong khu vực xung quanh</strong></h2>

<p>Trước khi chọn mua nhà, bạn nên tham khảo giá bán nhà khu vực xung quanh, để nắm được mặt bằng giá chung. Đó là công cụ cần có để mặc cả cũng như đàm phán giá cả sau này với người bán, đồng thời hạn chế tối đa tình trạng mua nhầm, mua hớ.</p>

<h2><strong>8. Người trẻ nên thuê nhà nếu chưa đủ khả năng tài chính</strong></h2>

<p>Nếu bạn chưa quá dư dã để mua nhà, thì thuê nhà không phải là một giải pháp tồi. Đồng thời, thuê nhà còn là một giải pháp linh động nếu bạn chưa ổn định công việc, thường xuyên phải thuyên chuyển, thay đổi nơi công tác. Nếu lỡ mua nhà ở Quận 12 và buộc đi làm ở Quận 7 thì trung bình một ngày bạn phải dành hơn 3 tiếng đồng hồ chỉ để di chuyển từ nhà và chỗ làm.</p>

<p>&nbsp;</p>

<p>Tổng hợp bởi BannhaAZ&nbsp;</p>', 1, NULL, CAST(N'2019-11-30T00:02:42.807' AS DateTime), NULL, CAST(N'2020-02-05T09:31:14.383' AS DateTime))
INSERT [dbo].[AZ_Services] ([Id], [Name], [Image], [Title], [NumberView], [Orders], [Summary], [Active], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (N'03d9901e-77bc-49b7-8509-8faafdf9b257', NULL, NULL, N'', 0, 0, N'', 0, NULL, CAST(N'2020-01-17T11:03:39.577' AS DateTime), NULL, CAST(N'2020-01-17T11:03:39.577' AS DateTime))
INSERT [dbo].[AZ_Services] ([Id], [Name], [Image], [Title], [NumberView], [Orders], [Summary], [Active], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (N'dd63b1f3-1436-4d42-9bf8-a67dc3f5c010', N'ĐĂNG RAO BÁN MÃI MÀ KHÔNG CÓ NGƯỜI HỎI! ĐÂU LÀ NHỮNG LÝ DO???', N'/Files/0512105113_richie_hawtin_in_the_cube_at_omnia_-_photo_credit_jacob_riglin.jpg', N'Nếu đã nhiều tháng kể từ khi đăng tin bán nhà mà chưa bán được, thậm chí không mấy ai hỏi thăm, bạn nên xem lại những lý do khiến nhà khó bán dưới đây.', 0, 2, N'<h2><strong>Lý do thứ 1: Đưa ra mức giá bán quá cao so với thị trường</strong></h2>

<p>Đa số chủ nhà mãi không bán được nhà bởi định giá căn nhà quá cao so với thực tế. Có thể căn nhà này có giá trị kỉ niệm, giá trị truyền thống lớn với gia đình bạn nhưng không vì thế khiến giá trị của nó cao hơn quá nhiều so với&nbsp;mức giá thị trường.</p>

<p>Đưa ra mức giá sai lệch từ đầu khiến bạn đánh mất nhiều khách hàng tiềm năng ngay từ đầu, và phải giảm dần giá bán theo thời gian. Do đó, để thanh khoản được giá tốt, chủ nhà nên tham khảo mức giá thị trường và đưa ra mức giá hợp lý so với mặt bằng chung.</p>

<h2><strong>Lý do thứ 2: Để lại quá nhiều nội thất mang tính cá nhân trong nhà</strong></h2>

<p>Nhiều gia chủ để lại quá nhiều tài sản cá nhân trong căn nhà đang rao bán. Có thể bạn cho rằng những món đồ nội thất tâm đắc, mang đậm “phong cách”, gu thẩm mỹ của cá nhân mình sẽ khiến người mua thích thú, nhưng thực tế lại phản tác dụng.</p>

<p>&nbsp;</p>

<p><img alt="ly-do-khong-ban-duoc-nha-1" onload="NcodeImageResizer.createOn(this);" sizes="(max-width: 750px) 100vw, 750px" src="https://blog.rever.vn/hs-fs/hubfs/ly-do-khong-ban-duoc-nha-1.jpg?width=750&amp;name=ly-do-khong-ban-duoc-nha-1.jpg" srcset="https://blog.rever.vn/hs-fs/hubfs/ly-do-khong-ban-duoc-nha-1.jpg?width=375&amp;name=ly-do-khong-ban-duoc-nha-1.jpg 375w, https://blog.rever.vn/hs-fs/hubfs/ly-do-khong-ban-duoc-nha-1.jpg?width=750&amp;name=ly-do-khong-ban-duoc-nha-1.jpg 750w, https://blog.rever.vn/hs-fs/hubfs/ly-do-khong-ban-duoc-nha-1.jpg?width=1125&amp;name=ly-do-khong-ban-duoc-nha-1.jpg 1125w, https://blog.rever.vn/hs-fs/hubfs/ly-do-khong-ban-duoc-nha-1.jpg?width=1500&amp;name=ly-do-khong-ban-duoc-nha-1.jpg 1500w, https://blog.rever.vn/hs-fs/hubfs/ly-do-khong-ban-duoc-nha-1.jpg?width=1875&amp;name=ly-do-khong-ban-duoc-nha-1.jpg 1875w, https://blog.rever.vn/hs-fs/hubfs/ly-do-khong-ban-duoc-nha-1.jpg?width=2250&amp;name=ly-do-khong-ban-duoc-nha-1.jpg 2250w" width="750" /></p>

<p><strong><em>Người mua đánh giá cao những căn nhà có nội thất đơn giản, với màu sắc trung tính.</em></strong></p>

<p>Người mua đánh giá cao những căn nhà có nội thất đơn giản, với màu sắc trung tính, để họ có thể dễ dàng hình dung cuộc sống sau này nếu dọn vào, cũng như áp đặt lối sống thường ngày của mình vào căn nhà đang xem. Chính những món đồ đầy tính cá nhân của chủ nhà khiến người mua cảm thấy khó gắn kết với căn nhà, mang đến sự tù túng, xa cách, từ đó mất điểm trong mắt người mua.</p>

<h2><strong>Lý do thứ 3:&nbsp;Thị trường giao dịch&nbsp;bất động sản đang ảm đạm</strong></h2>

<p>Nếu bạn rao bán nhà trong những thời điểm nhạy cảm như đợt khủng hoảng bất động sản năm 2009, thì có lẽ bạn phải chờ khá lâu để tìm được người mua. Nếu bạn cần bán gấp trong những trường hợp này thì phải chịu mất giá rất nhiều.</p>

<p>Cũng có thể hiểu, điều kiện thị trường ảnh hưởng rất lớn tới ngôi nhà có mức giá trung bình trong việc định giá. Chủ nhà có thể tìm đến những môi giới uy tín để nhận được những phân tích thị trường, thị hiếu của người mua trong các khoảng thời gian nhất định và liên tục, để chọn thời điểm bán nhà cũng như mức giá thích hợp.</p>

<h2><strong>Lý do thứ 4: Tự bán nhà hay chọn sàn bất động sản thiếu năng lực</strong></h2>

<p>Tự rao bán nhà, không thông qua môi giới có thể sẽ giúp bạn tiết kiệm được khoản tiền hoa hồng kha khá nhưng thị trường sẽ bị thu hẹp, khiến ít người biết đến căn nhà bạn đang bán, và dễ dẫn đến trường hợp bị ép giá.</p>

<p>Giao nhà bán cho sàn giao dịch chưa đủ năng lực cũng dẫn đến tình trạng tương tự. Do đó, để việc bán nhà diễn ra suôn sẻ, bạn nên giao nhà cho sàn có nhiều kinh nghiệm, với năng lực tài chính và ngân sách quảng cáo tốt, chiến dịch tiếp thị mạnh mẽ. Khi tìm đến những&nbsp;sàn bất động sản uy tín, căn nhà của bạn sẽ bán được giá tốt, đồng thời còn được hưởng chế độ hậu mãi tuyệt vời.</p>

<p><strong><em>Hãy tìm đến những sàn giao dịch uy tín để bán được nhà giá tốt như GODACO LAND, đồng thời còn được hỗ trợ nhiều vấn đề, giấy tờ liên quan.</em></strong></p>

<h2><strong>Lý do thứ 5: Thông tin rao bán nhà sơ sài, chưa đầy đủ</strong></h2>

<p>Cung cấp chưa đủ thông tin căn nhà là một trong những lý do khiến người mua mất niềm tin và không quan tâm đến căn nhà của bạn. Một&nbsp;tin rao bán nhà đầy đủ&nbsp;cần có thông tin về vị trí, diện tích, kết cấu căn nhà, kèm với hình ảnh thể hiện đầy đủ ưu điểm căn nhà của bạn, mọi thứ càng chi tiết, càng nhanh chóng tìm được khách hàng tiềm năng.</p>

<h2><strong>Lý do thứ 6: Để căn nhà bừa bộn, thiếu sự chăm sóc</strong></h2>

<p>Bạn cần đảm bảo căn nhà của bạn đang trong phiên bản đẹp nhất của nó. Bạn nên đầu tư nâng cấp cải tạo tốt nhất có thể bao gồm việc chỉnh sửa đồ đạc, vệ sinh sàn nhà, sơn lại cửa sổ,… để bán được mức giá tốt nhất.</p>

<p>Ấn tượng đầu tiên là vô cùng quan trọng, nó mang tính quyết định giá trị ngôi nhà của bạn trong mắt người mua, cũng như là cơ sở để đàm phán mức giá bán tốt nhất giành cho bạn.</p>

<h2><strong>Lý do thứ 7: Ngoại thất quá cũ kĩ</strong></h2>

<p>Với những khách hàng là người mua có nhu cầu ở thực, ngoại thất cũng là một điểm vô cùng quan trọng. Một lớp sơn quá cũ, mang đầy dấu hiệu thời gian gợi cho người khác cảm giác căn nhà không chắc chắn. Bạn nên sơn lại lớp ngoài để trông căn nhà có sức sống hơn, đồng thời nâng cao giá trị trong mắt người mua. &nbsp;</p>

<p>Trên đây là 7 lý do khiến bạn đăng tin rao bán nhà nhưng không ai mua, nếu bạn cần hỗ trợ gì thêm thì hãy liên hệ với Nhà Đất Văn Minh qua số hotline:&nbsp;0936.601.601 để được tư vấn trực tiếp nhé.</p>

<p>&nbsp;</p>

<p>====================</p>

<p>&nbsp;</p>', 1, NULL, CAST(N'2019-11-29T23:30:14.077' AS DateTime), NULL, CAST(N'2020-02-05T09:40:22.317' AS DateTime))
INSERT [dbo].[AZ_Services] ([Id], [Name], [Image], [Title], [NumberView], [Orders], [Summary], [Active], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (N'fdaec60c-73e9-4163-8b90-ac117dd34528', N'Tư vấn luật: Làm thế nào khi diện tích trên sổ đỏ chênh lệch với thực tế?', N'https://file4.batdongsan.com.vn/2019/09/17/zk7ggeWN/20190917103650-d8d4.jpg', N'Diện tích đất trên sổ đỏ và thực tế có sự chênh lệch gây ra những rắc rối nào đối với người sử dụng đất và hướng xử lý thế nào khi xảy ra trường hợp này?', 0, 2, N'<p>&nbsp;</p>

<p><strong><em>Diện tích trên sổ đỏ chênh lệch với thực tế rất dễ dẫn đến tranh chấp khi chuyển nhượng</em></strong></p>

<p><img alt="chênh lệch diện tích trên sổ đỏ" onload="NcodeImageResizer.createOn(this);" src="https://file4.batdongsan.com.vn/2019/09/17/zk7ggeWN/20190917103650-d8d4.jpg" title="Làm thế nào khi diện tích trên sổ đỏ chênh lệch với thực tế? " /></p>

<p>Diện tích giữa sổ đỏ và thực tế có sự chênh lệch dẫn đến những ảnh hưởng nhất định đối với người sử dụng đất.</p>

<p><strong>Thứ nhất</strong>, sự chênh lệch này rất dễ dẫn đến những tranh chấp khi chuyển nhượng. Giá đất khi chuyển nhượng có thể thỏa thuận tính theo thửa thay vì m2 và nếu hai bên không thực hiện đúng quy trình sang tên sổ đỏ tại cơ quan nhà nước, không đo đạc lại trước khi sang nhượng thì việc chênh lệch diện tích giữa sổ đỏ và thực tế rất dễ xảy ra.</p>

<p><strong>Thứ hai</strong>, nếu diện tích đất thực tế nhỏ hơn diện tích trên sổ đỏ, người sử dụng đất sẽ phải đóng thuế nhiều hơn thực tế bởi số tiền thuế tính theo diện tích trên sổ đỏ.</p>

<p><strong>Thứ ba</strong>, trong trường hợp diện tích thực tế lớn hơn diện tích trên sổ đỏ, người sử dụng đất sẽ mất quyền và lợi ích hợp pháp đối với phần diện tích không có trên sổ.</p>

<p>Khi diện tích trên sổ đỏ chênh lệch với thực tế, người sử dụng đất cần báo lên cơ quan chức năng yêu cầu giải quyết và đo đạc lại. Theo quy định về nguyên tắc cấp Giấy chứng nhận quyền sử dụng đất, quyền sở hữu nhà ở và tài sản khác gắn liền với đất tại Khoản 5 Điều 98 Luật đất đai 2013, sẽ có 2 trường hợp xảy ra:</p>

<p><strong>Trường hợp thứ nhất</strong>&nbsp;khi ranh giới thửa đất đang sử dụng không thay đổi so với ranh giới thửa đất tại thời điểm có giấy tờ về quyền sử dụng đất, không có tranh chấp với những người sử dụng đất liền kề thì khi cấp hoặc cấp đổi Giấy chứng nhận quyền sử dụng đất, quyền sở hữu nhà ở và tài sản khác gắn liền với đất diện tích đất được xác định theo số liệu đo đạc thực tế. Người sử dụng đất không phải nộp tiền sử dụng đất đối với phần diện tích chênh lệch nhiều hơn nếu có.</p>

<p><strong>Trường hợp thứ hai</strong>&nbsp;khi tiến hành đo đạc lại mà ranh giới thửa đất có thay đổi so với ranh giới thửa đất tại thời điểm có giấy tờ về quyền sử dụng đất và diện tích đất đo đạc thực tế nhiều hơn diện tích ghi trên giấy tờ về quyền sử dụng đất thì phần diện tích chênh lệch nhiều hơn (nếu có) được xem xét cấp Giấy chứng nhận quyền sử dụng đất, quyền sở hữu nhà ở và tài sản khác gắn liền với đất theo quy định tại Điều 99 của Luật đất đai 2013.</p>

<p>Trong trường hợp xin cấp đổi sổ đỏ, hồ sơ cần chuẩn bị gồm: đơn đề nghị cấp đổi sổ đỏ và bản gốc sổ đỏ đã cấp.</p>

<p>&nbsp;</p>

<p>====================</p>', 1, NULL, CAST(N'2019-11-29T22:49:19.350' AS DateTime), NULL, CAST(N'2020-02-05T09:38:05.920' AS DateTime))
INSERT [dbo].[AZ_Services] ([Id], [Name], [Image], [Title], [NumberView], [Orders], [Summary], [Active], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (N'3aa5d982-68bd-4d5d-a3a6-b0a7e97f7c96', N'Bất động sản Hải Phòng bùng nổ, tiềm năng sinh lời cao', N'https://file4.batdongsan.com.vn/2019/09/12/wxbwknn6/20190912110802-dcc6.jpg', N'Khai thác lợi thế cảng biển, Hải Phòng đang tập trung nguồn lực để hiện thực hóa mục tiêu trở thành thành phố công nghiệp phát triển hiện đại, thông minh, bền vững tầm cỡ khu vực Đông Nam Á. Bất động sản Hải Phòng theo đó cũng được dự báo sẽ tăng giá, đặc biệt là ở phân khúc biệt thự, căn hộ cao cấp.', 0, 2, N'<p><strong>Các chuyên gia cho rằng Hải Phòng đang có “thiên thời, địa lợi, nhân hòa” để tăng tốc phát triển, đặc biệt việc vận dụng các mô hình, cách làm mới sẽ giúp Hải Phòng trở thành động lực thúc đẩy phát triển kinh tế - xã hội cho khu vực miền Bắc và cả nước.</strong></p>

<p>Ngày 24/1/2019, Bộ Chính trị đã ban hành Nghị quyết số 45-NQ/TW về “Xây dựng và phát triển Thành phố Hải Phòng đến năm 2030, tầm nhìn đến năm 2045” với mục tiêu&nbsp;đến năm 2025 sẽ đưa&nbsp;Hải Phòng sẽ trở thành thành phố công nghiệp theo hướng hiện đại, trọng điểm kinh tế biển của cả nước, trung tâm dịch vụ logistics quốc gia, trung tâm đào tạo, nghiên cứu, ứng dụng và phát triển khoa học - công nghệ biển của Việt Nam. Cát Bà, Đồ Sơn cùng với Hạ Long trở thành trung tâm du lịch quốc tế.&nbsp;</p>

<p>&nbsp;</p>

<p><img alt="cảng biển Hải Phòng" onload="NcodeImageResizer.createOn(this);" src="https://file4.batdongsan.com.vn/2019/09/12/wxbwknn6/20190912110802-dcc6.jpg" title="Bất động sản Hải Phòng “bùng nổ”, tiềm năng sinh lời cao " /><br />
<strong><em>Hải Phòng đang khai thác hiệu quả lợi thế cảng biển nước sâu. Ảnh: Thúy Hồng</em></strong></p>

<p>Tỷ trọng đóng góp của Hải Phòng vào tổng sản phẩm (GDP) cả nước đạt khoảng 6,4%. Tỷ trọng đóng góp vào GDP vùng kinh tế trọng điểm Bắc Bộ là 23,7%. Tăng trưởng GRDP bình quân giai đoạn 2018 - 2025 tối thiểu là 13%. GRDP bình quân/người đạt 14.740 USD. Thu ngân sách trên địa bàn đạt 180.000 đến 190.000 tỷ đồng. Đóng góp của năng suất các yếu tố tổng hợp (TFP) vào GRDP từ 44 - 45%.&nbsp;</p>

<p>&nbsp;</p>

<p><img alt="Tổ hợp thương mại và căn hộ cao cấp The Minato Residence" onload="NcodeImageResizer.createOn(this);" src="https://file4.batdongsan.com.vn/2019/09/12/wxbwknn6/20190912110912-67a1.jpg" title="Bất động sản Hải Phòng “bùng nổ”, tiềm năng sinh lời cao 1" /><br />
<strong><em>Hải Phòng đang xây dựng phát triển theo mô hình thành phố hiện đại, thông minh, bền vững tầm cỡ khu vực Đông Nam Á.</em></strong></p>

<p>Đến năm 2030, Hải Phòng đặt mục tiêu trở thành thành phố công nghiệp phát triển hiện đại, thông minh, bền vững tầm cỡ khu vực Đông Nam Á; trung tâm dịch vụ logistics quốc tế hiện đại bằng cả đường biển, đường hàng không, đường bộ cao tốc, đường sắt tốc độ cao; trung tâm quốc tế về đào tạo, nghiên cứu, ứng dụng và phát triển khoa học - công nghệ với các ngành nghề hàng hải, đại dương học, kinh tế biển. Chính quyền đô thị được xây dựng và hoàn thiện phù hợp với yêu cầu của thành phố thông minh.</p>

<p>Tỷ trọng đóng góp của Hải Phòng vào GDP cả nước đạt 8,2%; đóng góp vào GDP vùng kinh tế trọng điểm Bắc Bộ 28,3%; tăng trưởng GRDP bình quân giai đoạn 2026 - 2030 thấp nhất là 12,5%; GRDP bình quân/người đạt 29.900 USD; thu ngân sách trên địa bàn đạt 300.000 - 310.000 tỷ đồng; đóng góp của TFP vào GRDP từ 48 - 50%.</p>

<p>Đặc biệt, Hải Phòng đặt mục tiêu đến năm 2045 trở thành thành phố có trình độ phát triển cao trong nhóm các thành phố hàng đầu châu Á và thế giới. Nắm bắt cơ hội này, các doanh nghiệp lớn trong nước và quốc tế đã liên tục “rót” vốn đầu tư vào Hải Phòng như: Công ty TNHH Minato Việt Nam (Liên doanh giữa Takara Leben và Fujita), AEON, Vingroup, Sun Group, FLC... khiến diện mạo của thành phố đổi mới từng ngày.&nbsp;</p>

<p>Vị trí đắc địa với lợi thế cảng biển quốc tế, hạ tầng giao thông đồng bộ, chuỗi tiện ích tiêu chuẩn quốc tế, không gian sống trong lành... đang là “đòn bẩy” khiến&nbsp;bất động sản Hải Phòng được&nbsp;dự báo sẽ “bùng nổ” trong thời gian tới. Theo đánh giá của Savills, Hải Phòng sẽ tiếp tục là điểm đến tiềm năng của các nhà đầu tư.&nbsp;</p>

<p>Bà Đỗ Thu Hằng - Phó giám đốc, Trưởng bộ phận nghiên cứu Savills Hà Nội cho biết, căn hộ dịch vụ là phân khúc có tình hình hoạt động luôn ổn định ở mức tốt với công suất cao. Nguồn cầu lớn nhất đến từ khách thuê người châu Á, cụ thể là Nhật Bản &amp; Hàn Quốc, 2 quốc gia có luồng vốn FDI lớn vào Hải Phòng. Đây cũng là đối tượng khách thuê thích sống theo cộng đồng nên sản phẩm căn hộ dịch vụ với thiết kế và tiện ích theo phong cách đặc trưng của một quốc gia sẽ thu hút khách thuê đến từ nước đó.</p>

<p>“Nhu cầu lớn, công suất cao đã thu hút các doanh nghiệp nước ngoài tham gia vào thị trường này, trong đó có thể kể đến hai nhà điều hành quốc tế quản lý các dự án hạng A và một chủ đầu tư Nhật Bản sẽ phát triển một dự án căn hộ dịch vụ trong tương lai”, bà Đỗ Thu Hằng cho hay.</p>

<p>Nếu như trước đây, các doanh nghiệp đến Hải Phòng chủ yếu đầu tư đất nền thô thì hiện nay đã dịch chuyển sang đầu tư phát triển chuỗi tiện ích tiêu chuẩn quốc tế như: Khách sạn 5 sao Nikko, Pullman, Hilton; Bệnh viện Y học biển, Bệnh viện Đa khoa Quốc tế Vinmec; Trường phổ thông liên cấp Vinschool; AEON Mall hay các trung tâm thương mại, giải trí sang trọng...&nbsp;</p>

<p>Hải Phòng đang tập trung số lượng lớn chuyên gia, kỹ sư nước ngoài đến công tác và làm việc, kéo theo nhu cầu căn hộ cao cấp cho thuê tăng nhanh. Nhóm khách hàng này sẵn sàng chi trả tiền thuê nhà cao hơn 30 - 50%, song yêu cầu khắt khe, chỉ tập trung vào các khu đô thị tiêu chuẩn quốc tế, có chuỗi tiện ích cao cấp, đảm bảo an ninh, an toàn và ở vị trí thuận tiện cho việc di chuyển.&nbsp;</p>

<p>&nbsp;</p>

<p><img alt="căn hộ cao cấp The Minato Residence" onload="NcodeImageResizer.createOn(this);" src="https://file4.batdongsan.com.vn/2019/09/12/wxbwknn6/20190912111118-5432.jpg" title="Bất động sản Hải Phòng “bùng nổ”, tiềm năng sinh lời cao 2" /><br />
<strong><em>Tổ hợp thương mại và căn hộ cao cấp The Minato Residence được đánh giá là không gian sống lý tưởng và có tiềm năng sinh lời cao.</em></strong></p>

<p>Được các chuyên gia đánh giá là viên ngọc quý trong số các dự án đầu tư mới tại Hải Phòng, Tổ hợp thương mại và căn hộ cao cấp The Minato Residence có tổng mức đầu tư 2.213,4 tỷ đồng (105 triệu USD) là dự án đầu tiên tại Việt Nam do chủ đầu tư Nhật Bản kiểm soát đồng bộ các khâu từ thiết kế, thi công, xây dựng và quản lý.</p>

<p>Trong công viên xanh 1,26ha, The Minato Residence gồm 2 tổ hợp thương mại và căn hộ cao cấp (26 tầng nổi, 1 tầng hầm, 926 căn hộ từ 1 - 3 phòng ngủ), khai thác trọn vẹn lợi thế của Khu đô thị ven sông Lạch Tray, được ví như “quần long hội tụ” “khí vượng sinh tài”.</p>

<p>Lấy cảm hứng từ thành phố cảng hiện đại của xứ sở hoa anh đào, các căn hộ cao cấp tại The Minato Residence được chủ đầu tư Nhật Bản dành thời gian, tâm huyết trong quá trình thiết kế, xây dựng thiên đường tiện ích, tối ưu không gian xanh, gió và ánh sáng.&nbsp;</p>

<p>Với việc tôn vinh các giá trị bền vững, The Minato Residence được các chuyên gia đánh giá là không gian sống lý tưởng tại thành phố cảng, vừa là chốn an cư yên bình, vừa là một trong số ít dự án bất động sản có tiềm năng sinh lời cao.&nbsp;</p>

<p>&nbsp;</p>

<p>====================</p>', 0, NULL, CAST(N'2019-11-30T00:02:02.750' AS DateTime), NULL, CAST(N'2020-01-14T15:21:26.030' AS DateTime))
INSERT [dbo].[AZ_Services] ([Id], [Name], [Image], [Title], [NumberView], [Orders], [Summary], [Active], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (N'111f1a6a-62de-425f-b517-bf60ec8f0818', N'1', N'3', N'2', 0, 4, N'<p>5</p>', 0, NULL, CAST(N'2020-01-17T11:22:55.103' AS DateTime), NULL, CAST(N'2020-01-17T11:22:55.103' AS DateTime))
SET IDENTITY_INSERT [dbo].[Constant] ON 

INSERT [dbo].[Constant] ([Id], [Name], [Value]) VALUES (1, N'Gmail', N'trongnv@gmail.com')
INSERT [dbo].[Constant] ([Id], [Name], [Value]) VALUES (2, N'Phone', N'0973002521')
INSERT [dbo].[Constant] ([Id], [Name], [Value]) VALUES (3, N'Price', N'3 450')
INSERT [dbo].[Constant] ([Id], [Name], [Value]) VALUES (4, N'Facebook', N'http://facebook.com')
SET IDENTITY_INSERT [dbo].[Constant] OFF
SET IDENTITY_INSERT [dbo].[Files] ON 

INSERT [dbo].[Files] ([Id], [Name], [Link], [Active], [CreatedDate], [Type]) VALUES (23, N'richie_hawtin_in_the_cube_at_omnia_-_photo_credit_jacob_riglin.jpg', N'/Files/01274a12-b94b-4e01-9757-494b65309678.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                     ', 0, CAST(N'2019-12-05T10:11:02.957' AS DateTime), 1)
INSERT [dbo].[Files] ([Id], [Name], [Link], [Active], [CreatedDate], [Type]) VALUES (24, N'swing.jpg', N'/Files/0512102811_swing.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         ', 0, CAST(N'2019-12-05T10:28:11.293' AS DateTime), 1)
INSERT [dbo].[Files] ([Id], [Name], [Link], [Active], [CreatedDate], [Type]) VALUES (25, N'richie_hawtin_in_the_cube_at_omnia_-_photo_credit_jacob_riglin.jpg', N'/Files/0512104434_richie_hawtin_in_the_cube_at_omnia_-_photo_credit_jacob_riglin.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                ', 0, CAST(N'2019-12-05T10:44:34.733' AS DateTime), 1)
INSERT [dbo].[Files] ([Id], [Name], [Link], [Active], [CreatedDate], [Type]) VALUES (26, N'choi-gi-o-ubud-bali-vietnamembassy-indonesia.org4_.jpg', N'/Files/0512104438_choi-gi-o-ubud-bali-vietnamembassy-indonesia.org4_.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                            ', 0, CAST(N'2019-12-05T10:44:38.870' AS DateTime), 1)
INSERT [dbo].[Files] ([Id], [Name], [Link], [Active], [CreatedDate], [Type]) VALUES (27, N'richie_hawtin_in_the_cube_at_omnia_-_photo_credit_jacob_riglin.jpg', N'/Files/0512105051_richie_hawtin_in_the_cube_at_omnia_-_photo_credit_jacob_riglin.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                ', 0, CAST(N'2019-12-05T10:50:51.433' AS DateTime), 1)
INSERT [dbo].[Files] ([Id], [Name], [Link], [Active], [CreatedDate], [Type]) VALUES (28, N'choi-gi-o-ubud-bali-vietnamembassy-indonesia.org4_.jpg', N'/Files/0512105113_choi-gi-o-ubud-bali-vietnamembassy-indonesia.org4_.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                            ', 1, CAST(N'2019-12-05T10:51:13.737' AS DateTime), 1)
INSERT [dbo].[Files] ([Id], [Name], [Link], [Active], [CreatedDate], [Type]) VALUES (29, N'richie_hawtin_in_the_cube_at_omnia_-_photo_credit_jacob_riglin.jpg', N'/Files/0512105113_richie_hawtin_in_the_cube_at_omnia_-_photo_credit_jacob_riglin.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                ', 1, CAST(N'2019-12-05T10:51:13.737' AS DateTime), 1)
INSERT [dbo].[Files] ([Id], [Name], [Link], [Active], [CreatedDate], [Type]) VALUES (30, N'choi-gi-o-ubud-bali-vietnamembassy-indonesia.org5_.jpg', N'/Files/0512105113_choi-gi-o-ubud-bali-vietnamembassy-indonesia.org5_.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                            ', 1, CAST(N'2019-12-05T10:51:13.740' AS DateTime), 1)
INSERT [dbo].[Files] ([Id], [Name], [Link], [Active], [CreatedDate], [Type]) VALUES (31, N'swing.jpg', N'/Files/0512105113_swing.jpg                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         ', 1, CAST(N'2019-12-05T10:51:13.763' AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[Files] OFF
INSERT [dbo].[OrderLink] ([Id], [OrderId], [Link], [Color], [Size], [Quantity], [Price], [ImageLink], [Note]) VALUES (N'00000000-0000-0000-0000-000000000000', N'6b17c5ca-32d0-4408-a994-130dd69f886f', N'1                                                                                                                                                                                                       ', N'2', N'3                                                                                                   ', 4, CAST(5 AS Numeric(18, 0)), N'6                                                                                                                                                                                                       ', N'7')
INSERT [dbo].[OrderLink] ([Id], [OrderId], [Link], [Color], [Size], [Quantity], [Price], [ImageLink], [Note]) VALUES (N'd861f3b2-5d07-4838-8fa7-017e783ac70a', N'2c367e4e-165c-4b34-86cb-ba452e55ef74', N'1                                                                                                                                                                                                       ', N'2', N'3                                                                                                   ', 4, CAST(5 AS Numeric(18, 0)), N'6                                                                                                                                                                                                       ', N'')
INSERT [dbo].[OrderLink] ([Id], [OrderId], [Link], [Color], [Size], [Quantity], [Price], [ImageLink], [Note]) VALUES (N'6ba514a3-c850-4b84-bec4-0fadbcdd18b4', N'd1493a5d-2be5-45d7-9b56-ee7a846fac83', N'1                                                                                                                                                                                                       ', N'2', N'3                                                                                                   ', 4, CAST(5 AS Numeric(18, 0)), N'6                                                                                                                                                                                                       ', N'')
INSERT [dbo].[OrderLink] ([Id], [OrderId], [Link], [Color], [Size], [Quantity], [Price], [ImageLink], [Note]) VALUES (N'c6102b66-5e9a-407c-a41b-29b55a22fff2', N'fc49da7b-8d5a-4292-ab85-f574ae041e17', N'2                                                                                                                                                                                                       ', N'2', N'2                                                                                                   ', 2, CAST(2 AS Numeric(18, 0)), N'2                                                                                                                                                                                                       ', N'')
INSERT [dbo].[OrderLink] ([Id], [OrderId], [Link], [Color], [Size], [Quantity], [Price], [ImageLink], [Note]) VALUES (N'c0d37c92-dddd-4216-ad8c-4e6511db4aa6', N'5e7a0be2-ddb1-438f-9029-ad7f93a8924a', N'4                                                                                                                                                                                                       ', N'4', N'4                                                                                                   ', NULL, CAST(4 AS Numeric(18, 0)), N'4                                                                                                                                                                                                       ', N'')
INSERT [dbo].[OrderLink] ([Id], [OrderId], [Link], [Color], [Size], [Quantity], [Price], [ImageLink], [Note]) VALUES (N'dbd16efa-8e2c-41e3-84e8-ed7cf242daa7', N'fbb82ed0-5ccd-4127-859d-5a3b0b269279', N'1                                                                                                                                                                                                       ', N'2', N'3                                                                                                   ', 4, CAST(5 AS Numeric(18, 0)), N'6                                                                                                                                                                                                       ', N'7')
INSERT [dbo].[OrderLink] ([Id], [OrderId], [Link], [Color], [Size], [Quantity], [Price], [ImageLink], [Note]) VALUES (N'489d63a6-5869-4098-a042-fa4543abab30', N'fbb82ed0-5ccd-4127-859d-5a3b0b269279', N'23                                                                                                                                                                                                      ', N'234', N'234                                                                                                 ', 234, CAST(234 AS Numeric(18, 0)), N'23                                                                                                                                                                                                      ', N'4234')
INSERT [dbo].[OrderLink] ([Id], [OrderId], [Link], [Color], [Size], [Quantity], [Price], [ImageLink], [Note]) VALUES (N'24c41f72-1d9f-400f-a867-fbccd25e91f2', N'e70361df-dd43-4acd-a1c1-3d95c7cb54da', N'1                                                                                                                                                                                                       ', N'2', N'3                                                                                                   ', 4, CAST(5 AS Numeric(18, 0)), N'6                                                                                                                                                                                                       ', N'7')
INSERT [dbo].[Orders] ([Id], [No], [Link], [Name], [Phone], [Email], [Address], [Summary], [CreatedDate], [Status], [Note], [IsDelete]) VALUES (N'fbb82ed0-5ccd-4127-859d-5a3b0b269279', N'0912021636          ', NULL, N'Antoine ttrong', N'56456      ', N'trognv 14                                                                                           ', N'456456', N'456', CAST(N'2019-12-09T14:16:36.107' AS DateTime), 1, NULL, NULL)
INSERT [dbo].[Orders] ([Id], [No], [Link], [Name], [Phone], [Email], [Address], [Summary], [CreatedDate], [Status], [Note], [IsDelete]) VALUES (N'71cc60a4-1364-42fa-8265-7cf6b6e429ad', N'0912021038          ', N'/Files/received-icon-100912021038.jpg                                                                                                                                                                   ', N'11111111111', N'43535345   ', N'trongnv1507@gmail.com                                                                               ', N'b', N'bbn', CAST(N'2019-12-09T14:10:38.110' AS DateTime), 4, N'', NULL)
INSERT [dbo].[Orders] ([Id], [No], [Link], [Name], [Phone], [Email], [Address], [Summary], [CreatedDate], [Status], [Note], [IsDelete]) VALUES (N'c102b87c-b179-4e4b-8adc-abe644082a8c', N'0912020942          ', N'/Files/CV_TranDung0912020942.pdf                                                                                                                                                                        ', N'Ngô Văn Trọng', N'0973002521 ', N'trongnv1507@gmail.com                                                                               ', N'Ngõ 195 Trần Cung - Bắc Từ Liêm - Hà Nội', N';;', CAST(N'2019-12-09T14:09:42.197' AS DateTime), 1, NULL, NULL)
INSERT [dbo].[Orders] ([Id], [No], [Link], [Name], [Phone], [Email], [Address], [Summary], [CreatedDate], [Status], [Note], [IsDelete]) VALUES (N'5e7a0be2-ddb1-438f-9029-ad7f93a8924a', N'0912023718          ', NULL, N'45345345', N'3453453    ', N'trongnv1507@gmail.com                                                                               ', N'Ngõ 195 Trần Cung - Bắc Từ Liêm - Hà Nội', N'dv', CAST(N'2019-12-09T14:37:19.420' AS DateTime), 2, N'', NULL)
INSERT [dbo].[Orders] ([Id], [No], [Link], [Name], [Phone], [Email], [Address], [Summary], [CreatedDate], [Status], [Note], [IsDelete]) VALUES (N'2c367e4e-165c-4b34-86cb-ba452e55ef74', N'1012104054          ', NULL, N'123', N'0809       ', N'trongnv1507@gmail.com                                                                               ', N'Ngõ 195 Trần Cung - Bắc Từ Liêm - Hà Nội', N'cxx', CAST(N'2019-12-10T10:40:54.537' AS DateTime), 1, NULL, NULL)
INSERT [dbo].[Orders] ([Id], [No], [Link], [Name], [Phone], [Email], [Address], [Summary], [CreatedDate], [Status], [Note], [IsDelete]) VALUES (N'd1493a5d-2be5-45d7-9b56-ee7a846fac83', N'0912053155          ', NULL, N'sac', N'12312312312', N'trongnv1507@gmail.com                                                                               ', N'2', N'3', CAST(N'2019-12-09T17:31:55.237' AS DateTime), 1, NULL, NULL)
INSERT [dbo].[Orders] ([Id], [No], [Link], [Name], [Phone], [Email], [Address], [Summary], [CreatedDate], [Status], [Note], [IsDelete]) VALUES (N'fc49da7b-8d5a-4292-ab85-f574ae041e17', N'1012105531          ', NULL, N'Antoine xXXX', N'223342     ', N'trongnv1507@gmail.com                                                                               ', N'Ngõ 195 Trần Cung - Bắc Từ Liêm - Hà Nội', N'c', CAST(N'2019-12-10T10:55:31.400' AS DateTime), 1, NULL, NULL)
INSERT [dbo].[OrderStatus] ([Id], [Name], [Active]) VALUES (1, N'Chờ tiếp nhận', 1)
INSERT [dbo].[OrderStatus] ([Id], [Name], [Active]) VALUES (2, N'Đã tiếp nhận', 1)
INSERT [dbo].[OrderStatus] ([Id], [Name], [Active]) VALUES (3, N'Chờ lấy hàng', 1)
INSERT [dbo].[OrderStatus] ([Id], [Name], [Active]) VALUES (4, N'Đang chuyển hàng về Việt Nam', 1)
INSERT [dbo].[OrderStatus] ([Id], [Name], [Active]) VALUES (5, N'Đã giao hàng', 1)
INSERT [dbo].[Services] ([Id], [Name], [Image], [Title], [NumberView], [Orders], [Summary], [Active], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (N'6b17c5ca-32d0-4408-a994-130dd69f886d', N'1', N'/Files/b416ba40-703f-4e71-9e8e-227dfce11019.jpg', N'2', 0, 4, N'5', 0, NULL, CAST(N'2019-11-29T22:18:11.303' AS DateTime), NULL, CAST(N'2019-11-29T00:00:00.000' AS DateTime))
INSERT [dbo].[Services] ([Id], [Name], [Image], [Title], [NumberView], [Orders], [Summary], [Active], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (N'6b17c5ca-32d0-4408-a994-130dd69f886f', N'Dịch vụ đặt mua hàng 1688', N'https://www.thuongdo.com/sites/default/files/dichvu/nhap-hang-1688.jpg', N'', 2, 2, N'<h3>2. Bảng giá dịch vụ mua hàng</h3>

<table>
	<thead>
		<tr>
			<th>GIÁ TRỊ ĐƠN HÀNG</th>
			<th>% PHÍ DỊCH VỤ</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>0 đến 30triệu</td>
			<td>5%</td>
		</tr>
		<tr>
			<td>30 đến 50triệu</td>
			<td>4%</td>
		</tr>
		<tr>
			<td>50 đến 100triệu</td>
			<td>3%</td>
		</tr>
		<tr>
			<td>100 đến 300triệu</td>
			<td>2%</td>
		</tr>
		<tr>
			<td>Trên 300tr</td>
			<td>%</td>
		</tr>
	</tbody>
</table>

<h3>3. Phí ship Trung Quốc</h3>

<table>
	<thead>
		<tr>
			<th>LOẠI HÌNH</th>
			<th colspan="2">GIẢI THÍCH</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Chuyển phát nhanh thông thường</td>
			<td>Kg đầu dựa vào quy định của nhà cung cấp trên trang Taobao hoặc 1688</td>
			<td>Kg tiếp theo nếu nhà cung cấp thuộc tỉnh Quảng Đông là 4 tệ, tỉnh khác là 8 tệ</td>
		</tr>
		<tr>
			<td>Chuyển phát nhanh siêu tốc</td>
			<td>Kg đầu dựa vào quy định của nhà cung cấp trên trang Taobao hoặc 1688</td>
			<td>Mỗi 0.5kg tiếp theo là 5 tệ/kg</td>
		</tr>
		<tr>
			<td>Chuyển phát thường bằng oto tải</td>
			<td colspan="2">Mỗi kg 1 tệ/kg + 70 tệ/đơn hàng</td>
		</tr>
	</tbody>
</table>

<h3>4. Phí vận chuyển theo cân nặng</h3>

<table border="1" cellpadding="1" cellspacing="1">
	<thead>
		<tr>
			<th scope="col">Tên</th>
			<th scope="col">Loại</th>
			<th scope="col">Hà Nội</th>
			<th scope="col">TP.HCM</th>
			<th scope="col">Hải Phòng</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>≤ 20kg</td>
			<td>kg</td>
			<td>23,000<sup>đ</sup></td>
			<td>31,000<sup>đ</sup></td>
			<td>26,000<sup>đ</sup></td>
		</tr>
		<tr>
			<td>21 → 100kg</td>
			<td>kg</td>
			<td>20,000<sup>đ</sup></td>
			<td>27,000<sup>đ</sup></td>
			<td>23,000<sup>đ</sup></td>
		</tr>
		<tr>
			<td>101 → 500kg</td>
			<td>kg</td>
			<td>17,000<sup>đ</sup></td>
			<td>23,000<sup>đ</sup></td>
			<td>20,000<sup>đ</sup></td>
		</tr>
		<tr>
			<td>&gt; 500kg</td>
			<td>kg</td>
			<td>14,000<sup>đ</sup></td>
			<td>20,000<sup>đ</sup></td>
			<td>17,000<sup>đ</sup></td>
		</tr>
		<tr>
			<td>≤ 5m3</td>
			<td>m3</td>
			<td>2,400,000<sup>đ</sup></td>
			<td>2,700,000<sup>đ</sup></td>
			<td>2,500,000<sup>đ</sup></td>
		</tr>
		<tr>
			<td>5 → 10m3</td>
			<td>m3</td>
			<td>2,100,000<sup>đ</sup></td>
			<td>2,500,000<sup>đ</sup></td>
			<td>2,200,000<sup>đ</sup></td>
		</tr>
		<tr>
			<td>10 → 20m3</td>
			<td>m3</td>
			<td>1,800,000<sup>đ</sup></td>
			<td>2,400,000<sup>đ</sup></td>
			<td>1,900,000<sup>đ</sup></td>
		</tr>
		<tr>
			<td>&gt; 20m3</td>
			<td>m3</td>
			<td>1,700,000<sup>đ</sup></td>
			<td>2,300,000<sup>đ</sup></td>
			<td>1,800,000<sup>đ</sup></td>
		</tr>
	</tbody>
</table>

<p><strong>Lưu ý:</strong></p>

<p><em>- Mọi mặt hàng đều có cân nặng thực tế và cân nặng quy đổi.</em></p>

<p><em>- Đối với những mặt hàng cồng kềnh thì khối lượng đơn hàng sẽ được tính bằng công thức quy đổi.</em></p>

<p><em>- Khối lượng quy đổi được tính theo công thức:</em>&nbsp;<strong>Chiều dài * chiều rộng * chiều cao / 6000 = Cân nặng đơn hàng</strong></p>

<p><em>- Cân nặng được áp dụng là mức cân nặng cao hơn của trọng lượng thực và khối lượng quy đổi.</em></p>

<p>-&nbsp;<em>Khối lượng đơn hàng sẽ được làm tròn theo 0.5kg</em></p>

<p><strong><em>VD: Đơn hàng có trọng lượng: 1.3kg thì sẽ được làm tròn lên 1.5kg, Đơn hàng có trọng lượng: 58.8kg sẽ được làm tròn lên: 59kg…</em></strong></p>

<p>Khách hàng chú ý, với đơn hàng của quý khách gồm nhiều sản phẩm, và về làm nhiều đợt thì hàng về tới đâu công ty sẽ tính phí tới đó. Phí vận chuyển sẽ tính theo số hàng về của khách trong một thời điểm chứ không tính theo tổng đơn hàng.</p>

<ol>
	<li>Cân nặng quy đổi</li>
	<li>Quy tắc làm tròn</li>
	<li>Tính giá vận chuyển khi hàng về</li>
</ol>

<h3>5. Phí kiểm đếm sản phẩm</h3>

<table>
	<thead>
		<tr>
			<th>SỐ LƯỢNG</th>
			<th>MỨC GIÁ (VNĐ)/ 1 sản phẩm</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>1-2 sản phẩm</td>
			<td>5,000<sup>đ</sup></td>
		</tr>
		<tr>
			<td>3-10 sản phẩm</td>
			<td>3,500<sup>đ</sup></td>
		</tr>
		<tr>
			<td>11-100 sản phẩm</td>
			<td>2,000<sup>đ</sup></td>
		</tr>
		<tr>
			<td>101-500 sản phẩm</td>
			<td>1,500<sup>đ</sup></td>
		</tr>
		<tr>
			<td>501-10000 sản phẩm</td>
			<td>1,000<sup>đ</sup></td>
		</tr>
	</tbody>
</table>

<h3>6. Phí đóng gỗ</h3>

<table>
	<thead>
		<tr>
			<th>&nbsp;</th>
			<th>Kg đầu tiên</th>
			<th>Kg tiếp theo</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Phí đóng kiện</td>
			<td>20&nbsp;<sup>tệ</sup></td>
			<td>1&nbsp;<sup>tệ</sup></td>
		</tr>
	</tbody>
</table>

<h3>7. Cấp độ thành viên</h3>

<table>
	<thead>
		<tr>
			<th>Tên cấp độ</th>
			<th>Tổng giá trị giao dịch</th>
			<th>Chiết khấu phí giao dịch</th>
			<th>Chiết khấu phí vận chuyển</th>
			<th>% đặt cọc</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Cấp độ 1 sao</td>
			<td>0<sup>đ</sup>&nbsp;- 100,000,000<sup>đ</sup></td>
			<td>%</td>
			<td>%</td>
			<td>90 %</td>
		</tr>
		<tr>
			<td>Cấp độ 2 sao</td>
			<td>100,000,000<sup>đ</sup>&nbsp;- 500,000,000<sup>đ</sup></td>
			<td>5 %</td>
			<td>1 %</td>
			<td>85 %</td>
		</tr>
		<tr>
			<td>Cấp độ 3 sao</td>
			<td>500,000,000<sup>đ</sup>&nbsp;- 1,000,000,000<sup>đ</sup></td>
			<td>8 %</td>
			<td>3 %</td>
			<td>75 %</td>
		</tr>
		<tr>
			<td>Cấp độ 4 sao</td>
			<td>1,000,000,000<sup>đ</sup>&nbsp;- 5,000,000,000<sup>đ</sup></td>
			<td>10 %</td>
			<td>5 %</td>
			<td>60 %</td>
		</tr>
		<tr>
			<td>Cấp độ 5 sao</td>
			<td>5,000,000,000<sup>đ</sup>&nbsp;- 50,000,000,000<sup>đ</sup></td>
			<td>15 %</td>
			<td>10 %</td>
			<td>50 %</td>
		</tr>
	</tbody>
</table>', 1, NULL, CAST(N'2012-02-02T00:00:00.000' AS DateTime), NULL, CAST(N'2018-05-03T00:04:13.567' AS DateTime))
INSERT [dbo].[Services] ([Id], [Name], [Image], [Title], [NumberView], [Orders], [Summary], [Active], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (N'8e09649b-257f-425b-b509-1e1a84457830', N'3', N'/Files/11ca59b4-0217-4531-ba7d-da36188f606e.jpg', N'4xx', 0, 2, N'xx', 0, NULL, CAST(N'2019-11-29T22:35:39.770' AS DateTime), NULL, CAST(N'2019-11-29T00:00:00.000' AS DateTime))
INSERT [dbo].[Services] ([Id], [Name], [Image], [Title], [NumberView], [Orders], [Summary], [Active], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (N'16efc728-169e-4037-9b38-6fa932e67a73', N'3', N'/Files/de4698ee-ebd2-4c7f-9265-088496a5fe87.jpg', N'3', 0, 2, N'3', 0, NULL, CAST(N'2019-11-29T22:33:25.923' AS DateTime), NULL, CAST(N'2019-11-29T00:00:00.000' AS DateTime))
INSERT [dbo].[Services] ([Id], [Name], [Image], [Title], [NumberView], [Orders], [Summary], [Active], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (N'7f1738ca-269b-4288-8041-881618f7f203', N'Dịch vụ đặt hàng Taobao', N'https://www.thuongdo.com/sites/default/files/dichvu/dat-hang-taobao.jpg', N'', 0, 2, N'<h2><strong>Đặt hàng order Taobao giá rẻ, tìm kiếm nguồn&nbsp;hàng bằng Tiếng Việt</strong></h2>

<p>Dịch vụ đặt hàng order&nbsp;Taobao giá rẻ và chuyên nghiệp, tại Thương Đô với những công cụ đặt hàng vô cùng tiện lợi bạn sẽ thấy chúng tôi như một phiên bản taobao tiếng Việt, việc order hàng taobao tại Việt Nam chưa bao giờ đơn giản đến vậy.</p>

<p><em><strong>Dù bạn là người mới hay đã là thành viên kỳ cựu của Taobao thì bạn vẫn có thể gặp phải những khó khăn?</strong></em></p>

<ol>
	<li>Tôi muốn đặt hàng Taobao, tôi muốn order hàng taobao về Việt Nam nhưng gặp rào cản về ngôn ngữ, cũng như chưa biết chuyển về Việt Nam như thế nào?</li>
	<li>Bế tắc khi nguồn&nbsp;<a href="https://www.thuongdo.com/" target="_blank">nhập hàng Trung Quốc</a>&nbsp;tại Việt Nam lại quá cao và muốn tìm nguồn hàng tận giá tận gốc trực tiếp tại Trung Quốc</li>
	<li>Tôi đã có đối tác Trung Quốc nhưng chưa biết thương lượng đàm phán ra sao, thanh toán cho họ có an toàn không và vận chuyển về Việt Nam thế nào?</li>
	<li>Tôi không có thời gian tìm kiếm nguồn hàng, tìm hiểu lập tài khoản để mua hàng taobao.</li>
	<li>Tôi muốn tìm các mặt hàng ĐỘC LẠ và ĐẪN ĐẦU thị trường không cần lo cạnh tranh về giá nhưng chưa biết nguồn hàng hợp lý</li>
	<li>Vận chuyển hàng hóa order từ Taobao về Việt Nam rất dễ rủi ro, phải làm thế nào để giảm thiểu rủi ro?</li>
</ol>

<p>Đây là những lo lắng mà hầu hết các bạn kinh doanh đều đang gặp phải hoặc đã từng gặp phải. Chúng tôi hiểu điều đó và mang đến cho quy khách giải pháp tiện lợi khi order taobao.</p>

<p><img alt="Đặt hàng Taobao, order hàng taobao, mua hàng taobao, nhập hàng taobao" src="https://www.thuongdo.com/sites/default/files/dichvu/order-hang-tabao.jpg" /></p>

<h2><strong>Giải pháp của chúng tôi giúp bạn đặt hàng trên Taobao đơn giản:</strong></h2>

<ol>
	<li>Order bất kỳ hàng gì trên taobao bằng công cụ tiện ích, phá vỡ rào cản ngôn ngữ và khoảng cách địa lý, giúp bạn dễ dàng theo dõi đơn hàng cũng như tài chính một cách chuyên nghiệp.</li>
	<li>Giá hàng đầu vào rẻ, giảm thiếu tới 50% chi phí, phù hợp mua buôn, sỉ, lẻ, mô hình vừa và nhỏ.</li>
	<li>Dịch vụ vận chuyển nhanh, an toàn đến tay khách hàng theo yêu cầu, hỗ trợ dịch vụ bảo hiểm rủi ro hàng hoá.</li>
	<li>Chúng tôi mang cho quý khách đội ngũ chuyên viên phục vụ thông thạo tiếng Trung, nhiều kinh nghiệm nhập hàng sẽ hỗ trợ quý khách chu đáo và chuyên nghiệp nhất.</li>
	<li>Đặt được nhiều hàng taobao với mẫu mã đa dạng và phong phú, chất lượng sản phẩm dịch vụ rất tốt nếu bạn tìm đúng nhà cung cấp (chúng tôi sẽ hướng dẫn)</li>
</ol>

<p>Với tiêu chí hợp tác cùng thành công “Chúng tôi sẽ tìm mọi phương pháp giúp các bạn đặt hàng taobao với chi phí tối thiểu, tối ưu quy trình” luôn nỗ lực trở hàng hãng vận chuyển quốc tế hàng đầu Việt Nam và vươn lên thị trường quốc tế.</p>

<p>Quý khách có thể yên tâm&nbsp;<a href="https://www.thuongdo.com/dat-hang-trung-quoc" target="_blank">đặt hàng Trung Quốc</a>&nbsp;cùng&nbsp;Thương Đô, dù quý khách có đặt 1 sản phẩm lẻ&nbsp;hay thậm trí hàng trăm container&nbsp;hàng hoá, bất kế chủng loại hàng hoá gì (trừ hàng quốc cấm), đủ loại kích cỡ… dù bạn mua để tiêu dùng hay kinh doanh chúng tôi đều có thể phục vụ bạn.</p>

<p><img alt="Đặt hàng Taobao, order hàng taobao, mua hàng taobao, nhập hàng taobao" src="https://www.thuongdo.com/sites/default/files/dichvu/dat-hang-taobao_0.jpg" /></p>

<p><em>Để đặt hàng trên Taobao được&nbsp;thuận tiên hơn quý khách vui lòng xem hướng dẫn bên dưới.</em></p>

<h3><a href="https://www.youtube.com/watch?v=4MsOCXk_vg4" target="_blank"><strong>Video&nbsp;hướng dẫn mua hàng trên Taobao</strong></a></h3>

<h2><a href="https://www.thuongdo.com/bang-gia-dich-vu" rel="nofollow" target="_blank"><strong>Tham khảo bảng giá dịch vụ Order Taobao</strong></a></h2>

<h3>1. Chi phí một đơn hàng order</h3>

<table>
	<thead>
		<tr>
			<th>&nbsp;</th>
			<th>Giải thích</th>
			<th>Bắt buộc</th>
			<th>Tùy chọn</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>1. Giá sản phẩm</td>
			<td>Là giá được niêm yết trên website Taobao</td>
			<td>✔️</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>2. Phí dịch vụ</td>
			<td>Phí giao dịch mua hàng khách trả cho Thương Đô</td>
			<td>✔️</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>3. Phí ship Trung Quốc</td>
			<td>Phí chuyển hàng từ nhà cung cấp tới kho của Thương Đô tại Trung Quốc</td>
			<td>✔️</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>4. Phí vận chuyển</td>
			<td>Phí vận chuyển từ kho Trung Quốc về kho của Thương Đô tại Việt Nam (Đơn vị Kg)</td>
			<td>✔️</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>5. Phí kiểm đếm</td>
			<td>Dịch vụ đảm bảo sản phẩm của khách không bị nhà cung cấp giao sai hoặc thiếu</td>
			<td>&nbsp;</td>
			<td>✔️</td>
		</tr>
		<tr>
			<td>6. Phí đóng gỗ</td>
			<td>Hình thức đảm bảo an toàn, hạn chế rủi ro đối với hàng dễ vỡ, dễ biến dạng</td>
			<td>&nbsp;</td>
			<td>✔️</td>
		</tr>
		<tr>
			<td>7. Phí ship tận nhà</td>
			<td>Là phí vận chuyển hàng từ kho của Thương Đô tại Việt Nam tới nhà của quý khách</td>
			<td>&nbsp;</td>
			<td>✔️</td>
		</tr>
	</tbody>
</table>

<p>Lưu ý: Những phí thuộc hình thức (*) là phí bắt buộc, còn lại là tùy chọn, quý khách có thể chọn sử dụng hoặc không</p>

<h3>2. Bảng giá dịch vụ mua hàng</h3>

<table>
	<thead>
		<tr>
			<th>GIÁ TRỊ ĐƠN HÀNG</th>
			<th>% PHÍ DỊCH VỤ</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>0 đến 30triệu</td>
			<td>5%</td>
		</tr>
		<tr>
			<td>30 đến 50triệu</td>
			<td>4%</td>
		</tr>
		<tr>
			<td>50 đến 100triệu</td>
			<td>3%</td>
		</tr>
		<tr>
			<td>100 đến 300triệu</td>
			<td>2%</td>
		</tr>
		<tr>
			<td>Trên 300tr</td>
			<td>%</td>
		</tr>
	</tbody>
</table>

<h3>3. Phí ship Trung Quốc</h3>

<table>
	<thead>
		<tr>
			<th>LOẠI HÌNH</th>
			<th colspan="2">GIẢI THÍCH</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Chuyển phát nhanh thông thường</td>
			<td>Kg đầu dựa vào quy định của nhà cung cấp trên trang Taobao</td>
			<td>Kg tiếp theo nếu nhà cung cấp thuộc tỉnh Quảng Đông là 4 tệ, tỉnh khác là 8 tệ</td>
		</tr>
		<tr>
			<td>Chuyển phát nhanh siêu tốc</td>
			<td>Kg đầu dựa vào quy định của nhà cung cấp trên trang Taobao&nbsp;</td>
			<td>Mỗi 0.5kg tiếp theo là 5 tệ/kg</td>
		</tr>
		<tr>
			<td>Chuyển phát thường bằng oto tải</td>
			<td colspan="2">Mỗi kg 1 tệ/kg + 70 tệ/đơn hàng</td>
		</tr>
	</tbody>
</table>

<h3>4. Phí vận chuyển theo cân nặng</h3>

<table border="1" cellpadding="1" cellspacing="1">
	<thead>
		<tr>
			<th scope="col">Tên</th>
			<th scope="col">Loại</th>
			<th scope="col">Hà Nội</th>
			<th scope="col">TP.HCM</th>
			<th scope="col">Hải Phòng</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>≤ 20kg</td>
			<td>kg</td>
			<td>26,000<sup>đ</sup></td>
			<td>34,000<sup>đ</sup></td>
			<td>29,000<sup>đ</sup></td>
		</tr>
		<tr>
			<td>21 → 100kg</td>
			<td>kg</td>
			<td>23,000<sup>đ</sup></td>
			<td>30,000<sup>đ</sup></td>
			<td>26,000<sup>đ</sup></td>
		</tr>
		<tr>
			<td>101 → 500kg</td>
			<td>kg</td>
			<td>20,000<sup>đ</sup></td>
			<td>26,000<sup>đ</sup></td>
			<td>23,000<sup>đ</sup></td>
		</tr>
		<tr>
			<td>&gt; 500kg</td>
			<td>kg</td>
			<td>Liên hệ</td>
			<td>Liên hệ</td>
			<td>Liên hệ</td>
		</tr>
		<tr>
			<td>≤ 5m3</td>
			<td>m3</td>
			<td>2,400,000<sup>đ</sup></td>
			<td>2,700,000<sup>đ</sup></td>
			<td>2,500,000<sup>đ</sup></td>
		</tr>
		<tr>
			<td>5 → 10m3</td>
			<td>m3</td>
			<td>2,100,000<sup>đ</sup></td>
			<td>2,500,000<sup>đ</sup></td>
			<td>2,200,000<sup>đ</sup></td>
		</tr>
		<tr>
			<td>10 → 20m3</td>
			<td>m3</td>
			<td>1,800,000<sup>đ</sup></td>
			<td>2,400,000<sup>đ</sup></td>
			<td>1,900,000<sup>đ</sup></td>
		</tr>
		<tr>
			<td>&gt; 20m3</td>
			<td>m3</td>
			<td>Liên hệ</td>
			<td>Liên hệ</td>
			<td>Liên hệ</td>
		</tr>
	</tbody>
</table>

<p><strong>Lưu ý:</strong></p>

<p><em>- Mọi mặt hàng đều có cân nặng thực tế và cân nặng quy đổi.</em></p>

<p><em>- Đối với những mặt hàng cồng kềnh thì khối lượng đơn hàng sẽ được tính bằng công thức quy đổi.</em></p>

<p><em>- Khối lượng quy đổi được tính theo công thức:</em>&nbsp;<strong>Chiều dài * chiều rộng * chiều cao / 6000 = Cân nặng đơn hàng</strong></p>

<p><em>- Cân nặng được áp dụng là mức cân nặng cao hơn của trọng lượng thực và khối lượng quy đổi.</em></p>

<p>-&nbsp;<em>Khối lượng đơn hàng sẽ được làm tròn theo 0.5kg</em></p>

<p><strong><em>VD: Đơn hàng có trọng lượng: 1.3kg thì sẽ được làm tròn lên 1.5kg, Đơn hàng có trọng lượng: 58.8kg sẽ được làm tròn lên: 59kg…</em></strong></p>

<p>Khách hàng chú ý, với đơn hàng của quý khách gồm nhiều sản phẩm, và về làm nhiều đợt thì hàng về tới đâu công ty sẽ tính phí tới đó. Phí vận chuyển sẽ tính theo số hàng về của khách trong một thời điểm chứ không tính theo tổng đơn hàng.</p>

<ol>
	<li>Cân nặng quy đổi</li>
	<li>Quy tắc làm tròn</li>
	<li>Tính giá vận chuyển khi hàng về</li>
</ol>

<h3>5. Phí kiểm đếm sản phẩm</h3>

<table>
	<thead>
		<tr>
			<th>SỐ LƯỢNG</th>
			<th>MỨC GIÁ (VNĐ)/ 1 sản phẩm</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>1-2 sản phẩm</td>
			<td>5,000<sup>đ</sup></td>
		</tr>
		<tr>
			<td>3-10 sản phẩm</td>
			<td>3,500<sup>đ</sup></td>
		</tr>
		<tr>
			<td>11-100 sản phẩm</td>
			<td>2,000<sup>đ</sup></td>
		</tr>
		<tr>
			<td>101-500 sản phẩm</td>
			<td>1,500<sup>đ</sup></td>
		</tr>
		<tr>
			<td>501-10000 sản phẩm</td>
			<td>1,000<sup>đ</sup></td>
		</tr>
	</tbody>
</table>

<h3>6. Phí đóng gỗ</h3>

<table>
	<thead>
		<tr>
			<th>&nbsp;</th>
			<th>Kg đầu tiên</th>
			<th>Kg tiếp theo</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Phí đóng kiện</td>
			<td>20&nbsp;<sup>tệ</sup></td>
			<td>1&nbsp;<sup>tệ</sup></td>
		</tr>
	</tbody>
</table>

<h3>7. Cấp độ thành viên</h3>

<table>
	<thead>
		<tr>
			<th>Tên cấp độ</th>
			<th>Tổng giá trị giao dịch</th>
			<th>Chiết khấu phí giao dịch</th>
			<th>Chiết khấu phí vận chuyển</th>
			<th>% đặt cọc</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Cấp độ 1 sao</td>
			<td>0<sup>đ</sup>&nbsp;- 100,000,000<sup>đ</sup></td>
			<td>%</td>
			<td>%</td>
			<td>90 %</td>
		</tr>
		<tr>
			<td>Cấp độ 2 sao</td>
			<td>100,000,000<sup>đ</sup>&nbsp;- 500,000,000<sup>đ</sup></td>
			<td>5 %</td>
			<td>1 %</td>
			<td>85 %</td>
		</tr>
		<tr>
			<td>Cấp độ 3 sao</td>
			<td>500,000,000<sup>đ</sup>&nbsp;- 1,000,000,000<sup>đ</sup></td>
			<td>8 %</td>
			<td>3 %</td>
			<td>75 %</td>
		</tr>
		<tr>
			<td>Cấp độ 4 sao</td>
			<td>1,000,000,000<sup>đ</sup>&nbsp;- 5,000,000,000<sup>đ</sup></td>
			<td>10 %</td>
			<td>5 %</td>
			<td>60 %</td>
		</tr>
		<tr>
			<td>Cấp độ 5 sao</td>
			<td>5,000,000,000<sup>đ</sup>&nbsp;- 50,000,000,000<sup>đ</sup></td>
			<td>15 %</td>
			<td>10 %</td>
			<td>50 %</td>
		</tr>
	</tbody>
</table>', 1, NULL, CAST(N'2019-11-30T00:02:42.807' AS DateTime), NULL, CAST(N'2019-12-09T14:46:13.800' AS DateTime))
INSERT [dbo].[Services] ([Id], [Name], [Image], [Title], [NumberView], [Orders], [Summary], [Active], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (N'dd63b1f3-1436-4d42-9bf8-a67dc3f5c010', N'Vận chuyển hàng Quảng Châu', N'https://www.thuongdo.com/sites/default/files/dichvu/van-chuyen-hang-quang-chau.jpg', N'Tại thời điểm kinh tế thị trường hội nhập như bây giờ, nhu cầu đặt hàng cũng như sử dụng dịch vụ từ các quốc gia khác nói chung và Trung Quốc nói riêng ngày được đẩy mạnh. Với ưu thế về uy tín, chất lượng, giá thành rẻ nên hàng Quảng Châu – Trung Quốc hiện đang được ưa chuộng tại thị trường Việt Nam. Để giúp các shop online, các đơn vị kinh doanh, doanh nghiệp cũng như người tiêu dùng Việt Nam tiếp cận được nguồn hàng Quảng Châu, Thương Đô Logistics đã triển khai dịch vụ order hàng quảng châu giá rẻ uy tín nhất tại Việt Nam với nhiều chính sách ưu đãi dành cho khách hàng', 0, 1, N'<h2>Khi&nbsp;nhập hàng&nbsp;&amp;&nbsp;vận chuyển hàng&nbsp;từ Quảng Châu&nbsp;về&nbsp;Việt Nam bạn sẽ gặp phải&nbsp;những trở ngại sau:</h2>

<table align="center" border="1" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
			<td>
			<p>&nbsp; &nbsp;<img alt="nhập hàng Trung Quốc" src="https://www.thuongdo.com/sites/default/files/dichvu/nhap-hang-trung-quoc-8.png" />&nbsp;&nbsp;<em>Bạn gặp<strong>&nbsp;rào cản&nbsp;ngôn ngữ&nbsp;</strong>khi giao dịch &amp; đàm phán&nbsp;với&nbsp;đối tác tại Quảng Châu.</em></p>

			<p>&nbsp; &nbsp;<img alt="nhập hàng Trung Quốc" src="https://www.thuongdo.com/sites/default/files/dichvu/nhap-hang-trung-quoc-8.png" />&nbsp; C<em>ần tìm&nbsp;đơn vị vận chuyển&nbsp;đảm bảo an toàn hàng hóa, thời gian vận chuyển nhanh chóng cũng như cách làm việc chuyên nghiệp.</em></p>

			<p><em>&nbsp; &nbsp;<img alt="nhập hàng Trung Quốc" src="https://www.thuongdo.com/sites/default/files/dichvu/nhap-hang-trung-quoc-8.png" />&nbsp;Có những đơn vị&nbsp;vận chuyển&nbsp;nhập hàng từ Quảng Châu&nbsp;chỉ nhận đơn hàng lớn bỏ qua những đơn hàng nhỏ lẻ&nbsp;của bạn.</em></p>

			<p><em>&nbsp; &nbsp;<img alt="nhập hàng Trung Quốc" src="https://www.thuongdo.com/sites/default/files/dichvu/nhap-hang-trung-quoc-8.png" />&nbsp;Thời gian nhận hàng hóa của bạn&nbsp;khá dài &amp;&nbsp;bạn mong chờ từng ngày để hàng đến được tay bạn.</em></p>

			<p>&nbsp; &nbsp;<img alt="nhập hàng Trung Quốc" src="https://www.thuongdo.com/sites/default/files/dichvu/nhap-hang-trung-quoc-8.png" />&nbsp; Chưa hài lòng với những&nbsp;đơn vị trước kia.</p>
			</td>
		</tr>
	</tbody>
</table>

<h2>Khi&nbsp;nhập hàng&nbsp;&amp;&nbsp;vận chuyển hàng&nbsp;từ Quảng Châu&nbsp;về&nbsp;Việt Nam bạn sẽ gặp phải&nbsp;những trở ngại sau:</h2>

<table align="center" border="1" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
			<td>
			<p>&nbsp; &nbsp;<img alt="nhập hàng Trung Quốc" src="https://www.thuongdo.com/sites/default/files/dichvu/nhap-hang-trung-quoc-8.png" />&nbsp;&nbsp;<em>Bạn gặp<strong>&nbsp;rào cản&nbsp;ngôn ngữ&nbsp;</strong>khi giao dịch &amp; đàm phán&nbsp;với&nbsp;đối tác tại Quảng Châu.</em></p>

			<p>&nbsp; &nbsp;<img alt="nhập hàng Trung Quốc" src="https://www.thuongdo.com/sites/default/files/dichvu/nhap-hang-trung-quoc-8.png" />&nbsp; C<em>ần tìm&nbsp;đơn vị vận chuyển&nbsp;đảm bảo an toàn hàng hóa, thời gian vận chuyển nhanh chóng cũng như cách làm việc chuyên nghiệp.</em></p>

			<p><em>&nbsp; &nbsp;<img alt="nhập hàng Trung Quốc" src="https://www.thuongdo.com/sites/default/files/dichvu/nhap-hang-trung-quoc-8.png" />&nbsp;Có những đơn vị&nbsp;vận chuyển&nbsp;nhập hàng từ Quảng Châu&nbsp;chỉ nhận đơn hàng lớn bỏ qua những đơn hàng nhỏ lẻ&nbsp;của bạn.</em></p>

			<p><em>&nbsp; &nbsp;<img alt="nhập hàng Trung Quốc" src="https://www.thuongdo.com/sites/default/files/dichvu/nhap-hang-trung-quoc-8.png" />&nbsp;Thời gian nhận hàng hóa của bạn&nbsp;khá dài &amp;&nbsp;bạn mong chờ từng ngày để hàng đến được tay bạn.</em></p>

			<p>&nbsp; &nbsp;<img alt="nhập hàng Trung Quốc" src="https://www.thuongdo.com/sites/default/files/dichvu/nhap-hang-trung-quoc-8.png" />&nbsp; Chưa hài lòng với những&nbsp;đơn vị trước kia.</p>
			</td>
		</tr>
	</tbody>
</table>

<p>Hãy để&nbsp;<strong>TB Logistics</strong>&nbsp;<strong>GIẢI QUYẾT</strong>&nbsp;những&nbsp;lo lắng&nbsp;này&nbsp;của bạn, bằng&nbsp;dịch vụ&nbsp;nhận&nbsp;vận chuyển hàng&nbsp;từ Quảng Châu&nbsp;về Việt Nam.</p>

<h2><strong>Dịch vụ đi kèm khi vận chuyển hàng từ Quảng Châu về Việt Nam</strong></h2>

<ol>
	<li>Dịch vụ tìm kiếm nguồn hàng, dẫn khách đi mua hàng</li>
	<li>Dịch vụ đặt hàng, đàm phán thương lượng</li>
	<li>Dịch vụ thanh toán</li>
	<li>Dịch vụ đóng gói</li>
	<li>Dịch vụ kiểm hàng</li>
	<li>Dịch vụ đặt vé, đặt phòng khách sạn, làm visa</li>
</ol>

<h2><strong>05 lý do bạn nên sử dụng dịch vụ vận chuyển hàng Quảng Châu của TB Logistics</strong></h2>

<p><strong><a href="https://thuongdo.vn/"><img alt="nhập hàng trung quốc" src="https://www.thuongdo.com/sites/default/files/dichvu/nhap-hang-trung-quoc-9.png" /></a>&nbsp;Giá vận chuyển hàng hóa thấp:&nbsp;</strong>Hiện nay, <strong>TB </strong> Logistic có mối quan hệ tốt với một số đối tác kinh doanh tại Quảng Châu. Chúng tôi cũng là đơn vị vận chuyển có sự am hiểu sâu sắc về yếu tố địa bàn, do đó có thể thương lượng để hạ mức chi phí của các dịch vụ xuống mức thấp nhất. Vì vậy khách hàng có thể yên tâm khi sử dụng dịch vụ vận chuyển hàng hóa của chúng tôi.</p>

<p><strong><a href="https://thuongdo.vn/"><img alt="nhập hàng trung quốc" src="https://www.thuongdo.com/sites/default/files/dichvu/nhap-hang-trung-quoc-9.png" /></a>&nbsp;</strong><strong>Tác phong phục vụ chuyên nghiệp:</strong>&nbsp;Với mục tiêu là làm hài lòng ngay cả những khách hàng khó tính nhất, <strong>TB </strong> Logistic luôn chú trọng đến lợi ích của khách hàng. Với đội ngũ nhân viên chuyên nghiệp, trẻ trung, năng động, thông thạo và giàu kinh nghiệm đàm phán giao dịch bằng Tiếng Trung. Chúng tôi sẽ giúp cho khách hàng tìm được nguồn hàng với chi phí vận chuyển thấp nhất.</p>

<p><strong><a href="https://thuongdo.vn/"><img alt="nhập hàng trung quốc" src="https://www.thuongdo.com/sites/default/files/dichvu/nhap-hang-trung-quoc-9.png" /></a>&nbsp;Nhập hàng sỉ lẻ dù là một món hàng:&nbsp;TB </strong> Logistics&nbsp;nhận order nhập hàng từ Quảng Châu&nbsp;về Việt Nam sỉ lẻ tất cả các&nbsp;hàng ngoại&nbsp;trừ&nbsp;hàng quốc cấm.</p>

<p><strong><a href="https://thuongdo.vn/"><img alt="nhập hàng trung quốc" src="https://www.thuongdo.com/sites/default/files/dichvu/nhap-hang-trung-quoc-9.png" /></a>&nbsp;</strong><strong>Đảm bảo hàng hóa vận chuyển:</strong>&nbsp;Nếu hàng hóa&nbsp;hư hỏng, mất mát trong quá trình vận chuyển chúng tôi cam kết hoàn tiền 100% cho Quý khách khi sử dụng dịch vụ chuyển hàng từ Quảng Châu&nbsp;về Việt Nam của <strong>TB </strong> Logistics&nbsp;- Thời gian ship hàng nhanh chóng. Chúng tôi có người kiểm hàng xác nhận và kiểm&nbsp;đếm ngay trước khi gửi. Vì vậy, hàng hóa của bạn&nbsp;được&nbsp;đảm bảo&nbsp;đầy&nbsp;đủ và chất lượng tốt hơn.</p>

<p><strong><a href="https://thuongdo.vn/"><img alt="nhập hàng trung quốc" src="https://www.thuongdo.com/sites/default/files/dichvu/nhap-hang-trung-quoc-9.png" /></a>&nbsp;Không tính thêm&nbsp;bất kỳ chi phí khi đã thỏa thuận:&nbsp;</strong>Trước khi nhận vận chuyển hàng chúng tôi đã tính hết toàn bộ chi phí với khách hàng và thông báo chi tiết từng loại hàng khác nhau. Vì thế khi nhận hàng hóa chúng tôi sẽ không thu thêm bất kỳ khoản phí nào khác của Quý khách.</p>

<p>&nbsp;</p>', 1, NULL, CAST(N'2019-11-29T23:30:14.077' AS DateTime), NULL, CAST(N'2019-09-16T00:04:06.507' AS DateTime))
INSERT [dbo].[Services] ([Id], [Name], [Image], [Title], [NumberView], [Orders], [Summary], [Active], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (N'fdaec60c-73e9-4163-8b90-ac117dd34528', N'Dịch vụ đặt hàng Alibaba', N'https://www.thuongdo.com/sites/default/files/dichvu/nhap-hang-alibaba.jpg', N'', 0, 2, N'<h2><strong>Dịch vụ mua hàng Alibaba</strong></h2>

<p>Hiện nay nhu cầu mua hàng trên Alibaba.com là tất yếu với hầu hết các bạn đang kinh doanh muốn tìm nguồn hàng giá gốc, còn nếu mua lẻ bạn có thể tham khảo dịch vụ&nbsp;<a href="https://www.thuongdo.com/dat-hang-taobao">đặt hàng&nbsp;taobao</a>&nbsp;hoặc&nbsp;<a href="https://www.thuongdo.com/dat-hang-tmall">đặt hàng&nbsp;tmall</a>&nbsp;nhé. Hàng hoá trên Alibaba vô cùng phong phú và đa dạng về chủng loại cũng như mẫu mã. Từ quần áo, đồ gia dụng, nội thất, máy móc thiết bị…. hàng gì cũng có cả. Tại đây chúng tôi cung cấp cho bạn dịch vụ mua hàng trên Alibaba để phục vụ cho việc kinh doanh của bạn tại Việt Nam.&nbsp;Cơ hội nhập được nguồn hàng giá gốc trên alibaba là vô cùng lớn, mặc dù alibaba Việt Nam cũng hỗ trợ Tiếng Việt nhưng bạn sẽ gặp phải một số rào cản sau.</p>

<h2><strong>Rào cản khi mua hàng trên Alibaba:</strong></h2>

<ol>
	<li>Rào cản giao tiếp do ngôn ngữ</li>
	<li>Khó khăn xác minh nhà sản xuất</li>
	<li>Thời gian vận chuyển lâu hơn</li>
	<li>Thủ tục nhập khẩu hải quan phức tạp và chi phí cao</li>
	<li>Rủi ra về thanh toán nếu không am hiểu sâu nghiệp vụ</li>
</ol>

<h2><strong>Ưu điểm khi mua hàng trên Alibaba:</strong></h2>

<ol>
	<li>Chi phí nhập hàng thấp&nbsp;hơn</li>
	<li>Nhà cung cấp thường cởi mở dù bạn nhập ít hay nhiều</li>
	<li>Bạn có thể lựa chọn nhiều nhà cung cấp khác nhau</li>
	<li>Sản phẩm đa dạng và phong phú</li>
	<li>Nền tảng thương mại điện tử hiện đại nhất</li>
</ol>

<p><em><strong>Chú ý:&nbsp;</strong>&nbsp;Dịch vụ mua hàng alibaba của Thương Đô sẽ giúp bạn khác phục toàn bộ rào cản trên và gia tăng sức mạnh của những lợi thế sẵn có. Nếu bạn quyết tâm nhập khẩu hàng từ nước ngoài thì lựa chọn nhập hàng Alibaba là hoàn toàn chuẩn xác. Với những con số thực tế về vốn hoá thị trường Alibaba là công ty thương mại điện tử lớn hàng đầu&nbsp;nhất thế giới, và lớn nhất tại Trung Quốc, mục tiêu chủ yếu của họ là kết nối nhà cung cấp, nhà xuất khẩu châu Á với các quốc gia trên toàn thế giới, với hàng trăm ngàn nhà cung cấp thì không sản phẩm nào là bạn không thể tìm thấy.</em></p>', 1, NULL, CAST(N'2019-11-29T22:49:19.350' AS DateTime), NULL, CAST(N'2018-10-30T00:04:10.050' AS DateTime))
INSERT [dbo].[Services] ([Id], [Name], [Image], [Title], [NumberView], [Orders], [Summary], [Active], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (N'3aa5d982-68bd-4d5d-a3a6-b0a7e97f7c96', N'Dịch vụ đặt mua hàng Tmall', N'https://www.thuongdo.com/sites/default/files/dichvu/dat-hang-tmall.jpg', N'', 0, 2, N'<h2><strong>Chúng tôi&nbsp;cung cấp dịch vụ mua hàng từ website Tmall&nbsp;về Việt Nam</strong></h2>

<p>Tmall là website với mô hình B2C (Bán hàng trực tiếp từ doanh nghiệp đến người tiêu dùng) vì vậy chất lượng sản phẩm luôn tốt hơn trên Taobao, 1688, Alibaba,... Đây là trang ruột trực thuộc tập đoàn Alibaba chuyên cung cấp các sản phẩm chính hãng, nếu bạn mua hàng trên Tmall thì khỏi lo hàng giả vì trên hệ thống của Tmall được tập đoàn Alibaba kiểm duyệt chặt chẽ hơn Taobao rất nhiều (Từ chất lượng sản phẩm đến giá cả) nên các bạn có thể yên tâm mua hàng.</p>

<p>Nếu bạn mua hàng về kinh doanh mà phân khúc khách hàng của bạn thuộc phân khúc khách hàng cao cấp thì Tmall là lựa chọn tuyệt vời, hoặc bạn cũng có thể mua hàng về dùng cá nhân ở đây thì chất lượng khỏi phải chê, mặc dù đắt hơn so với Alibaba, Taobao, 1688 nhưng hàng ở đây chất lượng và kể cả so về giá có cao hơn những trang trên nhưng so với sản phẩm cùng giá ở Việt Nam thì chất lượng chắc chắn tốt hơn.</p>

<p>Đó là một điều không thể nào phủ nhận cho đến nay. Vì vậy nhu cầu mua hàng từ Tmall về bán hay tiêu dùng đang gia tăng rất mạnh tại Việt Nam nên chúng tôi cho ra đời dịch vụ đặt hàng Tmall về Việt Nam cực kỳ tiện lợi, chuyên nghiệp và tiết kiệm chi phí.&nbsp;</p>

<h2><strong>Lý do bạn nên đặt hàng trên Tmall</strong></h2>

<p><a href="https://www.thuongdo.com/dat-hang-tmall"><img alt="đặt hàng tmall, mua hàng tmall, nhập hàng tmall" src="https://www.thuongdo.com/sites/default/files/dichvu/mua-hang-tmall.jpg" /></a></p>

<ol>
	<li>Tmall là hệ thống website thương mại điện tử trực thuộc của tập đoàn Alibaba.</li>
	<li>Tmall thuộc Mô hình giao dịch B2C liên kết các nhà cung cấp, các thương hiệu của quốc tế, các nhãn hàng NỘI ĐỊA Trung Quốc được Alibaba kiểm duyệt chặt chẽ từ chất lượng đến giá bán.</li>
	<li>Tập trung hàng triệu nhà cung cấp/gian hàng uy tín, đảm bảo phong phú mẫu mã, đa dạng tính năng của sản phẩm cho khách hàng tha hồ chọn lựa sản phẩm.</li>
	<li>Các cửa hàng trên Tmall ngoài sự kiểm soát chặt chẽ của Alibaba còn có tới 50% thị phần thương mại điện tử, lượng khách này đủ nhận biết và đánh giá chính xác chất lượng sản phẩm, không thể lừa dối được khách hàng.</li>
	<li>Tmall khác hẳn Alibaba, 1688, Taobao ở chỗ hàng trên đây toàn hàng chất lượng, rất ít hàng kém lượng, nếu bạn đã vượt lên đẳng cấp kinh doanh mới thì Tmall là lựa chọn của bạn, vì hàng ở đây chất lượng nên bạn không cần phải cạnh tranh quá nhiều về giá.</li>
</ol>

<p>Để việc mua hàng trên Tmall dễ dàng và đơn giản hơn, giúp khách hàng tiện lợi trong việc nhập hàng Tmall về kinh doanh buôn bán hoặc tiêu dùng tại Việt Nam. Thương Đô cho ra mắt dịch vụ đặt hàng Tmall về Việt Nam. Quý khách chỉ cần ngồi ở nhà lựa chọn hàng hoá và chúng tôi sẽ thay bạn làm tất cả những việc còn lại, bạn chỉ việc ngồi nhà chờ hàng về và tập chung bán hàng.</p>

<p>Chúng tôi áp dụng công cụ đặt hàng hiện đại nhất trong ngành, giúp quý khách tiện lợi trong việc quản lý đơn hàng, kiện hàng và thanh toán. Quý khách chỉ cần thực hiện 3 bước:</p>

<ol>
	<li>Đăng ký tài khoản và cài đặt công cụ đặt hàng của Thương Đô</li>
	<li>Tìm kiếm lựa chọn sản phẩm, nhà cung cấp uy tín</li>
	<li>Thanh toán và chờ hàng đến tay.</li>
</ol>

<h2><strong>Xem thêm video hướng dẫn đặt hàng Tmall</strong></h2>

<p><a href="https://www.thuongdo.com/dat-hang-tmall"><img alt="order tmall, đặt hàng tmall, mua hàng tmall, nhập hàng tmall" src="https://www.thuongdo.com/sites/default/files/dichvu/dat-hang-tmall_0.jpg" /></a></p>

<h2><strong>LƯU Ý khi mua hàng trên Tmall</strong></h2>

<ul>
	<li>Quý khách tìm hiểu kỹ về sản phẩm và uy tín nhà cung cấp như hướng dẫn của Thương Đô</li>
	<li>Không nên đặt sản phẩm ít giao dịch hoặc uy tín nhà cung cấp kém.</li>
	<li>Chọn sản phẩm có hình ảnh thực tế từ những người mua hàng</li>
	<li>Nên mua hàng cùng một nhà cung cấp để cước phí vận chuyển tại nước cung cấp sản phẩm rẻ hơn, hàng về cùng một đợt và tránh tình trạng sai sót của đơn hàng.</li>
</ul>

<p>Hy vọng thông qua bài giới thiệu này, quý khách hiểu hơn về dịch mua hàng Tmall về Việt Nam của Thương Đô. Nếu quý khách có thắc mắc vui lòng liên hệ cho Thương Đô để được tư vấn chi tiết hơn về phương thức nhập hàng.&nbsp;</p>', 1, NULL, CAST(N'2019-11-30T00:02:02.750' AS DateTime), NULL, CAST(N'2018-10-30T00:04:00.600' AS DateTime))
SET IDENTITY_INSERT [dbo].[VN_City] ON 

INSERT [dbo].[VN_City] ([Id], [Name], [Active]) VALUES (1, N'Hà Nội', 1)
INSERT [dbo].[VN_City] ([Id], [Name], [Active]) VALUES (2, N'Thái Nguyên', 1)
INSERT [dbo].[VN_City] ([Id], [Name], [Active]) VALUES (3, N'Bắc Ninh', 1)
INSERT [dbo].[VN_City] ([Id], [Name], [Active]) VALUES (4, N'Bắc Giang', 1)
SET IDENTITY_INSERT [dbo].[VN_City] OFF
SET IDENTITY_INSERT [dbo].[VN_Districts] ON 

INSERT [dbo].[VN_Districts] ([Id], [Name], [Active], [CityId]) VALUES (1, N'Đống Đa', 1, 1)
INSERT [dbo].[VN_Districts] ([Id], [Name], [Active], [CityId]) VALUES (2, N'Ba Đình', 1, 1)
INSERT [dbo].[VN_Districts] ([Id], [Name], [Active], [CityId]) VALUES (3, N'Cầu Giấy', 1, 1)
INSERT [dbo].[VN_Districts] ([Id], [Name], [Active], [CityId]) VALUES (4, N'Tây Hồ', 1, 1)
INSERT [dbo].[VN_Districts] ([Id], [Name], [Active], [CityId]) VALUES (5, N'Hoàn Kiếm', 1, 1)
INSERT [dbo].[VN_Districts] ([Id], [Name], [Active], [CityId]) VALUES (6, N'Hai Bà Trưng', 1, 1)
INSERT [dbo].[VN_Districts] ([Id], [Name], [Active], [CityId]) VALUES (7, N'Thanh Xuân', 1, 1)
INSERT [dbo].[VN_Districts] ([Id], [Name], [Active], [CityId]) VALUES (8, N'Hà Đông', 1, 1)
INSERT [dbo].[VN_Districts] ([Id], [Name], [Active], [CityId]) VALUES (9, N'Hoàng Mai', 1, 1)
INSERT [dbo].[VN_Districts] ([Id], [Name], [Active], [CityId]) VALUES (10, N'Nam Từ Liêm', 1, 1)
INSERT [dbo].[VN_Districts] ([Id], [Name], [Active], [CityId]) VALUES (11, N'TP Bắc Giang', 1, 4)
INSERT [dbo].[VN_Districts] ([Id], [Name], [Active], [CityId]) VALUES (12, N'Hiệp Hòa', 1, 4)
INSERT [dbo].[VN_Districts] ([Id], [Name], [Active], [CityId]) VALUES (13, N'Tân Yên', 1, 4)
INSERT [dbo].[VN_Districts] ([Id], [Name], [Active], [CityId]) VALUES (14, N'Lạng Giang', 1, 4)
INSERT [dbo].[VN_Districts] ([Id], [Name], [Active], [CityId]) VALUES (15, N'Lục Nam', 1, 4)
INSERT [dbo].[VN_Districts] ([Id], [Name], [Active], [CityId]) VALUES (16, N'Lục Ngạn', 1, 4)
INSERT [dbo].[VN_Districts] ([Id], [Name], [Active], [CityId]) VALUES (17, N'Sơn Động', 1, 4)
INSERT [dbo].[VN_Districts] ([Id], [Name], [Active], [CityId]) VALUES (18, N'Việt Yên', 1, 4)
INSERT [dbo].[VN_Districts] ([Id], [Name], [Active], [CityId]) VALUES (19, N'Yên Dũng', 1, 4)
INSERT [dbo].[VN_Districts] ([Id], [Name], [Active], [CityId]) VALUES (20, N'Yên Thế', 1, 4)
INSERT [dbo].[VN_Districts] ([Id], [Name], [Active], [CityId]) VALUES (21, N'TP Thái Nguyên', 1, 2)
INSERT [dbo].[VN_Districts] ([Id], [Name], [Active], [CityId]) VALUES (22, N'Đại Từ', 1, 2)
INSERT [dbo].[VN_Districts] ([Id], [Name], [Active], [CityId]) VALUES (23, N'Định Hóa', 1, 2)
INSERT [dbo].[VN_Districts] ([Id], [Name], [Active], [CityId]) VALUES (24, N'Đồng Hỷ', 1, 2)
INSERT [dbo].[VN_Districts] ([Id], [Name], [Active], [CityId]) VALUES (25, N'Phổ Yên', 1, 2)
INSERT [dbo].[VN_Districts] ([Id], [Name], [Active], [CityId]) VALUES (26, N'Phú Bình', 1, 2)
INSERT [dbo].[VN_Districts] ([Id], [Name], [Active], [CityId]) VALUES (27, N'Phú Lương', 1, 2)
INSERT [dbo].[VN_Districts] ([Id], [Name], [Active], [CityId]) VALUES (28, N'Sông Công', 1, 2)
INSERT [dbo].[VN_Districts] ([Id], [Name], [Active], [CityId]) VALUES (29, N'Võ Nhai', 1, 2)
INSERT [dbo].[VN_Districts] ([Id], [Name], [Active], [CityId]) VALUES (30, N'TP Bắc Ninh', 1, 3)
INSERT [dbo].[VN_Districts] ([Id], [Name], [Active], [CityId]) VALUES (31, N'Gia Bình', 1, 3)
INSERT [dbo].[VN_Districts] ([Id], [Name], [Active], [CityId]) VALUES (32, N'Lương Tài', 1, 3)
INSERT [dbo].[VN_Districts] ([Id], [Name], [Active], [CityId]) VALUES (33, N'Quế Võ', 1, 3)
INSERT [dbo].[VN_Districts] ([Id], [Name], [Active], [CityId]) VALUES (34, N'Thuận Thành', 1, 3)
INSERT [dbo].[VN_Districts] ([Id], [Name], [Active], [CityId]) VALUES (35, N'Tiên Du', 1, 3)
INSERT [dbo].[VN_Districts] ([Id], [Name], [Active], [CityId]) VALUES (36, N'Từ Sơn', 1, 3)
INSERT [dbo].[VN_Districts] ([Id], [Name], [Active], [CityId]) VALUES (37, N'Yên Phong', 1, 3)
SET IDENTITY_INSERT [dbo].[VN_Districts] OFF
SET IDENTITY_INSERT [dbo].[VN_Streets] ON 

INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1, N'Cầu Giấy', 1, 3)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (2, N'Chùa Hà', 1, 3)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (3, N'Đặng Thùy Trâm', 1, 3)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (4, N'Dịch Vọng', 1, 3)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (5, N'Dịch Vọng Hậu', 1, 3)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (6, N'Đỗ Quang', 1, 3)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (7, N'Doãn Kế Thiện', 1, 3)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (8, N'Đông Quan', 1, 3)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (9, N'Dương Đình Nghệ', 1, 3)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (10, N'Đường nối từ Trung Hòa qua khu đô thị Yên Hòa', 1, 3)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (11, N'Dương Quảng Hàm', 1, 3)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (12, N'Duy Tân', 1, 3)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (13, N'Hồ Tùng Mậu', 1, 3)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (14, N'Hoa Bằng', 1, 3)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (15, N'Hoàng Đạo Thúy', 1, 3)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (16, N'Hoàng Minh Giám', 1, 3)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (17, N'Hoàng Ngân', 1, 3)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (18, N'Hoàng Quốc Việt', 1, 3)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (19, N'Hoàng Sâm', 1, 3)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (20, N'Khuất Duy Tiến', 1, 3)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (21, N'Lạc Long Quân', 1, 3)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (22, N'Lê Đức Thọ', 1, 3)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (23, N'Lê Đức Thọ kéo dài', 1, 3)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (24, N'Lê Văn Lương', 1, 3)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (25, N'Mai Dịch', 1, 3)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (26, N'Nghĩa Tân', 1, 3)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (27, N'Nguyễn Chánh', 1, 3)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (28, N'Nguyễn Đình Hoàn', 1, 3)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (29, N'Nguyễn Khả Trạc', 1, 3)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (30, N'Nguyễn Khang', 1, 3)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (31, N'Nguyễn Khánh Toàn', 1, 3)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (32, N'Nguyễn Ngọc Vũ', 1, 3)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (33, N'Nguyễn Phong Sắc', 1, 3)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (34, N'Nguyễn Thị Định', 1, 3)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (35, N'Nguyễn Thị Thập', 1, 3)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (36, N'Nguyễn Văn Huyên', 1, 3)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (37, N'Phạm Hùng', 1, 3)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (38, N'Phạm Thận Duật', 1, 3)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (39, N'Phạm Tuấn Tài', 1, 3)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (40, N'Phạm Văn Đồng', 1, 3)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (41, N'Phan Văn Trường', 1, 3)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (42, N'Phùng Chí Kiên', 1, 3)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (43, N'Quan Hoa', 1, 3)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (44, N'Quan Nhân', 1, 3)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (45, N'Thành Thái', 1, 3)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (46, N'Tô Hiệu', 1, 3)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (47, N'Tôn Thất Thuyết', 1, 3)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (48, N'Trần Bình', 1, 3)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (49, N'Trần Cung', 1, 3)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (50, N'Trần Đăng Ninh', 1, 3)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (51, N'Trần Duy Hưng', 1, 3)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (52, N'Trần Kim Xuyến', 1, 3)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (53, N'Trần Quốc Hoàn', 1, 3)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (54, N'Trần Quý Kiên', 1, 3)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (55, N'Trần Thái Tông', 1, 3)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (56, N'Trần Tử Bình', 1, 3)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (57, N'Trần Vỹ', 1, 3)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (58, N'Trung Hòa', 1, 3)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (59, N'Trung Kính', 1, 3)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (60, N'Vũ Phạm Hàm', 1, 3)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (61, N'XuânThủy', 1, 3)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (62, N'Yên Hòa', 1, 3)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (63, N'An Trạch', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (64, N'Bích Câu', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (65, N'Cát Linh', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (66, N'Cầu Giấy', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (67, N'Cầu Mới', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (68, N'Chùa Bộc', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (69, N'Chùa Láng', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (70, N'Đặng Tiến Đông', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (71, N'Đặng Trần Côn', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (72, N'Đặng Văn Ngữ', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (73, N'Đào Duy Anh', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (74, N'Đoàn Thị Điểm', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (75, N'Đông Các', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (76, N'Đông Tác', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (77, N'Đường Đê La Thành', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (78, N'Đường Hòa Nam mới', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (79, N'Đường Ven hồ Ba Mẫu', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (80, N'Giải Phóng', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (81, N'Giảng Võ', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (82, N'Hàng Cháo', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (83, N'Hào Nam', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (84, N'Hồ Đắc Di', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (85, N'Hồ Giám', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (86, N'Hoàng Cầu', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (87, N'Hoàng Ngọc Phách', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (88, N'Hoàng Tích Trí', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (89, N'Huỳnh Thúc Kháng', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (90, N'Khâm Thiên', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (91, N'Khương Thượng', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (92, N'Kim Hoa', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (93, N'La Thành', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (94, N'Láng', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (95, N'Láng Hạ', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (96, N'Lê Duẩn', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (97, N'Lương Đình Của', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (98, N'Lý Văn Phúc', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (99, N'Mai Anh Tuấn', 1, 1)
GO
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (100, N'Nam Đồng', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (101, N'Ngõ Hàng Bột', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (102, N'Ngô Sỹ Liên', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (103, N'Ngô Tất Tố', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (104, N'Ngõ Thông Phong', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (105, N'Nguyễn Chí Thanh', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (106, N'Nguyên Hồng', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (107, N'Nguyễn Khuyến', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (108, N'Nguyễn Lương Bằng', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (109, N'Nguyễn Như Đổ', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (110, N'Nguyễn Phúc Lai', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (111, N'Nguyễn Thái Học', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (112, N'Nguyễn Trãi', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (113, N'Ô Chợ Dừa', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (114, N'Phạm Ngọc Thạch', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (115, N'Phan Phù Tiên', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (116, N'Phan Văn Trị', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (117, N'Pháo Đài Láng', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (118, N'Phổ Giác', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (119, N'Phương Mai', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (120, N'Quốc Tử Giám', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (121, N'Tây Sơn', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (122, N'Thái Hà', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (123, N'Thái Thịnh', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (124, N'Tôn Đức Thắng', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (125, N'Tôn Thất Tùng', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (126, N'Trần Hữu Tước', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (127, N'Trần Quang Diệu', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (128, N'Trần Quý Cáp', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (129, N'Trịnh Hoài Đức', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (130, N'Trúc Khê', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (131, N'Trung Liệt', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (132, N'Trường Chinh', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (133, N'Văn Miếu', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (134, N'Vĩnh Hồ', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (135, N'Võ Văn Dũng', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (136, N'Vọng', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (137, N'Vũ Ngọc Phan', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (138, N'Vũ Thạnh', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (139, N'Xã Đàn', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (140, N'Y Miếu', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (141, N'Yên Lãng', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (142, N'Yên Thế (Phố)', 1, 1)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (143, N'An Xá', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (144, N'Bà Huyện Thanh Quan', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (145, N'Bắc Sơn', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (146, N'Bưởi', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (147, N'Cao Bá Quát', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (148, N'Cầu Giấy', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (149, N'Châu Long', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (150, N'Chu Văn An', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (151, N'Chùa Một Cột', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (152, N'Cửa Bắc', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (153, N'Đặng Dung', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (154, N'Đặng Tất', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (155, N'Đào Tấn', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (156, N'Điện Biên Phủ', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (157, N'Độc lập', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (158, N'Đốc Ngữ', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (159, N'Đội Cấn 1', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (160, N'Đội Cấn 2', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (161, N'Đội Nhân', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (162, N'Giang Văn Minh', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (163, N'Giảng Võ', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (164, N'Hàng Bún', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (165, N'Hàng Cháo', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (166, N'Hàng Than', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (167, N'Hoàng Diệu', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (168, N'Hoàng Hoa Thám', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (169, N'Hoàng Văn Thụ', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (170, N'Hoè Nhai', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (171, N'Hồng Hà', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (172, N'Hồng Phúc', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (173, N'Hùng Vương', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (174, N'Khúc Hạo', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (175, N'Kim Mã 1', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (176, N'Kim Mã 2', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (177, N'Kim Mã Thượng', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (178, N'La Thành', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (179, N'Lạc Chính', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (180, N'Láng Hạ', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (181, N'Lê Duẩn', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (182, N'Lê Hồng Phong', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (183, N'Lê Trực', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (184, N'Liễu Giai', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (185, N'Linh Lang', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (186, N'Lý Văn Phúc', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (187, N'Mạc Đĩnh Chi', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (188, N'Mai Anh Tuấn', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (189, N'Mai Xuân Thưởng', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (190, N'Nam Cao', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (191, N'Nam Tràng', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (192, N'Nghĩa Dũng', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (193, N'Ngõ Châu Long', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (194, N'Ngõ Hàng Bún', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (195, N'Ngõ Hàng Đậu', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (196, N'Ngõ Núi Trúc', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (197, N'Ngõ Trúc Lạc', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (198, N'Ngọc Hà', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (199, N'Ngọc Khánh', 1, 2)
GO
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (200, N'Ngũ Xã', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (201, N'Nguyễn Biểu', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (202, N'Nguyễn Cảnh Chân', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (203, N'Nguyễn Chí Thanh', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (204, N'Nguyễn Công Hoan', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (205, N'Nguyên Hồng', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (206, N'Nguyễn Khắc Hiếu', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (207, N'Nguyễn Khắc Nhu', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (208, N'Nguyễn Phạm Tuân', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (209, N'Nguyễn Thái Học', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (210, N'Nguyễn Thiệp', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (211, N'Nguyễn Tri Phương', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (212, N'Nguyễn Trung Trực', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (213, N'Nguyễn Trường Tộ', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (214, N'Nguyễn Văn Ngọc', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (215, N'Núi Trúc', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (216, N'Ông Ích Khiêm', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (217, N'Phạm Hồng Thái', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (218, N'Phạm Huy Thông', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (219, N'Phan Đình Phùng', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (220, N'Phan Huy Ích', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (221, N'Phan Kế Bính', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (222, N'Phó Đức Chính', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (223, N'Phúc Xá', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (224, N'Quần Ngựa', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (225, N'Quan Thánh', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (226, N'Sơn Tây', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (227, N'Tân Ấp', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (228, N'Thanh Bảo', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (229, N'Thành Công', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (230, N'Thanh Niên', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (231, N'Tôn Thất Đàm', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (232, N'Tôn Thất Thiệp', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (233, N'Trần Huy Liệu', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (234, N'Trần Phú', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (235, N'Trần Tế Xương', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (236, N'Trấn Vũ', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (237, N'Trúc Bạch', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (238, N'Vạn Bảo', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (239, N'Văn Cao', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (240, N'Vạn Phúc', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (241, N'Vĩnh Phúc', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (242, N'Yên Ninh', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (243, N'Yên Phụ', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (244, N'Yên Thế', 1, 2)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (245, N'19-12', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (246, N'Ấu Triệu', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (247, N'Bà Triệu', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (248, N'Bạch Đằng', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (249, N'Bảo Khánh', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (250, N'Bảo Linh', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (251, N'Bát Đàn', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (252, N'Bát Sứ', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (253, N'Cao Thắng', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (254, N'Cầu Đất', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (255, N'Cầu Đông', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (256, N'Cầu Gỗ', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (257, N'Chả Cá', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (258, N'Chân Cầm', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (259, N'Chợ Gạo', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (260, N'Chương Dương Độ', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (261, N'Cổ Tân', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (262, N'Cổng Đục', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (263, N'Cửa Đông', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (264, N'Cửa Nam', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (265, N'Dã Tượng', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (266, N'Đặng Thái Thân', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (267, N'Đào Duy Từ', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (268, N'Điện Biên Phủ', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (269, N'Đinh Công Tráng', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (270, N'Đinh Lễ', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (271, N'Đinh Liệt', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (272, N'Đình Ngang', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (273, N'Đinh Tiên Hoàng', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (274, N'Đoàn Nhữ Hài', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (275, N'Đông Thái', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (276, N'Đồng Xuân', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (277, N'Đường Thành', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (278, N'Gầm Cầu', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (279, N'Gia Ngư', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (280, N'Hà Trung', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (281, N'Hai Bà Trưng', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (282, N'Hàm Long', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (283, N'Hàm Tử Quan', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (284, N'Hàn Thuyên', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (285, N'Hàng Bạc', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (286, N'Hàng Bài', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (287, N'Hàng Bè', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (288, N'Hàng Bồ', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (289, N'Hàng Bông', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (290, N'Hàng Buồm', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (291, N'Hàng Bút', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (292, N'Hàng Cá', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (293, N'Hàng Cân', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (294, N'Hàng Chai', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (295, N'Hàng Chiếu', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (296, N'Hàng Chĩnh', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (297, N'Hàng Cót', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (298, N'Hàng Da', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (299, N'Hàng Đào', 1, 5)
GO
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (300, N'Hàng Đậu', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (301, N'Hàng Điếu', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (302, N'Hàng Đồng', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (303, N'Hàng Đường', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (304, N'Hàng Gà', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (305, N'Hàng Gai', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (306, N'Hàng Giấy', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (307, N'Hàng Giầy', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (308, N'Hàng Hòm', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (309, N'Hàng Khay', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (310, N'Hàng Khoai', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (311, N'Hàng Lược', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (312, N'Hàng Mã', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (313, N'Hàng Mắm', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (314, N'Hàng Mành', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (315, N'Hàng Muối', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (316, N'Hàng Ngang', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (317, N'Hàng Nón', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (318, N'Hàng Quạt', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (319, N'Hàng Rươi', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (320, N'Hàng Thiếc', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (321, N'Hàng Thùng', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (322, N'Hàng Tre', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (323, N'Hàng Trống', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (324, N'Hàng Vải', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (325, N'Hàng Vôi', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (326, N'Hồ Hoàn Kiếm', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (327, N'Hỏa Lò', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (328, N'Hồng Hà', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (329, N'Huế', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (330, N'Lãn Ông', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (331, N'Lê Duẩn', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (332, N'Lê Lai', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (333, N'Lê Phụng Hiểu', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (334, N'Lê Thạch', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (335, N'Lê Thái Tổ', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (336, N'Lê Thánh Tông', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (337, N'Lê Văn Hưu', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (338, N'Lê Văn Linh', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (339, N'Liên Trì', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (340, N'Lò Rèn', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (341, N'Lò Sũ[5]', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (342, N'Lương Ngọc Quyến', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (343, N'Lương Văn Can', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (344, N'Lý Đạo Thành', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (345, N'Lý Nam Đế', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (346, N'Lý Quốc Sư', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (347, N'Lý Thái Tổ', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (348, N'Lý Thường Kiệt', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (349, N'Mã Mây', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (350, N'Nam Ngư', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (351, N'Ngõ Gạch', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (352, N'Ngô Quyền', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (353, N'Ngô Thì Nhậm', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (354, N'Ngõ Trạm', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (355, N'Ngô Văn Sở', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (356, N'Nguyễn Chế Nghĩa', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (357, N'Nguyễn Du', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (358, N'Nguyễn Gia Thiều', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (359, N'Nguyễn Hữu Huân', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (360, N'Nguyễn Khắc Cần', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (361, N'Nguyên Khiết', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (362, N'Nguyễn Quang Bích', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (363, N'Nguyễn Siêu', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (364, N'Nguyễn Thái Học', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (365, N'Nguyễn Thiện Thuật', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (366, N'Nguyễn Thiếp', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (367, N'Nguyễn Tư Giản', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (368, N'Nguyễn Văn Tố', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (369, N'Nguyễn Xí', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (370, N'Nhà Chung', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (371, N'Nhà Hỏa', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (372, N'Nhà thờ', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (373, N'Ô Quan Chưởng', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (374, N'Phạm Ngũ Lão', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (375, N'Phạm Sư Mạnh', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (376, N'Phan Bội Châu', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (377, N'Phan Chu Trinh', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (378, N'Phan Đình Phùng', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (379, N'Phan Huy Chú', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (380, N'Phủ Doãn', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (381, N'Phúc Tân', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (382, N'Phùng Hưng', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (383, N'Quán Sứ', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (384, N'Quang Trung', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (385, N'Tạ Hiện', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (386, N'Thanh Hà', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (387, N'Thanh Yên', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (388, N'Thợ Nhuộm', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (389, N'Thuốc Bắc', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (390, N'Tô Tịch', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (391, N'Tôn Thất Thiệp', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (392, N'Tông Đản', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (393, N'Tống Duy Tân', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (394, N'Trần Bình Trọng', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (395, N'Trần Hưng Đạo', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (396, N'Trần Khánh Dư', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (397, N'Trần Nguyên Hãn', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (398, N'Trần Nhật Duật', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (399, N'Trần Phú', 1, 5)
GO
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (400, N'Trần Quang Khải', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (401, N'Trần Quốc Toản', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (402, N'Tràng Thi', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (403, N'Tràng Tiền', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (404, N'Triệu Quốc Đạt', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (405, N'Trương Hán Siêu', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (406, N'Vạn Kiếp', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (407, N'Vọng Đức', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (408, N'Vọng Hà', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (409, N'Yết Kiêu', 1, 5)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (410, N'An Dương', 1, 4)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (411, N'An Dương Vương', 1, 4)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (412, N'Âu Cơ', 1, 4)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (413, N'Bùi Trang Chước', 1, 4)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (414, N'Đặng Thai Mai', 1, 4)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (415, N'Đồng Cổ', 1, 4)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (416, N'Hoàng Hoa Thám', 1, 4)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (417, N'Hồng Hà', 1, 4)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (418, N'Hùng Vương', 1, 4)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (419, N'Lạc Long Quân', 1, 4)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (420, N'Mai Xuân Thưởng', 1, 4)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (421, N'Nghi Tàm', 1, 4)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (422, N'Nguyễn Đình Thi', 1, 4)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (423, N'Nguyễn Hoàng Tôn', 1, 4)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (424, N'Nhật Chiêu', 1, 4)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (425, N'Phan Đình Phùng', 1, 4)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (426, N'Phú Gia', 1, 4)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (427, N'Phú Thượng', 1, 4)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (428, N'Phú Xá', 1, 4)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (429, N'Phúc Hoa', 1, 4)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (430, N'Quảng An', 1, 4)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (431, N'Quảng Bá', 1, 4)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (432, N'Quảng Khánh', 1, 4)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (433, N'Tam Đa', 1, 4)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (434, N'Tây Hồ', 1, 4)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (435, N'Thanh Niên', 1, 4)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (436, N'Thượng Thụy', 1, 4)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (437, N'Thụy Khuê', 1, 4)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (438, N'Tô Ngọc Vân', 1, 4)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (439, N'Trích Sài', 1, 4)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (440, N'Trịnh Công Sơn', 1, 4)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (441, N'Từ Hoa', 1, 4)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (442, N'Tứ Liên', 1, 4)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (443, N'Văn Cao', 1, 4)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (444, N'Vệ Hồ', 1, 4)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (445, N'Võ Chí Công', 1, 4)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (446, N'Võng Thị', 1, 4)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (447, N'Vũ Miên', 1, 4)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (448, N'Vũ Tuấn Chiêu', 1, 4)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (449, N'Xuân Diệu', 1, 4)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (450, N'Xuân La', 1, 4)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (451, N'Yên Hoa', 1, 4)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (452, N'Yên Phụ (đường đôi)', 1, 4)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (453, N'Yên Phụ (đường đơn bên trong, đằng sau Nghi Tàm', 1, 4)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (454, N'8-3', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (455, N'Bà Triệu', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (456, N'Bạch Đằng', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (457, N'Bạch Mai', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (458, N'Bùi Ngọc Dương', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (459, N'Bùi Thị Xuân', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (460, N'Cảm Hội', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (461, N'Cao Đạt', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (462, N'Chùa Quỳnh', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (463, N'Chùa Vua', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (464, N'Đại Cồ Việt', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (465, N'Đại La', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (466, N'Đỗ Hành', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (467, N'Đỗ Ngọc Du', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (468, N'Đoàn Trần Nghiệp', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (469, N'Đội Cung', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (470, N'Đống Mác', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (471, N'Đồng Nhân', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (472, N'Dương Văn Bé', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (473, N'Giải Phóng', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (474, N'Hàn Thuyên', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (475, N'Hàng Chuối', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (476, N'Hồ Xuân Hương', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (477, N'Hoa Lư', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (478, N'Hòa Mã', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (479, N'Hoàng Mai', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (480, N'Hồng Mai', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (481, N'Huế', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (482, N'Hương Viên', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (483, N'Kim Ngưu', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (484, N'Lạc Nghiệp', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (485, N'Lạc Trung', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (486, N'Lãng Yên', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (487, N'Lê Đại Hành', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (488, N'Lê Duẩn', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (489, N'Lê Gia Đỉnh', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (490, N'Lê Ngọc Hân', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (491, N'Lê Quý Đôn', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (492, N'Lê Thanh Nghị', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (493, N'Lê Văn Hưu', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (494, N'Lò Đúc', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (495, N'Lương Yên', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (496, N'Mạc Thị Bưởi', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (497, N'Mai Hắc Đế', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (498, N'Minh Khai', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (499, N'Ngô Thì Nhậm', 1, 6)
GO
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (500, N'Nguyễn An Ninh', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (501, N'Nguyễn Bỉnh Khiêm', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (502, N'Nguyễn Cao', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (503, N'Nguyễn Công Trứ', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (504, N'Nguyễn Đình Chiểu', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (505, N'Nguyễn Du', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (506, N'Nguyễn Hiền', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (507, N'Nguyễn Huy Tự', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (508, N'Nguyễn Khoái', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (509, N'Nguyễn Quyền', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (510, N'Nguyễn Thượng Hiền', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (511, N'Nguyễn Trung Ngạn', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (512, N'Phạm Đình Hổ', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (513, N'Phù Đổng Thiên Vương', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (514, N'Phùng Khắc Khoan', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (515, N'Quang Trung', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (516, N'Quỳnh Lôi', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (517, N'Quỳnh Mai', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (518, N'Tạ Quang Bửu', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (519, N'Tam Trinh', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (520, N'Tăng Bạt Hổ', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (521, N'Tây Kết', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (522, N'Thái Phiên', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (523, N'Thanh Nhàn', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (524, N'Thể Giao', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (525, N'Thi Sách', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (526, N'Thiền Quang', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (527, N'Thịnh Yên', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (528, N'Thọ Lão', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (529, N'Tô Hiến Thành', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (530, N'Trần Bình Trọng', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (531, N'Trần Cao Vân', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (532, N'Trần Đại Nghĩa', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (533, N'Trần Hưng Đạo', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (534, N'Trần Khánh Dư', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (535, N'Trần Khát Chân', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (536, N'Trần Nhân Tông', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (537, N'Trần Thánh Tông', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (538, N'Trần Xuân Soạn', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (539, N'Triệu Việt Vương', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (540, N'Trương Định', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (541, N'Tuệ Tĩnh', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (542, N'Tương Mai', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (543, N'Vân Đồn', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (544, N'Vạn Kiếp', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (545, N'Vĩnh Tuy', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (546, N'Võ Thị Sáu', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (547, N'Vọng', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (548, N'Vũ Hữu Lợi', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (549, N'Yên Bái', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (550, N'Yên Lạc', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (551, N'Yersin', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (552, N'Yết Kiêu', 1, 6)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (553, N'Bùi Xương Trạch', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (554, N'Chính Kinh', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (555, N'Cù Chính Lan', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (556, N'Cự Lộc', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (557, N'Định Công', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (558, N'Giải Phóng', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (559, N'Giáp Nhất', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (560, N'Hạ Đình', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (561, N'Hoàng Đạo Thành', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (562, N'Hoàng Đạo Thúy', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (563, N'Hoàng Minh Giám', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (564, N'Hoàng Ngân', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (565, N'Hoàng Văn Thái', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (566, N'Khuất Duy Tiến', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (567, N'Khương Đình', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (568, N'Khương Hạ', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (569, N'Khương Trung', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (570, N'Kim Giang', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (571, N'Lê Trọng Tấn', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (572, N'Lê Văn Lương', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (573, N'Lê Văn Thiêm', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (574, N'Lương Thế Vinh', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (575, N'Ngụy Như Kon Tum', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (576, N'Nguyễn Huy Tưởng', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (577, N'Nguyễn Lân', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (578, N'Nguyễn Ngọc Nại', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (579, N'Nguyễn Quý Đức', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (580, N'Nguyễn Thị Định', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (581, N'Nguyễn Thị Thập', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (582, N'Nguyễn Trãi', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (583, N'Nguyễn Tuân', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (584, N'Nguyễn Văn Trỗi', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (585, N'Nguyễn Viết Xuân', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (586, N'Nguyễn Xiển', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (587, N'Nguyễn Xuân Linh', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (588, N'Nhân Hòa', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (589, N'Phan Đình Giót', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (590, N'Phương Liệt', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (591, N'Quan Nhân', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (592, N'Thượng Đình', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (593, N'Tố Hữu', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (594, N'Tô Vĩnh Diện', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (595, N'Trần Điền', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (596, N'Triều Khúc', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (597, N'Trịnh Đình Cửu', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (598, N'Trường Chinh', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (599, N'Vọng', 1, 7)
GO
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (600, N'Vũ Hữu', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (601, N'Vũ Tông Phan', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (602, N'Vũ Trọng Phụng', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (603, N'Vương Thừa Vũ', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (604, N'Nguyễn Du', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (605, N'Nguyễn Hiền', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (606, N'Nguyễn Huy Tự', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (607, N'Nguyễn Khoái', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (608, N'Nguyễn Quyền', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (609, N'Nguyễn Thượng Hiền', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (610, N'Nguyễn Trung Ngạn', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (611, N'Phạm Đình Hổ', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (612, N'Phù Đổng Thiên Vương', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (613, N'Phùng Khắc Khoan', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (614, N'Quang Trung', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (615, N'Quỳnh Lôi', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (616, N'Quỳnh Mai', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (617, N'Tạ Quang Bửu', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (618, N'Tam Trinh', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (619, N'Tăng Bạt Hổ', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (620, N'Tây Kết', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (621, N'Thái Phiên', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (622, N'Thanh Nhàn', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (623, N'Thể Giao', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (624, N'Thi Sách', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (625, N'Thiền Quang', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (626, N'Thịnh Yên', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (627, N'Thọ Lão', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (628, N'Tô Hiến Thành', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (629, N'Trần Bình Trọng', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (630, N'Trần Cao Vân', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (631, N'Trần Đại Nghĩa', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (632, N'Trần Hưng Đạo', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (633, N'Trần Khánh Dư', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (634, N'Trần Khát Chân', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (635, N'Trần Nhân Tông', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (636, N'Trần Thánh Tông', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (637, N'Trần Xuân Soạn', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (638, N'Triệu Việt Vương', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (639, N'Trương Định', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (640, N'Tuệ Tĩnh', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (641, N'Tương Mai', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (642, N'Vân Đồn', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (643, N'Vạn Kiếp', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (644, N'Vĩnh Tuy', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (645, N'Võ Thị Sáu', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (646, N'Vọng', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (647, N'Vũ Hữu Lợi', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (648, N'Yên Bái', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (649, N'Yên Lạc', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (650, N'Yersin', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (651, N'Yết Kiêu', 1, 7)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (652, N'Đường 19-5', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (653, N'Đường Biên Giang', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (654, N'Đường Chiến Thắng', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (655, N'Đường Đa Sĩ', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (656, N'Đường Dương Nội', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (657, N'Đường Lê Trọng Tấn', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (658, N'Đường Nguyễn Khuyến', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (659, N'Đường Nguyễn Trực', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (660, N'Đường Phúc La', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (661, N'Đường Phùng Hưng', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (662, N'Đường Quang Trung', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (663, N'Đường Trần Phú', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (664, N'Đường Vạn Phúc', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (665, N'Phố An Hòa', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (666, N'Phố Ao Sen', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (667, N'Phố Ba La', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (668, N'Phố Bà Triệu', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (669, N'Phố Bạch Thái Bưởi', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (670, N'Phố Bế Văn Đàn', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (671, N'Phố Bùi Bằng Đoàn', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (672, N'Phố Cao Thắng', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (673, N'Phố Cầu Am', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (674, N'Phố Cầu Đơ', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (675, N'Phố Chu Văn An', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (676, N'Phố Cù Chính Lan', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (677, N'Phố Đại An', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (678, N'Phố Đinh Tiên Hoàng', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (679, N'Phố Dương Lâm', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (680, N'Phố Hà Cầu', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (681, N'Phố Hoàng Diệu', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (682, N'Phố Hoàng Hoa Thám', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (683, N'Phố Hoàng Văn Thụ', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (684, N'Phố Huỳnh Thúc Kháng', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (685, N'Phố La Dương', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (686, N'Phố La Nội', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (687, N'Phố Lê Hồng Phong', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (688, N'Phố Lê Hữu Trác', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (689, N'Phố Lê Lai', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (690, N'Phố Lê Lợi', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (691, N'Phố Lê Quý Đôn', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (692, N'Phố Lụa', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (693, N'Phố Lương Ngọc Quyến', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (694, N'Phố Lương Văn Can', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (695, N'Phố Lý Thường Kiệt', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (696, N'Phố Lý Tự Trọng', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (697, N'Phố Mậu Lương', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (698, N'Phố Mộ Lao', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (699, N'Phố Ngô Đình Mẫn', 1, 8)
GO
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (700, N'Phố Ngô Gia Khảm', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (701, N'Phố Ngô Gia Tự', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (702, N'Phố Ngô Quyền', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (703, N'Phố Ngô Thì Nhậm', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (704, N'Phố Ngô Thì Sĩ', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (705, N'Phố Nguyễn Công Trứ', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (706, N'Phố Nguyễn Du', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (707, N'Phố Nguyễn Thái Học', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (708, N'Phố Nguyễn Thanh Bình', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (709, N'Phố Nguyễn Thị Minh Khai', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (710, N'Phố Nguyễn Thượng Hiền', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (711, N'Phố Nguyễn Trãi', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (712, N'Phố Nguyễn Văn Lộc', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (713, N'Phố Nguyễn Văn Trác', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (714, N'Phố Nguyễn Văn Trỗi', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (715, N'Phố Nguyễn Viết Xuân', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (716, N'Phố Nhuệ Giang', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (717, N'Phố Phan Bội Châu', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (718, N'Phố Phan Chu Trinh', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (719, N'Phố Phan Đình Giót', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (720, N'Phố Phan Đình Phùng', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (721, N'Phố Phan Huy Chú', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (722, N'Phố Phú Lương', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (723, N'Phố Tản Đà', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (724, N'Phố Tây Sơn', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (725, N'Phố Thanh Bình', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (726, N'Phố Thành Công', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (727, N'Phố Tiểu Công nghệ', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (728, N'Phố Tô Hiến Thành', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (729, N'Phố Tô Hiệu', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (730, N'Phố Tố Hữu', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (731, N'Phố Trần Đăng Ninh', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (732, N'Phố Trần Hưng Đạo', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (733, N'Phố Trần Nhật Duật', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (734, N'Phố Trần Văn Chuông', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (735, N'Phố Trưng Nhị', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (736, N'Phố Trưng Trắc', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (737, N'Phố Trương Công Định', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (738, N'Phố Văn Khê', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (739, N'Phố Văn La', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (740, N'Phố Văn Phú', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (741, N'Phố Văn Quán', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (742, N'Phố Văn Yên', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (743, N'Phố Võ Thị Sáu', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (744, N'Phố Vũ Trọng Khánh', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (745, N'Phố Vũ Văn Cẩn', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (746, N'Phố Xa La', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (747, N'Phố Xốm', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (748, N'Phố Ỷ La', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (749, N'Phố Yên Bình', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (750, N'Phố Yên Lộ', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (751, N'Phố Yên Phúc', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (752, N'Phố Yết Kiêu', 1, 8)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (753, N'Đường Bằng A', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (754, N'Đường Bằng B', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (755, N'Đường Bằng Liệt', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (756, N'Đường Bờ Sông Sét', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (757, N'Đường Bùi Huy Bích', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (758, N'Đường Bùi Xương Trạch', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (759, N'Đường Cầu Bươu', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (760, N'Đường Cầu Lủ', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (761, N'Đường Cầu Thanh Trì', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (762, N'Đường Cầu Tiên', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (763, N'Đường Đại Kim', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (764, N'Đường Đại lộ Chu Văn An', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (765, N'Đường Đại Từ', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (766, N'Đường Đặng Xuân Bảng', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (767, N'Đường Đền Lừ', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (768, N'Đường Đền Lừ 1', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (769, N'Đường Đền Lừ 2', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (770, N'Đường Đền Lừ 3', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (771, N'Đường Định Công', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (772, N'Đường Định Công Hạ', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (773, N'Đường Định Công Thượng', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (774, N'Đường Đinh Tiên Hoàng', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (775, N'Đường Đoàn Kết', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (776, N'Đường Đội Cấn', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (777, N'Đường Đồng Mơ', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (778, N'Đường Đông Thiên', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (779, N'Đường Giải Phóng', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (780, N'Đường Giáp Bát', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (781, N'Đường Giáp Nhị', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (782, N'Đường Giáp Tứ', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (783, N'Đường Họa Mi', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (784, N'Đường Hoa Thược Dược', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (785, N'Đường Hoàng Đạo Thành', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (786, N'Đường Hoàng Liệt', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (787, N'Đường Hoàng Mai', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (788, N'Đường Hồng Quang', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (789, N'Đường Khuyến Lương', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (790, N'Đường Kim Đồng', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (791, N'Đường Kim Giang', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (792, N'Đường Kim Ngưu', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (793, N'Đường Lê Văn Phấn', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (794, N'Đường Linh Đàm', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (795, N'Đường Linh Đường', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (796, N'Đường Lĩnh Nam', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (797, N'Đường Lương Khánh Thiện', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (798, N'Đường Mai Động', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (799, N'Đường Mai Khai', 1, 9)
GO
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (800, N'Đường Minh Khai', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (801, N'Đường Nam Dư', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (802, N'Đường Nam Dư Thượng', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (803, N'Đường Nghiêm Xuân Yêm', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (804, N'Đường Ngọc Hồi', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (805, N'Đường Ngũ Nhạc', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (806, N'Đường Nguyễn An Ninh', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (807, N'Đường Nguyễn Cảnh Dị', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (808, N'Đường Nguyễn Chí Thanh', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (809, N'Đường Nguyễn Chính', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (810, N'Đường Nguyễn Công Thái', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (811, N'Đường Nguyễn Công Trứ', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (812, N'Đường Nguyễn Đức Cảnh', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (813, N'Đường Nguyễn Duy Trinh', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (814, N'Đường Nguyễn Hữu Thọ', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (815, N'Đường Nguyễn Khoái', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (816, N'Đường Nguyễn Ngọc Nại', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (817, N'Đường Nguyễn Tam Trinh', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (818, N'Đường Nguyễn Trãi', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (819, N'Đường Nguyễn Văn Trỗi', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (820, N'Đường Nguyễn Xiển', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (821, N'Đường Phan Đình Giót', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (822, N'Đường Pháp Vân', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (823, N'Đường Sen Ngoại', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (824, N'Đường Tam Trinh', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (825, N'Đường Tân Khai', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (826, N'Đường Tân Mai', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (827, N'Đường Tây Trà', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (828, N'Đường Thanh Lân', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (829, N'Đường Thanh Liệt', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (830, N'Đường Thanh Ngân', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (831, N'Đường Thịnh Liệt', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (832, N'Đường Thúy Lĩnh', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (833, N'Đường Trần Hòa', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (834, N'Đường Trần Nguyên Đán', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (835, N'Đường Trần Thủ Độ', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (836, N'Đường Trịnh Đình Cửu', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (837, N'Đường Trương Định', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (838, N'Đường Tứ Kỳ', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (839, N'Đường Tựu Liệt', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (840, N'Đường Vành Đai 2,5', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (841, N'Đường Vành Đai 3', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (842, N'Đường Vĩnh Hoàng', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (843, N'Đường Vĩnh Hưng', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (844, N'Đường Vĩnh Tuy', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (845, N'Đường Yên Lương', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (846, N'Đường Yên Sở', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (847, N'Phố Cầu Gỗ', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (848, N'Phố Đại Đồng', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (849, N'Phố Giáp Lục', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (850, N'Phố Hưng Phúc', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (851, N'Phố Lê Trọng Tấn', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (852, N'Phố Sở Thượng', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (853, N'Phố Thanh Đàm', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (854, N'Phố Trần Đại Nghĩa', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (855, N'Phố Trần Điền', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (856, N'Phố Tương Mai', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (857, N'Phố Vũ Tông Phan', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (858, N'Phố Yên Duyên', 1, 9)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (859, N'Đường 32', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (860, N'Đường 41', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (861, N'Đường 70', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (862, N'Đường 70A', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (863, N'Đường An Hải 4', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (864, N'Đường B6', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (865, N'Đường Bình Hòa 7', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (866, N'Đường Bùi Xuân Phái', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (867, N'Đường C18', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (868, N'Đường Cao Xuân Huy', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (869, N'Đường Cầu Diễn', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (870, N'đường Cầu Thăng Long', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (871, N'Đường Châu Văn Liêm', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (872, N'Đường CN1', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (873, N'Đường CN2', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (874, N'Đường CN3', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (875, N'Đường CN4', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (876, N'Đường CN5', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (877, N'Đường CN6', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (878, N'Đường CN7', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (879, N'Đường CN8', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (880, N'Đường CN9', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (881, N'Đường Cương Kiên', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (882, N'Đường Đại Linh', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (883, N'Đường Đại lộ Thăng Long', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (884, N'Đường Đại Mỗ', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (885, N'Đường Đê Đông Ngạc', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (886, N'Đường Đình Quán', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (887, N'Đường Đình Thôn', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (888, N'Đường Đỗ Đình Thiện', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (889, N'Đường Đỗ Đức Dục', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (890, N'Đường Do Nha', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (891, N'Đường Đỗ Xuân Hợp', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (892, N'Đường Đồng Bát', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (893, N'Đường Đồng Me', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (894, N'Đường Dương Khuê', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (895, N'Đường Giao Quang', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (896, N'Đường Hà Bá Tường', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (897, N'Đường Hàm Nghi', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (898, N'Đường Hồ Mễ Trì', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (899, N'Đường Hồ Tùng Mậu', 1, 10)
GO
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (900, N'Đường Hoài Thanh', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (901, N'Đường Hoàng Công Chất', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (902, N'Đường Hoàng Lan', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (903, N'Đường Hoàng Lan 1', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (904, N'Đường Hoàng Lan 2', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (905, N'Đường Hoàng Lan 3', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (906, N'Đường Hoàng Lan 6', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (907, N'Đường Hoàng Trọng Mậu', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (908, N'Đường Hòe Thị', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (909, N'Đường Hữu Hưng', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (910, N'Đường K1', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (911, N'Đường K1B', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (912, N'Đường K2', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (913, N'Đường K3', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (914, N'Đường K32', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (915, N'Đường K4', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (916, N'Đường Khuất Duy Tiến', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (917, N'Đường Láng Hòa Lạc', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (918, N'Đường Lê Đức Thọ', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (919, N'Đường Lê Duy Hiến', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (920, N'Đường Lê Quang Đạo', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (921, N'Đường Lê Văn Lương', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (922, N'Đường Liên Cơ', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (923, N'Đường Lương Thế Vinh', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (924, N'Đường Lưu Hữu Phước', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (925, N'Đường Lưu Úc', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (926, N'đường Mễ Trì', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (927, N'Đường Mễ Trì Hạ', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (928, N'Đường Mễ Trì Thượng', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (929, N'Đường Miếu Đầm', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (930, N'Đường Miêu Nha', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (931, N'Đường Mộc Lan', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (932, N'Đường Mộc Lan 1', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (933, N'Đường Mộc Lan 10', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (934, N'Đường Mộc Lan 12', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (935, N'Đường Mộc Lan 2', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (936, N'Đường Mộc Lan 22', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (937, N'Đường Mộc Lan 2-3', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (938, N'Đường Mộc Lan 2-8', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (939, N'Đường Mộc Lan 3', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (940, N'Đường Mộc Lan 3-1', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (941, N'Đường Mộc Lan 3-8', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (942, N'Đường Mộc Lan 4', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (943, N'Đường Mộc Lan 4-5', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (944, N'Đường Mộc Lan 5', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (945, N'Đường Mộc Lan 6', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (946, N'Đường Mộc Lan 6-30', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (947, N'Đường Mộc Lan 6-6', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (948, N'Đường Mộc Lan 9', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (949, N'đường Mỹ Đình', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (950, N'Đường Mỹ Đình 1', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (951, N'Đường Ngân Hàng', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (952, N'Đường Ngọa Long', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (953, N'Đường Ngọc Đại', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (954, N'Đường Ngọc Mạch', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (955, N'Đường Ngọc Trục', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (956, N'Đường Nguyễn Cảnh Hợp', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (957, N'Đường Nguyễn Cơ Thạch', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (958, N'Đường Nguyễn Hoàng', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (959, N'Đường Nguyễn Phong Sắc', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (960, N'Đường Nguyễn Trãi', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (961, N'Đường Nguyễn Văn Giáp', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (962, N'Đường Nguyễn Văn Xơ', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (963, N'Đường Nguyễn Xuân Nguyên', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (964, N'Đường Nhân Mỹ', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (965, N'Đường Nhổn', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (966, N'Đường Nhuệ Giang', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (967, N'Đường Nông Lâm', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (968, N'Đường Phạm Hùng', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (969, N'Đường Phạm Văn Đồng', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (970, N'Đường Phú Đô', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (971, N'Đường Phú Mỹ', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (972, N'Đường Phú Thứ', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (973, N'Đường Phúc Diễn', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (974, N'Đường Phùng Khoang', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (975, N'Đường Phùng Khoang 2', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (976, N'đường Phương Canh', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (977, N'Đường Quang Tiến', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (978, N'Đường Quốc lộ 32', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (979, N'Đường Quốc lộ 70', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (980, N'Đường Sa Đôi', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (981, N'Đường Tân Mỹ', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (982, N'Đường Tân Xuân', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (983, N'Đường Tăng Thiết Giáp', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (984, N'đường Tây Mỗ', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (985, N'Đường Thạch Thảo', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (986, N'Đường Thạch Thảo 1', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (987, N'Đường Thạch Thảo 12', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (988, N'Đường Thạch Thảo 2', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (989, N'Đường Thạch Thảo 3', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (990, N'Đường Thạch Thảo 4', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (991, N'Đường Thạch Thảo 5', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (992, N'Đường Thạch Thảo 5-6', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (993, N'Đường Thạch Thảo 6', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (994, N'Đường Thanh Bình', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (995, N'Đường Thanh Lâm', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (996, N'Đường Thị Cấm', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (997, N'Đường Thiên Hiền', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (998, N'Đường Thiện Thanh', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (999, N'Đường Tỉnh Lộ 70', 1, 10)
GO
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1000, N'Đường Tỉnh lộ 72', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1001, N'Đường Tô Hiệu', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1002, N'Đường Tố Hữu', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1003, N'Đường Tôn Thất Thuyết', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1004, N'Đường Trần Hữu Dực', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1005, N'Đường Trần Quang Đạo', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1006, N'Đường Trần Tấn Mới', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1007, N'Đường Trần Văn Cẩn', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1008, N'Đường Trần Văn Lai', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1009, N'Đường Trịnh Văn Bô', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1010, N'đường Trung Văn', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1011, N'Đường TT2', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1012, N'Đường TT3', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1013, N'Đường Tu Hoàng', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1014, N'Đường Văn Tiến Dũng', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1015, N'Đường Vũ Quỳnh', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1016, N'Đường Vườn Cam', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1017, N'Đường Xóm Chùa', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1018, N'Đường Xóm Dộc', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1019, N'Đường Xuân Canh', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1020, N'Đường Xuân La', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1021, N'Đường Xuân Phương', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1022, N'đường Yên Hòa Đại Mỗ', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1023, N'Phố Cầu Cốc', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1024, N'Phố Hỏa Lò', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1025, N'Phố Nguyễn Đổng Chi', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1026, N'Phố Phú Hà', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1027, N'Phố Trần Bình', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1028, N'Phố Trần Vỹ', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1029, N'Phố Vũ Hữu', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1030, N'Phố Vũ Hữu Lợi', 1, 10)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1031, N'Đường 18A', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1032, N'Đường 19/5', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1033, N'Đường 295', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1034, N'Đường 295B', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1035, N'Đường 298', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1036, N'Đường 299', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1037, N'Đường 398', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1038, N'Đường Á Lữ', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1039, N'Đường A Lữ Phương', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1040, N'Đường Ấp Phan', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1041, N'Đường Bàng Bá Lân', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1042, N'Đường Bảo Ngọc', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1043, N'Đường Cả Trọng', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1044, N'Đường Cao Kỳ Vân', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1045, N'Đường Châu Xuyên', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1046, N'Đường Chi Ly 1', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1047, N'Đường Chu Danh Tể', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1048, N'Đường Cung Nhượng 2', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1049, N'Đường Đào Sư Tích', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1050, N'Đường Đào Tấn', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1051, N'Đường Doãn Dịch', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1052, N'Đường Đoàn Kết', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1053, N'Đường Đồng Cửa', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1054, N'Đường Đồng Cửa 2', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1055, N'Đường Giáp Hải', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1056, N'Đường Giáp Lễ', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1057, N'Đường Giáp Nguột', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1058, N'Đường Giáp Văn Phúc', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1059, N'Đường Hồ Công Dự', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1060, N'Đường Hòa Sơn', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1061, N'Đường Hòa Yên', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1062, N'Đường Hoàng Hoa Thám', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1063, N'Đường Hoàng Quốc Việt', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1064, N'Đường Hoàng Văn Thụ', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1065, N'Đường Hùng Vương', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1066, N'Đường Huyền Quang', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1067, N'Đường Huỳnh Thúc Kháng', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1068, N'Đường Lê An', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1069, N'Đường Lê Hồng Phong', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1070, N'Đường Lê Lai', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1071, N'Đường Lê Lợi', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1072, N'Đường Lê Lý 2', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1073, N'Đường Lê Triện', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1074, N'Đường Lều Văn Minh', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1075, N'Đường Lương Văn Can', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1076, N'Đường Lương Văn Năm', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1077, N'Đường Lưu Nhân Trú', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1078, N'Đường Lý Thái Tổ', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1079, N'Đường Lý Tử Tấn', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1080, N'Đường Lý Tự Trọng', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1081, N'Đường Minh Khai', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1082, N'Đường Nam Dĩnh Kế', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1083, N'Đường Nghĩa Long', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1084, N'Đường Ngô Gia Tự', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1085, N'Đường Ngô Trang', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1086, N'Đường Ngô Văn Cảnh', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1087, N'Đường Nguyễn Cao', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1088, N'Đường Nguyễn Chí Thanh', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1089, N'Đường Nguyễn Công Hãng', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1090, N'Đường Nguyễn Công Trứ', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1091, N'Đường Nguyễn Đình Tuân', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1092, N'Đường Nguyễn Doãn Địch', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1093, N'Đường Nguyễn Du', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1094, N'Đường Nguyễn Duy Năng', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1095, N'Đường Nguyên Hồng', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1096, N'Đường Nguyễn Khuyến', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1097, N'Đường Nguyễn Thị Lưu', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1098, N'Đường Nguyễn Thị Lựu', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1099, N'Đường Nguyễn Thị Lưu 1', 1, 11)
GO
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1100, N'Đường Nguyễn Thị Lưu 2', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1101, N'Đường Nguyễn Thị Minh Khai', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1102, N'Đường Nguyễn Văn Cừ', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1103, N'Đường Nguyễn Văn Mẫn', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1104, N'Đường Ninh Văn Phan', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1105, N'Đường Phạm Liêu', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1106, N'Đường Phồn Xương', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1107, N'Đường Phùng Trạm', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1108, N'Đường Quách Nhẫn', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1109, N'Đường Quang Trung', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1110, N'Đường Quốc lộ 1A', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1111, N'Đường Quốc lộ 31', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1112, N'Đường Quốc lộ 37', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1113, N'Đường Số 398', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1114, N'Đường Song Khê', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1115, N'Đường Tân Ninh', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1116, N'Đường Tây Yên Tử', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1117, N'Đường Thái Đào', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1118, N'Đường Thân Cảnh Phúc', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1119, N'Đường Thân Cảnh Văn', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1120, N'Đường Thân Công Tài', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1121, N'Đường Thân Đức Luận', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1122, N'Đường Thân Khuê', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1123, N'Đường Thân Nhân Tín', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1124, N'Đường Thân Nhân Trung', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1125, N'Đường Thanh Niên', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1126, N'Đường Thánh Thiên', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1127, N'Đường Tiền Giang', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1128, N'Đường Tỉnh lộ 284', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1129, N'Đường Tỉnh Lộ 295B', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1130, N'Đường Tỉnh lộ 299', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1131, N'Đường Tố Hữu', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1132, N'Đường Trần Bình Trọng', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1133, N'Đường Trần Đăng Tuyển', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1134, N'Đường Trần Khát Chân', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1135, N'Đường Trần Quang Khải', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1136, N'Đường Trần Quốc Toản', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1137, N'Đường Vi Đức Lục', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1138, N'Đường Vi Đức Thắng', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1139, N'Đường Võ Nguyên Giáp', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1140, N'Đường Võ Thị Sáu', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1141, N'Đường Vương Văn Trà', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1142, N'Đường Xương Giang', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1143, N'Đường Yết Kiêu', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1144, N'Phố Cốc', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1145, N'Phố Cung Nhượng 1', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1146, N'Phố Lê Lý', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1147, N'Phố Mỹ Độ', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1148, N'Phố Ngô Xá', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1149, N'Phố Nguyễn Khắc Nhu', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1150, N'Phố Trần Nguyên Hãn', 1, 11)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1151, N'Đường 19/5', 1, 12)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1152, N'Đường 295', 1, 12)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1153, N'Đường 296', 1, 12)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1154, N'Đường 675', 1, 12)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1155, N'Đường Bắc Lý', 1, 12)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1156, N'Đường Hoàng Hoa Thám', 1, 12)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1157, N'Đường Hoàng Lương', 1, 12)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1158, N'Đường Hoàng Văn Thái', 1, 12)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1159, N'Đường Ngô Gia Tự', 1, 12)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1160, N'Đường Nguyễn Du', 1, 12)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1161, N'Đường Quốc Lộ 296', 1, 12)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1162, N'Đường Quốc lộ 37', 1, 12)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1163, N'Đường Số 3', 1, 12)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1164, N'Đường Tỉnh lộ 276', 1, 12)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1165, N'Đường Tỉnh Lộ 288', 1, 12)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1166, N'Đường Tỉnh lộ 295', 1, 12)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1167, N'Đường Tỉnh lộ 296', 1, 12)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1168, N'Đường Trường Chinh', 1, 12)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1169, N'Đường Tuệ Tĩnh', 1, 12)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1170, N'Đường Vành Đai 1', 1, 12)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1171, N'Phố 37', 1, 12)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1172, N'Đường 284', 1, 13)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1173, N'Đường 398', 1, 13)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1174, N'Đường Cao Thượng', 1, 13)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1175, N'Đường ĐT 284', 1, 13)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1176, N'Đường Hoàng Hoa Thám', 1, 13)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1177, N'Đường Hoàng Quốc Việt', 1, 13)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1178, N'Đường Kim Tràng', 1, 13)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1179, N'Đường Quốc Lộ 17', 1, 13)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1180, N'Đường Tân Quang', 1, 13)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1181, N'Đường Tỉnh lộ 284', 1, 13)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1182, N'Đường Tỉnh lộ 287', 1, 13)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1183, N'Đường Tỉnh Lộ 294', 1, 13)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1184, N'Đường Tỉnh lộ 295', 1, 13)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1185, N'Đường 265', 1, 20)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1186, N'Đường Cầu Gồ', 1, 20)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1187, N'Đường Hoa Bình', 1, 20)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1188, N'Đường Hoàng Hoa Thám', 1, 20)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1189, N'Đường Tỉnh lộ 242', 1, 20)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1190, N'Đường Tỉnh lộ 265', 1, 20)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1191, N'Đường Tỉnh lộ 287', 1, 20)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1192, N'Đường Tỉnh Lộ 294', 1, 20)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1193, N'Đường Tỉnh lộ 295', 1, 20)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1194, N'Đường 284', 1, 19)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1195, N'Đường 293', 1, 19)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1196, N'Đường 398', 1, 19)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1197, N'Đường Đào Sư Tích', 1, 19)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1198, N'Đường Hoàng Hoa Thám', 1, 19)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1199, N'Đường Làn 2', 1, 19)
GO
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1200, N'Đường Lê Đức Trung', 1, 19)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1201, N'Đường Lưu Viết Thoảng', 1, 19)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1202, N'Đường Ngô Uông', 1, 19)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1203, N'Đường Nguyễn Viết Chất', 1, 19)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1204, N'Đường Phạm Túc Minh', 1, 19)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1205, N'Phố Ba Tổng', 1, 19)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1206, N'Đường 11', 1, 18)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1207, N'Đường 295B', 1, 18)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1208, N'Đường 298', 1, 18)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1209, N'Đường 34', 1, 18)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1210, N'Đường ĐT 298', 1, 18)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1211, N'Đường N', 1, 18)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1212, N'Đường N16', 1, 18)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1213, N'Đường N7', 1, 18)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1214, N'Đường Nguyễn Thế Nho', 1, 18)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1215, N'Đường Nguyễn Văn Ty', 1, 18)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1216, N'Đường Như Thiết', 1, 18)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1217, N'Đường Quốc lộ 1A', 1, 18)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1218, N'Đường Quốc lộ 34', 1, 18)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1219, N'Đường Quốc lộ 37', 1, 18)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1220, N'Đường Tam Tầng', 1, 18)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1221, N'Đường Thân Nhân Tín', 1, 18)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1222, N'Đường Thân Nhân Trung', 1, 18)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1223, N'Đường Tỉnh lộ 284', 1, 18)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1224, N'Đường Tỉnh Lộ 295B', 1, 18)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1225, N'Đường Tỉnh Lộ 298', 1, 18)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1226, N'Phố 37', 1, 18)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1227, N'Phố Tràng', 1, 18)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1228, N'Đường 291', 1, 17)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1229, N'Đường 31', 1, 17)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1230, N'Đường Quốc lộ 279', 1, 17)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1231, N'Đường Quốc lộ 31', 1, 17)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1232, N'Đường Tỉnh lộ 293', 1, 17)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1233, N'Đường Tỉnh lộ 330', 1, 17)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1234, N'Đường 31', 1, 16)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1235, N'Đường Kim', 1, 16)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1236, N'Đường Lê Hồng Phong', 1, 16)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1237, N'Đường Quốc lộ 279', 1, 16)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1238, N'Đường Quốc lộ 31', 1, 16)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1239, N'Đường Tỉnh lộ 285', 1, 16)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1240, N'Đường 31', 1, 15)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1241, N'Đường Bình Minh', 1, 15)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1242, N'Đường Quốc lộ 31', 1, 15)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1243, N'Đường Quốc lộ 37', 1, 15)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1244, N'Đường Thanh Hưng', 1, 15)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1245, N'Đường Thanh Niên', 1, 15)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1246, N'Đường Tỉnh lộ 293', 1, 15)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1247, N'Phố 37', 1, 15)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1248, N'Đường 31', 1, 14)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1249, N'Đường Đại Lâm', 1, 14)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1250, N'Đường Mải Hạ', 1, 14)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1251, N'Đường Nguyễn Xuân Lan', 1, 14)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1252, N'Đường Phạm Văn Liêu', 1, 14)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1253, N'Đường Quốc lộ 1A', 1, 14)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1254, N'Đường Quốc lộ 31', 1, 14)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1255, N'Đường Tỉnh lộ 265', 1, 14)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1256, N'Đường Tỉnh Lộ 292', 1, 14)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1257, N'Đường Tỉnh lộ 295', 1, 14)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1258, N'Đường Vôi', 1, 14)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1259, N'Đường Yên Vinh', 1, 14)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1260, N'Đường 282', 1, 34)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1261, N'Đường 38', 1, 34)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1262, N'Đường Âu Cơ', 1, 34)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1263, N'Đường Công Hà', 1, 34)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1264, N'Đường Đại Bái', 1, 34)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1265, N'Đường Đông Cốc', 1, 34)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1266, N'Đường Đông Côi', 1, 34)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1267, N'Đường Luy Lâu', 1, 34)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1268, N'Đường Mai Bang', 1, 34)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1269, N'Đường Nghĩa xá', 1, 34)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1270, N'Đường Nguyễn Chí Tố', 1, 34)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1271, N'Đường Nguyễn Cư Đạo', 1, 34)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1272, N'Đường Nguyễn Du', 1, 34)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1273, N'Đường Quốc Lộ 17', 1, 34)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1274, N'Đường Quốc lộ 18', 1, 34)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1275, N'Đường Quốc lộ 281', 1, 34)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1276, N'Đường Quốc Lộ 282', 1, 34)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1277, N'Đường Quốc Lộ 38', 1, 34)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1278, N'Đường Quốc lộ 38B', 1, 34)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1279, N'Đường Tam Á', 1, 34)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1280, N'Đường Thuận Thành 3', 1, 34)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1281, N'Đường Thuận Thành 5', 1, 34)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1282, N'Đường Tỉnh lộ 280', 1, 34)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1283, N'Đường Tỉnh lộ 282', 1, 34)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1284, N'Đường Tỉnh lộ 282B', 1, 34)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1285, N'Đường Tỉnh Lộ 283', 1, 34)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1286, N'Đường Vương Văn Trà', 1, 34)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1287, N'Phố Khám', 1, 34)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1288, N'Phố Phố Keo', 1, 34)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1289, N'Phố Thanh Hoài', 1, 34)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1290, N'Đường 280', 1, 31)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1291, N'Đường 282', 1, 31)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1292, N'Đường 284', 1, 31)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1293, N'Đường ĐT 280', 1, 31)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1294, N'Đường Phú Dư', 1, 31)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1295, N'Đường Quốc Lộ 17', 1, 31)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1296, N'Đường Quốc lộ 228', 1, 31)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1297, N'Đường Thiên Thai', 1, 31)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1298, N'Đường Tỉnh lộ 280', 1, 31)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1299, N'Đường Tỉnh lộ 282', 1, 31)
GO
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1300, N'Đường Tỉnh Lộ B2', 1, 31)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1301, N'Đường Vũ Tuyên Hoàng', 1, 31)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1302, N'Đường 280', 1, 32)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1303, N'Đường 281', 1, 32)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1304, N'Đường 284', 1, 32)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1305, N'Đường 285', 1, 32)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1306, N'Đường Cổ Lãm', 1, 32)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1307, N'Đường Đồng Khởi', 1, 32)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1308, N'Đường ĐT 280', 1, 32)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1309, N'Đường Giáp Văn Cương', 1, 32)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1310, N'Đường Kim Đào', 1, 32)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1311, N'Đường Lương Tài', 1, 32)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1312, N'Đường Phương Xá', 1, 32)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1313, N'Đường Tỉnh lộ 280', 1, 32)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1314, N'Đường Tỉnh lộ 281', 1, 32)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1315, N'Đường Tỉnh Lộ 283', 1, 32)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1316, N'Đường Tỉnh lộ 284', 1, 32)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1317, N'Đường Tỉnh lộ 285', 1, 32)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1318, N'Đường 285', 1, 35)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1319, N'Đường 295', 1, 35)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1320, N'Đường 295B', 1, 35)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1321, N'Đường 38', 1, 35)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1322, N'Đường 38B', 1, 35)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1323, N'Đường Đại Thượng', 1, 35)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1324, N'Đường Đại Vi', 1, 35)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1325, N'Đường ĐT 287', 1, 35)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1326, N'Đường Dương Húc', 1, 35)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1327, N'Đường Hoài Trung', 1, 35)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1328, N'Đường Hoàn Sơn', 1, 35)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1329, N'Đường Khẩu Đông', 1, 35)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1330, N'Đường Lạc Vệ', 1, 35)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1331, N'Đường P', 1, 35)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1332, N'Đường Quốc lộ 1A', 1, 35)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1333, N'Đường Quốc Lộ 38', 1, 35)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1334, N'Đường Tỉnh lộ 270', 1, 35)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1335, N'Đường Tỉnh lộ 276', 1, 35)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1336, N'Đường Tỉnh lộ 287', 1, 35)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1337, N'Đường Tỉnh lộ 295', 1, 35)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1338, N'Đường TS11', 1, 35)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1339, N'Đường TS3', 1, 35)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1340, N'Phố Lý Thường Kiệt', 1, 35)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1341, N'Phố Ngô Xá', 1, 35)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1342, N'Đường 16', 1, 37)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1343, N'Đường 18', 1, 37)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1344, N'Đường 286', 1, 37)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1345, N'Đường 295', 1, 37)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1346, N'Đường Ấp Đồn', 1, 37)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1347, N'Đường ĐT 286', 1, 37)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1348, N'Đường ĐT 295', 1, 37)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1349, N'Đường Phong Nẫm', 1, 37)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1350, N'Đường Phú Mẫn', 1, 37)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1351, N'Đường Quốc lộ 16', 1, 37)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1352, N'Đường Quốc lộ 18', 1, 37)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1353, N'Đường Số 277', 1, 37)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1354, N'Đường Tỉnh lộ 271', 1, 37)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1355, N'Đường Tỉnh Lộ 277', 1, 37)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1356, N'Đường Tỉnh lộ 286', 1, 37)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1357, N'Đường Tỉnh lộ 295', 1, 37)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1358, N'Đường 18', 1, 33)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1359, N'Đường 22', 1, 33)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1360, N'Đường 279', 1, 33)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1361, N'Đường 291', 1, 33)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1362, N'Đường 42', 1, 33)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1363, N'Đường Bằng An', 1, 33)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1364, N'Đường Giang Liễu', 1, 33)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1365, N'Đường Hà Liễu', 1, 33)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1366, N'Đường Lạc Vệ', 1, 33)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1367, N'Đường Lãm Làng', 1, 33)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1368, N'Đường Mai Cương', 1, 33)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1369, N'Đường Mao Dộc', 1, 33)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1370, N'Đường Mao Trung', 1, 33)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1371, N'Đường Mao Yên', 1, 33)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1372, N'Đường Phương Cầu', 1, 33)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1373, N'Đường Quốc lộ 18', 1, 33)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1374, N'Đường Tỉnh Lộ 279', 1, 33)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1375, N'Đường Tỉnh lộ 291', 1, 33)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1376, N'Phố 24', 1, 33)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1377, N'Phố 36', 1, 33)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1378, N'Đường 15', 1, 36)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1379, N'Đường 179', 1, 36)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1380, N'Đường 1A', 1, 36)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1381, N'Đường 25', 1, 36)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1382, N'Đường 271', 1, 36)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1383, N'Đường 277', 1, 36)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1384, N'Đường 295', 1, 36)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1385, N'Đường 295B', 1, 36)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1386, N'Đường 6', 1, 36)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1387, N'Đường B2', 1, 36)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1388, N'Đường Đa Hội', 1, 36)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1389, N'Đường Đại Đình', 1, 36)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1390, N'Đường Đồng Kỵ 1', 1, 36)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1391, N'Đường ĐT 179', 1, 36)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1392, N'Đường ĐT 271', 1, 36)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1393, N'Đường HN2', 1, 36)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1394, N'Đường Hoàng Quốc Việt', 1, 36)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1395, N'Đường Hữu Nghị', 1, 36)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1396, N'Đường Lê Hồng Phong', 1, 36)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1397, N'Đường Lê Quang Đạo', 1, 36)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1398, N'Đường Lê Thái Tổ', 1, 36)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1399, N'Đường Long Vĩ', 1, 36)
GO
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1400, N'Đường Lý Chiêu Hoàng', 1, 36)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1401, N'Đường Lý Đạo Thành', 1, 36)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1402, N'Đường Lý Khánh Văn', 1, 36)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1403, N'Đường Lý Thái Tổ', 1, 36)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1404, N'Đường Lý Thánh Tông', 1, 36)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1405, N'Đường Lý Tự Trọng', 1, 36)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1406, N'Đường Minh Khai', 1, 36)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1407, N'Đường Ngoại Thương', 1, 36)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1408, N'Đường Nguyễn Công Hãng', 1, 36)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1409, N'Đường Nguyễn Giáo', 1, 36)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1410, N'Đường Nguyên Phi Ỷ Lan', 1, 36)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1411, N'Đường Nguyễn Văn Cừ', 1, 36)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1412, N'Đường Nguyễn Văn Trỗi', 1, 36)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1413, N'Đường Quốc lộ 1A', 1, 36)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1414, N'Đường Số 5', 1, 36)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1415, N'Đường Số 6', 1, 36)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1416, N'Đường Tân Lập', 1, 36)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1417, N'Đường Tân Thành', 1, 36)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1418, N'Đường Thịnh Lang', 1, 36)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1419, N'Đường Tỉnh lộ 179', 1, 36)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1420, N'Đường Tỉnh lộ 217', 1, 36)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1421, N'Đường Tỉnh Lộ 277', 1, 36)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1422, N'Đường Tỉnh lộ 287', 1, 36)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1423, N'Đường Tỉnh lộ 295', 1, 36)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1424, N'Đường Tô Hiến Thành', 1, 36)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1425, N'Đường Trần Phú', 1, 36)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1426, N'Đường Viềng', 1, 36)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1427, N'Đường Yên Lã', 1, 36)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1428, N'Phố Bính Hạ', 1, 36)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1429, N'Phố Lý Thường Kiệt', 1, 36)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1430, N'Phố Phù Lưu', 1, 36)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1431, N'Phố Trang Liệt', 1, 36)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1432, N'Đường 18', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1433, N'Đường 1A', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1434, N'Đường 1B', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1435, N'Đường 22', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1436, N'Đường 286', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1437, N'Đường 29', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1438, N'Đường 38', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1439, N'Đường 53', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1440, N'Đường 6', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1441, N'Đường 715', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1442, N'Đường Âu Cơ', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1443, N'Đường Bà Chúa Kho', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1444, N'Đường Ba Huyện', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1445, N'Đường Bế Văn Đàn', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1446, N'Đường Bình Than', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1447, N'Đường Bò Sơn', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1448, N'Đường Bồ Sơn', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1449, N'Đường Bồ Sơn 1', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1450, N'Đường Bò Sơn 2', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1451, N'Đường Bồ Sơn 2', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1452, N'Đường Bò Sơn 3', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1453, N'Đường Bồ Sơn 3', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1454, N'Đường Bò Sơn 4', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1455, N'Đường Bồ Sơn 4', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1456, N'Đường Bùi Xuân Phái', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1457, N'Đường Cao Bá Quát', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1458, N'Đường Cao Lỗ Vương', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1459, N'Đường Chế Lan Viên', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1460, N'Đường Chu Mẫu', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1461, N'Đường Chu Văn An', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1462, N'Đường Cổ Mễ', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1463, N'Đường Cổng Tiền', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1464, N'Đường Cổng Tiền 2', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1465, N'Đường Cù Chính Lan', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1466, N'Đường D18', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1467, N'Đường Đa Cấu', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1468, N'Đường Đa Cấu 2', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1469, N'Đường Đại Phúc', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1470, N'Đường Đại Phúc - 33', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1471, N'Đường Đại Phúc 10', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1472, N'Đường Đại Phúc 14', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1473, N'Đường Đại Phúc 24', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1474, N'Đường Đại Phúc 29', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1475, N'Đường Đại Phúc 30', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1476, N'Đường Đại Phúc 7', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1477, N'Đường Đại Phúc 8', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1478, N'Đường Đại Phúc 9', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1479, N'Đường Đại Tráng', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1480, N'Đường Đăng Đạo', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1481, N'Đường Đạo Chân', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1482, N'Đường Đào Duy Từ', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1483, N'Đường Đấu Mã', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1484, N'Đường Đỗ Trọng Vỹ', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1485, N'Đường Dốc Pháo Thủ', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1486, N'Đường Đông Cốc', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1487, N'Đường Đồng Soi', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1488, N'Đường Dương Ổ', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1489, N'Đường Giang Văn Minh', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1490, N'Đường H', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1491, N'Đường H2', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1492, N'Đường Hà Thành', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1493, N'Đường Hai Vân', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1494, N'Đường Hàn Mặc Tử', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1495, N'Đường Hàn Thuyên', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1496, N'Đường Hàng Cây', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1497, N'Đường Hồ Đắc Di', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1498, N'Đường Hồ Ngọc Lân', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1499, N'Đường Hồ Ngọc Lân 1', 1, 30)
GO
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1500, N'Đường Hồ Ngọc Lân 2', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1501, N'Đường Hồ Ngọc Lân 3', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1502, N'Đường Hồ Ngọc Lân 4', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1503, N'Đường Hòa Đình', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1504, N'Đường Hòa Đình 2', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1505, N'Đường Hòa Long', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1506, N'Đường Hoài Thanh', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1507, N'Đường Hoàng Hoa Thám', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1508, N'Đường Hoàng Long', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1509, N'Đường Hoàng Ngân', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1510, N'Đường Hoàng Quốc Việt', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1511, N'Đường Hoàng Tích Chí', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1512, N'Đường Hoàng Văn Thái', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1513, N'Đường Hoàng Văn Thụ', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1514, N'Đường Huyền Quang', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1515, N'Đường Khả Lễ', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1516, N'Đường Khả Lễ 1', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1517, N'Đường Khả Lễ 2', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1518, N'Đường Khả Lễ 3', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1519, N'Đường Kim Đồng', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1520, N'Đường Kim Lân', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1521, N'Đường Kinh Bắc', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1522, N'Đường Kinh Bắc 1', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1523, N'Đường Kinh Bắc 16', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1524, N'Đường Kinh Bắc 17', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1525, N'Đường Kinh Bắc 20', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1526, N'Đường Kinh Bắc 27', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1527, N'Đường Kinh Bắc 28', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1528, N'Đường Kinh Bắc 3', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1529, N'Đường Kinh Bắc 35', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1530, N'Đường Kinh Bắc 37', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1531, N'Đường Kinh Bắc 42', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1532, N'Đường Kinh Bắc 44', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1533, N'Đường Kinh Bắc 5', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1534, N'Đường Kinh Bắc 54', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1535, N'Đường Kinh Bắc 55', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1536, N'Đường Kinh Bắc 56', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1537, N'Đường Kinh Bắc 57', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1538, N'Đường Kinh Bắc 58', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1539, N'Đường Kinh Bắc 62', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1540, N'Đường Kinh Bắc 74', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1541, N'Đường Kinh Bắc 76', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1542, N'Đường Kinh Bắc 8', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1543, N'Đường Kinh Bắc 86', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1544, N'Đường Kinh Bắc 87', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1545, N'Đường Kinh Dương Vương', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1546, N'Đường L1', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1547, N'Đường Lạc Long Quân', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1548, N'Đường Lãm Làng', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1549, N'Đường Lãm Trại', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1550, N'Đường Làng Khả Lễ', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1551, N'Đường Lê Chân', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1552, N'Đường Lê Đình Tấn', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1553, N'Đường Lê Đức Thọ', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1554, N'Đường Lê Hồng Phong', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1555, N'Đường Lê Lai', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1556, N'Đường Lê Quý Đôn', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1557, N'Đường Lê Thái Tổ', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1558, N'Đường Lê Thánh Tông', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1559, N'Đường Lê Văn Duyệt', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1560, N'Đường Lê Văn Thịnh', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1561, N'Đường Lương Ngọc Quyến', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1562, N'Đường Lương Thế Vinh', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1563, N'Đường Luy Lâu', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1564, N'Đường Lý Anh Tông', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1565, N'Đường Lý Cao Tông', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1566, N'Đường Lý Chiêu Hoàng', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1567, N'Đường Lý Đăng Đạo', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1568, N'Đường Lý Đạo Thành', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1569, N'Đường Lý Nhân Tông', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1570, N'Đường Lý Thái Tổ', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1571, N'Đường Lý Thái Tông', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1572, N'Đường Lý Thần Tông', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1573, N'Đường Lý Thánh Tông', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1574, N'Đường Lý Thế Tông', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1575, N'Đường Mạc Đĩnh Chi', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1576, N'Đường Mai Bang', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1577, N'Đường Minh Khai', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1578, N'Đường Ngô Đăng Sở', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1579, N'Đường Ngô Gia Khảm', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1580, N'Đường Ngô Gia Tự', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1581, N'Đường Ngô Miễn Thiệu', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1582, N'Đường Ngô Quyền', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1583, N'Đường Ngô Tất Tố', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1584, N'Đường Ngô Xuân Quảng', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1585, N'Đường Ngọc Hân Công Chúa', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1586, N'Đường Nguyễn Bình', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1587, N'Đường Nguyễn Bỉnh Quân', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1588, N'Đường Nguyễn Cao', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1589, N'Đường Nguyễn Chiêu Huấn', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1590, N'Đường Nguyễn Công Hãn', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1591, N'Đường Nguyễn Công Hãng', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1592, N'Đường Nguyễn Công Trứ', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1593, N'Đường Nguyễn Đăng', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1594, N'Đường Nguyễn Đăng Đạo', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1595, N'Đường Nguyễn Đình Chiểu', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1596, N'Đường Nguyễn Đình Khôi', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1597, N'Đường Nguyễn Du', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1598, N'Đường Nguyễn Đức Ánh', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1599, N'Đường Nguyễn Đức Cảnh', 1, 30)
GO
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1600, N'Đường Nguyễn Gia Thiều', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1601, N'Đường Nguyễn Giản Thanh', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1602, N'Đường Nguyễn Hoàng Nghị', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1603, N'Đường Nguyễn Hữu Huân', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1604, N'Đường Nguyễn Hữu Nghiêm', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1605, N'Đường Nguyễn Huy Tưởng', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1606, N'Đường Nguyễn Khuyến', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1607, N'Đường Nguyễn Nhân Kính', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1608, N'Đường Nguyễn Quang Ca', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1609, N'Đường Nguyễn Quyền', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1610, N'Đường Nguyễn Tảo', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1611, N'Đường Nguyễn Thái Học', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1612, N'Đường Nguyễn Thị Lựu', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1613, N'Đường Nguyễn Thị Minh Khai', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1614, N'Đường Nguyễn Trãi', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1615, N'Đường Nguyễn Tri Phương', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1616, N'Đường Nguyễn Trường Tộ', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1617, N'Đường Nguyễn Tuân', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1618, N'Đường Nguyễn Văn Côn', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1619, N'Đường Nguyễn Văn Cừ', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1620, N'Đường Nguyễn Văn Siêu', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1621, N'Đường Nguyễn Văn Trỗi', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1622, N'Đường Nguyễn Viết Xuân', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1623, N'Đường Nguyễn Xuân Chính', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1624, N'Đường Niềm Xá', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1625, N'Đường Phạm Ngũ Lão', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1626, N'Đường Phan Huy Chú', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1627, N'Đường Phố Vũ', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1628, N'Đường Phúc Sơn', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1629, N'Đường Phương Cầu', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1630, N'Đường Phượng Vỹ', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1631, N'Đường Phương Vỹ 2', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1632, N'Đường Quốc Lộ 1', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1633, N'Đường Quốc lộ 18', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1634, N'Đường Quốc lộ 1A', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1635, N'Đường Quốc lộ 1B', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1636, N'Đường Quốc Lộ 38', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1637, N'Đường Quỳnh Đôi', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1638, N'Đường Rạp Hát', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1639, N'Đường Sao Mai', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1640, N'Đường Số 1', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1641, N'Đường Sơn Trung', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1642, N'Đường Tản Đà', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1643, N'Đường Thái Bảo', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1644, N'Đường Thành Bắc', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1645, N'Đường Thành Cổ', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1646, N'Đường Thanh Niên', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1647, N'Đường Thanh Phương', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1648, N'Đường Thành Sơn', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1649, N'Đường Thị Trung', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1650, N'Đường Thiên Đức', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1651, N'Đường Thiện Đức', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1652, N'Đường Thịnh Trung', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1653, N'Đường Thụ Ninh', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1654, N'Đường Thượng Đồng', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1655, N'Đường Tiền An', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1656, N'Đường Tỉnh Lộ 278', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1657, N'Đường Tỉnh lộ 286', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1658, N'Đường Tỉnh lộ 291', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1659, N'Đường Tô Hiến Thành', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1660, N'Đường Tô Hiệu', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1661, N'Đường Tô Ngọc Vân', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1662, N'Đường Tôn Thất Tùng', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1663, N'Đường Trần Bình Trọng', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1664, N'Đường Trần Công', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1665, N'Đường Trần Hưng Đạo', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1666, N'Đường Trần Huy Liễu', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1667, N'Đường Trần Khánh Dư', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1668, N'Đường Trần Lưu', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1669, N'Đường Trần Lựu', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1670, N'Đường Trần Quốc Toản', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1671, N'Đường Triệu Việt Vương', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1672, N'Đường Trương Văn Lĩnh', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1673, N'Đường Văn Cao', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1674, N'Đường Vạn Hạnh', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1675, N'Đường Vân Trại', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1676, N'Đường Việt Trang', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1677, N'Đường Võ Cường', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1678, N'Đường Võ Cường 10', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1679, N'Đường Võ Cường 107', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1680, N'Đường Võ Cường 11', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1681, N'Đường Võ Cường 110', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1682, N'Đường Võ Cường 117', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1683, N'Đường Võ Cường 14', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1684, N'Đường Võ Cường 17', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1685, N'Đường Võ Cường 18', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1686, N'Đường Võ Cường 3', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1687, N'Đường Võ Cường 38', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1688, N'Đường Võ Cường 5', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1689, N'Đường Võ Cường 6', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1690, N'Đường Võ Cường 66', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1691, N'Đường Võ Cường 7', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1692, N'Đường Võ Cường 73', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1693, N'Đường Võ Cường 8', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1694, N'Đường Võ Cường 87', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1695, N'Đường Võ Cường 99', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1696, N'Đường Võ Thị Sáu', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1697, N'Đường Vũ Đại Phúc', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1698, N'Đường Vũ Diệu', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1699, N'Đường Vũ Giới', 1, 30)
GO
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1700, N'Đường Vũ Kiệt', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1701, N'Đường Vũ Ninh', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1702, N'Đường Vũ Ninh 33', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1703, N'Đường Vương Văn Trà', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1704, N'Đường Xuân Ái', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1705, N'Đường Xuân Diệu', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1706, N'Đường Xuân Ổ', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1707, N'Đường Xuân Ổ A', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1708, N'Đường Xuân Ổ B', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1709, N'Đường Xuân Viên', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1710, N'Đường Y Na', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1711, N'Đường Yna', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1712, N'Phố 24', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1713, N'Phố 36', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1714, N'Phố Đặng Trần Côn', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1715, N'Phố Hai Bà Trưng', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1716, N'Phố Hàng Mã', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1717, N'Phố Hoàng Đạo Thúy', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1718, N'Phố Hoàng Tích Chù', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1719, N'Phố Lê Phụng Hiểu', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1720, N'Phố Lê Thanh Nghị', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1721, N'Phố Lê Trọng Tấn', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1722, N'Phố Lê Văn Hưu', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1723, N'Phố Lương Định Của', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1724, N'Phố Lý Quốc Sư', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1725, N'Phố Lý Thường Kiệt', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1726, N'Phố Nam Cao', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1727, N'Phố Ngô Sỹ Liên', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1728, N'Phố Nguyễn Khắc Cần', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1729, N'Phố Nguyễn Tự Cường', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1730, N'Phố Nhà Chung', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1731, N'Phố Phan Đăng Lưu', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1732, N'Phố Phó Đức Chính', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1733, N'Phố Phù Đổng Thiên Vương', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1734, N'Phố Tạ Quang Bửu', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1735, N'Phố Trần Nguyên Hãn', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1736, N'Phố Văn Miếu', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1737, N'Phố Yên Mẫn', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1738, N'Phố Lê Thanh Nghị', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1739, N'Phố Lê Trọng Tấn', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1740, N'Phố Lê Văn Hưu', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1741, N'Phố Lương Định Của', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1742, N'Phố Lý Quốc Sư', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1743, N'Phố Lý Thường Kiệt', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1744, N'Phố Nam Cao', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1745, N'Phố Ngô Sỹ Liên', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1746, N'Phố Nguyễn Khắc Cần', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1747, N'Phố Nguyễn Tự Cường', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1748, N'Phố Nhà Chung', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1749, N'Phố Phan Đăng Lưu', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1750, N'Phố Phó Đức Chính', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1751, N'Phố Phù Đổng Thiên Vương', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1752, N'Phố Tạ Quang Bửu', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1753, N'Phố Trần Nguyên Hãn', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1754, N'Phố Văn Miếu', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1755, N'Phố Yên Mẫn', 1, 30)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1756, N'Đường 19/8', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1757, N'Đường 3', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1758, N'Đường 3/2', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1759, N'Đường 30/4', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1760, N'Đường 505', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1761, N'Đường Bắc Kạn', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1762, N'Đường Bắc Nam', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1763, N'Đường Bắc Sơn', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1764, N'Đường Bến Oánh', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1765, N'Đường Bến Tượng', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1766, N'Đường Cách Mạng Tháng Tám', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1767, N'Đường Chiến Thắng', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1768, N'Đường Chu Văn An', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1769, N'Đường Đại Lộ Đông Tây', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1770, N'Đường Đê Nông Lâm', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1771, N'Đường Đoàn Thị Điểm', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1772, N'Đường Đội Cấn', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1773, N'Đường Đồng Bẩm', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1774, N'Đường Đồng Hỷ', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1775, N'Đường Dương Tự Minh', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1776, N'Đường Ga Quán Triều', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1777, N'Đường Ga Thái Nguyên', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1778, N'Đường Gang Thép', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1779, N'Đường Gia Sàng', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1780, N'Đường Hoàng Hoa Thám', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1781, N'Đường Hoàng Ngân', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1782, N'Đường Hoàng Văn Thụ', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1783, N'Đường Hùng Vương', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1784, N'Đường Hương Sơn', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1785, N'Đường Lê Hữu Trác', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1786, N'Đường Lê Quý Đôn', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1787, N'Đường Lương Ngọc Quyến', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1788, N'Đường Lương Thế Vinh', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1789, N'Đường Lưu Nhân Chú', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1790, N'Đường Lưu Nhân Trú', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1791, N'Đường Minh Cầu', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1792, N'Đường Mỏ Bạch', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1793, N'Đường Nam Núi Cốc', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1794, N'Đường Nguyễn Công Hoan', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1795, N'Đường Nguyễn Đình Chiểu', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1796, N'Đường Nguyễn Du', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1797, N'Đường Nguyễn Huệ', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1798, N'Đường Nguyễn Thái Học', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1799, N'Đường Nha Trang', 1, 21)
GO
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1800, N'Đường Núi Cốc', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1801, N'Đường Núi Voi', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1802, N'Đường Phan Bội Châu', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1803, N'Đường Phan Đình Phùng', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1804, N'Đường Phố Hương', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1805, N'Đường Phố Yên', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1806, N'Đường Phổ Yên', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1807, N'Đường Phủ Liễn', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1808, N'Đường Phú Thái', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1809, N'Đường Phú Xá', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1810, N'Đường Phúc Chu', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1811, N'Đường Phúc Hà', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1812, N'Đường Phúc Trìu', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1813, N'Đường Phúc Xuân', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1814, N'Đường Quan Triều', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1815, N'Đường Quang Trung', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1816, N'Đường Quang Vinh', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1817, N'Đường Quốc lộ 1B', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1818, N'Đường Quốc lộ 3', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1819, N'Đường Quốc lộ 37', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1820, N'Đường Quốc lộ 922', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1821, N'Đường Số 5', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1822, N'Đường Tân Cương', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1823, N'Đường Tân Quang', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1824, N'Đường Tân Thành', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1825, N'Đường Tân Thịnh', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1826, N'Đường Thanh Niên Xung Phong', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1827, N'Đường Thịnh Đán', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1828, N'Đường Thịnh Đức', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1829, N'Đường Thống Nhất', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1830, N'Đường Tích Lương', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1831, N'Đường Tố Hữu', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1832, N'Đường Trịnh B', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1833, N'Đường Trịnh Bá', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1834, N'Đường Túc Duyên', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1835, N'Đường Văn Cao', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1836, N'Đường Việt Bắc', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1837, N'Đường Vó Ngựa', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1838, N'Đường Vương Thừa Vũ', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1839, N'Đường Xuân Hòa', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1840, N'Đường Z115', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1841, N'Phố 37', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1842, N'Phố Cột Cờ', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1843, N'Phố Đầm Xanh', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1844, N'Phố Đồng Quang', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1845, N'Phố Phùng Chí Kiên', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1846, N'Phố Xương Rồng', 1, 21)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1847, N'Đường 261', 1, 22)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1848, N'Đường 263', 1, 22)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1849, N'Đường Đồng Khốc', 1, 22)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1850, N'Đường Phố Đình', 1, 22)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1851, N'Đường Quốc lộ 37', 1, 22)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1852, N'Đường Tỉnh lộ 264', 1, 22)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1853, N'Đường Tỉnh lộ 270', 1, 22)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1854, N'Phố 37', 1, 22)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1855, N'Đường 3', 1, 23)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1856, N'Đường Quán Vuông', 1, 23)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1857, N'Đường Tỉnh lộ 254', 1, 23)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1858, N'Đường Tỉnh lộ 264', 1, 23)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1859, N'Đường Tỉnh lộ 264B', 1, 23)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1860, N'Đường Tỉnh lộ 268', 1, 23)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1861, N'Đường Quốc Lộ 17', 1, 24)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1862, N'Đường Quốc lộ 1B', 1, 24)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1863, N'Đường Quốc lộ 217', 1, 24)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1864, N'Đường Tỉnh lộ 265', 1, 24)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1865, N'Đường Tỉnh lộ 269', 1, 24)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1866, N'Đường 261', 1, 25)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1867, N'Đường 274', 1, 25)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1868, N'Đường 3', 1, 25)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1869, N'Đường 47', 1, 25)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1870, N'Đường Đỗ Cận', 1, 25)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1871, N'Đường ĐT 261', 1, 25)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1872, N'Đường ĐT 266', 1, 25)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1873, N'Đường Hoàng Quốc Việt', 1, 25)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1874, N'Đường Hồng Diện', 1, 25)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1875, N'Đường Lê Hồng Phong', 1, 25)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1876, N'Đường Lý Nam Đế', 1, 25)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1877, N'Đường Nguyễn Cấu', 1, 25)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1878, N'Đường Nguyễn Thị Minh Khai', 1, 25)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1879, N'Đường Phạm Tu', 1, 25)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1880, N'Đường Phạm Văn Đồng', 1, 25)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1881, N'Đường Quốc Lộ 261', 1, 25)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1882, N'Đường Quốc lộ 3', 1, 25)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1883, N'Đường Số 6', 1, 25)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1884, N'Đường Tỉnh lộ 261', 1, 25)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1885, N'Đường Tỉnh lộ 301', 1, 25)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1886, N'Đường Trần Quang Khải', 1, 25)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1887, N'Đường Trường Chinh', 1, 25)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1888, N'Đường Vành Đai 5', 1, 25)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1889, N'Phố Tôn Đức Thắng', 1, 25)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1890, N'Phố Trần Nguyên Hãn', 1, 25)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1891, N'Đường ĐT 261', 1, 26)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1892, N'Đường Phú Gia', 1, 26)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1893, N'Đường Quốc lộ 37', 1, 26)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1894, N'Đường Tỉnh lộ 261C', 1, 26)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1895, N'Đường Tỉnh lộ 266', 1, 26)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1896, N'Đường Tỉnh lộ 269B', 1, 26)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1897, N'Đường Tỉnh lộ 287', 1, 26)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1898, N'Phố 37', 1, 26)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1899, N'Đường 263', 1, 27)
GO
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1900, N'Đường 3', 1, 27)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1901, N'Đường Quốc lộ 1B', 1, 27)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1902, N'Đường Quốc lộ 3', 1, 27)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1903, N'Đường Tỉnh lộ 268', 1, 27)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1904, N'Phố 37', 1, 27)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1905, N'Đường 262', 1, 28)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1906, N'Đường 3', 1, 28)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1907, N'Đường 3/2', 1, 28)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1908, N'Đường 30/4', 1, 28)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1909, N'Đường An Châu', 1, 28)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1910, N'Đường Bình Sơn', 1, 28)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1911, N'Đường Cách Mạng Tháng Mười', 1, 28)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1912, N'Đường Cách Mạng Tháng Tám', 1, 28)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1913, N'Đường Kim Đồng', 1, 28)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1914, N'Đường Lê Hồng Phong', 1, 28)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1915, N'Phố Lý Thường Kiệt', 1, 28)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1916, N'Phố Ngô Sỹ Liên', 1, 28)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1917, N'Đường Quốc lộ 1B', 1, 29)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1918, N'Đường Thái Long', 1, 29)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1919, N'Đường Tỉnh lộ 242', 1, 29)
INSERT [dbo].[VN_Streets] ([Id], [Name], [Active], [DistrictId]) VALUES (1920, N'Đường Tỉnh lộ 265', 1, 29)
SET IDENTITY_INSERT [dbo].[VN_Streets] OFF
SET IDENTITY_INSERT [dbo].[VN_Wards] ON 

INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (1, N'Cát Linh', 1, 1)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (2, N'Hàng Bột', 1, 1)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (3, N'Khâm Thiệt', 1, 1)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (4, N'Khương Thượng', 1, 1)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (5, N'Kim Liên', 1, 1)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (6, N'Láng Hạ', 1, 1)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (7, N'Láng Thượng', 1, 1)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (8, N'Ngã Tư Sở', 1, 1)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (9, N'Ô Chợ Dừa', 1, 1)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (10, N'Phương Liên', 1, 1)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (11, N'Phương Mai', 1, 1)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (12, N'Quang Trung', 1, 1)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (13, N'Quốc Tử Giám', 1, 1)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (14, N'Thịnh Quang', 1, 1)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (15, N'Trung Liệt', 1, 1)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (16, N'Trung Phụng', 1, 1)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (17, N'Trung Tự', 1, 1)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (18, N'Văn Miếu', 1, 1)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (19, N'Cống Vị', 1, 2)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (20, N'Điện Biên', 1, 2)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (21, N'Đội Cấn', 1, 2)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (22, N'Giảng Võ', 1, 2)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (23, N'Kim Mã', 1, 2)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (24, N'Liễu Giai', 1, 2)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (25, N'Ngọc Hà', 1, 2)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (26, N'Ngọc Khánh', 1, 2)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (27, N'Quán Thánh', 1, 2)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (28, N'Thành Công', 1, 2)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (29, N'Trúc Bạch', 1, 2)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (30, N'Vĩnh Phúc', 1, 2)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (31, N'Dịch Vọng Hậu', 1, 3)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (32, N'Quan Hoa', 1, 3)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (33, N'Dịch Vọng', 1, 3)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (34, N'Mai Dịch', 1, 3)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (35, N'Nghĩa Đô', 1, 3)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (36, N'Nghĩa Tân', 1, 3)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (37, N'Trung Hòa', 1, 3)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (38, N'Yên Hòa', 1, 3)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (60, N'Bưởi', 1, 4)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (61, N'Nhật Tân', 1, 4)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (62, N'Phú Thượng', 1, 4)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (63, N'Xuân La', 1, 4)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (64, N'Yên Phụ', 1, 4)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (65, N'Thụy Khuê', 1, 4)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (66, N'Quảng An', 1, 4)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (67, N'Tứ Liên', 1, 4)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (68, N'Chương Dương Độ', 1, 5)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (69, N'Cửa Đông', 1, 5)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (70, N'Cửa Nam', 1, 5)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (71, N'Đồng Xuân', 1, 5)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (72, N'Hàng Bạc', 1, 5)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (73, N'Hàng Bài', 1, 5)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (74, N'Hàng Bồ', 1, 5)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (75, N'Hàng Bông', 1, 5)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (76, N'Hàng Buồm', 1, 5)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (77, N'Hàng Đào', 1, 5)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (78, N'Hàng Gai', 1, 5)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (79, N'Hàng Mã', 1, 5)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (80, N'Hàng Trống', 1, 5)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (81, N'Lý Thái Tổ', 1, 5)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (82, N'Phan Chu Trinh', 1, 5)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (83, N'Phúc Tân', 1, 5)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (84, N'Trần Hưng Đạo', 1, 5)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (85, N'Tràng Tiền', 1, 5)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (86, N'Bạch Đằng', 1, 6)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (87, N'Bách Khoa', 1, 6)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (88, N'Bạch Mai', 1, 6)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (89, N'Bùi Thị Xuân', 1, 6)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (90, N'Cầu Dền', 1, 6)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (91, N'Đống Mác', 1, 6)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (92, N'Đồng Nhân', 1, 6)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (93, N'Đồng Tâm', 1, 6)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (94, N'Lê Đại Hành', 1, 6)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (95, N'Minh Khai', 1, 6)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (96, N'Ngô Thì Nhậm', 1, 6)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (97, N'Nguyễn Du', 1, 6)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (98, N'Phạm Đình Hổ', 1, 6)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (99, N'Phố Huế', 1, 6)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (100, N'Quỳnh Lôi', 1, 6)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (101, N'Quỳnh Mai', 1, 6)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (102, N'Thanh Lương', 1, 6)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (103, N'Thanh Nhàn', 1, 6)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (104, N'Trương Định', 1, 6)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (105, N'Vĩnh Tuy', 1, 6)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (106, N'Hạ Đình', 1, 7)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (107, N'Khương Đình', 1, 7)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (108, N'Khương Mai', 1, 7)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (109, N'Khương Trung', 1, 7)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (110, N'Kim Giang', 1, 7)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (111, N'Nhân Chính', 1, 7)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (112, N'Phương Liệt', 1, 7)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (113, N'Thanh Xuân Bắc', 1, 7)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (114, N'Thanh Xuân Nam', 1, 7)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (115, N'Thanh Xuân Trung', 1, 7)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (116, N'Thượng Đình', 1, 7)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (117, N'Nguyễn Du', 1, 7)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (118, N'Phạm Đình Hổ', 1, 7)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (119, N'Phố Huế', 1, 7)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (120, N'Quỳnh Lôi', 1, 7)
GO
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (121, N'Quỳnh Mai', 1, 7)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (122, N'Thanh Lương', 1, 7)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (123, N'Thanh Nhàn', 1, 7)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (124, N'Trương Định', 1, 7)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (125, N'Vĩnh Tuy', 1, 7)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (126, N'Biên Giang', 1, 8)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (127, N'Đồng Mai', 1, 8)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (128, N'Dương Nội', 1, 8)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (129, N'Hà Cầu', 1, 8)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (130, N'Kiến Hưng', 1, 8)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (131, N'La Khê', 1, 8)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (132, N'Mộ Lao', 1, 8)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (133, N'Nguyễn Trãi', 1, 8)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (134, N'Phú La', 1, 8)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (135, N'Phú Lãm', 1, 8)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (136, N'Phú Lương', 1, 8)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (137, N'Phúc La', 1, 8)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (138, N'Quang Trung', 1, 8)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (139, N'Vạn Phúc', 1, 8)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (140, N'Văn Quán', 1, 8)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (141, N'Yên Nghĩa', 1, 8)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (142, N'Yết Kiêu', 1, 8)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (143, N'Cầu Diễn', 1, 10)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (144, N'Đại Mỗ', 1, 10)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (145, N'Mễ Trì', 1, 10)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (146, N'Mỹ Đình 1', 1, 10)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (147, N'Mỹ Đình 2', 1, 10)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (148, N'Phú Đô', 1, 10)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (149, N'Phương Canh', 1, 10)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (150, N'Tây Mỗ', 1, 10)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (151, N'Trung Văn', 1, 10)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (152, N'Xuân Phương', 1, 10)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (153, N'Đại Kim', 1, 9)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (154, N'Định Công', 1, 9)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (155, N'Giáp Bát', 1, 9)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (156, N'Hoàng Liệt', 1, 9)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (157, N'Hoàng Văn Thụ', 1, 9)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (158, N'Lĩnh Nam', 1, 9)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (159, N'Mai Động', 1, 9)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (160, N'Tân Mai', 1, 9)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (161, N'Thanh Trì', 1, 9)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (162, N'Thịnh Liệt', 1, 9)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (163, N'Trần Phú', 1, 9)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (164, N'Tương Mai', 1, 9)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (165, N'Vĩnh Hưng', 1, 9)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (166, N'Yên Sở', 1, 9)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (167, N'Phường Hoàng Văn Thụ', 1, 11)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (168, N'Phường Lê Lợi', 1, 11)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (169, N'Phường Mỹ Độ', 1, 11)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (170, N'Phường Ngô Quyền', 1, 11)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (171, N'Phường Thọ Xương', 1, 11)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (172, N'Phường Trần Nguyên Hãn', 1, 11)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (173, N'Phường Trần Phú', 1, 11)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (174, N'Phường Xương Gian', 1, 11)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (175, N'Xã Đa Mai', 1, 11)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (176, N'Xã Dĩnh Kế', 1, 11)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (177, N'Xã Dĩnh Trì', 1, 11)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (178, N'Xã Đồng Sơn', 1, 11)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (179, N'Xã Song Khê', 1, 11)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (180, N'Xã Song Mai', 1, 11)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (181, N'Xã Tân Mỹ', 1, 11)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (182, N'Xã Tân Tiến', 1, 11)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (183, N'Thị trấn Bách Nhẫn', 1, 12)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (184, N'Thị trấn Thắng', 1, 12)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (185, N'Xã Bắc Lý', 1, 12)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (186, N'Xã Châu Minh', 1, 12)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (187, N'Xã Đại Thành', 1, 12)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (188, N'Xã Danh Thắng', 1, 12)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (189, N'Xã Đoan Bái', 1, 12)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (190, N'Xã Đông Lỗ', 1, 12)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (191, N'Xã Đồng Tân', 1, 12)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (192, N'Xã Đức Thắng', 1, 12)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (193, N'Xã Hòa Sơn', 1, 12)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (194, N'Xã Hoàng An', 1, 12)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (195, N'Xã Hoàng Lương', 1, 12)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (196, N'Xã Hoàng Thanh', 1, 12)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (197, N'Xã Hoàng Vân', 1, 12)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (198, N'Xã Hợp Thịnh', 1, 12)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (199, N'Xã Hùng Sơn', 1, 12)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (200, N'Xã Hương Lâm', 1, 12)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (201, N'Xã Lương Phong', 1, 12)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (202, N'Xã Mai Đình', 1, 12)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (203, N'Xã Mai Trung', 1, 12)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (204, N'Xã Ngọc Sơn', 1, 12)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (205, N'Xã Quang Minh', 1, 12)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (206, N'Xã Thái Sơn', 1, 12)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (207, N'Xã Thanh Vân', 1, 12)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (208, N'Xã Thường Thắng', 1, 12)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (209, N'Xã Xuân Cẩm', 1, 12)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (210, N'Thị trấn Cao Thượng', 1, 13)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (211, N'Thị trấn Nhã Nam', 1, 13)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (212, N'Xã An Dương', 1, 13)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (213, N'Xã Cao Thượng', 1, 13)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (214, N'Xã Cao Xá', 1, 13)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (215, N'Xã Đại Hóa', 1, 13)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (216, N'Xã Hợp Đức', 1, 13)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (217, N'Xã Lam Cốt', 1, 13)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (218, N'Xã Lan Giới', 1, 13)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (219, N'Xã Liên Chung', 1, 13)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (220, N'Xã Liên Sơn', 1, 13)
GO
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (221, N'Xã Ngọc Châu', 1, 13)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (222, N'Xã Ngọc Lý', 1, 13)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (223, N'Xã Ngọc Thiện', 1, 13)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (224, N'Xã Ngọc Vân', 1, 13)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (225, N'Xã Nhã Nam', 1, 13)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (226, N'Xã Phúc Hòa', 1, 13)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (227, N'Xã Phúc Sơn', 1, 13)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (228, N'Xã Quang Tiến', 1, 13)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (229, N'Xã Quế Nham', 1, 13)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (230, N'Xã Song Vân', 1, 13)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (231, N'Xã Tân Trung', 1, 13)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (232, N'Xã Việt Lập', 1, 13)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (233, N'Xã Việt Ngọc', 1, 13)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (234, N'Thị trấn Kép', 1, 14)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (235, N'Thị trấn Vôi', 1, 14)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (236, N'Xã An Hà', 1, 14)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (237, N'Xã Đại Lâm', 1, 14)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (238, N'Xã Đào Mỹ', 1, 14)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (239, N'Xã Dương Đức', 1, 14)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (240, N'Xã Hương Lạc', 1, 14)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (241, N'Xã Hương Sơn', 1, 14)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (242, N'Xã Mỹ Hà', 1, 14)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (243, N'Xã Mỹ Thái', 1, 14)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (244, N'Xã Nghĩa Hòa', 1, 14)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (245, N'Xã Nghĩa Hưng', 1, 14)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (246, N'Xã Phi Mô', 1, 14)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (247, N'Xã Quang Thịnh', 1, 14)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (248, N'Xã Tân Dĩnh', 1, 14)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (249, N'Xã Tân Hưng', 1, 14)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (250, N'Xã Tân Thanh', 1, 14)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (251, N'Xã Tân Thịnh', 1, 14)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (252, N'Xã Thái Đào', 1, 14)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (253, N'Xã Tiên Lục', 1, 14)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (254, N'Xã Xuân Hương', 1, 14)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (255, N'Xã Xương Lâm', 1, 14)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (256, N'Xã Yên Mỹ', 1, 14)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (257, N'Thị trấn Đồi Ngô', 1, 15)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (258, N'Thị trấn Lục Nam', 1, 15)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (259, N'Xã Bắc Lũng', 1, 15)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (260, N'Xã Bảo Đài', 1, 15)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (261, N'Xã Bảo Sơn', 1, 15)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (262, N'Xã Bình Sơn', 1, 15)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (263, N'Xã Cẩm Lý', 1, 15)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (264, N'Xã Chu Điện', 1, 15)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (265, N'Xã Cương Sơn', 1, 15)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (266, N'Xã Đan Hội', 1, 15)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (267, N'Xã Đông Hưng', 1, 15)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (268, N'Xã Đông Phú', 1, 15)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (269, N'Xã Huyền Sơn', 1, 15)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (270, N'Xã Khám Lạng', 1, 15)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (271, N'Xã Lan Mẫu', 1, 15)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (272, N'Xã Lục Sơn', 1, 15)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (273, N'Xã Nghĩa Phương', 1, 15)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (274, N'Xã Phương Sơn', 1, 15)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (275, N'Xã Tam Dị', 1, 15)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (276, N'Xã Thanh Lâm', 1, 15)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (277, N'Xã Tiên Hưng', 1, 15)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (278, N'Xã Tiên Nha', 1, 15)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (279, N'Xã Trường Giang', 1, 15)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (280, N'Xã Trường Sơn', 1, 15)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (281, N'Xã Vô Tranh', 1, 15)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (282, N'Xã Vũ Xá', 1, 15)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (283, N'Xã Yên Sơn', 1, 15)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (284, N'Thị trấn Chũ', 1, 16)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (285, N'Xã Biển Động', 1, 16)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (286, N'Xã Biên Sơn', 1, 16)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (287, N'Xã Cấm Sơn', 1, 16)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (288, N'Xã Đèo Gia', 1, 16)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (289, N'Xã Đồng Cốc', 1, 16)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (290, N'Xã Giáp Sơn', 1, 16)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (291, N'Xã Hộ Đáp', 1, 16)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (292, N'Xã Hồng Giang', 1, 16)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (293, N'Xã Kiên Lao', 1, 16)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (294, N'Xã Kiên Thành', 1, 16)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (295, N'Xã Kim Sơn', 1, 16)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (296, N'Xã Mỹ An', 1, 16)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (297, N'Xã Nam Dương', 1, 16)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (298, N'Xã Nghĩa Hồ', 1, 16)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (299, N'Xã Phì Điền', 1, 16)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (300, N'Xã Phong Minh', 1, 16)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (301, N'Xã Phong Vân', 1, 16)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (302, N'Xã Phú Nhuận', 1, 16)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (303, N'Xã Phượng Sơn', 1, 16)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (304, N'Xã Quý Sơn', 1, 16)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (305, N'Xã Sa Lý', 1, 16)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (306, N'Xã Sơn Hải', 1, 16)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (307, N'Xã Tân Hoa', 1, 16)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (308, N'Xã Tân Lập', 1, 16)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (309, N'Xã Tân Mộc', 1, 16)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (310, N'Xã Tân Quang', 1, 16)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (311, N'Xã Tân Sơn', 1, 16)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (312, N'Xã Thanh Hải', 1, 16)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (313, N'Xã Trù Hựu', 1, 16)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (314, N'Thị trấn Thanh Sơn', 1, 17)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (315, N'Thị trấn Thị trấn An Châu', 1, 17)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (316, N'Xã An Bá', 1, 17)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (317, N'Xã An Châu', 1, 17)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (318, N'Xã An Lạc', 1, 17)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (319, N'Xã An Lập', 1, 17)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (320, N'Xã Bồng Am', 1, 17)
GO
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (321, N'Xã Cẩm Đàn', 1, 17)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (322, N'Xã Chiên Sơn', 1, 17)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (323, N'Xã Dương Hưu', 1, 17)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (324, N'Xã Giáo Liêm', 1, 17)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (325, N'Xã Hữu Sản', 1, 17)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (326, N'Xã Lệ Viễn', 1, 17)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (327, N'Xã Long Sơn', 1, 17)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (328, N'Xã Ngọc Châu', 1, 17)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (329, N'Xã Phúc Thắng', 1, 17)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (330, N'Xã Quế Sơn', 1, 17)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (331, N'Xã Thạch Sơn', 1, 17)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (332, N'Xã Thanh Luận', 1, 17)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (333, N'Xã Tuấn Đạo', 1, 17)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (334, N'Xã Tuấn Mậu', 1, 17)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (335, N'Xã Vân Sơn', 1, 17)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (336, N'Xã Vĩnh Khương', 1, 17)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (337, N'Xã Yên Định', 1, 17)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (338, N'Thị trấn Bích Động', 1, 18)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (339, N'Thị trấn Nếnh', 1, 18)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (340, N'Xã Bích Sơn', 1, 18)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (341, N'Xã Hoàng Ninh', 1, 18)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (342, N'Xã Hồng Thái', 1, 18)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (343, N'Xã Hương Mai', 1, 18)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (344, N'Xã Minh Đức', 1, 18)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (345, N'Xã Nếnh', 1, 18)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (346, N'Xã Nghĩa Trung', 1, 18)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (347, N'Xã Ninh Sơn', 1, 18)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (348, N'Xã Quang Châu', 1, 18)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (349, N'Xã Quảng Minh', 1, 18)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (350, N'Xã Tăng Tiến', 1, 18)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (351, N'Xã Thượng Lan', 1, 18)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (352, N'Xã Tiên Sơn', 1, 18)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (353, N'Xã Trung Sơn', 1, 18)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (354, N'Xã Tự Lan', 1, 18)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (355, N'Xã Vân Hà', 1, 18)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (356, N'Xã Vân Trung', 1, 18)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (357, N'Xã Việt Tiến', 1, 18)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (358, N'Thị trấn Neo', 1, 19)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (359, N'Thị trấn Tân Dân', 1, 19)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (360, N'Xã Cảnh Thụy', 1, 19)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (361, N'Xã Đồng Phúc', 1, 19)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (362, N'Xã Đồng Tâm', 1, 19)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (363, N'Xã Đồng Việt', 1, 19)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (364, N'Xã Đức Giang', 1, 19)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (365, N'Xã Hồng Kỳ', 1, 19)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (366, N'Xã Hương Gián', 1, 19)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (367, N'Xã Lãng Sơn', 1, 19)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (368, N'Xã Lão Hộ', 1, 19)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (369, N'Xã Nham Sơn', 1, 19)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (370, N'Xã Nội Hoàng', 1, 19)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (371, N'Xã Quỳnh Sơn', 1, 19)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (372, N'Xã Tân An', 1, 19)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (373, N'Xã Tân Liễu', 1, 19)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (374, N'Xã Thắng Cương', 1, 19)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (375, N'Xã Tiến Dũng', 1, 19)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (376, N'Xã Tiền Phong', 1, 19)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (377, N'Xã Trí Yên', 1, 19)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (378, N'Xã Tư Mại', 1, 19)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (379, N'Xã Xuân Phú', 1, 19)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (380, N'Xã Yên Lư', 1, 19)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (381, N'Thị trấn Bố Hạ', 1, 20)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (382, N'Thị trấn Cầu Gồ', 1, 20)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (383, N'Xã An Thượng', 1, 20)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (384, N'Xã Bố Hạ', 1, 20)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (385, N'Xã Canh Nậu', 1, 20)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (386, N'Xã Đồng Hưu', 1, 20)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (387, N'Xã Đồng Kỳ', 1, 20)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (388, N'Xã Đồng Lạc', 1, 20)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (389, N'Xã Đông Sơn', 1, 20)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (390, N'Xã Đồng Tâm', 1, 20)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (391, N'Xã Đồng Tiến', 1, 20)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (392, N'Xã Đồng Vương', 1, 20)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (393, N'Xã Hương Vĩ', 1, 20)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (394, N'Xã Phồn Xương', 1, 20)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (395, N'Xã Tam Hiệp', 1, 20)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (396, N'Xã Tam Tiến', 1, 20)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (397, N'Xã Tân Hiệp', 1, 20)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (398, N'Xã Tân Sỏi', 1, 20)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (399, N'Xã Tiến Thắng', 1, 20)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (400, N'Xã Xuân Lương', 1, 20)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (401, N'Thị trấn Hồ', 1, 34)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (402, N'Thị trấn Hồ Huyện', 1, 34)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (403, N'Xã An Bình', 1, 34)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (404, N'Xã Đại Đồng Thành', 1, 34)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (405, N'Xã Đình Tổ', 1, 34)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (406, N'Xã Gia Đông', 1, 34)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (407, N'Xã Hà Mãn', 1, 34)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (408, N'Xã Hoài Thượng', 1, 34)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (409, N'Xã Mão Điền', 1, 34)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (410, N'Xã Nghĩa Đạo', 1, 34)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (411, N'Xã Ngũ Thái', 1, 34)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (412, N'Xã Nguyệt Đức', 1, 34)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (413, N'Xã Ninh Xá', 1, 34)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (414, N'Xã Song Hồ', 1, 34)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (415, N'Xã Song Liễu', 1, 34)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (416, N'Xã Thanh Khương', 1, 34)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (417, N'Xã Trạm Lộ', 1, 34)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (418, N'Xã Trí Quả', 1, 34)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (419, N'Xã Xuân Lâm', 1, 34)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (420, N'Thị trấn Gia Bình', 1, 31)
GO
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (421, N'Xã Bình Dương', 1, 31)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (422, N'Xã Cao Đức', 1, 31)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (423, N'Xã Đại Bái', 1, 31)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (424, N'Xã Đại Lai', 1, 31)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (425, N'Xã Đông Cứu', 1, 31)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (426, N'Xã Giang Sơn', 1, 31)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (427, N'Xã Lãng Ngâm', 1, 31)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (428, N'Xã Nhân Thắng', 1, 31)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (429, N'Xã Quỳnh Phú', 1, 31)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (430, N'Xã Song Giang', 1, 31)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (431, N'Xã Thái Bảo', 1, 31)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (432, N'Xã Vạn Ninh', 1, 31)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (433, N'Xã Xuân Lai', 1, 31)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (434, N'Thị trấn Thứa', 1, 32)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (435, N'Xã An Thịnh', 1, 32)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (436, N'Xã Bình Định', 1, 32)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (437, N'Xã Lai Hạ', 1, 32)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (438, N'Xã Lâm Thao', 1, 32)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (439, N'Xã Minh Tân', 1, 32)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (440, N'Xã Mỹ Hương', 1, 32)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (441, N'Xã Phú Hòa', 1, 32)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (442, N'Xã Phú Lương', 1, 32)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (443, N'Xã Quảng Phú', 1, 32)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (444, N'Xã Tân Lãng', 1, 32)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (445, N'Xã Trung Chính', 1, 32)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (446, N'Xã Trung Kênh', 1, 32)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (447, N'Xã Trừng Xá', 1, 32)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (448, N'Xã Việt Đoàn', 1, 35)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (449, N'Xã Tri Phương', 1, 35)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (450, N'Xã Tân Chi', 1, 35)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (451, N'Xã Phú Lâm', 1, 35)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (452, N'Xã Phật Tích', 1, 35)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (453, N'Xã Nội Duệ', 1, 35)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (454, N'Xã Minh Đạo', 1, 35)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (455, N'Xã Liên Bão', 1, 35)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (456, N'Xã Lạc Vệ', 1, 35)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (457, N'Xã Hoàn Sơn', 1, 35)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (458, N'Xã Hiên Vân', 1, 35)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (459, N'Xã Đại Đồng', 1, 35)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (460, N'Xã Cảnh Hưng', 1, 35)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (461, N'Thị trấn Lim', 1, 35)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (462, N'Thị trấn Chờ', 1, 37)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (463, N'Xã Đông Phong', 1, 37)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (464, N'Xã Đông Thọ', 1, 37)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (465, N'Xã Đông Tiến', 1, 37)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (466, N'Xã Dũng Liệt', 1, 37)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (467, N'Xã Hòa Tiến', 1, 37)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (468, N'Xã Long Châu', 1, 37)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (469, N'Xã Tam Đa', 1, 37)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (470, N'Xã Tam Giang', 1, 37)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (471, N'Xã Thụy Hòa', 1, 37)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (472, N'Xã Trung Nghĩa', 1, 37)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (473, N'Xã Văn Môn', 1, 37)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (474, N'Xã Yên Phụ', 1, 37)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (475, N'Xã Yên Trung', 1, 37)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (476, N'Thị trấn Phố Mới', 1, 33)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (477, N'Xã Bằng An', 1, 33)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (478, N'Xã Bồng Lai', 1, 33)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (479, N'Xã Cách Bi', 1, 33)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (480, N'Xã Châu Phong', 1, 33)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (481, N'Xã Chi Lăng', 1, 33)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (482, N'Xã Đại Xuân', 1, 33)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (483, N'Xã Đào Viên', 1, 33)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (484, N'Xã Đức Long', 1, 33)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (485, N'Xã Hán Quảng', 1, 33)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (486, N'Xã Mộ Đạo', 1, 33)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (487, N'Xã Ngọc Xá', 1, 33)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (488, N'Xã Nhân Hoà', 1, 33)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (489, N'Xã Phù Lãng', 1, 33)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (490, N'Xã Phù Lương', 1, 33)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (491, N'Xã Phương Liễu', 1, 33)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (492, N'Xã Phượng Mao', 1, 33)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (493, N'Xã Quế Tân', 1, 33)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (494, N'Xã Việt Hùng', 1, 33)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (495, N'Xã Việt Thống', 1, 33)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (496, N'Xã Yên Giả', 1, 33)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (497, N'Phường Châu Khê', 1, 36)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (498, N'Phường Đình Bảng', 1, 36)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (499, N'Phường Đồng Kỵ', 1, 36)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (500, N'Phường Đông Ngàn', 1, 36)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (501, N'Phường Đồng Nguyên', 1, 36)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (502, N'Phường Tân Hồng', 1, 36)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (503, N'Phường Trang Hạ', 1, 36)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (504, N'Xã Đồng Nguyên', 1, 36)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (505, N'Xã Hương Mạc', 1, 36)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (506, N'Xã Phù Chẩn', 1, 36)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (507, N'Xã Phù Khê', 1, 36)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (508, N'Xã Tam Sơn', 1, 36)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (509, N'Xã Tương Giang', 1, 36)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (510, N'Phường Đại Phúc', 1, 30)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (511, N'Phường Cam Giá', 1, 21)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (512, N'Phường Đồng Bẩm', 1, 21)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (513, N'Phường Đồng Quang', 1, 21)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (514, N'Phường Gia Sàng', 1, 21)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (515, N'Phường Hoàng Văn Thụ', 1, 21)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (516, N'Phường Hương Sơn', 1, 21)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (517, N'Phường Phan Đình Phùng', 1, 21)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (518, N'Phường Phú Xá', 1, 21)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (519, N'Phường Quán Triều', 1, 21)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (520, N'Phường Quang Trung', 1, 21)
GO
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (521, N'Phường Quang Vinh', 1, 21)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (522, N'Phường Tân Lập', 1, 21)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (523, N'Phường Tân Long', 1, 21)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (524, N'Phường Tân Thành', 1, 21)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (525, N'Phường Tân Thịnh', 1, 21)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (526, N'Phường Thịnh Đán', 1, 21)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (527, N'Phường Tích Lương', 1, 21)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (528, N'Phường Trung Thành', 1, 21)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (529, N'Phường Trưng Vương', 1, 21)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (530, N'Phường Túc Duyên', 1, 21)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (531, N'Xã Cao Ngạn', 1, 21)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (532, N'Xã Phúc Hà', 1, 21)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (533, N'Xã Phúc Trìu', 1, 21)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (534, N'Xã Phúc Xuân', 1, 21)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (535, N'Xã Quyết Thắng', 1, 21)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (536, N'Xã Tân Cương', 1, 21)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (537, N'Xã Thịnh Đức', 1, 21)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (538, N'Xã Thủy Đường', 1, 21)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (539, N'Thị trấn Đại Từ', 1, 22)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (540, N'Thị trấn Quân Chu', 1, 22)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (541, N'Xã An Khánh', 1, 22)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (542, N'Xã Bản Ngoại', 1, 22)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (543, N'Xã Bình Thuận', 1, 22)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (544, N'Xã Cát Nê', 1, 22)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (545, N'Xã Cù Vân', 1, 22)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (546, N'Xã Đức Lương', 1, 22)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (547, N'Xã Hà Thượng', 1, 22)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (548, N'Xã Hoàng Nông', 1, 22)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (549, N'Xã Hùng Sơn', 1, 22)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (550, N'Xã Khôi Kỳ', 1, 22)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (551, N'Xã Ký Phú', 1, 22)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (552, N'Xã La Bằng', 1, 22)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (553, N'Xã Lục Ba', 1, 22)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (554, N'Xã Minh Tiến', 1, 22)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (555, N'Xã Mỹ Yên', 1, 22)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (556, N'Xã Na Mao', 1, 22)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (557, N'Xã Phú Cường', 1, 22)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (558, N'Xã Phú Lạc', 1, 22)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (559, N'Xã Phú Thịnh', 1, 22)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (560, N'Xã Phú Xuyên', 1, 22)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (561, N'Xã Phục Linh', 1, 22)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (562, N'Xã Phúc Lương', 1, 22)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (563, N'Xã Quân Chu', 1, 22)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (564, N'Xã Tân Linh', 1, 22)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (565, N'Xã Tân Thái', 1, 22)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (566, N'Xã Tiên Hội', 1, 22)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (567, N'Xã Vạn Thọ', 1, 22)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (568, N'Xã Văn Yên', 1, 22)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (569, N'Xã Yên Lãng', 1, 22)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (570, N'Thị trấn Chợ Chu', 1, 23)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (571, N'Xã Bảo Cường', 1, 23)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (572, N'Xã Bảo Linh', 1, 23)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (573, N'Xã Bình Thành', 1, 23)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (574, N'Xã Bình Yên', 1, 23)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (575, N'Xã Bộc Nhiêu', 1, 23)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (576, N'Xã Điềm Mặc', 1, 23)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (577, N'Xã Định Biên', 1, 23)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (578, N'Xã Đồng Thịnh', 1, 23)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (579, N'Xã Kim Phượng', 1, 23)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (580, N'Xã Kim Sơn', 1, 23)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (581, N'Xã Lam Vỹ', 1, 23)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (582, N'Xã Linh Thông', 1, 23)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (583, N'Xã Phú Đình', 1, 23)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (584, N'Xã Phú Tiến', 1, 23)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (585, N'Xã Phúc Chu', 1, 23)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (586, N'Xã Phượng Tiến', 1, 23)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (587, N'Xã Quy Kỳ', 1, 23)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (588, N'Xã Sơn Phú', 1, 23)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (589, N'Xã Tân Dương', 1, 23)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (590, N'Xã Tân Thịnh', 1, 23)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (591, N'Xã Thanh Định', 1, 23)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (592, N'Xã Trung Hội', 1, 23)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (593, N'Xã Trung Lương', 1, 23)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (594, N'Thị trấn Chùa Hang', 1, 24)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (595, N'Thị trấn Sông Cầu', 1, 24)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (596, N'Thị trấn Trại Cau', 1, 24)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (597, N'Xã Cây Thị', 1, 24)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (598, N'Xã Hòa Bình', 1, 24)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (599, N'Xã Hóa Thượng', 1, 24)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (600, N'Xã Hóa Trung', 1, 24)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (601, N'Xã Hợp Tiến', 1, 24)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (602, N'Xã Huống Thượng', 1, 24)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (603, N'Xã Khe Mo', 1, 24)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (604, N'Xã Linh Sơn', 1, 24)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (605, N'Xã Minh Lập', 1, 24)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (606, N'Xã Nam Hòa', 1, 24)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (607, N'Xã Quang Sơn', 1, 24)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (608, N'Xã Tân Lợi', 1, 24)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (609, N'Xã Tân Long', 1, 24)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (610, N'Xã Văn Hán', 1, 24)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (611, N'Xã Văn Lăng', 1, 24)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (612, N'Phường Ba Hàng', 1, 25)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (613, N'Phường Bắc Sơn', 1, 25)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (614, N'Phường Bãi Bông', 1, 25)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (615, N'Phường Đồng Tiến', 1, 25)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (616, N'Xã Đắc Sơn', 1, 25)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (617, N'Xã Đông Cao', 1, 25)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (618, N'Xã Hồng Tiến', 1, 25)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (619, N'Xã Minh Đức', 1, 25)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (620, N'Xã Nam Tiến', 1, 25)
GO
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (621, N'Xã Phúc Tân', 1, 25)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (622, N'Xã Phúc Thuận', 1, 25)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (623, N'Xã Tân Hương', 1, 25)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (624, N'Xã Tân Phú', 1, 25)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (625, N'Xã Thành Công', 1, 25)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (626, N'Xã Thuận Thành', 1, 25)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (627, N'Xã Tiên Phong', 1, 25)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (628, N'Xã Trung Thành', 1, 25)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (629, N'Xã Vạn Phái', 1, 25)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (630, N'Thị trấn Hương Sơn', 1, 26)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (631, N'Xã Bàn Đạt', 1, 26)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (632, N'Xã Bảo Lý', 1, 26)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (633, N'Xã Đào Xá', 1, 26)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (634, N'Xã Điềm Thụy', 1, 26)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (635, N'Xã Đồng Liên', 1, 26)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (636, N'Xã Dương Thành', 1, 26)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (637, N'Xã Hà Châu', 1, 26)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (638, N'Xã Kha Sơn', 1, 26)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (639, N'Xã Lương Phú', 1, 26)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (640, N'Xã Nga My', 1, 26)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (641, N'Xã Nhã Lộng', 1, 26)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (642, N'Xã Tân Đức', 1, 26)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (643, N'Xã Tân Hòa', 1, 26)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (644, N'Xã Tân Khánh', 1, 26)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (645, N'Xã Tân Kim', 1, 26)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (646, N'Xã Tân Thành', 1, 26)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (647, N'Xã Thanh Ninh', 1, 26)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (648, N'Xã Thượng Đình', 1, 26)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (649, N'Xã Úc Kỳ', 1, 26)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (650, N'Xã Xuân Phương', 1, 26)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (651, N'Thị trấn Đu', 1, 27)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (652, N'Thị trấn Giang Tiên', 1, 27)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (653, N'Xã Cổ Lũng', 1, 27)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (654, N'Xã Động Đạt', 1, 27)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (655, N'Xã Hợp Thành', 1, 27)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (656, N'Xã Ôn Lương', 1, 27)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (657, N'Xã Phấn Mễ', 1, 27)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (658, N'Xã Phú Đô', 1, 27)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (659, N'Xã Phủ Lý', 1, 27)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (660, N'Xã Sơn Cẩm', 1, 27)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (661, N'Xã Tức Tranh', 1, 27)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (662, N'Xã Vô Tranh', 1, 27)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (663, N'Xã Yên Đổ', 1, 27)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (664, N'Xã Yên Lạc', 1, 27)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (665, N'Xã Yên Ninh', 1, 27)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (666, N'Xã Yên Trạch', 1, 27)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (667, N'Xã Bá Xuyên', 1, 28)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (668, N'Phường Bách Quang', 1, 28)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (669, N'Xã Bình Sơn', 1, 28)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (670, N'Phường Cải Đan', 1, 28)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (671, N'Phường Lương Châu', 1, 28)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (672, N'Phường Lương Sơn', 1, 28)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (673, N'Phường Mỏ Chè', 1, 28)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (674, N'Phường Phố Cò', 1, 28)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (675, N'Xã Tân Quang', 1, 28)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (676, N'Phường Thắng Lợi', 1, 28)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (677, N'Xã Vinh Sơn', 1, 28)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (678, N'Thị trấn Đình Cả', 1, 29)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (679, N'Xã Bình Long', 1, 29)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (680, N'Xã Cúc Đường', 1, 29)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (681, N'Xã Dân Tiến', 1, 29)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (682, N'Xã La Hiên', 1, 29)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (683, N'Xã Lâu Thượng', 1, 29)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (684, N'Xã Liên Minh', 1, 29)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (685, N'Xã Nghinh Tường', 1, 29)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (686, N'Xã Phú Thượng', 1, 29)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (687, N'Xã Phương Giao', 1, 29)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (688, N'Xã Sảng Mộc', 1, 29)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (689, N'Xã Thần Sa', 1, 29)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (690, N'Xã Thượng Nung', 1, 29)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (691, N'Xã Tràng Xá', 1, 29)
INSERT [dbo].[VN_Wards] ([Id], [Name], [Active], [DistrictId]) VALUES (692, N'Xã Vũ Chấn', 1, 29)
SET IDENTITY_INSERT [dbo].[VN_Wards] OFF
SET ANSI_PADDING ON
GO
/****** Object:  Index [RoleNameIndex]    Script Date: 2/24/2020 5:48:43 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [RoleNameIndex] ON [dbo].[AspNetRoles]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_UserId]    Script Date: 2/24/2020 5:48:43 PM ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserClaims]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_UserId]    Script Date: 2/24/2020 5:48:43 PM ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserLogins]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_RoleId]    Script Date: 2/24/2020 5:48:43 PM ******/
CREATE NONCLUSTERED INDEX [IX_RoleId] ON [dbo].[AspNetUserRoles]
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_UserId]    Script Date: 2/24/2020 5:48:43 PM ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserRoles]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UserNameIndex]    Script Date: 2/24/2020 5:48:43 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UserNameIndex] ON [dbo].[AspNetUsers]
(
	[UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AZ_Products] ADD  CONSTRAINT [DF_Products_Id]  DEFAULT (newid()) FOR [Id]
GO
ALTER TABLE [dbo].[AZ_Products] ADD  CONSTRAINT [DF_Products_NumOfFloor]  DEFAULT ((0)) FOR [NumOfFloor]
GO
ALTER TABLE [dbo].[AZ_Products] ADD  CONSTRAINT [DF_Products_NumOfBedroom]  DEFAULT ((0)) FOR [NumOfBedroom]
GO
ALTER TABLE [dbo].[AZ_Products] ADD  CONSTRAINT [DF_Products_NumOfWcs]  DEFAULT ((0)) FOR [NumOfWcs]
GO
ALTER TABLE [dbo].[AZ_Products] ADD  CONSTRAINT [DF_Products_IsVIP]  DEFAULT ((0)) FOR [IsVIP]
GO
ALTER TABLE [dbo].[Orders] ADD  CONSTRAINT [DF_Orders_Id]  DEFAULT (newid()) FOR [Id]
GO
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]
GO
/****** Object:  StoredProcedure [dbo].[AZ_Constant_GetList]    Script Date: 2/24/2020 5:48:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[AZ_Constant_GetList]
-- Add the parameters for the stored procedure here
	
	@name NVARCHAR(200) = '',	
    @filter FilterType READONLY,
    @sort SortType READONLY,
    @skip INT,
    @take INT
AS
BEGIN   

 ------ Filter ------
    DECLARE @NameFilter NVARCHAR(MAX) = dbo.Fn_Filter_GetStringValueField(@filter,'Name');	
    DECLARE @isNameFilter BIT = dbo.Fn_IsFilterField(@filter,'Name');	
    DECLARE @TypeFilter NVARCHAR(MAX) = dbo.Fn_Filter_GetStringValueField(@filter,'Type');	
    DECLARE @isTypeFilter BIT = dbo.Fn_IsFilterField(@filter,'Type');	

	
 ------ End Filter ------

 ------ Sort ------	
	DECLARE @isSortName BIT = dbo.Fn_IsSortField(@sort,'Name');	
    DECLARE @isSortNameASC BIT = dbo.Fn_IsSortFieldAsc(@sort,'Name');
	DECLARE @isSortValue BIT = dbo.Fn_IsSortField(@sort,'Value');	
    DECLARE @isSortValueASC BIT = dbo.Fn_IsSortFieldAsc(@sort,'Value');	

	
  ------ END Sort ------
 
 SELECT a.*	,
		b.Name AS ParentName
  INTO #TMP
  FROM [dbo].[AZ_Constant]	 a
  LEFT JOIN dbo.AZ_Constant b ON  a.ParentId=b.Id
	 
WHERE  1=1	
	AND (@Name IS NULL OR @Name = '' OR (@Name <> '' AND a.[Name] LIKE '%' + @Name + '%' ))	
	AND (@isNameFilter =0 OR(@isNameFilter=1 and a.Name LIKE '%' + @NameFilter + '%' ))				
	AND (@isTypeFilter =0 OR(@isTypeFilter=1 and a.Type LIKE '%' + @TypeFilter + '%' ))	
			

 DECLARE @totalCount INT = (SELECT COUNT(Id) FROM #TMP)

SELECT
   *
    ,@totalCount AS 'Total'
FROM #TMP temp

ORDER BY
	
	CASE WHEN @isSortName = 1 AND @isSortNameASC = 1 THEN temp.[Name]  END ASC,
	CASE WHEN @isSortName = 1 AND @isSortNameASC != 1 THEN temp.[Name]  END DESC,	
	temp.Type ASC,temp.ParentId desc,temp.ValueString,temp.ValueInt,temp.Name
    OFFSET(@skip) ROWS
    FETCH NEXT(@take) ROWS ONLY

 ----- DROP TABLE -----
DROP TABLE #TMP


END


GO
/****** Object:  StoredProcedure [dbo].[AZ_Files_GetList]    Script Date: 2/24/2020 5:48:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[AZ_Files_GetList]
-- Add the parameters for the stored procedure here
	@idFolder INT=0,
	@type INT=0,
	@name NVARCHAR(200) = '',	
    @filter FilterType READONLY,
    @sort SortType READONLY,
    @skip INT,
    @take INT
AS
BEGIN   

 ------ Filter ------
    DECLARE @NameFilter VARCHAR(MAX) = dbo.Fn_Filter_GetStringValueField(@filter,'Name');	
    DECLARE @isNameFilter BIT = dbo.Fn_IsFilterField(@filter,'Name');	
    DECLARE @ValueFilter VARCHAR(MAX) = dbo.Fn_Filter_GetStringValueField(@filter,'Value');	
    DECLARE @isValueFilter BIT = dbo.Fn_IsFilterField(@filter,'Value');	

	
 ------ End Filter ------

 ------ Sort ------	
	DECLARE @isSortName BIT = dbo.Fn_IsSortField(@sort,'Name');	
    DECLARE @isSortNameASC BIT = dbo.Fn_IsSortFieldAsc(@sort,'Name');
	DECLARE @isSortValue BIT = dbo.Fn_IsSortField(@sort,'Value');	
    DECLARE @isSortValueASC BIT = dbo.Fn_IsSortFieldAsc(@sort,'Value');	

	
  ------ END Sort ------
 
 SELECT *
  INTO #TMP
  FROM AZ_Files	 a
	 
WHERE  1=1	 AND Active=1
	AND a.FolderId=@idFolder
	AND Type=@type
	AND (@Name IS NULL OR @Name = '' OR (@Name <> '' AND a.[Name] LIKE '%' + @Name + '%' ))	
	AND (@isNameFilter =0 OR(@isNameFilter=1 and a.Name LIKE '%' + @NameFilter + '%' ))			
			

 DECLARE @totalCount INT = (SELECT COUNT(Id) FROM #TMP)

SELECT
   *
    ,@totalCount AS 'Total'
FROM #TMP temp

ORDER BY
	
	CASE WHEN @isSortName = 1 AND @isSortNameASC = 1 THEN temp.[Name]  END ASC,
	CASE WHEN @isSortName = 1 AND @isSortNameASC != 1 THEN temp.[Name]  END DESC,		
	temp.Id DESC	
    OFFSET(@skip) ROWS
    FETCH NEXT(@take) ROWS ONLY

 ----- DROP TABLE -----
DROP TABLE #TMP


END


GO
/****** Object:  StoredProcedure [dbo].[AZ_GetLocation]    Script Date: 2/24/2020 5:48:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[AZ_GetLocation]
@name NVARCHAR(20),
@id INT =0
AS
DECLARE @sql nvarchar(4000)
SELECT @sql = ' SELECT * FROM ' +@name +' WHERE Active=1 '


IF(@name='VN_Districts')
BEGIN
	IF(@id <> 0)
	SET @sql=@sql+' AND CityId=' +CAST(@id AS varchar(5))
END

IF(@name='VN_Wards')
BEGIN
	IF(@id <> 0)
	SET @sql=@sql+' AND DistrictId=' +CAST(@id AS varchar(5))
END
IF(@name='VN_Streets')
BEGIN
	IF(@id <> 0)
	SET @sql=@sql+' AND DistrictId=' +CAST(@id AS varchar(5))
END
SET @sql=@sql+' ORDER BY Name'
EXECUTE sp_executesql @sql
GO
/****** Object:  StoredProcedure [dbo].[AZ_Products_GetList]    Script Date: 2/24/2020 5:48:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[AZ_Products_GetList]
-- Add the parameters for the stored procedure here	
	@name NVARCHAR(200) = '',	
	@typeTransaction INT,
	@typeProperty INT,
    @filter FilterType READONLY,
    @sort SortType READONLY,
    @skip INT,
    @take INT
AS
BEGIN   

 ------ Filter ------
	DECLARE @IdFilter UNIQUEIDENTIFIER = dbo.Fn_Filter_GetStringValueField(@filter,'Id');	
    DECLARE @isIdFilter BIT = dbo.Fn_IsFilterField(@filter,'Id');
    DECLARE @NameFilter VARCHAR(MAX) = dbo.Fn_Filter_GetStringValueField(@filter,'Name');	
    DECLARE @isNameFilter BIT = dbo.Fn_IsFilterField(@filter,'Name');
	DECLARE @PhoneFilter VARCHAR(MAX) = dbo.Fn_Filter_GetStringValueField(@filter,'Phone');	
    DECLARE @isPhoneFilter BIT = dbo.Fn_IsFilterField(@filter,'Phone');	
    DECLARE @EmailFilter VARCHAR(MAX) = dbo.Fn_Filter_GetStringValueField(@filter,'Email');	
    DECLARE @isEmailFilter BIT = dbo.Fn_IsFilterField(@filter,'Email');	
	DECLARE @NoFilter VARCHAR(MAX) = dbo.Fn_Filter_GetStringValueField(@filter,'No');	
    DECLARE @isNoFilter BIT = dbo.Fn_IsFilterField(@filter,'No');	
	DECLARE @StatusFilter VARCHAR(MAX) = dbo.Fn_Filter_GetStringValueField(@filter,'Status');	
    DECLARE @isStatusFilter BIT = dbo.Fn_IsFilterField(@filter,'Status');	
	DECLARE @IsVIPFilter BIT = dbo.Fn_Filter_GetStringValueField(@filter,'IsVIP');	
    DECLARE @isIsVIPFilter BIT = dbo.Fn_IsFilterField(@filter,'IsVIP');	
	DECLARE @IsActiveFilter BIT = dbo.Fn_Filter_GetStringValueField(@filter,'IsActive');	
    DECLARE @isIsActiveFilter BIT = dbo.Fn_IsFilterField(@filter,'IsActive');	
	DECLARE @ProjectIdFilter INT = dbo.Fn_Filter_GetStringValueField(@filter,'ProjectId');	
    DECLARE @isProjectIdFilter BIT = dbo.Fn_IsFilterField(@filter,'ProjectId');


	DECLARE @BedroomFilter INT = dbo.Fn_Filter_GetStringValueField(@filter,'Bedroom');	
    DECLARE @isBedroomFilter BIT = dbo.Fn_IsFilterField(@filter,'Bedroom');
	DECLARE @PriceRangeMinFilter INT = dbo.Fn_Filter_GetStringValueField(@filter,'PriceRangeMin');	
	DECLARE @PriceRangeMaxFilter INT = dbo.Fn_Filter_GetStringValueField(@filter,'PriceRangeMax');	
    DECLARE @isPriceRangeFilter BIT = dbo.Fn_IsFilterField(@filter,'PriceRangeMin');
	DECLARE @CityFilter INT = dbo.Fn_Filter_GetStringValueField(@filter,'City');	
    DECLARE @isCityFilter BIT = dbo.Fn_IsFilterField(@filter,'City');
	DECLARE @DistrictFilter INT = dbo.Fn_Filter_GetStringValueField(@filter,'District');	
    DECLARE @isDistrictFilter BIT = dbo.Fn_IsFilterField(@filter,'District');
	DECLARE @WardFilter INT = dbo.Fn_Filter_GetStringValueField(@filter,'Ward');	
    DECLARE @isWardFilter BIT = dbo.Fn_IsFilterField(@filter,'Ward');
	DECLARE @StreetFilter INT = dbo.Fn_Filter_GetStringValueField(@filter,'Street');	
    DECLARE @isStreetFilter BIT = dbo.Fn_IsFilterField(@filter,'Street');
	DECLARE @HouseDirectionFilter INT = dbo.Fn_Filter_GetStringValueField(@filter,'HouseDirection');	
    DECLARE @isHouseDirectionFilter BIT = dbo.Fn_IsFilterField(@filter,'HouseDirection');
	DECLARE @AreaMinFilter INT = dbo.Fn_Filter_GetStringValueField(@filter,'AreaMin');	
	DECLARE @AreaMaxFilter INT = dbo.Fn_Filter_GetStringValueField(@filter,'AreaMax');	
    DECLARE @isAreaFilter BIT = dbo.Fn_IsFilterField(@filter,'AreaMin');
	

	


	
 ------ End Filter ------

 ------ Sort ------	
	DECLARE @isSortName BIT = dbo.Fn_IsSortField(@sort,'Name');	
    DECLARE @isSortNameASC BIT = dbo.Fn_IsSortFieldAsc(@sort,'Name');
	DECLARE @isSortPrice BIT = dbo.Fn_IsSortField(@sort,'Price');	
    DECLARE @isSortPriceASC BIT = dbo.Fn_IsSortFieldAsc(@sort,'Price');
	DECLARE @isSortNo BIT = dbo.Fn_IsSortField(@sort,'No');	
    DECLARE @isSortNoASC BIT = dbo.Fn_IsSortFieldAsc(@sort,'No');	

	
  ------ END Sort ------

SELECT * INTO #TypeTransaction FROM dbo.AZ_Constant WHERE Type='TypeTransaction'
SELECT * INTO #TypeProperty FROM dbo.AZ_Constant WHERE Type='TypeProperty'
 
 SELECT m.[Id]
      ,m.[Name]
      ,m.[TypeTransaction]
	  ,#TypeTransaction.Name AS TypeTransactionName
      ,m.[TypeProperty]
	  ,#TypeProperty.Name AS TypePropertyName
	  ,m.ProjectId
      ,m.[City]
      ,m.[District]
      ,m.[Ward]
      ,m.[Street]
      ,m.[Area]
      ,m.[Price]
      ,m.[Unit]
      ,m.[Address]
      ,m.[HouseDirection]
      ,m.[BalconyDirection]
      ,m.[NumOfFloor]
      ,m.[NumOfBedroom]
      ,m.[NumOfWcs]
      ,m.[Summary]
      ,m.[CreatedDate]
      ,m.[CreatedBy]
      ,m.[ModifiedDate]
      ,m.[ModifiedBy]
      ,m.[IsActive]
      ,m.[IsDelete]
	  ,m.ImageMain
	  ,m.ImageList
	  ,m.IsVIP
	  ,c.Name AS CityName
	  ,d.Name AS DistrictName
	  ,w.Name AS WardName
	  ,stre.Name AS StreetName
	  ,cs.Name AS UnitName
	  ,pr.Name AS ProjectName	  
  INTO #TMP
  FROM [dbo].[AZ_Products]	 m
  LEFT JOIN dbo.AZ_Projects pr ON pr.Id=m.ProjectId
  LEFT JOIN dbo.VN_City c ON c.Id=m.City
  LEFT JOIN dbo.VN_Districts d ON d.Id=m.District
  LEFT JOIN dbo.VN_Wards w ON w.Id=m.Ward
  LEFT JOIN dbo.VN_Streets stre ON stre.Id=m.Street
  LEFT JOIN dbo.AZ_Constant cs ON cs.Id=m.Unit AND cs.Type='Unit' AND cs.ParentId=m.TypeTransaction
  LEFT JOIN #TypeProperty ON  #TypeProperty.Id=m.TypeProperty AND #TypeProperty.ParentId=m.TypeTransaction
  LEFT JOIN #TypeTransaction ON  #TypeTransaction.Id=m.TypeTransaction 
	 
WHERE  1=1	
	AND (@isIdFilter =0 OR(@isIdFilter=1 and m.Id =@IdFilter))	
	AND (@Name IS NULL OR @Name = '' OR (@Name <> '' AND m.[Name] LIKE '%' + @Name + '%' ))
	AND (m.IsDelete IS NULL OR m.IsDelete=0)		
	AND (@isNameFilter =0 OR(@isNameFilter=1 and m.Name LIKE '%' + @NameFilter + '%' ))	
	AND (@typeTransaction=0 OR (@typeTransaction<>0 AND m.[TypeTransaction]=@typeTransaction))
	AND (@typeProperty=0 OR (@typeProperty<>0 AND m.[TypeProperty]=@typeProperty))
	AND (@isIsVIPFilter =0 OR(@isIsVIPFilter=1 and m.IsVIP=@IsVIPFilter))	
	AND (@isIsActiveFilter =0 OR(@isIsActiveFilter=1 and m.IsActive=@IsActiveFilter))	
	AND (@isBedroomFilter =0 OR(@isBedroomFilter=1 and m.NumOfBedroom >=@BedroomFilter))	
	AND (@isCityFilter =0 OR(@isCityFilter=1 and m.City =@CityFilter))
	AND (@isDistrictFilter =0 OR(@isDistrictFilter=1 and m.District =@DistrictFilter))
	AND (@isWardFilter =0 OR(@isWardFilter=1 and m.Ward =@WardFilter))
	AND (@isStreetFilter =0 OR(@isStreetFilter=1 and m.Street =@StreetFilter))
	AND (@isHouseDirectionFilter =0 OR(@isHouseDirectionFilter=1 and m.Ward =@HouseDirectionFilter))
	AND (@isPriceRangeFilter =0 OR(@isPriceRangeFilter=1 and m.Price >=@PriceRangeMinFilter ANd m.Price <=@PriceRangeMaxFilter))
	AND (@isAreaFilter =0 OR(@isAreaFilter=1 and m.Area >=@AreaMinFilter ANd m.Area <=@AreaMaxFilter))
	AND (@isProjectIdFilter =0 OR(@isProjectIdFilter=1 and m.ProjectId =@ProjectIdFilter))
	

 DECLARE @totalCount INT = (SELECT COUNT(Id) FROM #TMP)

SELECT
   *
    ,@totalCount AS 'Total'
FROM #TMP temp

ORDER BY
	
	CASE WHEN @isSortName = 1 AND @isSortNameASC = 1 THEN temp.[Name]  END ASC,
	CASE WHEN @isSortName = 1 AND @isSortNameASC != 1 THEN temp.[Name]  END DESC,
	CASE WHEN @isSortPrice = 1 AND @isSortPriceASC = 1 THEN temp.[Price]  END ASC,
	CASE WHEN @isSortPrice = 1 AND @isSortPriceASC != 1 THEN temp.[Price]  END DESC,
	temp.ModifiedDate DESC	
    OFFSET(@skip) ROWS
    FETCH NEXT(@take) ROWS ONLY

 ----- DROP TABLE -----
DROP TABLE #TMP,#TypeTransaction,#TypeProperty


END


GO
/****** Object:  StoredProcedure [dbo].[AZ_Projects_GetList]    Script Date: 2/24/2020 5:48:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[AZ_Projects_GetList]
-- Add the parameters for the stored procedure here
	
	@name NVARCHAR(200) = '',	
    @filter FilterType READONLY,
    @sort SortType READONLY,
    @skip INT,
    @take INT
AS
BEGIN   

 ------ Filter ------
    DECLARE @NameFilter NVARCHAR(MAX) = dbo.Fn_Filter_GetStringValueField(@filter,'Name');	
    DECLARE @isNameFilter BIT = dbo.Fn_IsFilterField(@filter,'Name');	
    DECLARE @TypeFilter NVARCHAR(MAX) = dbo.Fn_Filter_GetStringValueField(@filter,'Type');	
    DECLARE @isTypeFilter BIT = dbo.Fn_IsFilterField(@filter,'Type');	

	
 ------ End Filter ------

 ------ Sort ------	
	DECLARE @isSortName BIT = dbo.Fn_IsSortField(@sort,'Name');	
    DECLARE @isSortNameASC BIT = dbo.Fn_IsSortFieldAsc(@sort,'Name');
	DECLARE @isSortValue BIT = dbo.Fn_IsSortField(@sort,'Value');	
    DECLARE @isSortValueASC BIT = dbo.Fn_IsSortFieldAsc(@sort,'Value');	

	
  ------ END Sort ------
 
 SELECT a.*			
  INTO #TMP
  FROM [dbo].[AZ_Projects]	 a
  
	 
WHERE  1=1	
	AND (@Name IS NULL OR @Name = '' OR (@Name <> '' AND a.[Name] LIKE '%' + @Name + '%' ))	
	AND (@isNameFilter =0 OR(@isNameFilter=1 and a.Name LIKE '%' + @NameFilter + '%' ))					
			

 DECLARE @totalCount INT = (SELECT COUNT(Id) FROM #TMP)

SELECT
   *
    ,@totalCount AS 'Total'
FROM #TMP temp

ORDER BY
	
	CASE WHEN @isSortName = 1 AND @isSortNameASC = 1 THEN temp.[Name]  END ASC,
	CASE WHEN @isSortName = 1 AND @isSortNameASC != 1 THEN temp.[Name]  END DESC,
	temp.ModifiedDate desc
    OFFSET(@skip) ROWS
    FETCH NEXT(@take) ROWS ONLY

 ----- DROP TABLE -----
DROP TABLE #TMP


END


GO
/****** Object:  StoredProcedure [dbo].[AZ_Services_GetList]    Script Date: 2/24/2020 5:48:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[AZ_Services_GetList]
-- Add the parameters for the stored procedure here	
	@name NVARCHAR(200) = '',	
    @filter FilterType READONLY,
    @sort SortType READONLY,
    @skip INT,
    @take INT
AS
BEGIN   

 ------ Filter ------
    DECLARE @NameFilter VARCHAR(MAX) = dbo.Fn_Filter_GetStringValueField(@filter,'Name');	
    DECLARE @isNameFilter BIT = dbo.Fn_IsFilterField(@filter,'Name');	
    DECLARE @EmailFilter VARCHAR(MAX) = dbo.Fn_Filter_GetStringValueField(@filter,'Email');	
    DECLARE @isEmailFilter BIT = dbo.Fn_IsFilterField(@filter,'Email');	
	DECLARE @NoFilter VARCHAR(MAX) = dbo.Fn_Filter_GetStringValueField(@filter,'No');	
    DECLARE @isNoFilter BIT = dbo.Fn_IsFilterField(@filter,'No');	

	
 ------ End Filter ------

 ------ Sort ------	
	DECLARE @isSortName BIT = dbo.Fn_IsSortField(@sort,'Name');	
    DECLARE @isSortNameASC BIT = dbo.Fn_IsSortFieldAsc(@sort,'Name');
	DECLARE @isSortNo BIT = dbo.Fn_IsSortField(@sort,'No');	
    DECLARE @isSortNoASC BIT = dbo.Fn_IsSortFieldAsc(@sort,'No');
	DECLARE @isSortOrders BIT = dbo.Fn_IsSortField(@sort,'Orders');	
    DECLARE @isSortOrdersASC BIT = dbo.Fn_IsSortFieldAsc(@sort,'Orders');

	
  ------ END Sort ------
 
 SELECT a.[Id]
      ,a.[Name]
      ,a.[Image]
      ,a.[Title]
      ,a.[NumberView]
      ,a.[Orders]
      ,a.[Summary]
      ,a.[Active]
      ,a.[CreatedBy]
      ,a.[CreatedDate]
      ,a.[ModifiedBy]
      ,a.[ModifiedDate]
  INTO #TMP
  FROM [dbo].[AZ_Services]	a 
	 
WHERE  1=1	
	AND (@Name IS NULL OR @Name = '' OR (@Name <> '' AND (a.[Name] LIKE '%' + @Name + '%'   OR a.[Title] LIKE '%' + @Name + '%') ))
	AND (a.Active IS NULL OR a.Active=1)	
	AND (@isNameFilter =0 OR(@isNameFilter=1 and a.Name LIKE '%' + @NameFilter + '%' ))
			
	
	

 DECLARE @totalCount INT = (SELECT COUNT(Id) FROM #TMP)

SELECT
   *
    ,@totalCount AS 'Total'
FROM #TMP temp

ORDER BY
	
	CASE WHEN @isSortName = 1 AND @isSortNameASC = 1 THEN temp.[Name]  END ASC,
	CASE WHEN @isSortName = 1 AND @isSortNameASC != 1 THEN temp.[Name]  END DESC,	
	CASE WHEN @isSortOrders = 1 AND @isSortOrdersASC = 1 THEN temp.Orders  END ASC,
	CASE WHEN @isSortOrders = 1 AND @isSortOrdersASC != 1 THEN temp.Orders  END DESC,
	temp.Orders,temp.[ModifiedDate] DESC	
    OFFSET(@skip) ROWS
    FETCH NEXT(@take) ROWS ONLY

 ----- DROP TABLE -----
DROP TABLE #TMP


END


GO
/****** Object:  StoredProcedure [dbo].[Constant_GetList]    Script Date: 2/24/2020 5:48:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[Constant_GetList]
-- Add the parameters for the stored procedure here
	
	@name NVARCHAR(200) = '',	
    @filter FilterType READONLY,
    @sort SortType READONLY,
    @skip INT,
    @take INT
AS
BEGIN   

 ------ Filter ------
    DECLARE @NameFilter VARCHAR(MAX) = dbo.Fn_Filter_GetStringValueField(@filter,'Name');	
    DECLARE @isNameFilter BIT = dbo.Fn_IsFilterField(@filter,'Name');	
    DECLARE @ValueFilter VARCHAR(MAX) = dbo.Fn_Filter_GetStringValueField(@filter,'Value');	
    DECLARE @isValueFilter BIT = dbo.Fn_IsFilterField(@filter,'Value');	

	
 ------ End Filter ------

 ------ Sort ------	
	DECLARE @isSortName BIT = dbo.Fn_IsSortField(@sort,'Name');	
    DECLARE @isSortNameASC BIT = dbo.Fn_IsSortFieldAsc(@sort,'Name');
	DECLARE @isSortValue BIT = dbo.Fn_IsSortField(@sort,'Value');	
    DECLARE @isSortValueASC BIT = dbo.Fn_IsSortFieldAsc(@sort,'Value');	

	
  ------ END Sort ------
 
 SELECT *
  INTO #TMP
  FROM [dbo].[Constant]	 a
	 
WHERE  1=1	
	AND (@Name IS NULL OR @Name = '' OR (@Name <> '' AND a.[Name] LIKE '%' + @Name + '%' ))	
	AND (@isNameFilter =0 OR(@isNameFilter=1 and a.Name LIKE '%' + @NameFilter + '%' ))
	AND (@isValueFilter =0 OR(@isValueFilter=1 and a.Value LIKE '%' + @ValueFilter + '%' ))				
			

 DECLARE @totalCount INT = (SELECT COUNT(Id) FROM #TMP)

SELECT
   *
    ,@totalCount AS 'Total'
FROM #TMP temp

ORDER BY
	
	CASE WHEN @isSortName = 1 AND @isSortNameASC = 1 THEN temp.[Name]  END ASC,
	CASE WHEN @isSortName = 1 AND @isSortNameASC != 1 THEN temp.[Name]  END DESC,	
	CASE WHEN @isSortValue = 1 AND @isSortValueASC = 1 THEN temp.[Value]  END ASC,
	CASE WHEN @isSortValue = 1 AND @isSortValueASC != 1 THEN temp.[Value]  END DESC,	
	temp.Id DESC	
    OFFSET(@skip) ROWS
    FETCH NEXT(@take) ROWS ONLY

 ----- DROP TABLE -----
DROP TABLE #TMP


END


GO
/****** Object:  StoredProcedure [dbo].[Files_GetList]    Script Date: 2/24/2020 5:48:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Files_GetList]
-- Add the parameters for the stored procedure here
	@type INT=0,
	@name NVARCHAR(200) = '',	
    @filter FilterType READONLY,
    @sort SortType READONLY,
    @skip INT,
    @take INT
AS
BEGIN   

 ------ Filter ------
    DECLARE @NameFilter VARCHAR(MAX) = dbo.Fn_Filter_GetStringValueField(@filter,'Name');	
    DECLARE @isNameFilter BIT = dbo.Fn_IsFilterField(@filter,'Name');	
    DECLARE @ValueFilter VARCHAR(MAX) = dbo.Fn_Filter_GetStringValueField(@filter,'Value');	
    DECLARE @isValueFilter BIT = dbo.Fn_IsFilterField(@filter,'Value');	

	
 ------ End Filter ------

 ------ Sort ------	
	DECLARE @isSortName BIT = dbo.Fn_IsSortField(@sort,'Name');	
    DECLARE @isSortNameASC BIT = dbo.Fn_IsSortFieldAsc(@sort,'Name');
	DECLARE @isSortValue BIT = dbo.Fn_IsSortField(@sort,'Value');	
    DECLARE @isSortValueASC BIT = dbo.Fn_IsSortFieldAsc(@sort,'Value');	

	
  ------ END Sort ------
 
 SELECT *
  INTO #TMP
  FROM Files	 a
	 
WHERE  1=1	 AND Active=1
	AND Type=@type
	AND (@Name IS NULL OR @Name = '' OR (@Name <> '' AND a.[Name] LIKE '%' + @Name + '%' ))	
	AND (@isNameFilter =0 OR(@isNameFilter=1 and a.Name LIKE '%' + @NameFilter + '%' ))			
			

 DECLARE @totalCount INT = (SELECT COUNT(Id) FROM #TMP)

SELECT
   *
    ,@totalCount AS 'Total'
FROM #TMP temp

ORDER BY
	
	CASE WHEN @isSortName = 1 AND @isSortNameASC = 1 THEN temp.[Name]  END ASC,
	CASE WHEN @isSortName = 1 AND @isSortNameASC != 1 THEN temp.[Name]  END DESC,		
	temp.Id DESC	
    OFFSET(@skip) ROWS
    FETCH NEXT(@take) ROWS ONLY

 ----- DROP TABLE -----
DROP TABLE #TMP


END


GO
/****** Object:  StoredProcedure [dbo].[OrderLink_GetList]    Script Date: 2/24/2020 5:48:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[OrderLink_GetList]
-- Add the parameters for the stored procedure here
	@idOrders UNIQUEIDENTIFIER,
	@name NVARCHAR(200) = '',	
    @filter FilterType READONLY,
    @sort SortType READONLY,
    @skip INT,
    @take INT
AS
BEGIN   

 ------ Filter ------
    DECLARE @NameFilter VARCHAR(MAX) = dbo.Fn_Filter_GetStringValueField(@filter,'Name');	
    DECLARE @isNameFilter BIT = dbo.Fn_IsFilterField(@filter,'Name');	
    DECLARE @EmailFilter VARCHAR(MAX) = dbo.Fn_Filter_GetStringValueField(@filter,'Email');	
    DECLARE @isEmailFilter BIT = dbo.Fn_IsFilterField(@filter,'Email');	
	DECLARE @NoFilter VARCHAR(MAX) = dbo.Fn_Filter_GetStringValueField(@filter,'No');	
    DECLARE @isNoFilter BIT = dbo.Fn_IsFilterField(@filter,'No');	

	
 ------ End Filter ------

 ------ Sort ------	
	DECLARE @isSortName BIT = dbo.Fn_IsSortField(@sort,'Name');	
    DECLARE @isSortNameASC BIT = dbo.Fn_IsSortFieldAsc(@sort,'Name');
	DECLARE @isSortNo BIT = dbo.Fn_IsSortField(@sort,'No');	
    DECLARE @isSortNoASC BIT = dbo.Fn_IsSortFieldAsc(@sort,'No');	

	
  ------ END Sort ------
 
 SELECT *
  INTO #TMP
  FROM [dbo].[OrderLink] a   
	 
WHERE  1=1	
	AND OrderId=@idOrders		
	

 DECLARE @totalCount INT = (SELECT COUNT(Id) FROM #TMP)

SELECT
   *
    ,@totalCount AS 'Total'
FROM #TMP temp

ORDER BY		
	temp.Quantity DESC	
    OFFSET(@skip) ROWS
    FETCH NEXT(@take) ROWS ONLY

 ----- DROP TABLE -----
DROP TABLE #TMP


END


GO
/****** Object:  StoredProcedure [dbo].[Orders_GetList]    Script Date: 2/24/2020 5:48:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Orders_GetList]
-- Add the parameters for the stored procedure here
	@type NVARCHAR(200) = '',
	@name NVARCHAR(200) = '',	
    @filter FilterType READONLY,
    @sort SortType READONLY,
    @skip INT,
    @take INT
AS
BEGIN   

 ------ Filter ------
    DECLARE @NameFilter VARCHAR(MAX) = dbo.Fn_Filter_GetStringValueField(@filter,'Name');	
    DECLARE @isNameFilter BIT = dbo.Fn_IsFilterField(@filter,'Name');
	DECLARE @PhoneFilter VARCHAR(MAX) = dbo.Fn_Filter_GetStringValueField(@filter,'Phone');	
    DECLARE @isPhoneFilter BIT = dbo.Fn_IsFilterField(@filter,'Phone');	
    DECLARE @EmailFilter VARCHAR(MAX) = dbo.Fn_Filter_GetStringValueField(@filter,'Email');	
    DECLARE @isEmailFilter BIT = dbo.Fn_IsFilterField(@filter,'Email');	
	DECLARE @NoFilter VARCHAR(MAX) = dbo.Fn_Filter_GetStringValueField(@filter,'No');	
    DECLARE @isNoFilter BIT = dbo.Fn_IsFilterField(@filter,'No');	
	DECLARE @StatusFilter VARCHAR(MAX) = dbo.Fn_Filter_GetStringValueField(@filter,'Status');	
    DECLARE @isStatusFilter BIT = dbo.Fn_IsFilterField(@filter,'Status');	

	
 ------ End Filter ------

 ------ Sort ------	
	DECLARE @isSortName BIT = dbo.Fn_IsSortField(@sort,'Name');	
    DECLARE @isSortNameASC BIT = dbo.Fn_IsSortFieldAsc(@sort,'Name');
	DECLARE @isSortNo BIT = dbo.Fn_IsSortField(@sort,'No');	
    DECLARE @isSortNoASC BIT = dbo.Fn_IsSortFieldAsc(@sort,'No');	

	
  ------ END Sort ------
 
 SELECT a.[Id]
      ,a.[No]
	  ,a.[Link]
      ,a.[Name]
      ,a.[Phone]
      ,a.[Email]
      ,a.[Address]
      ,a.[Summary]
      ,a.[CreatedDate]
      ,a.[Status]
	  ,os.Name AS StatusString
      ,a.[Note]
      ,a.[IsDelete]
  INTO #TMP
  FROM [dbo].[Orders]	 a
  LEFT JOIN dbo.OrderStatus os ON os.Id=a.[Status] AND os.Active=1  
	 
WHERE  1=1	
	AND (@Name IS NULL OR @Name = '' OR (@Name <> '' AND a.[No] LIKE '%' + @Name + '%' ))
	AND (a.IsDelete IS NULL OR a.IsDelete=0)	
	AND (@isNameFilter =0 OR(@isNameFilter=1 and a.Name LIKE '%' + @NameFilter + '%' ))
	AND (@isEmailFilter =0 OR(@isEmailFilter=1 and a.Email LIKE '%' + @EmailFilter + '%' ))				
	AND (@isPhoneFilter =0 OR(@isPhoneFilter=1 and a.Phone LIKE '%' + @PhoneFilter + '%' ))
	AND (@isNoFilter =0 OR(@isNoFilter=1 and a.No LIKE '%' + @NoFilter + '%' ))					
	AND (@isStatusFilter =0 OR(@isStatusFilter=1 and a.Status in (SELECT Result FROM DBO.[UF_StrToTableInt](@StatusFilter,';#')  )))				
	AND ((@type ='file' AND  a.Link IS NOT null) OR (@type = 'link' AND a.Link IS null))
	

 DECLARE @totalCount INT = (SELECT COUNT(Id) FROM #TMP)

SELECT
   *
    ,@totalCount AS 'Total'
FROM #TMP temp

ORDER BY
	
	CASE WHEN @isSortName = 1 AND @isSortNameASC = 1 THEN temp.[Name]  END ASC,
	CASE WHEN @isSortName = 1 AND @isSortNameASC != 1 THEN temp.[Name]  END DESC,	
	CASE WHEN @isSortNo = 1 AND @isSortNoASC = 1 THEN temp.[No]  END ASC,
	CASE WHEN @isSortNo = 1 AND @isSortNoASC != 1 THEN temp.[No]  END DESC,	
	temp.CreatedDate DESC	
    OFFSET(@skip) ROWS
    FETCH NEXT(@take) ROWS ONLY

 ----- DROP TABLE -----
DROP TABLE #TMP


END


GO
/****** Object:  StoredProcedure [dbo].[Services_GetList]    Script Date: 2/24/2020 5:48:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Services_GetList]
-- Add the parameters for the stored procedure here	
	@name NVARCHAR(200) = '',	
    @filter FilterType READONLY,
    @sort SortType READONLY,
    @skip INT,
    @take INT
AS
BEGIN   

 ------ Filter ------
    DECLARE @NameFilter VARCHAR(MAX) = dbo.Fn_Filter_GetStringValueField(@filter,'Name');	
    DECLARE @isNameFilter BIT = dbo.Fn_IsFilterField(@filter,'Name');	
    DECLARE @EmailFilter VARCHAR(MAX) = dbo.Fn_Filter_GetStringValueField(@filter,'Email');	
    DECLARE @isEmailFilter BIT = dbo.Fn_IsFilterField(@filter,'Email');	
	DECLARE @NoFilter VARCHAR(MAX) = dbo.Fn_Filter_GetStringValueField(@filter,'No');	
    DECLARE @isNoFilter BIT = dbo.Fn_IsFilterField(@filter,'No');	

	
 ------ End Filter ------

 ------ Sort ------	
	DECLARE @isSortName BIT = dbo.Fn_IsSortField(@sort,'Name');	
    DECLARE @isSortNameASC BIT = dbo.Fn_IsSortFieldAsc(@sort,'Name');
	DECLARE @isSortNo BIT = dbo.Fn_IsSortField(@sort,'No');	
    DECLARE @isSortNoASC BIT = dbo.Fn_IsSortFieldAsc(@sort,'No');	

	
  ------ END Sort ------
 
 SELECT a.[Id]
      ,a.[Name]
      ,a.[Image]
      ,a.[Title]
      ,a.[NumberView]
      ,a.[Orders]
      ,a.[Summary]
      ,a.[Active]
      ,a.[CreatedBy]
      ,a.[CreatedDate]
      ,a.[ModifiedBy]
      ,a.[ModifiedDate]
  INTO #TMP
  FROM [dbo].[Services]	a 
	 
WHERE  1=1	
	AND (@Name IS NULL OR @Name = '' OR (@Name <> '' AND (a.[Name] LIKE '%' + @Name + '%'   OR a.[Title] LIKE '%' + @Name + '%') ))
	AND (a.Active IS NULL OR a.Active=1)	
	AND (@isNameFilter =0 OR(@isNameFilter=1 and a.Name LIKE '%' + @NameFilter + '%' ))
			
	
	

 DECLARE @totalCount INT = (SELECT COUNT(Id) FROM #TMP)

SELECT
   *
    ,@totalCount AS 'Total'
FROM #TMP temp

ORDER BY
	
	CASE WHEN @isSortName = 1 AND @isSortNameASC = 1 THEN temp.[Name]  END ASC,
	CASE WHEN @isSortName = 1 AND @isSortNameASC != 1 THEN temp.[Name]  END DESC,	
	temp.[ModifiedDate] DESC	
    OFFSET(@skip) ROWS
    FETCH NEXT(@take) ROWS ONLY

 ----- DROP TABLE -----
DROP TABLE #TMP


END


GO
USE [master]
GO
ALTER DATABASE [dathangtb_com_trong5] SET  READ_WRITE 
GO
