USE [BanNhaAZ]
GO
/****** Object:  Table [dbo].[AZ_Constant]    Script Date: 1/20/2020 11:18:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AZ_Constant](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Type] [nchar](20) NULL,
	[Name] [nvarchar](100) NULL,
	[ValueString] [nvarchar](100) NULL,
	[ValueInt] [int] NULL,
	[ParentId] [int] NULL,
 CONSTRAINT [PK_Constant] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AZ_Contents]    Script Date: 1/20/2020 11:18:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AZ_Contents](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NULL,
	[Content] [nvarchar](max) NULL,
 CONSTRAINT [PK_Content] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AZ_Files]    Script Date: 1/20/2020 11:18:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AZ_Files](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FolderId] [int] NULL,
	[Name] [nvarchar](500) NULL,
	[Link] [nchar](500) NULL,
	[Thumbail] [nchar](50) NULL,
	[Active] [bit] NULL,
	[CreatedDate] [datetime] NULL,
	[Type] [int] NULL,
 CONSTRAINT [PK_Files] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AZ_Folder]    Script Date: 1/20/2020 11:18:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AZ_Folder](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[ParentId] [int] NULL,
	[IsActive] [bit] NULL,
	[Orders] [int] NULL,
 CONSTRAINT [PK_Folder] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AZ_Products]    Script Date: 1/20/2020 11:18:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AZ_Products](
	[Id] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](200) NOT NULL,
	[TypeTransaction] [int] NOT NULL,
	[TypeProperty] [int] NOT NULL,
	[City] [int] NOT NULL,
	[District] [int] NOT NULL,
	[Ward] [int] NULL,
	[Street] [int] NULL,
	[Area] [float] NULL,
	[Price] [float] NULL,
	[Unit] [int] NULL,
	[Address] [nvarchar](200) NULL,
	[HouseDirection] [int] NULL,
	[BalconyDirection] [int] NULL,
	[NumOfFloor] [int] NULL,
	[NumOfBedroom] [int] NULL,
	[NumOfWcs] [int] NULL,
	[ImageMain] [nvarchar](200) NULL,
	[ImageList] [nvarchar](max) NULL,
	[Summary] [nvarchar](max) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [nchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [nchar](50) NULL,
	[IsActive] [bit] NULL,
	[IsDelete] [bit] NULL,
	[DeletedDate] [datetime] NULL,
	[IsVIP] [bit] NULL,
 CONSTRAINT [PK_Products] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AZ_Services]    Script Date: 1/20/2020 11:18:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AZ_Services](
	[Id] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](250) NULL,
	[Image] [varchar](250) NULL,
	[Title] [nvarchar](1000) NULL,
	[NumberView] [int] NULL,
	[Orders] [int] NULL,
	[Summary] [nvarchar](max) NULL,
	[Active] [bit] NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_Services] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[VN_City]    Script Date: 1/20/2020 11:18:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VN_City](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NULL,
	[Active] [bit] NULL,
 CONSTRAINT [PK_VN_City] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[VN_Districts]    Script Date: 1/20/2020 11:18:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VN_Districts](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NULL,
	[Active] [bit] NULL,
	[CityId] [int] NULL,
 CONSTRAINT [PK_District] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[VN_Wards]    Script Date: 1/20/2020 11:18:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VN_Wards](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NULL,
	[Active] [bit] NULL,
	[CityId] [int] NULL,
 CONSTRAINT [PK_VN_Wards] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AZ_Products] ADD  CONSTRAINT [DF_Products_Id]  DEFAULT (newid()) FOR [Id]
GO
ALTER TABLE [dbo].[AZ_Products] ADD  CONSTRAINT [DF_Products_NumOfFloor]  DEFAULT ((0)) FOR [NumOfFloor]
GO
ALTER TABLE [dbo].[AZ_Products] ADD  CONSTRAINT [DF_Products_NumOfBedroom]  DEFAULT ((0)) FOR [NumOfBedroom]
GO
ALTER TABLE [dbo].[AZ_Products] ADD  CONSTRAINT [DF_Products_NumOfWcs]  DEFAULT ((0)) FOR [NumOfWcs]
GO
ALTER TABLE [dbo].[AZ_Products] ADD  CONSTRAINT [DF_Products_IsVIP]  DEFAULT ((0)) FOR [IsVIP]
GO
/****** Object:  StoredProcedure [dbo].[AZ_Constant_GetList]    Script Date: 1/20/2020 11:18:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[AZ_Constant_GetList]
-- Add the parameters for the stored procedure here
	
	@name NVARCHAR(200) = '',	
    @filter FilterType READONLY,
    @sort SortType READONLY,
    @skip INT,
    @take INT
AS
BEGIN   

 ------ Filter ------
    DECLARE @NameFilter NVARCHAR(MAX) = dbo.Fn_Filter_GetStringValueField(@filter,'Name');	
    DECLARE @isNameFilter BIT = dbo.Fn_IsFilterField(@filter,'Name');	
    DECLARE @TypeFilter NVARCHAR(MAX) = dbo.Fn_Filter_GetStringValueField(@filter,'Type');	
    DECLARE @isTypeFilter BIT = dbo.Fn_IsFilterField(@filter,'Type');	

	
 ------ End Filter ------

 ------ Sort ------	
	DECLARE @isSortName BIT = dbo.Fn_IsSortField(@sort,'Name');	
    DECLARE @isSortNameASC BIT = dbo.Fn_IsSortFieldAsc(@sort,'Name');
	DECLARE @isSortValue BIT = dbo.Fn_IsSortField(@sort,'Value');	
    DECLARE @isSortValueASC BIT = dbo.Fn_IsSortFieldAsc(@sort,'Value');	

	
  ------ END Sort ------
 
 SELECT a.*	,
		b.Name AS ParentName
  INTO #TMP
  FROM [dbo].[AZ_Constant]	 a
  LEFT JOIN dbo.AZ_Constant b ON  a.ParentId=b.Id
	 
WHERE  1=1	
	AND (@Name IS NULL OR @Name = '' OR (@Name <> '' AND a.[Name] LIKE '%' + @Name + '%' ))	
	AND (@isNameFilter =0 OR(@isNameFilter=1 and a.Name LIKE '%' + @NameFilter + '%' ))				
	AND (@isTypeFilter =0 OR(@isTypeFilter=1 and a.Type LIKE '%' + @TypeFilter + '%' ))	
			

 DECLARE @totalCount INT = (SELECT COUNT(Id) FROM #TMP)

SELECT
   *
    ,@totalCount AS 'Total'
FROM #TMP temp

ORDER BY
	
	CASE WHEN @isSortName = 1 AND @isSortNameASC = 1 THEN temp.[Name]  END ASC,
	CASE WHEN @isSortName = 1 AND @isSortNameASC != 1 THEN temp.[Name]  END DESC,	
	temp.Type ASC,temp.ParentId desc,temp.ValueString,temp.ValueInt,temp.Name
    OFFSET(@skip) ROWS
    FETCH NEXT(@take) ROWS ONLY

 ----- DROP TABLE -----
DROP TABLE #TMP


END


GO
/****** Object:  StoredProcedure [dbo].[AZ_Files_GetList]    Script Date: 1/20/2020 11:18:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[AZ_Files_GetList]
-- Add the parameters for the stored procedure here
	@idFolder INT=0,
	@type INT=0,
	@name NVARCHAR(200) = '',	
    @filter FilterType READONLY,
    @sort SortType READONLY,
    @skip INT,
    @take INT
AS
BEGIN   

 ------ Filter ------
    DECLARE @NameFilter VARCHAR(MAX) = dbo.Fn_Filter_GetStringValueField(@filter,'Name');	
    DECLARE @isNameFilter BIT = dbo.Fn_IsFilterField(@filter,'Name');	
    DECLARE @ValueFilter VARCHAR(MAX) = dbo.Fn_Filter_GetStringValueField(@filter,'Value');	
    DECLARE @isValueFilter BIT = dbo.Fn_IsFilterField(@filter,'Value');	

	
 ------ End Filter ------

 ------ Sort ------	
	DECLARE @isSortName BIT = dbo.Fn_IsSortField(@sort,'Name');	
    DECLARE @isSortNameASC BIT = dbo.Fn_IsSortFieldAsc(@sort,'Name');
	DECLARE @isSortValue BIT = dbo.Fn_IsSortField(@sort,'Value');	
    DECLARE @isSortValueASC BIT = dbo.Fn_IsSortFieldAsc(@sort,'Value');	

	
  ------ END Sort ------
 
 SELECT *
  INTO #TMP
  FROM AZ_Files	 a
	 
WHERE  1=1	 AND Active=1
	AND a.FolderId=@idFolder
	AND Type=@type
	AND (@Name IS NULL OR @Name = '' OR (@Name <> '' AND a.[Name] LIKE '%' + @Name + '%' ))	
	AND (@isNameFilter =0 OR(@isNameFilter=1 and a.Name LIKE '%' + @NameFilter + '%' ))			
			

 DECLARE @totalCount INT = (SELECT COUNT(Id) FROM #TMP)

SELECT
   *
    ,@totalCount AS 'Total'
FROM #TMP temp

ORDER BY
	
	CASE WHEN @isSortName = 1 AND @isSortNameASC = 1 THEN temp.[Name]  END ASC,
	CASE WHEN @isSortName = 1 AND @isSortNameASC != 1 THEN temp.[Name]  END DESC,		
	temp.Id DESC	
    OFFSET(@skip) ROWS
    FETCH NEXT(@take) ROWS ONLY

 ----- DROP TABLE -----
DROP TABLE #TMP


END


GO
/****** Object:  StoredProcedure [dbo].[AZ_GetLocation]    Script Date: 1/20/2020 11:18:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[AZ_GetLocation]
@name NVARCHAR(20),
@id INT =0
AS
DECLARE @sql nvarchar(4000)
SELECT @sql = ' SELECT * FROM ' +@name +' WHERE Active=1 '

IF(@name='VN_Districts')
BEGIN
	IF(@id <> 0)
	SET @sql=@sql+' AND CityId=' +CAST(@id AS varchar(5))
END

IF(@name='VN_Wards')
BEGIN
	IF(@id <> 0)
	SET @sql=@sql+' AND CityId=' +CAST(@id AS varchar(5))
END
SET @sql=@sql+' ORDER BY Name'
EXECUTE sp_executesql @sql
GO
/****** Object:  StoredProcedure [dbo].[AZ_Products_GetList]    Script Date: 1/20/2020 11:18:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[AZ_Products_GetList]
-- Add the parameters for the stored procedure here	
	@name NVARCHAR(200) = '',	
	@typeTransaction INT,
	@typeProperty INT,
    @filter FilterType READONLY,
    @sort SortType READONLY,
    @skip INT,
    @take INT
AS
BEGIN   

 ------ Filter ------
	DECLARE @IdFilter UNIQUEIDENTIFIER = dbo.Fn_Filter_GetStringValueField(@filter,'Id');	
    DECLARE @isIdFilter BIT = dbo.Fn_IsFilterField(@filter,'Id');
    DECLARE @NameFilter VARCHAR(MAX) = dbo.Fn_Filter_GetStringValueField(@filter,'Name');	
    DECLARE @isNameFilter BIT = dbo.Fn_IsFilterField(@filter,'Name');
	DECLARE @PhoneFilter VARCHAR(MAX) = dbo.Fn_Filter_GetStringValueField(@filter,'Phone');	
    DECLARE @isPhoneFilter BIT = dbo.Fn_IsFilterField(@filter,'Phone');	
    DECLARE @EmailFilter VARCHAR(MAX) = dbo.Fn_Filter_GetStringValueField(@filter,'Email');	
    DECLARE @isEmailFilter BIT = dbo.Fn_IsFilterField(@filter,'Email');	
	DECLARE @NoFilter VARCHAR(MAX) = dbo.Fn_Filter_GetStringValueField(@filter,'No');	
    DECLARE @isNoFilter BIT = dbo.Fn_IsFilterField(@filter,'No');	
	DECLARE @StatusFilter VARCHAR(MAX) = dbo.Fn_Filter_GetStringValueField(@filter,'Status');	
    DECLARE @isStatusFilter BIT = dbo.Fn_IsFilterField(@filter,'Status');	
	DECLARE @IsVIPFilter BIT = dbo.Fn_Filter_GetStringValueField(@filter,'IsVIP');	
    DECLARE @isIsVIPFilter BIT = dbo.Fn_IsFilterField(@filter,'IsVIP');	
	DECLARE @IsActiveFilter BIT = dbo.Fn_Filter_GetStringValueField(@filter,'IsActive');	
    DECLARE @isIsActiveFilter BIT = dbo.Fn_IsFilterField(@filter,'IsActive');	


	
 ------ End Filter ------

 ------ Sort ------	
	DECLARE @isSortName BIT = dbo.Fn_IsSortField(@sort,'Name');	
    DECLARE @isSortNameASC BIT = dbo.Fn_IsSortFieldAsc(@sort,'Name');
	DECLARE @isSortNo BIT = dbo.Fn_IsSortField(@sort,'No');	
    DECLARE @isSortNoASC BIT = dbo.Fn_IsSortFieldAsc(@sort,'No');	

	
  ------ END Sort ------

SELECT * INTO #TypeTransaction FROM dbo.AZ_Constant WHERE Type='TypeTransaction'
SELECT * INTO #TypeProperty FROM dbo.AZ_Constant WHERE Type='TypeProperty'
 
 SELECT m.[Id]
      ,m.[Name]
      ,m.[TypeTransaction]
	  ,#TypeTransaction.Name AS TypeTransactionName
      ,m.[TypeProperty]
	  ,#TypeProperty.Name AS TypePropertyName
      ,m.[City]
      ,m.[District]
      ,m.[Ward]
      ,m.[Street]
      ,m.[Area]
      ,m.[Price]
      ,m.[Unit]
      ,m.[Address]
      ,m.[HouseDirection]
      ,m.[BalconyDirection]
      ,m.[NumOfFloor]
      ,m.[NumOfBedroom]
      ,m.[NumOfWcs]
      ,m.[Summary]
      ,m.[CreatedDate]
      ,m.[CreatedBy]
      ,m.[ModifiedDate]
      ,m.[ModifiedBy]
      ,m.[IsActive]
      ,m.[IsDelete]
	  ,m.ImageMain
	  ,m.ImageList
	  ,m.IsVIP
	  ,c.Name AS CityName
	  ,d.Name AS DistrictName
	  ,w.Name AS WardName
	  ,cs.Name AS UnitName
  INTO #TMP
  FROM [dbo].[AZ_Products]	 m
  LEFT JOIN dbo.VN_City c ON c.Id=m.City
  LEFT JOIN dbo.VN_Districts d ON d.Id=m.District
  LEFT JOIN dbo.VN_Wards w ON w.Id=m.Ward
  LEFT JOIN dbo.AZ_Constant cs ON cs.Id=m.Unit AND cs.Type='Unit' AND cs.ParentId=m.TypeTransaction
  LEFT JOIN #TypeProperty ON  #TypeProperty.Id=m.TypeProperty AND #TypeProperty.ParentId=m.TypeTransaction
  LEFT JOIN #TypeTransaction ON  #TypeTransaction.Id=m.TypeTransaction 
	 
WHERE  1=1	
	AND (@isIdFilter =0 OR(@isIdFilter=1 and m.Id =@IdFilter))	
	AND (@Name IS NULL OR @Name = '' OR (@Name <> '' AND m.[Name] LIKE '%' + @Name + '%' ))
	AND (m.IsDelete IS NULL OR m.IsDelete=0)		
	AND (@isNameFilter =0 OR(@isNameFilter=1 and m.Name LIKE '%' + @NameFilter + '%' ))	
	AND (@typeTransaction=0 OR (@typeTransaction<>0 AND m.[TypeTransaction]=@typeTransaction))
	AND (@typeProperty=0 OR (@typeProperty<>0 AND m.[TypeProperty]=@typeProperty))
	AND (@isIsVIPFilter =0 OR(@isIsVIPFilter=1 and m.IsVIP=1))	
	AND (@isIsActiveFilter =0 OR(@isIsActiveFilter=1 and m.IsActive=1))	
	

 DECLARE @totalCount INT = (SELECT COUNT(Id) FROM #TMP)

SELECT
   *
    ,@totalCount AS 'Total'
FROM #TMP temp

ORDER BY
	
	CASE WHEN @isSortName = 1 AND @isSortNameASC = 1 THEN temp.[Name]  END ASC,
	CASE WHEN @isSortName = 1 AND @isSortNameASC != 1 THEN temp.[Name]  END DESC,	
	temp.ModifiedDate DESC	
    OFFSET(@skip) ROWS
    FETCH NEXT(@take) ROWS ONLY

 ----- DROP TABLE -----
DROP TABLE #TMP,#TypeTransaction,#TypeProperty


END


GO
/****** Object:  StoredProcedure [dbo].[AZ_Services_GetList]    Script Date: 1/20/2020 11:18:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[AZ_Services_GetList]
-- Add the parameters for the stored procedure here	
	@name NVARCHAR(200) = '',	
    @filter FilterType READONLY,
    @sort SortType READONLY,
    @skip INT,
    @take INT
AS
BEGIN   

 ------ Filter ------
    DECLARE @NameFilter VARCHAR(MAX) = dbo.Fn_Filter_GetStringValueField(@filter,'Name');	
    DECLARE @isNameFilter BIT = dbo.Fn_IsFilterField(@filter,'Name');	
    DECLARE @EmailFilter VARCHAR(MAX) = dbo.Fn_Filter_GetStringValueField(@filter,'Email');	
    DECLARE @isEmailFilter BIT = dbo.Fn_IsFilterField(@filter,'Email');	
	DECLARE @NoFilter VARCHAR(MAX) = dbo.Fn_Filter_GetStringValueField(@filter,'No');	
    DECLARE @isNoFilter BIT = dbo.Fn_IsFilterField(@filter,'No');	

	
 ------ End Filter ------

 ------ Sort ------	
	DECLARE @isSortName BIT = dbo.Fn_IsSortField(@sort,'Name');	
    DECLARE @isSortNameASC BIT = dbo.Fn_IsSortFieldAsc(@sort,'Name');
	DECLARE @isSortNo BIT = dbo.Fn_IsSortField(@sort,'No');	
    DECLARE @isSortNoASC BIT = dbo.Fn_IsSortFieldAsc(@sort,'No');
	DECLARE @isSortOrders BIT = dbo.Fn_IsSortField(@sort,'Orders');	
    DECLARE @isSortOrdersASC BIT = dbo.Fn_IsSortFieldAsc(@sort,'Orders');

	
  ------ END Sort ------
 
 SELECT a.[Id]
      ,a.[Name]
      ,a.[Image]
      ,a.[Title]
      ,a.[NumberView]
      ,a.[Orders]
      ,a.[Summary]
      ,a.[Active]
      ,a.[CreatedBy]
      ,a.[CreatedDate]
      ,a.[ModifiedBy]
      ,a.[ModifiedDate]
  INTO #TMP
  FROM [dbo].[AZ_Services]	a 
	 
WHERE  1=1	
	AND (@Name IS NULL OR @Name = '' OR (@Name <> '' AND (a.[Name] LIKE '%' + @Name + '%'   OR a.[Title] LIKE '%' + @Name + '%') ))
	AND (a.Active IS NULL OR a.Active=1)	
	AND (@isNameFilter =0 OR(@isNameFilter=1 and a.Name LIKE '%' + @NameFilter + '%' ))
			
	
	

 DECLARE @totalCount INT = (SELECT COUNT(Id) FROM #TMP)

SELECT
   *
    ,@totalCount AS 'Total'
FROM #TMP temp

ORDER BY
	
	CASE WHEN @isSortName = 1 AND @isSortNameASC = 1 THEN temp.[Name]  END ASC,
	CASE WHEN @isSortName = 1 AND @isSortNameASC != 1 THEN temp.[Name]  END DESC,	
	CASE WHEN @isSortOrders = 1 AND @isSortOrdersASC = 1 THEN temp.Orders  END ASC,
	CASE WHEN @isSortOrders = 1 AND @isSortOrdersASC != 1 THEN temp.Orders  END DESC,
	temp.Orders,temp.[ModifiedDate] DESC	
    OFFSET(@skip) ROWS
    FETCH NEXT(@take) ROWS ONLY

 ----- DROP TABLE -----
DROP TABLE #TMP


END


GO
