
DECLARE  @Error TABLE(
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Row] [int]  NULL,
	[CandidateId] [nchar](12) NOT NULL,
	[FullName] [nvarchar](50) NOT NULL,
	[Phone] [varchar](30) NOT NULL,
	[Email] [nchar](100) NOT NULL,
	[DOB] [date] NULL,	
	[Technology] [nchar](200) NULL,	
	[SocialLink] [nchar](200) NULL,	
	[ErrorMessage] [Nvarchar](200) NULL
	)

IF NOT EXISTS ( SELECT 1 FROM [dbo].[Rec_Employees] WHERE Email = 'chantroinoiay25251325@yahoo.com1' OR Phone='3570560941' )
BEGIN    
	BEGIN TRY	    
		INSERT INTO [dbo].[Rec_Employees] (CandidateId,FullName,Email,Phone,DOB,SocialLink,CreatedDate,CreatedBy,IsActive,IsImport)			
		VALUES ('CAND00062381',NULL,'Email','Phone',NULl,'SocialLink',GETDATE(),'CreatedBy',1,1)		
	END TRY
	BEGIN CATCH		

		INSERT INTO @Error (Row,CandidateId,FullName,Email,Phone,DOB,SocialLink,ErrorMessage)		
		VALUES (1,'CAND00062381','FullName','Email','Phone',NULl,'SocialLink','Error ' + CONVERT(VARCHAR, ERROR_NUMBER(), 1) + ': '+ ERROR_MESSAGE())				
	END CATCH
END
else 
BEGIN
    INSERT INTO @Error (Row,CandidateId,FullName,Email,Phone,DOB,SocialLink,ErrorMessage)		
		VALUES (1,'CAND00062381','FullName','Email','Phone',NULl,'SocialLink','The Email or Phone already exists in the system')				
END

SELECT * FROM @Error
DELETE FROM @Error