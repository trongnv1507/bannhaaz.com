USE [dathangtb_com_trong2]
GO
/****** Object:  UserDefinedTableType [dbo].[FilterType]    Script Date: 2/11/2020 3:20:51 PM ******/
CREATE TYPE [dbo].[FilterType] AS TABLE(
	[Field] [varchar](50) NOT NULL,
	[ValueString] [nvarchar](max) NULL,
	[ValueDateTimeFrom] [datetime] NULL,
	[ValueDateTimeTo] [datetime] NULL,
	[ValueDecimalFrom] [decimal](18, 0) NULL,
	[ValueDecimalTo] [decimal](18, 0) NULL,
	[ValueBit] [bit] NULL,
	[IsActive] [bit] NOT NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[SortType]    Script Date: 2/11/2020 3:20:51 PM ******/
CREATE TYPE [dbo].[SortType] AS TABLE(
	[Field] [varchar](250) NOT NULL,
	[Asc] [bit] NULL,
	[IsActive] [bit] NOT NULL
)
GO
/****** Object:  UserDefinedFunction [dbo].[Fn_Filter_GetStringValueField]    Script Date: 2/11/2020 3:20:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[Fn_Filter_GetStringValueField]
(
	@filter FilterType READONLY,
	@fieldName VARCHAR(50)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN

	DECLARE @result NVARCHAR(MAX) = NULL;
	
	IF EXISTS(SELECT TOP 1 f.Field FROM @filter AS f WHERE UPPER(f.Field) = UPPER(@fieldName))
	BEGIN
	  IF EXISTS(SELECT TOP 1 f.ValueString FROM @filter AS f WHERE UPPER(f.Field) = UPPER(@fieldName) AND NULLIF(f.ValueString, '') IS NULL)
	   BEGIN
	     SET @result = ''
	   END
	   ELSE
		BEGIN
		 SET @result = (SELECT TOP 1 f.ValueString FROM @filter AS f WHERE UPPER(f.Field) = UPPER(@fieldName) AND NULLIF(f.ValueString, '') IS NOT NULL)
		END
	END
	
	RETURN @result

END

GO
/****** Object:  UserDefinedFunction [dbo].[Fn_IsFilterField]    Script Date: 2/11/2020 3:20:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[Fn_IsFilterField]
(
	@filter FilterType READONLY,
	@fieldName VARCHAR(50)
)
RETURNS BIT
AS
BEGIN

	DECLARE @result BIT = (CASE  
									WHEN EXISTS(SELECT TOP 1 f.IsActive FROM @filter AS f WHERE UPPER(f.Field) = UPPER(@fieldName))
									THEN
										(SELECT TOP 1 f.IsActive FROM @filter AS f WHERE UPPER(f.Field) = UPPER(@fieldName))
									ELSE
										(SELECT 0)
								END)
	RETURN @result

END

GO
/****** Object:  UserDefinedFunction [dbo].[Fn_IsSortField]    Script Date: 2/11/2020 3:20:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[Fn_IsSortField]
(
	@sort SortType READONLY,
	@fieldName VARCHAR(50)
)
RETURNS BIT
AS
BEGIN

	DECLARE @result BIT = (CASE  
									WHEN EXISTS(SELECT TOP 1 f.IsActive FROM @sort AS f WHERE UPPER(f.Field) = UPPER(@fieldName))
									THEN
										(SELECT TOP 1 f.IsActive FROM @sort AS f WHERE UPPER(f.Field) = UPPER(@fieldName))
									ELSE
										(SELECT 0)
								END)
	RETURN @result

END



GO
/****** Object:  UserDefinedFunction [dbo].[Fn_IsSortFieldAsc]    Script Date: 2/11/2020 3:20:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[Fn_IsSortFieldAsc]
(
	@sort SortType READONLY,
	@fieldName VARCHAR(50)
)
RETURNS BIT
AS
BEGIN
	DECLARE @result BIT = (CASE  
									WHEN EXISTS(SELECT TOP 1 f.[Asc] FROM @sort AS f WHERE UPPER(f.Field) = UPPER(@fieldName))
									THEN
										(SELECT TOP 1 f.[Asc] FROM @sort AS f WHERE UPPER(f.Field) = UPPER(@fieldName))
									ELSE
										(SELECT 0)
								END)
	RETURN @result

END
GO
/****** Object:  UserDefinedFunction [dbo].[UF_StrToTable]    Script Date: 2/11/2020 3:20:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--select * from [UF_StrToTable] ('(Blanks);#2A',';#')

CREATE FUNCTION[dbo].[UF_StrToTable](@String NVARCHAR(MAX), @Delimiter CHAR(2))  
RETURNS @Temptable TABLE(Result VARCHAR(8000))  
AS  
BEGIN  
	DECLARE @INDEX int, @SLICE VARCHAR(8000)  
	SELECT @INDEX = 1  
	IF LEN(@String) < 1 OR @String IS NULL  
		return  
	WHILE @INDEX != 0  
		BEGIN  
			SET @INDEX = CHARINDEX(@Delimiter, @String)  
			IF @INDEX != 0  
			BEGIN  
				SET @SLICE = LEFT(@String, @INDEX - 1)  
			END  
			ELSE  
				BEGIN  
				SET @SLICE = @String  
				END  
			IF(LEN(@SLICE) > 0)  
			BEGIN  
			--Trongnv add : When Area is (Blanks)
				IF(@SLICE='(Blanks)')
					SET @SLICE=' ' 
			--
				INSERT INTO @Temptable(Result) VALUES(@SLICE)  
			END  
			SET @String = RIGHT(@String, LEN(@String) - @INDEX-1)  
			IF LEN(@String) = 0  
				break  
		END  
	RETURN  
END   
GO
/****** Object:  UserDefinedFunction [dbo].[UF_StrToTableInt]    Script Date: 2/11/2020 3:20:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--select * from [UF_StrToTable] ('(Blanks);#2A',';#')

Create FUNCTION[dbo].[UF_StrToTableInt](@String NVARCHAR(MAX), @Delimiter CHAR(2))  
RETURNS @Temptable TABLE(Result Int)  
AS  
BEGIN  
	DECLARE @INDEX int, @SLICE VARCHAR(8000)  
	SELECT @INDEX = 1  
	IF LEN(@String) < 1 OR @String IS NULL  
		return  
	WHILE @INDEX != 0  
		BEGIN  
			SET @INDEX = CHARINDEX(@Delimiter, @String)  
			IF @INDEX != 0  
			BEGIN  
				SET @SLICE = LEFT(@String, @INDEX - 1)  
			END  
			ELSE  
				BEGIN  
				SET @SLICE = @String  
				END  
			IF(LEN(@SLICE) > 0)  
			BEGIN  
			--Trongnv add : When Area is (Blanks)
				IF(@SLICE='(Blanks)')
					SET @SLICE=' ' 
			--
				INSERT INTO @Temptable(Result) VALUES(@SLICE)  
			END  
			SET @String = RIGHT(@String, LEN(@String) - @INDEX-1)  
			IF LEN(@String) = 0  
				break  
		END  
	RETURN  
END   
GO
